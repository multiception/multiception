# Multiception

Main Multiception package meant to be part of the [multiception package](https://gitlab.utc.fr/multiception/multiception).

## Launching
The main launch file is `multi-vehicle.launch`, that should at least take the name of the dataset being played. This is to retrieve the right `config/location/` and `spoof/` configuration files.
Other arguments can also be passed to activate or deactivate certain parts, e.g:
```bash
roslaunch multiception multi-vehicle.launch dataset:=Multiception/Intersection signs:=true trust:=false rviz:=true debug:=0000 zoegrey:=false egotracking:=false multracking:=true
```

The environment variable `DATASETSFOLDER` should be set to the path where dataset configs are stored (cloned from [hds_vehint/datasets/config](https://gitlab.utc.fr/hds_vehint/datasets/configs))

Non multi vehicle (i.e non-prefixed bags) datasets can also be used with `mono-vehicle.launch` which takes similar arguments.

## Playing bags
Bags should be started in another terminal, and without several topics as they are re-generated:
- `[/CARNAME]/span/enuposeCovs`
- `[/CARNAME]/vlp32C/velodyne_points`
