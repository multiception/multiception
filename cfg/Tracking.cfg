#!/usr/bin/env python
PACKAGE = "multiception"

from dynamic_reconfigure.parameter_generator_catkin import *

gen = ParameterGenerator()

gen.add("frequency",  double_t, 0, "Duration of the track buffers (s⁻¹)", 30.00, 0.00, 100.00)
gen.add("buffer_dur", double_t, 0, "Duration of the track buffers (s)",    1.00, 0.00, 1000.00)
gen.add("heading_dir_unknown", bool_t, 0, "Sometimes the estimated heading is precise but the oriention vector direction is unknown. In that case, can the heading be changed to match that of the track", False)

fil = gen.add_group("Filtering")
filter_enum = gen.enum([gen.const("Kalman", int_t, 0, "Kalman Filter"),
                        gen.const("SCI",    int_t, 1, "Split Covariance Intersection Filter"),
                        gen.const("IFCI",   int_t, 2, "Improved Fast Covariance Intersection Filter"),
                        gen.const("ITCI",   int_t, 3, "Information Theoretic Covariance Intersection Filter"),
                        gen.const("CI",     int_t, 4, "Covariance Intersection Filter"),
                        gen.const("OldSCI", int_t, 5, "Old version of the CI Filter (w computed inside or outside the observation model)"),
                        gen.const("OldCI",  int_t, 6, "Old version of the SCI Filter (w computed inside or outside the observation model)")],
                        "Filter to use for track prediction and update")

fil.add("filter", int_t, 0, "Filter to use for track prediction and update", 1, 0, 4, edit_method=filter_enum)
fil.add("noise_ST_x" , double_t, 0, "x evolution noise for the Static model (m²)",         0.02, 0.01, 100.00)
fil.add("noise_ST_y" , double_t, 0, "y evolution noise for the Static model (m²)",         0.02, 0.01, 100.00)
fil.add("noise_ST_O" , double_t, 0, "Heading evolution noise for the Static model (rad²)", 0.02, 0.01, 100.00)

fil.add("noise_CT_x" , double_t, 0, "x evolution noise for the Coordinated Turn model (m²)",               0.02, 0.01, 100.00)
fil.add("noise_CT_y" , double_t, 0, "y evolution noise for the Coordinated Turn model (m²)",               0.02, 0.01, 100.00)
fil.add("noise_CT_O" , double_t, 0, "Heading evolution noise for the Coordinated Turn model (rad²)",       0.10, 0.01, 100.00)
fil.add("noise_CT_v" , double_t, 0, "Speed evolution noise for the Coordinated Turn model (m/s²)",         0.10, 0.01, 100.00)
fil.add("noise_CT_w" , double_t, 0, "Angular vel evolution noise for the Coordinated Turn model (rad/s²)", 2.00, 0.01, 100.00)

fil.add("noise_CA_x" , double_t, 0, "x evolution noise for the Constant Acc model (m²)",                0.02, 0.01, 100.00)
fil.add("noise_CA_y" , double_t, 0, "y evolution noise for the Constant Acc model (m²)",                0.02, 0.01, 100.00)
fil.add("noise_CA_vx", double_t, 0, "x speed evolution noise for the Constant Acc model (m/s²)",        0.10, 0.01, 100.00)
fil.add("noise_CA_vy", double_t, 0, "y speed evolution noise for the Constant Acc model (m/s²)",        0.10, 0.01, 100.00)

ass = gen.add_group("Association")
assoc_enum = gen.enum( [gen.const("Mahalanobis",      int_t, 0, "Mahalanobis Distance"),
                        gen.const("MahalanobisClass", int_t, 1, "Mahalanobis Distance + Class penalty")],
                        "Cost function between two objects")

ass.add("associator", int_t, 0, "Cost function between two objects", 0, 0, 2, edit_method=assoc_enum)
ass.add("assoc_gate", double_t, 0, "Euclidean distance used to quickly remove obvious outliers (m)", 2.00, 0.01, 100.00)
ass.add("assoc_cost_max", double_t, 0, "Maximal cost between two objects (unit depends on the cost computation method)", 2.00, 0.01, 100.00)
ass.add("assoc_penalty_cost", double_t, 0,"Cost multiplied by the penalty described in assoc_penalty_file", 0.00, 0.01, 100.00)
ass.add("assoc_penalty_file", str_t, 0,"Path to the class penalty configuration file", "")
ass.add("merge_cost_max", double_t, 0, "Maximal cost to merge two tracks after fusion. 0 is don't merge",    0, 0.00,  10.00)

exs = gen.add_group("Existence")
exist_enum = gen.enum( [gen.const("Naive",         int_t, 0, "Naive existence estimation (delete when too old or uncertain)"),
                        gen.const("Filtering",     int_t, 1, "Mass Function filter based existence estimation"),
                        gen.const("Observability", int_t, 2, "Mass Function filter based existence estimation + considers visibility")],
                        "Method used to estimate the existence of objects")

exs.add("existence_estimator",       int_t, 0, "Method used to estimate the existence of objects", 1, 0, 3, edit_method=exist_enum)
exs.add("thresh_delete_time",        double_t, 0, "[Naive] How long before an unobserved track is deleted (s)",                     -1.00, -1.00,  10.00)
exs.add("thresh_delete_uncertain",   double_t, 0, "[Naive] How big the position covariance can become before deleting (trace, m²)", -1.00, -1.00, 100.00)
exs.add("max_decay_time",            double_t, 0, "[Filtering] How long before half the existence of a track is decayed (s)",        1.00,  0.10,  10.00)
exs.add("max_deterioration",         double_t, 0, "[Filtering] How much an unobserved track can be deteriorated",                    0.10,  0.01,   1.00)
exs.add("value_birth",               double_t, 0, "[Filtering] Value of existence when a track is first created",                    0.10,  0.01,   1.00)
exs.add("thresh_valid",              double_t, 0, "[Filtering] Existence value to consider a track valid enough",                    0.10,  0.01,   1.00)
exs.add("thresh_invalid",            double_t, 0, "[Filtering] Existence value to consider a track no longer valid",                 0.10,  0.01,   1.00)
exs.add("thresh_delete_nonexistent", double_t, 0, "[Filtering] Non-Existence value to delete a track",                               0.10,  0.01,   1.00)
exs.add("thresh_delete_unknown",     double_t, 0, "[Filtering] How uncertain a track can become before being deleted",               0.95,  0.01,   1.00)

exit(gen.generate(PACKAGE, "multiception", "Tracking"))
