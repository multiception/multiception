# Post Configs)
In the form `filter_exist_detect_trust_spoof.yaml` with:
- `filter` being `caut`, `mode` or `bold`
- `exist`  being `caut`, `mode` or `bold`
- `detect` being `none`, `coop` or `both` (active in both standalone and cooperative)
- `trust`  being `none`, `caut`, `mode` or `bold`
- `spoof`  being `none`, `easy`, `mode` or `hard`

(`caut` is cautious, `mode` is moderate)
