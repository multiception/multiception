#! /bin/bash

IP_LOCAL="$(avahi-resolve --name -4 sivadell.local|cut -f2)"
HN_REMOTE1="pcmasstefa-gi-0.utc"
HN_REMOTE2="pcwelteant-gi-0.utc"
MASTER_URI="http://$IP_LOCAL:11311"
SETUP_FILE="~/ws/multiception_ws/devel/setup.zsh"
TMUX_MUL='tmux -L multiception'

if [ -n $1 ] && [ "$1" == "clean" ] ; then
    $TMUX_MUL kill-server 2> /dev/null
    ssh limaanto@$HN_REMOTE1 -t "$TMUX_MUL kill-server 2> /dev/null"
    ssh limaanto@$HN_REMOTE2 -t "$TMUX_MUL kill-server 2> /dev/null"
    exit 0
fi

if [ -n $1 ] && [ "$1" == "sync" ] ; then
    $TMUX_MUL new-session -d -s "multiception_sync" -n "local"
    $TMUX_MUL split-pane -t "multiception_sync:0" "rsync -avz ~/ws/multiception_ws/src limaanto@$HN_REMOTE1:ws/multiception_ws && ssh limaanto@$HN_REMOTE1 -t 'cd ~/ws/multiception_ws && source /opt/ros/noetic/setup.zsh && catkin_make'"
    $TMUX_MUL split-pane -t "multiception_sync:0" "rsync -avz ~/ws/multiception_ws/src limaanto@$HN_REMOTE2:ws/multiception_ws && ssh limaanto@$HN_REMOTE2 -t 'cd ~/ws/multiception_ws && source /opt/ros/noetic/setup.zsh && catkin_make'"
    $TMUX_MUL send -t "multiception_sync:0.0" "cd ~/ws/multiception_ws && catkin_make && cd - && exit" ENTER
    $TMUX_MUL attach -t "multiception_sync"
    exit 0
fi

if [ -n $1 ] && [ "$1" == "launch" ] ; then
    # Eat the first argument if it was used for this script, to later pass all remaining argument (eg: signs:=true) to the launches
    shift
fi

# Start the tmux (terminal multiplexer) session and windows, one for each machine
# Options are (d)ont attach to current terminal yet, (s)ession name and (n)amed window
$TMUX_MUL new-session -d -s "multiception" -n "local"
$TMUX_MUL new-window -n "remote1"
$TMUX_MUL new-window -n "remote2"

# Send the connection commands to all three terminals
$TMUX_MUL split -t "multiception:0"
$TMUX_MUL send -t "multiception:1" "ssh limaanto@$HN_REMOTE1" ENTER
$TMUX_MUL send -t "multiception:2" "ssh limaanto@$HN_REMOTE2" ENTER

# Setup the terminal variables
$TMUX_MUL send -t "multiception:0.0" "source $SETUP_FILE && export ROS_IP=$IP_LOCAL && export ROS_MASTER_URI=$MASTER_URI" ENTER
$TMUX_MUL send -t "multiception:0.1" "source $SETUP_FILE && export ROS_IP=$IP_LOCAL && export ROS_MASTER_URI=$MASTER_URI" ENTER
$TMUX_MUL send -t "multiception:1" "source $SETUP_FILE && export ROS_HOSTNAME=$HN_REMOTE1 && export ROS_MASTER_URI=$MASTER_URI" ENTER
$TMUX_MUL send -t "multiception:2" "source $SETUP_FILE && export ROS_HOSTNAME=$HN_REMOTE2 && export ROS_MASTER_URI=$MASTER_URI" ENTER

# Start the ROS program
# Remote terminals get nested tmuxes to allow for remote window splitting
# To avoid creating too much zombie sessions, kill the previous ones when a new connection is made
$TMUX_MUL send -t "multiception:0.1" 'roscore&' ENTER "cd ~/datasets/SCA20-01-002/rosbags && rosbag play --clock --pause zoeblue/*.bag zoegrey/*.bag zoewhite/*.bag infra.bag -s 120 -u 90 -r 0.01" ENTER
$TMUX_MUL send -t "multiception:0.0" "roslaunch multiception multi-vehicle.launch --wait rviz:=true  mapserver:=false zoeblue:=true  zoegrey:=false zoewhite:=false $*" ENTER
$TMUX_MUL send -t "multiception:1" "$TMUX_MUL kill-session -t multiceptionr 2> /dev/null" ENTER "$TMUX_MUL new-session -d -s multiceptionr \; send 'roslaunch multiception multi-vehicle.launch --wait $* rviz:=false plot:=false mapserver:=false zoeblue:=false zoegrey:=true  zoewhite:=false' ENTER \; attach" ENTER
$TMUX_MUL send -t "multiception:2" "$TMUX_MUL kill-session -t multiceptionr 2> /dev/null" ENTER "$TMUX_MUL new-session -d -s multiceptionr \; send 'roslaunch multiception multi-vehicle.launch --wait $* rviz:=false plot:=false mapserver:=true  zoeblue:=false zoegrey:=false zoewhite:=true' ENTER \; attach" ENTER

$TMUX_MUL attach -t "multiception" \; select-window -t "multiception:0.1" \;
exit $?
