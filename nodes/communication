#!/usr/bin/env python

import numpy as np
import pickle, base64, time
import rospy, roslib.message

from multiception.msg import CPM, Bytes

class CommunicationNode:
	def __init__(self):
		rospy.init_node('communication')
		topic_rx = rospy.get_param('~receive_topic_name')
		topic_tx = rospy.get_param('~send_topic_name')

		self.sub = rospy.Subscriber(topic_tx, CPM, self.send)
		self.pub = rospy.Publisher(topic_rx, CPM, queue_size=10)
		self.simu_pub = rospy.Publisher('com/out', Bytes, queue_size=10)
		self.simu_sub = rospy.Subscriber('com/in', Bytes, self.receive)

	def send(self, msg):
		msg_str = b'^' + base64.b64encode(pickle.dumps(msg)) + b'$'
		self.simu_pub.publish(msg_str)

	def receive(self, msg_str):
		try:
			data = base64.b64decode(bytes(msg_str.data))
			msg = pickle.loads(data)
			self.pub.publish(msg)
		except TypeError:
			rospy.logwarn('Message decoding error')
		except pickle.UnpicklingError as e:
			rospy.logwarn('Message unpickling error: {}'.format(e))
		except (AttributeError,  EOFError, ImportError, IndexError) as e:
			rospy.logwarn(traceback.format_exc(e))
		except Exception as e:
			rospy.logerr(traceback.format_exc(e))

if __name__=='__main__':
	cn = CommunicationNode()
	rospy.spin()
