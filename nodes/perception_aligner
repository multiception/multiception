#!/usr/bin/env python

import math
import yaml
import numpy as np

import rospy
from geometry_msgs.msg import Polygon, Point32
from simulation import _tools, _ros_bridge
from multiception.msg import CPM, Perceptions, CPMSensor, CPMPerception, StateStamped
from threading import Lock
from multiception.evidential_grid_map import EvidentialGridMap

class AlignerNode:
	'''
	Transforms perceptions from one frame to another using buffered states

	Listens to an input perceptions and state topic, and publishes the transformed perceptions.
	Keeps a buffer of those states to find the closest one upon perceptions reception
	and transforms the perception's state and covariance accordingly into the same frame as the state.
	'''

	def __init__(self):
		rospy.init_node('aligner')

		topics_in = rospy.get_param('~topics_in')
		topics_out = rospy.get_param('~topics_out')

		self.states = []
		self.state_lock = Lock()
		self.pubs = [rospy.Publisher(topic_out, Perceptions, queue_size=10) for topic_out in topics_out.split(';')]

		self.sub_state = rospy.Subscriber('~state', StateStamped, self.callback_state)
		self.subs = [rospy.Subscriber(topic_in, Perceptions, self.callback_percepts, self.pubs[i]) for i, topic_in in enumerate(topics_in.split(';'))]

	def callback_state(self, state):
		with self.state_lock:
			self.states.append(state)
			self.states = [s for s in self.states if state.header.stamp.to_sec()-s.header.stamp.to_sec()<10]

	def callback_percepts(self, percepts, publisher):
		with self.state_lock:
			try:
				self.align(percepts)
				publisher.publish(percepts)
			except IndexError as e:
				rospy.logwarn_throttle(2, 'Cannot align perceptions, {}.'.format(e))

	def align(self, percepts):
		if len(self.states)<2:
			raise IndexError('not enough states received')

		# Get the ego state at the time of perception
		percept_times = list(set([percepts.header.stamp.to_sec()]+[p.header.stamp.to_sec() for p in percepts.perceptions]))
		state_times = [s.header.stamp.to_sec() for s in self.states]

		xs    = np.interp(percept_times, state_times, [s.state.x for s in self.states])
		ys    = np.interp(percept_times, state_times, [s.state.y for s in self.states])
		yaws  = np.interp(percept_times, state_times, [s.state.O for s in self.states])
		vs    = np.interp(percept_times, state_times, [s.state.v for s in self.states])
		yawrs = np.interp(percept_times, state_times, [s.state.w for s in self.states])

		transfos = dict([(t, [xs[i], ys[i], yaws[i], vs[i], yawrs[i]]) for i, t in enumerate(percept_times)])
		covarsd = {}
		covarsi = {}
		for t in percept_times:
			closest_covd = closest_covi = None

			for s in self.states:
				if s.header.stamp.to_sec()<t:
					closest_covd = s.state.covd
					closest_covi = s.state.covi

			if closest_covd is None:
				raise IndexError('states received out-of-sequence')
			covarsd[t] = np.reshape(closest_covd, (5,5))[:3,:3]
			covarsi[t] = np.reshape(closest_covi, (5,5))[:3,:3]

		for p in percepts.perceptions:
			t = p.header.stamp.to_sec()
			xa, ya, Oa, va, wa = transfos[t]
			xb, yb, Ob, vb, wb = p.state.x, p.state.y, p.state.O, p.state.v, p.state.w
			ca, sa, cb, sb = np.cos(Oa), np.sin(Oa), np.cos(Ob), np.sin(Ob)
			aTb = _tools.transfo_to_matrix(transfos[t])
			aRb = aTb[:2,:2]

			# Rotate the velocity vector
			# 0Vb = w * R' aXb + aRb * aVb + 0Va
			accvec = wa * np.array([[-sa,ca],[-ca,-sa]]) @ [xb-xa,yb-ya]
			velvec = accvec + aRb @ [vb*cb, vb*sb] + [va*ca, va*sa]
			v = np.linalg.norm(velvec)

			p.header.frame_id = self.states[0].header.frame_id
			p.state.x, p.state.y, _ = aTb @ [p.state.x, p.state.y, 1]
			p.state.O += transfos[t][2]

			pcovd = np.reshape(p.state.covd, (5,5))
			pcovi = np.reshape(p.state.covi, (5,5))
			_tools.rotate_cov_2D(transfos[t][2], pcovd, pcovi)
			# TODO: use J*P*J' instead of R*P*R', with J the jacobian of R?
			# TODO: replace with sum over the whole matrix
			pcovd[:2,:2] += covarsd[t][:2,:2]; pcovd[2,2] += covarsd[t][2,2]
			pcovi[:2,:2] += covarsi[t][:2,:2]; pcovi[2,2] += covarsi[t][2,2]
			p.state.covd = np.reshape(pcovd, 5*5)
			p.state.covi = np.reshape(pcovi, 5*5)

		# NB: source infos are already in the static frame as it is better not to transform grids too much
		percepts.header.frame_id = self.states[0].header.frame_id

if __name__=='__main__':
	aligner_node = AlignerNode()
	rospy.spin()
