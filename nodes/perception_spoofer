#!/usr/bin/env python

import yaml
import rospy
import random
import numpy as np
from map_server_msgs.msg import MapTile
from map_lib import msgToMapTile
from multiception.msg import Perceptions
from multiception.object import SimulatedVehicle

class PerceptionSpooferNode:
	def __init__(self):
		rospy.init_node('perception_spoofer')
		self.parse_config(rospy.get_param('~config_file'))
		self.id_ignored = -1
		self.id_spoofed_size = -1
		self.id_spoofed_position = -1
		self.false_detection = None
		self.links = None
		self.links_to_id = []

		self.pub = rospy.Publisher('output', Perceptions, queue_size=10)
		self.sub = rospy.Subscriber('input', Perceptions, self.callback)
		self.sub_map = rospy.Subscriber('/maptile', MapTile, self.callback_map)

	def parse_config(self, config_path):
		with open(config_path) as f:
			config = yaml.safe_load(f)

		self.ghosts = [SimulatedVehicle(**g) for g in config.get('ghosts', [])]
		self.ignored_links = [[a['t0'], a['t0']+a.get('duration', 1E30), a['links']] for a in config.get('ignored-links', [])]

	def callback_map(self, msg):
		maptile = msgToMapTile(msg)

		for g in self.ghosts:
			g.set_path(maptile)

		links = []
		for lid, link in maptile.links.items():
			links.append(link.polyline)
			self.links_to_id += [lid]*link.polyline.shape[1]

		self.links = np.hstack(links)[:2,:-1].T
		self.links_to_id = np.array(self.links_to_id)[:-1]
		self.sub_map.unregister()

	def spoof(self, msg):
		objects = { o.object_id: o for o in msg.perceptions }
		oids = list(objects.keys())

		if self.id_spoofed_size not in oids:
			self.id_spoofed_size = -1
			if len(oids)>0:
				self.id_spoofed_size = random.choice(oids)
				oids.remove(self.id_spoofed_size)
		if self.id_spoofed_size>0:
			objects[self.id_spoofed_size].length += 10
			objects[self.id_spoofed_size].width  += 50
			objects[self.id_spoofed_size].height += 100

		#if self.id_spoofed_position not in oids:
			#self.id_spoofed_position = -1
			#if len(oids)>0:
				#self.id_spoofed_position = random.choice(oids)
				#oids.remove(self.id_spoofed_position)
		#if self.id_spoofed_position>0:
			#objects[self.id_spoofed_position].state.x += 100
			#objects[self.id_spoofed_position].state.y -= 100

		if self.id_ignored not in oids:
			self.id_ignored = -1
			if len(oids)>0:
				self.id_ignored = random.choice(oids)
				oids.remove(self.id_ignored)
		if self.id_ignored>0:
			del msg.perceptions[msg.perceptions.index(objects[self.id_ignored])]

		if self.false_detection is None:
			if len(oids)>0:
				self.false_detection = objects[random.choice(oids)]
				self.false_detection.object_id = 3000
		if self.false_detection is not None:
			self.false_detection.state.x += np.random.normal(1, 10)
			self.false_detection.state.y += np.random.normal(-3, 10)
			msg.perceptions.append(self.false_detection)

		rospy.loginfo('Spoofed %i %i %i', self.id_spoofed_size, self.id_ignored, -1 if self.false_detection is None else self.false_detection.object_id)

	def remove_objects_on_ignored_links(self, msg):
		if self.links is None or len(msg.perceptions)==0: return
		t = msg.header.stamp.to_sec()

		ignored_links = { link_id for tbeg, tend, link_ids in self.ignored_links for link_id in link_ids if t>tbeg and t<tend }
		object_positions = np.array([(p.state.x, p.state.y) for p in msg.perceptions])
		distances = np.linalg.norm(object_positions[None,:,:]-self.links[:,None,:], axis=2)
		map_matched_links = self.links_to_id[distances.argmin(axis=0)]
		msg.perceptions = [p for i,p in enumerate(msg.perceptions) if map_matched_links[i] not in ignored_links]

	def callback(self, msg):
		t = msg.header.stamp.to_sec()
		self.remove_objects_on_ignored_links(msg)
		ghosts = [g.step(t) for g in self.ghosts]
		msg.perceptions += [g.to_msg() for g in ghosts if g is not None]
		self.pub.publish(msg)

if __name__=='__main__':
	psn = PerceptionSpooferNode()
	rospy.spin()
