#include <future>
#include <chrono>
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <ros_msg_parser/ros_parser.hpp>

#include <pcl_conversions/pcl_conversions.h>
#include <mobileye/FreeSpacePoints.h>
#include <grid_map_msgs/GridMap.h>
#include <grid_map_ros/GridMapRosConverter.hpp>

#include <dynamic_reconfigure/server.h>
#include <multiception/SourceInfoConfig.h>

#include <lidar_utils/shape.h>
#include <perception_utils/LimiterFovMap.h>
#include <multiception/StateStamped.h>
#include <multiception/SourceStamped.h>
// #include <perception_utils/geometry.hpp>
#include <perception_utils/polygons.h>
#include <perception_utils/grid_map.h>
#include <perception_utils/transform.h>
#include <boost/geometry.hpp>

using PointT = lidar_utils::PointXYZIRTC;
using PointCloudT = pcl::PointCloud<PointT>;
using perception_utils::Polygon2;
using perception_utils::PointList2;
using perception_utils::EvidentialGridMap;
using perception_utils::TransformManagerROS;

constexpr float ZOE_LENGTH = 4.084, ZOE_WIDTH = 1.730; //, ZOE_HEIGHT = 1.562;

Polygon2 freespaceFromMobileye(const mobileye::FreeSpacePoints& fs, float dmax)
{
	Polygon2 polygon(2, fs.fspoints.size()+2);
	for(size_t i=0; i<fs.fspoints.size(); i++)
	{
		polygon(0,i+1) = fs.fspoints[i].pt_pose_x;
		polygon(1,i+1) = -fs.fspoints[i].pt_pose_y;

		// When points are too far away, we transform in polar coordinates to constrain the distance
		const float dist = sqrt(polygon(0,i+1)*polygon(0,i+1) + polygon(1,i+1)*polygon(1,i+1));
		if(dist > dmax)
		{
			const float theta = std::atan2(polygon(1,i+1), polygon(0,i+1));
			polygon(0,i+1) = std::cos(theta) * std::min(dmax, dist);
			polygon(1,i+1) = std::sin(theta) * std::min(dmax, dist);
		}
	}
	polygon(0,0) = polygon(0,fs.fspoints.size()+1) = 0.1;
	polygon(1,0) = polygon(1,fs.fspoints.size()+1) = 0.0;
	return polygon;
}

std::shared_ptr<Polygon2> limitFovMin(const Polygon2& fovMin, const std_msgs::Header& header, ros::ServiceClient& client, bool invertY, float simplifMax)
{
	perception_utils::LimiterFovMap limiterParams;
	perception_utils::convert::eigenToJsk(fovMin, limiterParams.request.polygon);
	limiterParams.request.header = header;
	limiterParams.request.limit_buildings = limiterParams.request.limit_signs = limiterParams.request.limit_road = true;

	if(client and client.exists() and client.call(limiterParams))
	{
		Polygon2 limitedFov;
		std::shared_ptr<Polygon2> limitedFovSimpl { new Polygon2 };
		perception_utils::convert::jskToEigen(limiterParams.response.polygon, limitedFov);
		Polygon2 freespSimpl; perception_utils::simplifyPolygon(limitedFov, *limitedFovSimpl, simplifMax);
		if(invertY) limitedFovSimpl->row(1) *= -1;
		return limitedFovSimpl;
	}
	else
	{
		return nullptr;
	}
}

Polygon2 getCarPolygon(float Length=ZOE_LENGTH, float width=ZOE_WIDTH)
{
	constexpr float s = 1.294; // Shift to centroid
	const float l = 0.5 * ZOE_LENGTH;
	const float w = 0.5 * ZOE_WIDTH;

	Polygon2 corners(2, 5);
	corners << s+l, s-l, s-l, s+l, s+l,
	             w,   w,  -w,  -w,   w;

	return corners;
}

std::shared_ptr<Polygon2> dummyLimit(const Polygon2& fovMin)
{
	return std::make_shared<Polygon2>(fovMin);
}

class SourceGeneratorNode
{
public:
	SourceGeneratorNode()
	: private_nh_("~"), tf_manager_(public_nh_, private_nh_, "base", "world", true, false, true)
	{
		pub_ = private_nh_.advertise<multiception::SourceStamped>("source_infos", 10);
		config_server_.setCallback(boost::bind(&SourceGeneratorNode::callbackConfig, this, _1, _2));
		sub_ = public_nh_.subscribe("input", 10, &SourceGeneratorNode::callback, this);
	}

protected:
	ros::NodeHandle public_nh_, private_nh_;
	ros::Publisher pub_;
	ros::Subscriber sub_;
	ros::ServiceClient cli_limiter_;
	TransformManagerROS tf_manager_;

	dynamic_reconfigure::Server<multiception::SourceInfoConfig> config_server_;
	float range_min_, range_max_, opening_left_, opening_right_;
	float resolution_, simplif_max_, smooth_distance_, fade_distance_;

	void callback(RosMsgParser::ShapeShifter::ConstPtr genericMsg)
	{
		std::shared_ptr<Polygon2> freesp, fovmin;
		multiception::SourceStamped::Ptr source { new multiception::SourceStamped };
		source->header.frame_id = "world";

		Eigen::Isometry3f transform = Eigen::Isometry3f::Identity();
		std::array<float, 3> sigmas { 0 };

		if(genericMsg->getDataType()=="multiception/StateStamped")
		{
			multiception::StateStamped::Ptr msg { new multiception::StateStamped };
			genericMsg->instantiate(*msg);
			source->header.stamp = msg->header.stamp;

			// Delay the span pose to the previous message, otherwhise transforms are never ready
			// Also, force the source frame to be span, otherwise TFs are used between world and base, which is not what we want
			const std::string frameFrom = tf::resolve(tf::getPrefixParam(private_nh_), "span");
			if(not tf_manager_.getTransform(frameFrom, perception_utils::type_helpers::getHeaderTime(msg->header)-1/50., transform, sigmas))
				return;

			callbackState(msg, freesp, fovmin);
		}
		else if(genericMsg->getDataType()=="mobileye/FreeSpacePoints")
		{
			mobileye::FreeSpacePoints::Ptr msg { new mobileye::FreeSpacePoints };
			genericMsg->instantiate(*msg);
			source->header.stamp = msg->header.stamp;

			if(not tf_manager_.getTransform(msg->header.frame_id, perception_utils::type_helpers::getHeaderTime(msg->header), transform, sigmas))
				return;

			callbackMobileye(msg, freesp, fovmin);
		}
		else if(genericMsg->getDataType()=="sensor_msgs/PointCloud2")
		{
			PointCloudT::Ptr msg { new PointCloudT };
			genericMsg->instantiate(*msg);
			source->header.stamp = ros::Time(perception_utils::type_helpers::getHeaderTime(msg->header));

			if(not tf_manager_.getTransform(msg->header.frame_id, perception_utils::type_helpers::getHeaderTime(msg->header), transform, sigmas))
				return;

			callbackLidar(msg, freesp, fovmin);
		}
		else
		{
			ROS_WARN_STREAM("Unsupported message type: " << genericMsg->getDataType());
			return;
		}

		Polygon2 freespSimpl, freespSimplWorld, fovminWorld;
		perception_utils::simplifyPolygon(*freesp, freespSimpl, simplif_max_);
		perception_utils::doTransform(freespSimpl, freespSimplWorld, transform);
		perception_utils::doTransform(*fovmin, fovminWorld, transform);
		perception_utils::convert::eigenToJsk(freespSimplWorld, source->source.freesp);
		perception_utils::convert::eigenToJsk(fovminWorld, source->source.fovmin);

		// Compute the observability grid from the three polygons
		const EvidentialGridMap observability = fovToObservabilityGrid(freespSimplWorld, fovminWorld, transform);
		if((observability.getLength()>0).all())
		{
			grid_map::GridMapRosConverter::toMessage(observability, source->source.observability);
			pub_.publish(source);
		}
	}

	void callbackState(multiception::StateStamped::ConstPtr state, std::shared_ptr<Polygon2>& freesp, std::shared_ptr<Polygon2>& fovmin)
	{
		freesp = std::make_shared<Polygon2>(Polygon2::Zero(2,3));
		fovmin = std::make_shared<Polygon2>(getCarPolygon(7, 4));
	}

	void callbackLidar(PointCloudT::ConstPtr pc, std::shared_ptr<Polygon2>& freesp, std::shared_ptr<Polygon2>& fovmin)
	{
		if(!cli_limiter_)
			cli_limiter_ = public_nh_.serviceClient<perception_utils::LimiterFovMap>("limit_fov_map", true);

		const std_msgs::Header header = pcl_conversions::fromPCL(pc->header);
		fovmin = std::make_shared<Polygon2>(perception_utils::polygonFromRadius(range_max_));
		std::future<std::shared_ptr<Polygon2>> fovminLimitedFuture = std::async(std::launch::async, limitFovMin, std::cref(*fovmin), std::cref(header), std::ref(cli_limiter_), false, simplif_max_);

		freesp = std::make_shared<Polygon2>(lidar_utils::computeFreespace(pc, -1.2, 3, range_max_, 5, 10, 4));
		std::shared_ptr<Polygon2> fovminLimited = fovminLimitedFuture.get();
		if(fovminLimited)
		{
			fovmin = fovminLimited;
		}
		else
		{
			ROS_WARN("Issue calling the limit_fov_map service");
		}
	}

	void callbackMobileye(mobileye::FreeSpacePoints::ConstPtr msg, std::shared_ptr<Polygon2>& freesp, std::shared_ptr<Polygon2>& fovmin)
	{
		if(!cli_limiter_)
			cli_limiter_ = public_nh_.serviceClient<perception_utils::LimiterFovMap>("limit_fov_map", true);

		fovmin = std::make_shared<Polygon2>(perception_utils::polygonFromRadius(range_max_, opening_left_, opening_right_));
		std::future<std::shared_ptr<Polygon2>> fovminLimitedFuture = std::async(std::launch::async, limitFovMin, std::cref(*fovmin), std::cref(msg->header), std::ref(cli_limiter_), true, simplif_max_);

		freesp = std::make_shared<Polygon2>(freespaceFromMobileye(*msg, range_max_));
		std::shared_ptr<Polygon2> fovminLimited = fovminLimitedFuture.get();
		if(fovminLimited)
		{
			fovmin = fovminLimited;
		}
		else
		{
			ROS_WARN("Issue calling the limit_fov_map service");
		}
	}

	void callbackConfig(const multiception::SourceInfoConfig& config, uint32_t)
	{
		range_min_ = config.range_min;
		range_max_ = config.range_max;
		opening_left_ = -config.opening_left;
		opening_right_ = config.opening_right;

		resolution_ = config.resolution;
		simplif_max_ = config.simplif_max;
		smooth_distance_ = config.smooth_distance;
		fade_distance_ = config.fade_distance;
	}

	/*EvidentialGridMap fovToObservabilityGridPre(const Polygon2& freesp, const Polygon2& fovmin) const
	{
		grid_map::Polygon freespGM, fovminGM;
		perception_utils::convert::eigenToGridMap(freesp, freespGM);
		perception_utils::convert::eigenToGridMap(fovmin, fovminGM);

		EvidentialGridMap gm(std::vector<std::string>{"o", "O"});
		gm.setGeometry(grid_map::Length(1,1), resolution_);
		gm.extendToInclude(fovminGM);
		perception_utils::reset(gm);

		EvidentialGridMap::Matrix& o = gm["o"];
		EvidentialGridMap::Matrix& O = gm["O"];
		const float smoothFac = -std::log(2) / smooth_distance_;
		const float fadeFac = -std::log(2) / fade_distance_;
		const PointList2 centers = perception_utils::generateCellCenters(gm);
		const Eigen::ArrayXf dfre = perception_utils::distancePointPolygon(centers, freesp);
		const Eigen::ArrayXf d = centers.colwise().norm();
		const Eigen::ArrayXf detectabilities = (1 - (dfre*smoothFac).exp()) * (d*fadeFac).exp();
		const Eigen::Map<const Eigen::ArrayXXf> detectability (detectabilities.data(), o.rows(), o.cols());

		for(grid_map::PolygonIterator it(gm, fovminGM); !it.isPastEnd(); ++it) {
			const grid_map::Index index(*it);
			o(index[0],index[1]) = detectability(index[0], index[1]);
		}
		for(grid_map::PolygonIterator it(gm, freespGM); !it.isPastEnd(); ++it) {
			const grid_map::Index index(*it);
			o(index[0],index[1]) = 0;
			O(index[0],index[1]) = detectability(index[0], index[1]);
		}

		gm["o,O"] = 1 - (o + O).array();
		return gm;
	}*/

	EvidentialGridMap fovToObservabilityGrid(const Polygon2& freesp, const Polygon2& fovmin, const Eigen::Isometry3f& transform) const
	{
		grid_map::Polygon freespGM, fovminGM;
		perception_utils::convert::eigenToGridMap(freesp, freespGM);
		perception_utils::convert::eigenToGridMap(fovmin, fovminGM);

		perception_utils::BoostPolygon freespBoost, fovminBoost;
		perception_utils::convert::eigenToBoost(freesp, freespBoost);
		perception_utils::convert::eigenToBoost(fovmin, fovminBoost);

		// Make a hole the same size of the polygon so that the distance function also works when it is inside
		perception_utils::BoostPolygon freespBoostWithHole(freespBoost), fovminBoostWithHole(fovminBoost);

		freespBoostWithHole.inners().resize(1);
		freespBoostWithHole.inners()[0].reserve(freespBoost.outer().size());
		for(const auto& p : freespBoost.outer()) boost::geometry::append(freespBoostWithHole.inners()[0], p);

		fovminBoostWithHole.inners().resize(1);
		fovminBoostWithHole.inners()[0].reserve(fovminBoost.outer().size());
		for(const auto& p : fovminBoost.outer()) boost::geometry::append(fovminBoostWithHole.inners()[0], p);

		grid_map::Position pos, gridPos;
		grid_map::Length gridLength;
		fovminGM.getBoundingBox(gridPos, gridLength);

		EvidentialGridMap gm(std::vector<std::string>{"o", "O"});
		if(not gridLength.allFinite()) return gm; // Sometimes there are infs on startup even though fovMinGM contains points

		// Align on resolution
		gridLength += 2*resolution_;
		gridPos = (gridPos/resolution_).array().floor() * resolution_;

		gm.setGeometry(gridLength, resolution_, gridPos);
		perception_utils::reset(gm, 0);

		const perception_utils::Point3 orig = transform * perception_utils::Point3(0, 0, 0).homogeneous();
		const perception_utils::BoostPoint2 borig (orig[0], orig[1]);
		EvidentialGridMap::Matrix& o = gm["o"];
		EvidentialGridMap::Matrix& O = gm["O"];
		for(grid_map::PolygonIterator it(gm, fovminGM); !it.isPastEnd(); ++it) {
			const grid_map::Index index(*it);
			gm.getPosition(index, pos);
			const perception_utils::BoostPoint2 p (pos[0], pos[1]);
			const float d = boost::geometry::distance(p, borig);

			if(freesp.cols()<=4) o(index[0],index[1]) = 1; // State grid are just good to go
			else if(d>range_min_)
			{
				// Skip points too close to the source
				const bool inmin = boost::geometry::within(p, fovminBoost);
				const bool infre = boost::geometry::within(p, freespBoost);
				const float dfre = boost::geometry::distance(p, freespBoostWithHole);

				const float df = (1-std::exp((-std::log(2)*dfre)/smooth_distance_)) * std::exp((-std::log(2)*d)/fade_distance_);
				if(infre) O(index[0],index[1]) = (df>0.1) ? df : 0;
				else if(inmin) o(index[0],index[1]) = (df>0.1) ? df : 0;
				// implicit else oO=1
			}
		}

		gm["o,O"] = 1 - (o + O).array();
		return gm;
	}
};

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "source_generator");

	SourceGeneratorNode node;
	ros::spin();
	return 0;
}
