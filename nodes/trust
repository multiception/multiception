#!/usr/bin/env python3

import rospy
import pickle
import logging
import traceback
import numpy as np
from threading import Lock
from geometry_msgs.msg import Point32
from dynamic_reconfigure.server import Server
from pyqtgraph import QtGui, QtCore, GraphicsLayoutWidget, setConfigOption, PlotDataItem, ImageItem

from std_msgs.msg import Bool
from map_server_msgs.msg import MapTile
from map_lib import msgToMapTile

from multiception.msg import Trust, Bytes, Perceptions
from multiception.cfg import TrustConfig
from multiception.trust import TrustBuilder
from multiception.belief import MassFunction
from multiception.object import Object, plotdebug_objects
from multiception.evidential_grid_map import EvidentialGridMap, plotdebug_gridmap

class TrustNode(QtCore.QObject):
	signal_update = QtCore.pyqtSignal()

	def __init__(self):
		super().__init__()
		np.seterr(all='raise', under='ignore')
		rospy.init_node('trust_builder')
		self.trust_builder = TrustBuilder()
		self.config_server = Server(TrustConfig, self.callback_config)
		self.pub_trust = rospy.Publisher('trust', Trust, queue_size=10)

		self.trust_builder.debugplot = rospy.get_param('~debug', False)
		if self.trust_builder.debugplot:
			logging.getLogger('rosout').setLevel(rospy.impl.rosout._rospy_to_logging_levels[rospy.DEBUG])
			setConfigOption('background', 'w')
			setConfigOption('foreground', 'k')
			self.debugplot_win = GraphicsLayoutWidget(show=True, title=rospy.get_name())
			self.debugplot_win.resize(1920, 500)
			self.debugplot_figs = {}
			self.debugplot_sourceids = []
			self.debugplot_curves = {}
			self.debugplot_curves_items = {}
			self.debugplot_ref_items = {}
			self.old_ref_items = {}
			self.signal_update.connect(self.draw)
			self.debugplot_timer = QtCore.QTimer()
			self.debugplot_timer.timeout.connect(lambda: self.draw(True))
			self.debugplot_timer.start(2000) # Wake up every 2 seconds to check if the windows have to be closed

		topics_in = rospy.get_param('~topics_in').split(';')
		self.subs_comp = [rospy.Subscriber(topic, Perceptions, self.callback, topic, queue_size=1) for topic in topics_in]
		self.sub_map   = rospy.Subscriber('/maptile', MapTile, self.callback_map)
		self.sub_exp   = rospy.Subscriber('/trigger_export/truster', Bool, self.export)

		initial_trust_filter = MassFunction({'t': 1.0 ,'tT': 0.0})
		initial_trust_local  = MassFunction({'t': 0.8 ,'tT': 0.2})
		initial_trust_remote = MassFunction({'t': 0.0 ,'tT': 1.0})

		self.trust_builder.initial_trusts['filter']              = initial_trust_filter
		self.trust_builder.initial_trusts['lidar']               = initial_trust_local
		self.trust_builder.initial_trusts['mobileye']            = initial_trust_local
		self.trust_builder.initial_trusts['egotracker']          = initial_trust_local

		self.trust_builder.initial_trusts['zoeblue/filter']      = initial_trust_filter
		self.trust_builder.initial_trusts['zoegrey/filter']      = initial_trust_filter
		self.trust_builder.initial_trusts['zoewhite/filter']     = initial_trust_filter
		self.trust_builder.initial_trusts['zoeblue/lidar']       = initial_trust_local
		self.trust_builder.initial_trusts['zoegrey/lidar']       = initial_trust_local
		self.trust_builder.initial_trusts['zoewhite/lidar']      = initial_trust_local
		self.trust_builder.initial_trusts['zoeblue/mobileye']    = initial_trust_local
		self.trust_builder.initial_trusts['zoegrey/mobileye']    = initial_trust_local
		self.trust_builder.initial_trusts['zoewhite/mobileye']   = initial_trust_local
		self.trust_builder.initial_trusts['zoeblue/egotracker']  = initial_trust_local
		self.trust_builder.initial_trusts['zoegrey/egotracker']  = initial_trust_local
		self.trust_builder.initial_trusts['zoewhite/egotracker'] = initial_trust_local
		self.trust_builder.initial_trusts['zoeblue/multracker']  = initial_trust_local
		self.trust_builder.initial_trusts['zoegrey/multracker']  = initial_trust_local
		self.trust_builder.initial_trusts['zoewhite/multracker'] = initial_trust_local

		self.trust_builder.initial_trusts['zoeblue']             = initial_trust_remote
		self.trust_builder.initial_trusts['zoegrey']             = initial_trust_remote
		self.trust_builder.initial_trusts['zoewhite']            = initial_trust_remote

	def callback_config(self, config, level):
		self.trust_builder.mode                = self.trust_builder.Mode(config.mode)
		self.trust_builder.combi_fun           = self.trust_builder.COMBI_FUNS[config.rule]
		self.trust_builder.COST_MISPLACED_OBJECTS = config.cost_misplaced_objects
		self.trust_builder.COST_NONDETECTABLE_OBJECTS = config.cost_nondetectable_objects
		self.trust_builder.FREESPACE_CONFIRMATION_AREA = config.freespace_confirmation_area
		self.trust_builder.HLTIME_LOCAL        = config.half_life_time_local
		self.trust_builder.HLTIME_REMOTE       = config.half_life_time_remote

		self.trust_builder.weights['past']        = config.w_past
		self.trust_builder.weights['cohe']        = config.w_cohe
		self.trust_builder.weights['cohe-obd']    = config.w_cohe_obd
		self.trust_builder.weights['cohe-atc']    = config.w_cohe_atc
		self.trust_builder.weights['cohe-spc']    = config.w_cohe_spc
		self.trust_builder.weights['cohe-spc-ro'] = config.w_cohe_spc_ro
		self.trust_builder.weights['cohe-spc-bu'] = config.w_cohe_spc_bu
		self.trust_builder.weights['cons']        = config.w_cons
		self.trust_builder.weights['conf']        = config.w_conf
		self.trust_builder.weights['conf-osi']    = config.w_conf_osi
		self.trust_builder.weights['conf-fsi']    = config.w_conf_fsi
		self.trust_builder.weights['conf-ofi']    = config.w_conf_ofi
		self.trust_builder.weights['conf-odi']    = config.w_conf_odi
		self.trust_builder.weights['conf-odi-rc'] = config.w_conf_odi_rc
		self.trust_builder.weights['conf-odi-cr'] = config.w_conf_odi_cr

		static_names = config.static_names.split(';') if len(config.static_names)>0 else []
		static_val_trust = config.values_trust.split(';') if len(config.values_trust)>0 else []
		static_val_nottrust = config.values_nottrust.split(';') if len(config.values_nottrust)>0 else []
		if len(static_names)==len(static_val_trust) and len(static_names)==len(static_val_nottrust):
			for name, t, T in zip(static_names, static_val_trust, static_val_nottrust):
				self.trust_builder.set_static_source(name, MassFunction({ 't':float(t),'T':float(T),'tT':1-float(t)-float(T) }))
		else:
			rospy.logwarn('Not the same number of static trust values and names: %i!=%i!=%i', len(static_names), len(static_val_trust), len(static_val_nottrust))

		return config

	def callback_map(self, msg):
		self.trust_builder.set_map(msgToMapTile(msg))

	def callback(self, msg, topic):
		t = msg.header.stamp.to_sec()
		for source in msg.sources:
			objs = [Object.from_msg(percept) for percept in msg.perceptions if percept.source_id==source.source_id]
			dete = EvidentialGridMap.from_msg(source.observability)

			try:
				self.trust_builder.callback(source.source_id, t, objs, dete)
			except (RuntimeError, ValueError, Warning) as e:
				rospy.logwarn('Trust update caused %s', ''.join(traceback.format_exception(None, e, e.__traceback__)))

		trusts = self.trust_builder.get_latest_trusts()
		trust_msg = Trust()
		trust_msg.header.frame_id = "world"
		trust_msg.header.stamp = msg.header.stamp
		for sourceid, (t, trust) in trusts.items():
			trust_msg.sources.append(sourceid)
			trust_msg.trusts.append(trust.to_msg())
		self.pub_trust.publish(trust_msg)

		if self.trust_builder.debugplot: self.update_plot(trusts)

	def update_plot(self, trusts):
		with self.trust_builder.mutex:
			self.old_curves_items = self.debugplot_curves_items
			self.debugplot_curves_items = { sourceid: (
				PlotDataItem(self.trust_builder.debugtrusts[sourceid]['t']-self.trust_builder.debugtrusts[sourceid]['t'].min(), self.trust_builder.debugtrusts[sourceid]['hist'][:,:1].sum(axis=1), pen='b'),
				PlotDataItem(self.trust_builder.debugtrusts[sourceid]['t']-self.trust_builder.debugtrusts[sourceid]['t'].min(), self.trust_builder.debugtrusts[sourceid]['hist'][:,:2].sum(axis=1), pen='g'),
				PlotDataItem(self.trust_builder.debugtrusts[sourceid]['t']-self.trust_builder.debugtrusts[sourceid]['t'].min(), self.trust_builder.debugtrusts[sourceid]['hist'][:,:3].sum(axis=1), pen='r'),
			) for sourceid in self.trust_builder.debugtrusts }

			self.old_ref_items.update(self.debugplot_ref_items)
			self.debugplot_ref_items.update({ sourceid: (
				*(plotdebug_objects(objs, col=(255, 127, 14)) if len(objs:=self.trust_builder.history[sourceid].latest()[1])>0 else []),
				*(plotdebug_objects(refobjs, col=(31, 119, 180)) if len(refobjs:=[o for ref_objs,_ in self.trust_builder.references[sourceid] for o in ref_objs]) else []),
				*(plotdebug_gridmap(ref_dete, opacity=0.95) for _,ref_dete in self.trust_builder.references[sourceid]),
			) for sourceid in self.trust_builder.history })

		self.signal_update.emit()

	@QtCore.pyqtSlot()
	def draw(self, close_only=False):
		if rospy.is_shutdown():
			self.debugplot_win.close()

		if rospy.is_shutdown() or close_only:
			return

		with self.trust_builder.mutex:
			# Recreate the layout if new sources have been received
			sourceids = sorted( set(self.debugplot_sourceids) | set(self.debugplot_curves_items.keys()) )
			if self.debugplot_sourceids!=sourceids:
				self.debugplot_sourceids = sourceids
				self.debugplot_win.clear()
				self.debugplot_figs = {}

				for sourceid in self.debugplot_sourceids:
					self.debugplot_figs[sourceid] = [None, None]
					self.debugplot_figs[sourceid][0] = self.debugplot_win.addPlot(title=sourceid)
					self.debugplot_figs[sourceid][0].setYRange(0, 1)

				self.debugplot_win.nextRow()

				for sourceid in self.debugplot_sourceids:
					self.debugplot_figs[sourceid][1] = self.debugplot_win.addPlot()
					self.debugplot_figs[sourceid][1].setAspectLocked(lock=True, ratio=1)

				return
			else:
				# Otherwise just clear all figs
				for figcurve,figref in self.debugplot_figs.values():
					figcurve.clear()
					figref.clear()

			for sourceid, items in self.debugplot_curves_items.items():
				for item in items:
					self.debugplot_figs[sourceid][0].addItem(item)

			for sourceid, items in self.debugplot_ref_items.items():
				for item in items:
					self.debugplot_figs[sourceid][1].addItem(item)

	def export(self, _):
		from pathlib import Path
		export_path = (Path('/tmp')/rospy.get_param('/run_id')/'truster'/rospy.get_namespace()[1:-1]).with_suffix('.pkl')
		export_path.parent.mkdir(exist_ok=True, parents=True)

		with self.trust_builder.mutex:
			rospy.logwarn(f'Exporting to {export_path}')
			with open(export_path, 'wb') as f:
				pickle.dump(tn.trust_builder, f)
			rospy.logwarn('Done')

if __name__=='__main__':
	tn = TrustNode()

	if tn.trust_builder.debugplot:
		QtGui.QApplication.instance().exec_()
	rospy.spin()

