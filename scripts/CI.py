import numpy as np
from numpy import trace
from numpy.linalg import det, inv
import scipy.optimize
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

plt.rc('text', usetex=True)
np.set_printoptions(precision=3, linewidth=999, suppress=True, sign=' ')

def plot_ellipse(mean, cov, alpha, ax, **kwargs):
    s = np.arange(0, 2*np.pi, 0.01)
    contour = mean[:2] + scipy.linalg.sqrtm(-2*np.log(alpha)*cov[:2,:2]) @ np.array([np.cos(s), np.sin(s)])
    lines = ax.plot(contour[0,:], contour[1,:], **kwargs)
    ax.plot(mean[0,0], mean[1,0], '+', c=lines[0].get_c())

    # Heading uncertainty
    if len(mean)>2:
        covheading = min(np.pi/2,np.sqrt(cov[2,2]))
        a = np.array([np.cos(mean[2,0]-covheading), np.sin(mean[2,0]-covheading)])
        b = np.array([np.cos(mean[2,0]+covheading), np.sin(mean[2,0]+covheading)])
        contour = mean[:2] + scipy.linalg.sqrtm(-2*np.log(alpha)*cov[:2,:2]) @ np.array([a, [0,0], b]).T
        ax.plot(contour[0,:], contour[1,:], c=lines[0].get_c())

    # Speed uncertainty
    if len(mean)>3:
        c,s = np.cos(mean[2]), np.sin(mean[2])
        v = mean[3]/10
        vd = np.sqrt(cov[3,3])/20
        contour = mean[:2] + np.array([[0,0], [c*(v-vd),s*(v-vd)], [c*v,s*v], [c*(v+vd),s*(v+vd)]]).T
        ax.plot(contour[0,0:2], contour[1,0:2], c=lines[0].get_c(), lw=2)
        ax.plot(contour[0,1:3], contour[1,1:3], c=lines[0].get_c(), ls=('dashed'), lw=2)
        ax.plot(contour[0,2:4], contour[1,2:4], c=lines[0].get_c(), ls=('dotted'), lw=2)

SIMULATED_DATA = True
if SIMULATED_DATA:
    #x, P = np.array([[1, 2, 1.5, 2]]).T, np.diag([1,3,1,4])
    x, P = np.array([[1, 2, 2.5, 2.1]]).T, np.array([[1,0.5,0.2,0.2],[0.5,3,0.3,0.3],[0.2,0.3,0.8,0.1],[0.2,0.3,0.1,4]])
    Pd, Pi = P*0.8, P*0.2
    #y, R, C = np.array([[1.2, 1.7]]).T, np.array([[3,0],[0,0.5]]), np.array([[1,0,0,0],[0,1,0,0]])
    y, R, C = np.array([[1.2, 1.7]]).T, np.array([[3,-0.4],[-0.4,0.5]]), np.array([[1,0,0,0],[0,1,0,0]])
    Rd, Ri =np.zeros((2,2)), R
    Rd, Ri =R*0.3, R*0.7
else:
    x=np.array([[-127.22,-159.72,1.60,2.17,0.00,0.00]]).T
    y=np.array([[-127.25,-160.25,1.49,2.01]]).T
    C = np.array([[1, 0, 0, 0, 0, 0],[0, 1, 0, 0, 0, 0],[0, 0, 1, 0, 0, 0],[0, 0, 0, 1, 0, 0]], dtype=float)
    Pd, Pi = np.array([[ 7.05,3.72,0.43,0.,3.79,0.],[ 3.72,8.02,0.,0.64,0.,4.82],[ 0.43,0.,8.05,0., 75.39,0.],[ 0.,0.64,0.,9.09,0., 75.39],[ 3.79,0., 75.39,0.,750.2 ,0.],[ 0.,4.82,0., 75.39,0.,750.2 ]]), np.array([[ 2.35,1.24,0.14,0.,1.26,0.],[ 1.24,2.67,0.,0.21,0.,1.61],[ 0.14,0.,2.68,0., 25.13,0.],[ 0.,0.21,0.,3.03,0., 25.13],[ 1.26,0., 25.13,0.,250.,0.],[ 0.,1.61,0., 25.13,0.,250.]])
    Rd, Ri = np.array([[ 7.02,3.72,0.,0.],[ 3.72,7.98,0.,0.],[ 0.,0.,0.46,0.],[ 0.,0.,0.,1.5 ]]), np.array([[ 2.34,1.24,0.,0.],[ 1.24,2.66,0.,0.],[ 0.,0.,0.15,0.],[ 0.,0.,0.,0.5 ]])
    P = Pd+Pi
    R = Rd+Ri

I1 = inv(P)
CI1 = inv(C@P@C.T)
I2 = inv(R)

w_basic = scipy.optimize.minimize_scalar(lambda w, A, B: trace(inv( w*A+(1-w)*B )), bounds=(0,1), method='bounded', args=(I1, C.T@I2@C)).x
w_reduc = scipy.optimize.minimize_scalar(lambda w, A, B: trace(inv( w*A+(1-w)*B )), bounds=(0,1), method='bounded', args=(CI1, I2)).x
w_spltb = scipy.optimize.minimize_scalar(lambda w: trace(inv( inv(Pd/w + Pi) + C.T@inv(Rd/(1-w) + Ri)@C )), method='bounded', bounds=(0.001,0.999)).x
w_spltr = scipy.optimize.minimize_scalar(lambda w: trace(inv( inv(C@(Pd/w+Pi)@C.T) + inv(Rd/(1-w) + Ri) )), method='bounded', bounds=(0.001,0.999)).x
w = w_reduc
Pw = P-(1-1/w)*(C.T@C@P@C.T@C)

P_basic = inv( w_basic*I1 + (1-w_basic)*C.T@I2@C)
P_kalmn = inv( I1 + C.T@I2@C)
P_reduc = inv( I1+C.T@((w-1)*inv(C@P@C.T)+(1-w)*I2)@C )
#P_realt = inv( I1 - C.T@(C@I1@C.T-w*inv(C@P@C.T) - (1-w)*I2 )@C )
P_realt = Pw - Pw@C.T@inv( (C@Pw@C.T) + R/(1-w) )@C@Pw

#P_spinv = inv(inv( inv(Pd)+C.T@((w_spltr-1)*inv(C@Pd@C.T))@C) + Pi)
P_spinv = inv(Pd-(1-1/w_spltr)*(C.T@C@Pd@C.T@C) + Pi)
R_spinv = inv(Rd/(1-w_spltr) + Ri)
P_split = inv(P_spinv+C.T@R_spinv@C)
P_splti = P_split @ (P_spinv@Pi@P_spinv + C.T@R_spinv@Ri@R_spinv@C) @ P_split
P_spltd = P_split - P_splti
P_spinvO = inv(Pd/w_spltb + Pi)
R_spinvO = inv(Rd/(1-w_spltb) + Ri)
P_splitO = inv(P_spinvO+C.T@R_spinvO@C)
P_spltiO = P_splitO @ (P_spinvO@Pi@P_spinvO + C.T@R_spinvO@Ri@R_spinvO@C) @ P_splitO
P_spltdO = P_splitO - P_spltiO

x_basic = P_basic@(w_basic*I1@x + (1-w_basic)*C.T@I2@y)
x_kalmn = P_kalmn@(I1@x + C.T@I2@y)
x_reduc = P_reduc@(I1@x + (w-1)*C.T@inv(C@P@C.T)@C@x + (1-w)*C.T@I2@y)
#x_realt = x + Pw@C.T@np.linalg.inv(C@Pw@C.T+R/(1-w))@(y-C@x)
x_realt = P_realt@(inv(Pw)@x + (1-w)*C.T@I2@y)
x_split = P_split @ (P_spinv@x + C.T@R_spinv@y)
x_splitO = P_splitO @ (P_spinvO@x + C.T@R_spinvO@y)

print('w    ObsExt {:0.3f}    StaRed {:0.3f}    Split  {:0.3f}    SplitOld  {:0.3f}\n'.format(w_basic, w_reduc, w_spltr, w_spltb))
for a,b,c in [
    ('State     ', x.flatten(), Pd+1j*Pi),
    ('Observ    ', y.flatten(),Rd+1j*Ri),
    ('Kalman    ', x_kalmn.flatten(),P_kalmn),
    ('ObsExt    ', x_basic.flatten(),P_basic),
    ('StaRed    ', x_reduc.flatten(),P_reduc),
    ('StaRedAlt ', x_realt.flatten(),P_realt),
    ('SplObsExt ', x_split.flatten(),P_spltd+1j*P_splti),
    ('SplStaRed ', x_splitO.flatten(),P_spltdO+1j*P_spltiO),
    ]:
    print('{}     {}\n{}\n\n'.format(a,b,c))

for a,b,c,d in [
        (x,P,'Track',{'c':'tab:grey'}),
        (y,R,'Observation',{'c':'tab:grey'}),
        (x_kalmn,P_kalmn,'Kalman Baseline',{'c':'k'}),
        (x_basic,P_basic,'Observation Ext',{'c':'tab:red'}),
        #(x_reduc,P_reduc,'State Reduction',{'c':'tab:green'}),
        (x_realt,P_realt,'State Reduction Alt',{'c':'tab:olive'}),
        #(x_splitO,P_spltiO,'Split ObsExt Ind',{'c':'tab:cyan', 'ls':'--'}),
        #(x_splitO,P_spltdO+P_spltiO,'Split ObsExt Tot',{'c':'tab:cyan'}),
        #(x_split,P_splti,'Split StaRed Ind',{'c':'tab:blue', 'ls':'--'}),
        #(x_split,P_spltd+P_splti,'Split StaRed Tot',{'c':'tab:blue'}),
    ]:
    plot_ellipse(a,b,0.95, plt.gca(), label='{} {:.1f}'.format(c, det(b)), **d)
plt.tight_layout()
plt.legend()
plt.title('Fusion Result')
plt.axis('equal')


i = np.arange(0.1, 0.99, 0.1)
ws_basic = np.array([1/det( w*I1+(1-w)*C.T@I2@C ) for w in i])
wi_basic = np.array([det( w*I1+(1-w)*C.T@I2@C ) for w in i])
ws_reduc = np.array([1/det( w*inv(C@P@C.T)+(1-w)*I2 ) for w in i])
ws_redcm = np.array([1/det( I1 + C.T@((w-1)*inv(C@P@C.T)+(1-w)*I2)@C ) for w in i])
ws_rdcma = np.array([det( P-(1-1/w)*(C.T@C@P@C.T@C) - (P-(1-1/w)*(C.T@C@P@C.T@C))@C.T@inv( (C@(P-(1-1/w)*(C.T@C@P@C.T@C))@C.T) + R/(1-w) )@C@(P-(1-1/w)*(C.T@C@P@C.T@C)) ) for w in i])
ws_spltb = np.array([1/det( inv(Pd/w + Pi) + C.T@inv(Rd/(1-w) + Ri)@C ) for w in i])
ws_spltr = np.array([1/det( inv(C@(Pd/w+Pi)@C.T) + inv(Rd/(1-w) + Ri) ) for w in i])
ws_splta = np.array([1/det( C@inv(Pd/w + Pi)@C.T + inv(Rd/(1-w) + Ri) ) for w in i])
ws_spltm = np.array([1/det( inv( inv(Pd)+(w-1)*C.T@inv(C@Pd@C.T)@C + Pi)+C.T@inv(Rd/(1-w) + Ri)@C ) for w in i])

plt.figure()
plt.plot(i, ws_basic/np.max(ws_basic), label='Obser Ext * {:.1f}'.format(np.max(ws_basic)), c='tab:red', lw=3)
plt.plot(i, ws_reduc/np.max(ws_reduc), label='State Red * {:.1f}'.format(np.max(ws_reduc)), c='tab:green', lw=2)
#plt.plot(i, ws_redcm/np.max(ws_redcm), label='State Man * {:.1f}'.format(np.max(ws_redcm)), c='tab:olive')
plt.plot(i, ws_rdcma/np.max(ws_rdcma), label='State ManAlt * {:.1f}'.format(np.max(ws_rdcma)), c='tab:brown')
#plt.plot(i, ws_spltb/np.max(ws_spltb), label='Split Ext * {:.1f}'.format(np.max(ws_spltb)), c='tab:cyan', lw=3)
#plt.plot(i, ws_spltr/np.max(ws_spltr), label='Split Red * {:.1f}'.format(np.max(ws_spltr)), c='tab:blue', lw=2)
#plt.plot(i, ws_splta/np.max(ws_splta), label='Split Alt * {:.1f}'.format(np.max(ws_splta)), c='b')
#plt.plot(i, ws_spltm/np.max(ws_spltm), label='Split Man * {:.1f}'.format(np.max(ws_spltm)), c='tab:purple')
plt.tight_layout()
plt.legend()
plt.axis('equal')
plt.title('$\omega$ det optimization')

ws_basic = np.array([trace(inv( w*I1+(1-w)*C.T@I2@C )) for w in i])
ws_reduc = np.array([trace(inv( w*inv(C@P@C.T)+(1-w)*I2 )) for w in i])
ws_redcm = np.array([trace(inv( I1 + C.T@((w-1)*inv(C@P@C.T)+(1-w)*I2)@C )) for w in i])
ws_rdcma = np.array([trace( P-(1-1/w)*(C.T@C@P@C.T@C) - (P-(1-1/w)*(C.T@C@P@C.T@C))@C.T@inv( (C@(P-(1-1/w)*(C.T@C@P@C.T@C))@C.T) + R/(1-w) )@C@(P-(1-1/w)*(C.T@C@P@C.T@C)) ) for w in i])
ws_spltb = np.array([trace(inv( inv(Pd/w + Pi) + C.T@inv(Rd/(1-w) + Ri)@C )) for w in i])
ws_spltr = np.array([trace(inv( inv(C@(Pd/w+Pi)@C.T) + inv(Rd/(1-w) + Ri) )) for w in i])
ws_splta = np.array([trace(inv( C@inv(Pd/w + Pi)@C.T + inv(Rd/(1-w) + Ri) )) for w in i])
ws_spltm = np.array([trace(inv( inv( inv(Pd)+(w-1)*C.T@inv(C@Pd@C.T)@C + Pi)+C.T@inv(Rd/(1-w) + Ri)@C )) for w in i])

plt.figure()
plt.plot(i, ws_basic/np.max(ws_basic), label='Obser Ext * {:.1f}'.format(np.max(ws_basic)), c='tab:red', lw=3)
#plt.plot(i, wi_basic/np.max(wi_basic), label='INFORMATIONEL * {:.1f}'.format(np.max(wi_basic)), c='tab:olive', lw=1)
plt.plot(i, ws_reduc/np.max(ws_reduc), label='State Red * {:.1f}'.format(np.max(ws_reduc)), c='tab:green', lw=2)
#plt.plot(i, ws_redcm/np.max(ws_redcm), label='State Man * {:.1f}'.format(np.max(ws_redcm)), c='tab:olive')
plt.plot(i, ws_rdcma/np.max(ws_rdcma), label='State ManAlt * {:.1f}'.format(np.max(ws_rdcma)), c='tab:brown')
#plt.plot(i, ws_spltb/np.max(ws_spltb), label='Split Ext * {:.1f}'.format(np.max(ws_spltb)), c='tab:cyan', lw=3)
#plt.plot(i, ws_spltr/np.max(ws_spltr), label='Split Red * {:.1f}'.format(np.max(ws_spltr)), c='tab:blue', lw=2)
#plt.plot(i, ws_splta/np.max(ws_splta), label='Split Alt * {:.1f}'.format(np.max(ws_splta)), c='b')
#plt.plot(i, ws_spltm/np.max(ws_spltm), label='Split Man * {:.1f}'.format(np.max(ws_spltm)), c='tab:purple')
plt.tight_layout()
plt.legend()
plt.axis('equal')
plt.title('$\omega$ trace optimization')

def slider_update(w):
    ax.clear()
    P_basic = inv( w*I1 + (1-w)*C.T@I2@C)
    P_kalmn = inv( I1 + C.T@I2@C)
    P_reduc = inv( I1+C.T@((w-1)*inv(C@P@C.T)+(1-w)*I2)@C )
    #P_realt = inv( I1 - C.T@(C@I1@C.T-w*inv(C@P@C.T) - (1-w)*I2 )@C )
    Pw = P-(1-1/w)*(C.T@C@P@C.T@C)
    P_realt = Pw - Pw@C.T@inv( (C@Pw@C.T) + R/(1-w) )@C@Pw

    #P_spinv = inv(inv( inv(Pd)+C.T@((w-1)*inv(C@Pd@C.T))@C) + Pi)
    P_spinv = inv(Pd-(1-1/w)*(C.T@C@Pd@C.T@C) + Pi)
    R_spinv = inv(Rd/(1-w) + Ri)
    P_split = inv(P_spinv+C.T@R_spinv@C)
    P_splti = P_split @ (P_spinv@Pi@P_spinv + C.T@R_spinv@Ri@R_spinv@C) @ P_split
    P_spltd = P_split - P_splti
    P_spinvO = inv(Pd/w + Pi)
    R_spinvO = inv(Rd/(1-w) + Ri)
    P_splitO = inv(P_spinvO+C.T@R_spinvO@C)
    P_spltiO = P_splitO @ (P_spinvO@Pi@P_spinvO + C.T@R_spinvO@Ri@R_spinvO@C) @ P_splitO
    P_spltdO = P_splitO - P_spltiO

    x_basic = P_basic@(w*I1@x + (1-w)*C.T@I2@y)
    x_kalmn = P_kalmn@(I1@x + C.T@I2@y)
    x_reduc = P_reduc@(I1@x + (w-1)*C.T@inv(C@P@C.T)@C@x + (1-w)*C.T@I2@y)
    #x_realt = x + Pw@C.T@np.linalg.inv(C@Pw@C.T+R/(1-w))@(y-C@x)
    x_realt = P_realt@(inv(Pw)@x + (1-w)*C.T@I2@y)
    x_split = P_split @ (P_spinv@x + C.T@R_spinv@y)
    x_splitO = P_splitO @ (P_spinvO@x + C.T@R_spinvO@y)
    #print(P_spltd+1j*P_splti)
    print(P_basic-P_realt)
    print('')

    for a,b,c,d in [
        (x,P,'Track',{'c':'tab:grey'}),
        (y,R,'Observation',{'c':'tab:grey'}),
        (x_kalmn,P_kalmn,'Kalman Baseline',{'c':'k'}),
        (x_basic,P_basic,'Observation Ext',{'c':'tab:red'}),
        #(x_reduc,P_reduc,'State Reduction',{'c':'tab:green'}),
        (x_realt,P_realt,'State Reduction Alt',{'c':'tab:olive'}),
        #(x_splitO,P_spltiO,'Split ObsExt Ind',{'c':'tab:cyan', 'ls':'--', 'lw':3}),
        #(x_splitO,P_spltdO+P_spltiO,'Split ObsExt Tot',{'c':'tab:cyan', 'lw':3}),
        #(x_split,P_splti,'Split StaRed Ind',{'c':'tab:blue', 'ls':'--'}),
        #(x_split,P_spltd+P_splti,'Split StaRed Tot',{'c':'tab:blue'}),
    ]:
        plot_ellipse(a,b,0.95, ax, label='{} {:.1f}'.format(c, det(b)), **d)
    ax.legend()

fig, ax = plt.subplots()
slider = Slider(plt.axes([0.25, 0.1, 0.65, 0.03]), '$\omega$', 0.01, 0.99, valinit=w_spltr, valstep=0.05)
slider.on_changed(slider_update)
slider_update(w_spltr)
ax.axis('equal')

plt.show()
