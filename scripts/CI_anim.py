import numpy as np
from numpy import trace
from numpy.linalg import det, inv
import scipy.optimize
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

#plt.rc('text', usetex=True)
np.set_printoptions(precision=3, linewidth=999, suppress=True, sign=' ')
plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.spines.top'] = False

def plot_ellipse(mean, cov, alpha, ax, fill=False, **kwargs):
    s = np.linspace(0, 2*np.pi, 100)
    contour = mean[:2] + scipy.linalg.sqrtm(-2*np.log(alpha)*cov[:2,:2]) @ np.array([np.cos(s), np.sin(s)])
    if fill:
        ax.fill(contour[0,:], contour[1,:], alpha=0.5, **kwargs)
    else:
        lines = ax.plot(contour[0,:], contour[1,:], **kwargs)
        ax.plot(mean[0,0], mean[1,0], '+', c=lines[0].get_c())

C = np.diag([1,1])
x1, P1 = np.array([[0, .5]]).T, np.diag([1,3])
x2, P2 = np.array([[1.1, 0]]).T, np.diag([1,3])
y1, R1 = np.array([[0, .5]]).T, np.diag([3,1])
y2, R2 = np.array([[1.1, .8]]).T, np.diag([1,2])

I1 = inv(P1)
I2 = inv(P2)
I3 = inv(R1)
I4 = inv(R2)

def slider_update(w):
    ax.clear()
    P1_ = inv( w*I1 + (1-w)*C.T@I3@C)
    x1_ = P1_@(w*I1@x1 + (1-w)*C.T@I3@y1)
    P2_ = inv( w*I2 + (1-w)*C.T@I4@C)
    x2_ = P2_@(w*I2@x2 + (1-w)*C.T@I4@y2)
    for mean, cov, color, fill in [
        (x1, P1, 'tab:grey', False),
        (x2, P2, 'tab:grey', False),
        (y1, R1, 'tab:grey', False),
        (y2, R2, 'tab:grey', False),
        (x1_, P1_, 'tab:blue', True),
        (x2_, P2_, 'tab:blue', True),
    ]:
        plot_ellipse(mean, cov, 0.95, ax, fill=fill, c=color)

fig, ax = plt.subplots(figsize=(12, 10))
slider = Slider(plt.axes([0.95, 0.2, 0.0225, 0.6]), '$\omega$', 0, 1, valinit=0, valstep=0.05, orientation="vertical")
slider.on_changed(slider_update)
slider_update(0)
ax.axis('equal')

plt.show()
