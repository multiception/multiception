from shapely.geometry import Polygon, Point
import matplotlib.pyplot as plt
import numpy as np

freespace = Polygon([
	[ 85,115],
	[ 20, 60],
	[ 40, 60],
	[ 40, 50],
	[ 30, 20],
	[ 80,  5],
	[ 80, 18],
	[ 90, 18],
	[ 90,  5],
	[100,  5],
	[ 95,  65],
	[ 95, 107],
	[ 85, 115]
])

detectable = [
	Polygon([
		[20,60],
		[40,60],
		[40,60],
		[40,50],
		[30,20],
		[ 0,45],
		[20,60]
	]),
	Polygon([
		[80, 5],
		[80,18],
		[90,18],
		[90, 5],
		[80, 5]
	])
]

blur = 5
fade = 70
orig = Point(85,115)

@np.vectorize
def detectability(x,y):
	p = Point(x,y)

	if freespace.covers(p):
		d = freespace.exterior.distance(p)
		v = -1+np.exp(-(np.log(1/(1-0.5))*(d))/blur)

	elif detectable[0].covers(p) or detectable[1].covers(p):
		d = min(detectable[0].exterior.distance(p), detectable[1].exterior.distance(p))
		v = 1-np.exp(-(np.log(1/(1-0.5))*(d))/blur)

	else:
		v = 0
		#d = min(freespace.exterior.distance(p), detectable[0].exterior.distance(p), detectable[1].exterior.distance(p))
		#v = np.exp(-(np.log(1/(0.5))*(d))/blur)

	f = np.exp(-(np.log(1/(0.5))*( orig.distance(p) ))/fade)
	return v*f

#plt.plot(freespace.exterior.xy[0], freespace.exterior.xy[1])
#plt.plot(detectable[0].exterior.xy[0], detectable[0].exterior.xy[1])
#plt.plot(detectable[1].exterior.xy[0], detectable[1].exterior.xy[1])

X,Y = np.mgrid[0:141:2, 0:136:2]
mD = detectability(X,Y)
plt.axis('equal')
plt.imsave('/tmp/tsecFUSION.png', mD.T, cmap='RdBu', vmin=-1, vmax=1)
#plt.show()
