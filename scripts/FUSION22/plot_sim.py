#! /usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path

import pandas as pd
#from pyqtgraph import QtGui, QtCore, GraphicsLayoutWidget, setConfigOption, mkPen
import matplotlib.pyplot as plt

if __name__=='__main__':
	parser = ArgumentParser()
	parser.add_argument('export_dir', type=str)
	export_dir = parser.parse_args().export_dir

	dfs = {}
	paths = Path(export_dir).expanduser().resolve().glob('*.csv')
	for path in paths:
		dfs[path.stem] = pd.read_csv(path)

	categories = {}
	for title in dfs.keys():
		cat = title.split('_')[0]
		if cat not in categories: categories[cat] = []
		categories[cat].append(title)

	#wins = {}
	#display_all = False
	#setConfigOption('background', 'w')
	#setConfigOption('foreground', 'k')
	#for cat, titles in categories.items():
		#wins[cat] = GraphicsLayoutWidget(title=cat, show=True, size=(400,400))
		#for sid in ['i', 'j', 'k'] if display_all else ['i']:
			#for sid_other in ['i', 'j', 'k'] if display_all else ['j']:
				#fig = wins[cat].addPlot()
				#fig.setYRange(0, 1)
				#fig.addLegend()
				#for i,title in enumerate(sorted(titles)):
					#fig.plot(dfs[title]['t'], dfs[title][f'{sid}-{sid_other}-t']+dfs[title][f'{sid}-{sid_other}-T'], pen=mkPen((i,len(titles)), width=3, style=QtCore.Qt.DashLine))
					#fig.plot(dfs[title]['t'], dfs[title][f'{sid}-{sid_other}-t'], name=title.split('_')[1], pen=mkPen((i,len(titles)), width=2))
			#wins[cat].nextRow()

	#QtGui.QApplication.instance().exec_()

	wins = {}
	plt.rcParams['text.usetex'] = True
	plt.rcParams['font.size'] = 15
	display_all = False
	for cat, titles in categories.items():
		wins[cat] = plt.figure()
		fig = wins[cat]
		for sid in ['i', 'j', 'k'] if display_all else ['i']:
			for sid_other in ['i', 'j', 'k'] if display_all else ['j']:
				#fig = wins[cat].addPlot()
				#fig.setYRange(0, 1)
				#fig.addLegend()
				for i,title in enumerate(sorted(titles, reverse='observability' in cat)):
					label = title.split('_')[1]
					label = label[0] + '.' + label[1] if len(label)>1 else label
					if 'existence' in cat: label = '%s$' % label
					elif 'observability' in cat: label = '$%s$' % label
					elif 'misobscom' in cat: label = '$%s$' % label
					elif 'missedcom' in cat: label = '$%s$' % label
					elif 'incohobjs' in cat: label = '$%s$' % label
					plt.plot(dfs[title]['t'], dfs[title][f'{sid}-{sid_other}-t']+dfs[title][f'{sid}-{sid_other}-T'], lw=3, linestyle='dashed')
					plt.plot(dfs[title]['t'], dfs[title][f'{sid}-{sid_other}-t'], label=label, lw=2, color=plt.gca().lines[-1].get_color())
					plt.xlabel('Time (s)')
					plt.ylabel('$m^\mathcal{T}$')
		plt.legend()
		plt.tight_layout()

	plt.show()
