#! /usr/bin/env python3

import argparse
from pathlib import Path
from copy import deepcopy

import numpy as np
from shapely.geometry import Polygon, Point
from pyqtgraph import QtGui, QtCore, GraphicsLayoutWidget, setConfigOption, mkPen

import rospy
from multiception import filtering, association, existence
from multiception.msg import Class
from multiception.trust import TrustBuilder
from multiception.tracking import Tracker
from multiception.belief import MassFunction
from multiception.evidential_grid_map import EvidentialGridMap, plotdebug_gridmap
from multiception.object import StaticObject, ConstantVelObject, CoordinatedTurnObject

np.set_printoptions(precision=2, linewidth=300, suppress=True, sign=' ')

class SimulationConfig:
	def __init__(self):
		self.title = 'undefined'
		self.plot = False
		self.save = False
		self.debug = False
		self.debugplot = False
		self.duration = 2.0
		self.n_sources = 3
		self.n_beacons = 1
		self.mode = TrustBuilder.Mode.GLOBALCONF.value
		self.export_dir = '/tmp'

		self.standalone_trust = 1.0
		self.initial_trust_ego = 0.8
		self.initial_trust_oth = 0.0
		self.exist_sources = 1.0
		self.exist_beacons = 0.8
		self.nexist_sources = 0.0
		self.nexist_beacons = 0.0

		self.observability_range = 18
		self.observability_valfr = 0.9
		self.observability_valob = 0.9

		self.n_missedref = 0
		self.n_missedcom = 0
		self.n_misobsref = 0
		self.n_misobscom = 0
		self.n_incohobjs = 0

		self.wrong_shapes = False
		self.wrong_speeds = False

class Simulation:
	class Source:
		def __init__(self, name: str, trust_mode: TrustBuilder.Mode):
			self.object = CoordinatedTurnObject()
			self.object.covde[:, :] = 0
			self.object.covin[:, :] = 0
			np.fill_diagonal(self.object.covin, 0.5)
			self.object.classif = Class.OBST_CAR
			self.object.exist = MassFunction({'e': 1, 'eE': 0})
			self.object.oid = ord(name)
			self.object.source_id = name
			self.object.shape = np.array([3.,2.,3.])

			self.fov = np.zeros(0)
			self.observability = EvidentialGridMap(['o','O'])

			self.tracker = Tracker(None, None, {}, 0)
			self.tracker.filter_cls = filtering.SCI
			self.tracker.associator = association.AssociatorMahalanobis(10, 5)
			self.tracker.existence_estimator = existence.ExistenceEstimatorObservability(
				thresh_delete_time=-1,
				thresh_delete_uncertain=-1,
				max_decay_time=0.3,
				max_deterioration=0.1,
				value_birth=0.1,
				thresh_valid=0.0,
				thresh_invalid=0.1,
				thresh_delete_nonexistent=0.1,
				thresh_delete_unknown=0.99
			)
			self.tracker.freq = 0
			self.tracker.evo_noises = { CoordinatedTurnObject: np.diag([0.10, 0.10, 0.30, 1.00, 1.00]) }
			self.tracker.buffers_duration = 1.0
			self.tracker.merge_cost_max = 0
			self.tracker.heading_dir_unknown = False
			self.multracker = deepcopy(self.tracker)

			self.trust_builder = TrustBuilder()
			self.trust_builder.ref_track_buffer = self.multracker.tracks_buffer
			self.trust_builder.ref_observability_buffer = self.multracker.obscon_buffer
			self.trust_builder.mode                = trust_mode
			self.trust_builder.HLTIME_LOCAL        = 5
			self.trust_builder.HLTIME_REMOTE       = 2
			self.trust_builder.WEIGHT_COHERENCY    = 0.33
			self.trust_builder.WEIGHT_CONSISTENCY  = 0.33
			self.trust_builder.WEIGHT_CONFIRMATION = 0.33
			self.trust_builder.WEIGHT_SIMILARITY   = 0.9
			self.trust_builder.WEIGHT_MISSED_REF   = 0.4
			self.trust_builder.WEIGHT_MISSED_COM   = 0.6
			self.trust_builder.WEIGHT_NONDETECTABLE = 0.6
			self.trust_builder.COST_MISPLACED_OBJECTS = 0.25
			self.trust_builder.COST_NONDETECTABLE_OBJECTS = 0.6

	def __init__(self, config: SimulationConfig):
		self.config = config
		self.time = np.zeros(0)

		beacons = [
			#CoordinatedTurnObject(0, -10),
			#CoordinatedTurnObject(0, 0),
			#CoordinatedTurnObject(0, 10),
			#CoordinatedTurnObject(-5, 5),
			CoordinatedTurnObject(0, 5)
		]
		for i, beacon in enumerate(beacons):
			beacon.covde[:, :] = 0
			beacon.covin[:, :] = 0
			np.fill_diagonal(beacon.covin, 0.5)
			beacon.classif = Class.SIGN_UNKNOWN
			beacon.exist = MassFunction({'e': self.config.exist_beacons, 'E': self.config.nexist_beacons, 'eE': 1-self.config.exist_beacons-self.config.nexist_beacons})
			beacon.oid = i+3
			beacon.source_id = ''
			beacon.shape = np.array([3.,2.,3.])
		self.beacons = beacons[:1]
		self.fake_beacons = beacons[1:(1+self.config.n_misobscom)]

		self.sources = { name: Simulation.Source(name, TrustBuilder.Mode(self.config.mode)) for name in ['i', 'j', 'k'] }
		self.sources['i'].object.state[:3] = -3, -8, np.pi/2
		self.sources['j'].object.state[:3] = -6,  0, np.pi/8
		self.sources['k'].object.state[:3] =  6,  4, np.pi
		self.detections = {}

		circle = np.arange(-np.pi/6, np.pi/6+np.pi/18, np.pi/18)
		points = self.config.observability_range*np.array([np.cos(circle), np.sin(circle)]).T
		fov = np.vstack(( np.zeros(2), points, np.zeros(2) ))
		fov = np.hstack(( fov, np.ones((len(fov), 1)) ))
		objects = np.vstack([o.state[:2] for o in self.beacons + [s.object for s in self.sources.values()]])

		for sid, source in self.sources.items():
			source.object.exist = MassFunction({'e': self.config.exist_sources, 'E': self.config.nexist_sources, 'eE': 1-self.config.exist_sources-self.config.nexist_sources})
			transfo = np.array([[np.cos(source.object.O),-np.sin(source.object.O),source.object.x],[np.sin(source.object.O),np.cos(source.object.O),source.object.y],[0,0,1]])
			source.fov = Polygon((transfo @ fov.T)[:2].T)
			source.observability.setGeometry([self.config.observability_range*2,self.config.observability_range*2], 1, source.object.state[:2])
			source.observability[''] = 0
			source.observability['o'] = 0
			source.observability['O'] = 0
			source.observability['o,O'] = 1
			self.detections[sid] = [detection for detection in (self.beacons + [s.object for s in self.sources.values()]) if source.fov.contains(Point(detection.state[:2])) or source.object.oid==detection.oid]
			for y in range(self.config.observability_range*2):
				for x in range(self.config.observability_range*2):
					pos = np.array([source.object.x+self.config.observability_range-x-0.5,source.object.y+self.config.observability_range-y-0.5])
					if source.fov.contains(Point(pos)) or np.linalg.norm(pos-source.object.state[:2])<1:
						if np.any(np.linalg.norm(pos-objects, axis=1)<1):
							source.observability['o'][x,y] = self.config.observability_valob
							source.observability['o,O'][x,y] = 1-self.config.observability_valob
						else:
							source.observability['O'][x,y] = self.config.observability_valfr
							source.observability['o,O'][x,y] = 1-self.config.observability_valfr

		if self.config.debug:
			rospy.init_node('simu_FUSION', anonymous=True, log_level=rospy.DEBUG)

		if self.config.debugplot:
			setConfigOption('background', 'w')
			setConfigOption('foreground', 'k')
			self.debugwin = GraphicsLayoutWidget(title=f'{self.config.title}-debug')
			self.debugfigs = { sid: {} for sid in self.sources }
			self.debugfigs['ref'] = {}
			for sid in self.sources:
				self.debugfigs['ref'][sid] = self.debugwin.addPlot()

			for sid in self.sources:
				self.debugwin.nextRow()
				for sido in self.sources:
					self.debugfigs[sido][sid] = self.debugwin.addPlot()

			for figs in self.debugfigs.values():
				for fig in figs.values():
					fig.setAspectLocked(lock=True, ratio=1)

	def step(self, t: float):
		tracks_at_t = {}
		obs_at_t = {}
		multracks_at_t = {}
		mulobs_at_t = {}
		for s in self.sources.values(): s.object.t = t
		for b in self.beacons: b.t = t

		for sid, source in self.sources.items():
			rospy.logdebug('------ Local perception of %s ------', sid)
			source.tracker.add_obs(sid, t, deepcopy(self.detections[sid]))
			source.tracker.add_source(sid, t, source)
			tracks_at_t[sid], obs_at_t[sid] = source.tracker.step(t)

		### ---- communication -----
		# j optionnaly lies to others at this point
		tracks_at_t['jtrue'] = tracks_at_t['j']
		tracks_at_t['j'] = deepcopy(np.array([t for i,t in enumerate(tracks_at_t['j']) if i>=self.config.n_missedcom] + self.fake_beacons[:self.config.n_misobscom]))
		for i in range(min(len(tracks_at_t['j']), self.config.n_incohobjs)):
			tracks_at_t['j'][i].shape *= 1.7

		for sid, source in self.sources.items():
			rospy.logdebug('------ Trust estimation of %s ------', sid)
			for sid_other in self.sources.keys():
				source.trust_builder.callback(sid_other, t, tracks_at_t['jtrue'] if sid=='j' and sid_other=='j' else tracks_at_t[sid_other], obs_at_t[sid_other])
				source.multracker.set_trust(sid_other, t, source.trust_builder.get_latest_trusts()[sid_other][1])

		# Cooperative perception
		for sid, source in self.sources.items():
			rospy.logdebug('------ Cooperative perceptions of %s ------', sid)
			for sid_other, source_other in self.sources.items():
				source.multracker.add_obs(sid_other, t, tracks_at_t['jtrue'] if sid=='j' and sid_other=='j' else tracks_at_t[sid_other])
				source.multracker.add_source(sid_other, t, source_other)
			multracks_at_t[sid], mulobs_at_t[sid] = source.multracker.step(t)
			del source.multracker.tracks_buffer[t][:self.config.n_missedref]

		if self.config.debugplot:
			for figs in self.debugfigs.values():
				for fig in figs.values():
					fig.clear()
			for sid, source in self.sources.items():
				self.debugfigs['ref'][sid].addItem(plotdebug_gridmap(obs_at_t[sid], opacity=1))
				self.debugfigs['ref'][sid].plot(np.array([o.state[:2] for o in tracks_at_t[sid]]), symbol='x', pen=None, pxMode=True)
				for sido, ref in source.trust_builder.references.items():
					for ref_objs, ref_dete in ref:
						self.debugfigs[sid][sido].addItem(plotdebug_gridmap(ref_dete, opacity=1))
						self.debugfigs[sid][sido].plot(np.array([o.state[:2] for o in ref_objs]), symbol='x', pen=None, pxMode=True)
			self.debugwin.show()
			QtGui.QApplication.instance().exec_()

	def run(self):
		self.time = np.arange(0.1, self.config.duration, 0.1)
		t0 = self.time[0]

		for sid, source in self.sources.items():
			source.tracker.t = source.multracker.t = t0
			source.tracker.set_trust(sid, t0, MassFunction({'t': self.config.standalone_trust, 'tT': 1-self.config.standalone_trust}))
			source.multracker.set_trust(sid, t0, MassFunction({'t': self.config.initial_trust_ego, 'tT': 1-self.config.initial_trust_ego}))
			source.trust_builder.initial_trusts[sid] = MassFunction({'t': self.config.initial_trust_ego, 'tT': 1-self.config.initial_trust_ego})
			for sid_other in set(self.sources.keys())-set([sid]):
				source.multracker.set_trust(sid_other, t0, MassFunction({'t': self.config.initial_trust_oth, 'tT': 1-self.config.initial_trust_oth}))
				source.trust_builder.initial_trusts[sid_other] = MassFunction({'t': self.config.initial_trust_oth, 'tT': 1-self.config.initial_trust_oth})

		for t in self.time:
			self.step(t)

	def display(self):
		#pen_sources = {
			#'i': (mkPen(color=(255,   0,   0), width=2), mkPen(color=(  0, 255,   0), width=2)),
			#'j': (mkPen(color=(255, 100, 100), width=2, style=QtCore.Qt.DashLine), mkPen(color=(100, 255, 100), width=2, style=QtCore.Qt.DashLine)),
			#'k': (mkPen(color=(255, 200, 200), width=2, style=QtCore.Qt.DotLine), mkPen(color=(200, 255, 200), width=2, style=QtCore.Qt.DotLine))
		#}
		setConfigOption('background', 'w')
		setConfigOption('foreground', 'k')
		win = GraphicsLayoutWidget(title=self.config.title)
		for source in self.sources.values():
			for sid_other, source_other in self.sources.items():
				trusts = [source.trust_builder.history[sid_other][t_other][0] for t_other in sorted(source.trust_builder.history[sid_other].keys())]
				fig = win.addPlot()
				fig.setYRange(0, 1)
				fig.plot(self.time, [t['t']+t['T'] for t in trusts], pen=mkPen(color=(255, 0, 0), width=2))
				fig.plot(self.time, [t['t'] for t in trusts], pen=mkPen(color=(0, 255, 0), width=2))
			win.nextRow()

		win.show()
		QtGui.QApplication.instance().exec_()

	def export(self, path: Path):
		trusts = {}
		trusts['t'] = self.time
		for sid, source in self.sources.items():
			for sid_other, source_other in self.sources.items():
				trust = [source.trust_builder.history[sid_other][t_other][0] for t_other in sorted(source.trust_builder.history[sid_other].keys())]
				trusts[f'{sid}-{sid_other}-t'] = np.array([t['t'] for t in trust])
				trusts[f'{sid}-{sid_other}-T'] = np.array([t['T'] for t in trust])

		from pandas import DataFrame
		trusts_df = DataFrame.from_dict(trusts).set_index('t')
		trusts_df.to_csv(path, header=True)

if __name__=='__main__':
	config = SimulationConfig()

	parser = argparse.ArgumentParser()
	for k,v in config.__dict__.items():
		parser.add_argument(f'--{k}', type=type(v), nargs='?')
	args = parser.parse_args()
	for k,v in args.__dict__.items():
		if v is not None:
			config.__dict__[k] = v

	sim = Simulation(config)
	
	#if sim.config.debugplot:
		#from pyqtgraph import QtGui, QtCore, GraphicsLayoutWidget, setConfigOption, mkPen
		#setConfigOption('background', 'w')
		#setConfigOption('foreground', 'k')
		#win = GraphicsLayoutWidget(title=sim.config.title, show=True)
		#for v in sim.sources.values():
			#fig = win.addPlot()
			#fig.setAspectLocked(lock=True, ratio=1)
			#fig.plot(np.array([v.object.state[:2]]), symbol='x', pen=None, pxMode=True)
			#fig.plot(np.array([detection.state[:2] for detection in sim.beacons + [s.object for s in sim.sources.values()] if v.fov.contains(Point(detection.state[:2])) or v.object.oid==detection.oid]), symbol='o', pen=None, pxMode=True)
			#fig.plot(*v.fov.exterior.xy)
			#fig.addItem(plotdebug_gridmap(v.observability, opacity=.4))
		#QtGui.QApplication.instance().exec_()
		
	sim.run()
	if sim.config.save: sim.export((Path(sim.config.export_dir).expanduser().resolve()/sim.config.title).with_suffix('.csv'))
	if sim.config.plot: sim.display()
	'''
	To be varied:
	- Detection existence (the lower, the slower trust is to converge)
	- Observability of ego
	- Observability of the other
	- 
	'''
