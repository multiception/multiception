#! /bin/bash

source devel/setup.bash
export NUMBA_DISABLE_JIT=1
EXPORT_DIR="/tmp/$(date +%s)"
mkdir $EXPORT_DIR

# rosrun multiception run_sim.py --title='existence_01' --exist_sources=0.1 --exist_beacons=0.1 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/existence_01.log &
# rosrun multiception run_sim.py --title='existence_02' --exist_sources=0.2 --exist_beacons=0.2 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/existence_02.log &
# rosrun multiception run_sim.py --title='existence_04' --exist_sources=0.4 --exist_beacons=0.4 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/existence_04.log &
# rosrun multiception run_sim.py --title='existence_09' --exist_sources=0.9 --exist_beacons=0.9 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/existence_09.log &
# 
rosrun multiception run_sim.py --title='observability_01' --observability_valob=0.1 --observability_valfr=0.1 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/observability_01.log &
rosrun multiception run_sim.py --title='observability_02' --observability_valob=0.2 --observability_valfr=0.2 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/observability_02.log &
rosrun multiception run_sim.py --title='observability_04' --observability_valob=0.4 --observability_valfr=0.4 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/observability_04.log &
rosrun multiception run_sim.py --title='observability_09' --observability_valob=0.9 --observability_valfr=0.9 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/observability_09.log &

# rosrun multiception run_sim.py --title='missedref_0' --n_missedref=0 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedref_0.log &
# rosrun multiception run_sim.py --title='missedref_1' --n_missedref=1 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedref_1.log &
# rosrun multiception run_sim.py --title='missedref_2' --n_missedref=2 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedref_2.log &
# rosrun multiception run_sim.py --title='missedref_3' --n_missedref=3 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedref_3.log &
# rosrun multiception run_sim.py --title='missedref_4' --n_missedref=4 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedref_4.log &
# rosrun multiception run_sim.py --title='missedref_5' --n_missedref=5 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedref_5.log &

# rosrun multiception run_sim.py --title='missedcom_0' --n_missedcom=0 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedcom_0.log &
# rosrun multiception run_sim.py --title='missedcom_1' --n_missedcom=1 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedcom_1.log &
# rosrun multiception run_sim.py --title='missedcom_2' --n_missedcom=2 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedcom_2.log &
# rosrun multiception run_sim.py --title='missedcom_3' --n_missedcom=3 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/missedcom_3.log &

rosrun multiception run_sim.py --title='incohobjs_0' --n_incohobjs=0 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/incohobjs_0.log &
rosrun multiception run_sim.py --title='incohobjs_1' --n_incohobjs=1 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/incohobjs_1.log &
rosrun multiception run_sim.py --title='incohobjs_2' --n_incohobjs=2 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/incohobjs_2.log &
rosrun multiception run_sim.py --title='incohobjs_3' --n_incohobjs=3 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/incohobjs_3.log &

rosrun multiception run_sim.py --title='misobscom_0' --n_misobscom=0 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/misobscom_0.log &
rosrun multiception run_sim.py --title='misobscom_1' --n_misobscom=1 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/misobscom_1.log &
rosrun multiception run_sim.py --title='misobscom_2' --n_misobscom=2 --save=True --debug=True --export_dir=$EXPORT_DIR > $EXPORT_DIR/misobscom_2.log &

wait

rosrun multiception plot_sim.py $EXPORT_DIR
