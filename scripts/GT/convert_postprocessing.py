#! /usr/bin/env python3

import cv2
import numpy as np
from yaml import dump
from enum import IntEnum
from pathlib import Path
from struct import unpack_from
from argparse import ArgumentParser
from concurrent.futures import ProcessPoolExecutor

BYTE_SIZE = 1
SHORT_SIZE = 2
DOUBLE_SIZE = 8

# Boilerplate for class property decorator
# https://stackoverflow.com/questions/1697501/staticmethod-with-property
class classproperty(property):
	def __get__(self, cls, owner):
		return classmethod(self.fget).__get__(None, owner)()

class BF(IntEnum):
	FREE = 1
	INFRA = 2
	STATIC = 4
	DYNAMIC = 8

	@classproperty
	def NONE(cls): return 0
	@classproperty
	def MOVABLE(cls): return cls.STATIC | cls.DYNAMIC
	@classproperty
	def IMMOVABLE(cls): return cls.INFRA | cls.STATIC
	@classproperty
	def OCCUPANCY(cls): return cls.INFRA | cls.STATIC | cls.DYNAMIC
	@classproperty
	def PASSABLE(cls): return cls.FREE | cls.DYNAMIC
	@classproperty
	def UNKNOWN(cls): return cls.FREE | cls.INFRA | cls.STATIC | cls.DYNAMIC
	@classproperty
	def ALL(cls): return cls.UNKNOWN
	@classproperty
	def SIZE(cls): return 2**4

def parse_args():
	parser = ArgumentParser(description="Post-processed ground truth evaluation")
	parser.add_argument("-o", "--output", help="Output directory (filenames will be preserved)", default=".")
	parser.add_argument("-r", "--resolution", type=float, help="Force another resolution.", default=None)
	parser.add_argument("-b", "--observation", help="Specify that this is an observation grid", action="store_true")
	parser.add_argument("-c", "--occupancy", help="Specify that this is an occupancy grid", action="store_true")
	parser.add_argument("PATHS", nargs="+", type=str, help="Path to post-processed .bin files")
	args = parser.parse_args()
	if not args.observation and not args.occupancy:
		raise RuntimeError("Either --observation or --occupancy must be specified!")
	return args

def read_bf(gridbin, offset, bf_default_size):
	bf = np.zeros(bf_default_size)
	bf_size, = unpack_from("<B", gridbin, offset)
	offset += BYTE_SIZE

	if bf_size==0:
		bf[-1] = 1
	else:
		events_masses = unpack_from("<"+"Bd"*bf_size, gridbin, offset)
		bf[list(events_masses[::2])] = events_masses[1::2]

	return bf, offset+bf_size*(BYTE_SIZE+DOUBLE_SIZE)

def read_obs_cell(gridbin, offset):
	bf, offset = read_bf(gridbin, offset, BF.SIZE)
	return bf, offset

def read_occ_cell(gridbin, offset):
	bf, offset = read_bf(gridbin, offset, BF.SIZE)
	#velocity_x, velocity_y = unpack_from("<dd", gridbin, offset)
	#cellid, = unpack_from("<H", gridbin, offset + 2*DOUBLE_SIZE)
	#return (bf, velocity_x, velocity_y, cellid), offset + 2*DOUBLE_SIZE + SHORT_SIZE

	return bf, offset + 2*DOUBLE_SIZE + SHORT_SIZE

def read_grid(path):
	with open(path, "rb") as f:
		gridbin = f.read()

	origin_x, origin_y, length_x, length_y, resolution = unpack_from("<ddddd", gridbin)
	offset = 5*DOUBLE_SIZE
	size_x = int(length_x/resolution)+1
	size_y = int(length_y/resolution)+1
	print(">>>", path.stem, size_x, size_y, len(gridbin))

	read_cell = read_occ_cell if args.occupancy else read_obs_cell
	grid = [None]*size_y*size_x

	for j in range(size_x):
		for i in range(size_y):
			grid[j*size_y+i], offset = read_cell(gridbin, offset)

	return [origin_x, origin_y], [length_x, length_y], resolution, np.array(grid).reshape((size_x, size_y, BF.SIZE))

def write_grid(basepath, stem, grid, origin, resolution):
	# Change time base from s^-1 to s
	stamp = float(stem)/10
	path = basepath / str(stamp)

	# Change origin from center to topleft
	origin = [
		origin[0]-(grid.shape[0]*resolution)/2,
		origin[1]+(grid.shape[1]*resolution)/2,
	]

	# "Binarize" occupancy and free
	THRESH = 0.5
	grid_greyscale = np.ones(grid.shape[:2])*127
	grid_greyscale[(grid[:,:, BF.STATIC]>THRESH)|(grid[:,:, BF.DYNAMIC]>THRESH)] = 255
	grid_greyscale[(grid[:,:, BF.INFRA]>THRESH)|(grid[:,:, BF.FREE]>THRESH)] = 0

	if args.resolution is not None:
		neww = int(np.ceil((grid.shape[0]*resolution)/args.resolution))
		newh = int(np.ceil((grid.shape[1]*resolution)/args.resolution))
		grid_greyscale = cv2.resize(grid_greyscale, (neww, newh), cv2.INTER_NEAREST)
		resolution = args.resolution

	# Save for loading in multiception/script/existence_evaluation.py
	# Copied from multiception/script/GT/export_gt.py
	cv2.imwrite(f"{path}.png", np.rot90(grid_greyscale[:,::-1]))
	with open(f"{path}.yml", 'w') as f:
		dump({'lefttop_corner': origin, 'cell_size': round(resolution, 2)}, f)
	print("<<<", path.stem)

def convert_grid(path):
	path = Path(path).expanduser().resolve(strict=True)
	origin, length, resolution, grid = read_grid(path)
	write_grid(OUTDIR, path.stem, grid, origin, resolution)
	return True

if __name__=="__main__":
	args = parse_args()
	OUTDIR = Path(args.output).expanduser().resolve()
	OUTDIR.mkdir(parents=True, exist_ok=True)

	with ProcessPoolExecutor() as p:
		rets = list(p.map(convert_grid, args.PATHS))
	#rets = list(map(convert_grid, args.PATHS))
