#! /usr/bin/env bash

rosrun multiception convert_itsc_messages.py -o /tmp/ITSC.bag ~/itsc21-data/2021-04-13-22-56-54.bag
rosrun multiception gt_from_bag.py --output /tmp/ITSC.pkl /tmp/ITSC.bag /zoe{blue,grey,white}/statep
rosrun multiception export_gt.py --map=~/ws/multiception_ws/src/multiception/config/location/Multiception/Roundabout.yaml /tmp/ITSC.pkl ~/GT/ITSC --margin=10
rosrun multiception existence_evaluation.py ~/GT/ITSC /tmp/ITSC.bag /zoeblue/egotracker/tracks --pose-topic /zoeblue/statep -f
