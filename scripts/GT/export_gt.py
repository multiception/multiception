#!/usr/bin/env python3

import sys, cv2, csv, yaml, pickle, itertools
import numpy as np
from yaml import safe_load
from pathlib import Path
from typing import Tuple
from argparse import ArgumentParser

from roslaunch.substitution_args import resolve_args
from map_lib import LoaderSqlite, MapTile, polygonFromLinks
'''
Usage exemple:
python scripts/GT/export_gt.py --map=config/location/Multiception/Overtaking3.yaml --static-angles={{10..13}:-0.8,9:-2.4} ~/GT/Overtaking3.pkl ~/GT/Overtaking3
'''
np.set_printoptions(precision=10, linewidth=999, suppress=True, sign=' ')

EXCLUDED_RS_TYPES = [4, 7]

def export_csv_from_track(trackid: int, track: np.ndarray, path_out: Path) -> Path:
    path = path_out/f'{trackid}.csv'
    with open(path, 'w') as f:
        w = csv.writer(f, delimiter=',')
        w.writerow(['%t','x','y','O','v','l','w'])
        for row in track:
            w.writerow(row.tolist())
    return path

def transfo_to_matrix(x: float, y: float, O: float):
    c, s = np.cos(O), np.sin(O)
    return np.array([
        [c, -s, x],
        [s,  c, y],
        [0,  0, 1]
    ])

def make_object_poly(tracks: np.ndarray):
    return np.stack([
        transfo_to_matrix(track[1], track[2], track[3]) @
        [[ track[5]/2, -track[5]/2, -track[5]/2,  track[5]/2],
         [ track[6]/2,  track[6]/2, -track[6]/2, -track[6]/2],
         [          1,           1,           1,           1]]
    for track in tracks])

def img_from_polygons(polygons: np.ndarray, margin: float = 10.0, cell_size: float = 10.0, bg: int = 0, fgs: int = 255) -> Tuple[np.ndarray, np.ndarray]:
    mins = np.nanmin(polygons, axis=(0,2)) - margin
    maxs = np.nanmax(polygons, axis=(0,2)) + margin
    orig = np.floor(np.array([mins[0], maxs[1], 1])/cell_size) * cell_size # Align origins with cells
    end = np.floor(np.array([maxs[0], mins[1], 1])/cell_size) * cell_size

    rotat = np.array([[np.cos(-np.pi/2), np.sin(-np.pi/2), 0], [-np.sin(-np.pi/2), np.cos(-np.pi/2), 0], [0,0,1]])
    translat = np.array([[1, 0, orig[0]], [0, 1, orig[1]], [0, 0, 1]])
    scale = np.diag([-cell_size, -cell_size, 1])
    transfo = np.linalg.inv(translat @ scale @ rotat)

    polygons_px = (transfo @ polygons.T).T
    end_px = (transfo @ end).astype(int)

    img = np.full((end_px[1], end_px[0]), bg, dtype=np.uint8)
    polygons_px = np.transpose(polygons_px, axes=(0,2,1))[:,:,:2]
    if isinstance(fgs, int): fgs = itertools.repeat(fgs)
    for poly_, fg in zip(polygons_px, fgs):
        poly = poly_[np.all(~np.isnan(poly_), axis=1)].astype(int)
        cv2.fillPoly(img, [poly], fg)

    return img, orig[:2]

def export_png_from_polygons(t: float, polygons: np.ndarray, path_out: Path, **kwargs) -> Tuple[Path, Path]:
    path_img = f"{path_out}/{t:.2f}.png"
    path_infos = f"{path_out}/{t:.2f}.yml"

    img, orig = img_from_polygons(polygons, **kwargs)
    cv2.imwrite(path_img, np.rot90(img[:,::-1]))

    with open(path_infos, 'w') as f:
        yaml.dump({'lefttop_corner': orig.tolist(), 'cell_size': kwargs.get("cell_size", 1.0)}, f)

    return path_img, path_infos

def parse_args():
    parser = ArgumentParser(description='Builds a evaluation CSVs and PNGs from a PKL containing manually labeled GT')
    parser.add_argument('PATH_PKL', type=str, help='Path to the .yml file containing the map configuration')
    parser.add_argument('OUTPUT_DIR', type=str, help='Path the output ground-truth folder')
    parser.add_argument('--map', type=str, help='Optional path to the .yml file containing the map configuration', default=None)
    parser.add_argument('--map-id0', type=int, help='Offset added to object ids', default=10000)
    parser.add_argument('--dt', type=float, help='Time step used to interpolate tracks in PNGs (s)', default=0.1)
    parser.add_argument('--margin', type=float, help='Empty space appendend around PNGs (m)', default=10.0)
    parser.add_argument('--cell-size', type=float, help='Cell size of the PNG (m)', default=1.0)
    parser.add_argument('--static-angles', type=str, help='Angles of static objects in the form id:anglerad', default=[], action='append')
    parser.add_argument('--static-always', help='Wether static objects are always present or only when visible', action="store_true")
    parser.add_argument('-s', '--skip-signs', help='Generate GT without signs from the map', action="store_true")
    return parser.parse_args()

def load_map(map_config_file: Path) -> MapTile:
    map_config_file = Path(map_config_file).expanduser().resolve()

    # Get map config (reference, radius, etc)
    with open(map_config_file , 'r') as f:
        config = yaml.safe_load(f)

    maptile = MapTile()
    loader = LoaderSqlite(resolve_args(config["map_file_name"]), config['srid'])
    loader.setArea(config["rad_lon"], config["rad_lat"], config['radius'], config['srid'])
    loader.loadMap(maptile, "links=*;shapepoints=*;nodes=*;roadsignals=*")
    maptile.connect()

    return maptile

def traces_to_tracks(traces, shapes, static_angles, duplicate_static):
    tracks = {}
    if duplicate_static: # Precompute all times
        all_ts = list(sorted(set(t for trace in traces.values() for t, _ in trace)))
        print(all_ts)

    for track_id, trace in traces.items():
        trace = np.array(list(reversed(trace)))
        centers = np.array([shapes[t][i] for t, i in trace]).mean(axis=1)

        if track_id in static_angles:
            meanx, meany = centers.mean(axis=0)
            ts = all_ts if duplicate_static else [t for t, _ in trace]
            tracks[track_id] = np.array([(t, meanx, meany, static_angles[track_id], 0, 4.0, 2.0) for t in ts])
            #print(tracks[track_id][:,1:])
        else:
            dts = np.diff(trace[:,0])
            velocities = np.diff(centers, axis=0) / dts[:,None]
            velocities = np.vstack((velocities[:1], velocities)) # Duplicate first velocity so len(centers)==len(velocities)
            vs = np.linalg.norm(velocities, axis=1)
            thetas = np.arctan2(velocities[:,1], velocities[:,0])
            tracks[track_id] = np.array([(t, x, y, O, v, 4.0, 2.0) for (t,_), (x,y), O, v in zip(trace, centers, thetas, vs)])
    return tracks

def interpolate(track, t):
    if len(track)==0: return None

    # If t is already in the track, return that element
    dts = np.abs(t-track[:,0])
    closest_dt = dts.argmin()
    if abs(t-track[closest_dt, 0]) < 0.001: return track[closest_dt]

    # Otherwise, interpolate
    upper = np.searchsorted(track[:,0], t, side="right")
    lower = upper - 1
    if lower<0 or upper>=len(track):
        return None

    ratio = (track[upper,0]-t) / (track[upper,0]-track[lower,0])
    return ratio*track[lower] + (1-ratio)*track[upper]

def follow(links, t, i):
    if t in links and i in links[t]:
        return [(t, i)] + follow(links, *links[t][i])
    else:
        return [(t,i)]

def stack_padded(*args, defval=np.nan):
    maxvert = max((poly.shape[1] for polys in args for poly in polys), default=0)
    npolys = sum((len(polys) for polys in args))
    stacked = np.full((npolys, 3, maxvert), defval)
    for i, poly in enumerate(itertools.chain(*args)):
        stacked[i, :poly.shape[0], :poly.shape[1]] = poly
    stacked[:,2,:] = 1
    return stacked

def main():
    sys.setrecursionlimit(int(10e6))

    args = parse_args()
    path_pkl = Path(args.PATH_PKL).expanduser().resolve(strict=True)
    path_out = Path(args.OUTPUT_DIR).expanduser().resolve()
    path_out.mkdir(exist_ok=True, parents=True)

    # Process map signs and road if yaml specified
    if args.map is not None:
        path_map = Path(args.map).expanduser().resolve(strict=True)
        maptile = load_map(path_map)
        polys_road = list(polygonFromLinks(maptile, 1, 10).values())

        if args.skip_signs:
            signs = np.zeros((0, 7))
            polys_sign = np.zeros((0, 3, 4))
        else:
            signs = np.vstack([[0.0, rs.geometry[0], rs.geometry[1], 0, 0, 0.5, 0.5] for _, rs in maptile.roadSignals.items() if rs.type not in EXCLUDED_RS_TYPES])
            polys_sign = make_object_poly(signs)
            for i in range(len(signs)):
                export_csv_from_track(args.map_id0+i, signs[i:(i+1)], path_out)

        map_polys_padded = stack_padded(polys_road, polys_sign)
        fgs_map = [0]*len(polys_road) + [255]*len(polys_sign)
    else:
        map_polys_padded = np.zeros((0,3,0))
        fgs_map = []

    # Process manual gt
    with open(path_pkl, "rb") as f:
        shapes, links = pickle.load(f)

    # Sort by time
    shapes = dict(sorted(shapes.items(), key=lambda x: x[0]))
    links = dict(sorted(links.items(), key=lambda x: x[0]))

    processed_shapes = set()
    traces = dict() # {id: {t: center}}
    for t, shapest in reversed(shapes.items()):
        for i in range(len(shapest)):
            if (t,i) not in processed_shapes:
                traces[len(traces)] = track = follow(links, t, i)
                #print(t, i, track)
                processed_shapes.update(set(track))

    static_angles =  { int((st:=angle.split(":"))[0]): float(st[1]) for angle in args.static_angles }
    print(static_angles)

    tracks = traces_to_tracks(traces, shapes, static_angles, args.static_always)
    for track_id, track in tracks.items():
        export_csv_from_track(track_id, track, path_out)

    times = { t for track in tracks.values() for t in track[:,0] }
    for t in np.arange(min(times), max(times)+args.dt, args.dt):
        interp_tracks = np.vstack([interp_track for track in tracks.values() if (interp_track:=interpolate(track, t)) is not None])
        track_polys = make_object_poly(interp_tracks)
        polys_padded = stack_padded(map_polys_padded, track_polys)
        fgs = fgs_map + [255]*len(track_polys)
        export_png_from_polygons(t, polys_padded, path_out, cell_size=args.cell_size, margin=args.margin, bg=127, fgs=fgs)

if __name__=="__main__":
    main()
