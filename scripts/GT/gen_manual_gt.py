#!/usr/bin/env python3

import sys, pickle
from argparse import ArgumentParser
from pathlib import Path
from rosbag import Bag
from tqdm import tqdm
import numpy as np
from sensor_msgs.point_cloud2 import read_points

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

PICKLE_PATH_LIDAR = Path("/tmp/gtlidar.pkl")

def parse_args():
    parser = ArgumentParser()
    parser.add_argument("-o", "--output", type=str, help="Path to the output pkl.")
    parser.add_argument("ROSBAG", type=str, help="Path to rosbag containing topics to read.")
    parser.add_argument("TOPIC", nargs="+", type=str, help="Topic containing a point cloud to read.")
    return parser.parse_args()

def load_pointclouds(bagpath, topics):
    pointclouds = { topic: {} for topic in topics } # {topic:{t:[points]}}
    with Bag(bagpath) as bag:
        for topic, msg, t in tqdm(bag.read_messages(topics), total=bag.get_message_count()):
            pointclouds[topic][msg.header.stamp.to_sec()] = np.array(list(read_points(msg)))[:,:3]
    return pointclouds

class GtWindow(QtGui.QWidget):
    def __init__(self, pointclouds):
        super().__init__()
        self.setWindowTitle("Ground Truth Generator")
        pg.setConfigOptions(background="w", foreground="k")
        self.layout = QtGui.QVBoxLayout()
        self.fig = pg.PlotWidget()
        self.fig.setAspectLocked(lock=True, ratio=1)
        self.layout.addWidget(self.fig)

        # TODO use ids for shapes and links if deletetion is implemented
        self.pointclouds = pointclouds
        self.lidar_plot = pg.ScatterPlotItem(pen=None, brush=None, size=0.3, pxMode=False)
        self.fig.addItem(self.lidar_plot)

        self.temp_shape = []
        self.temp_shape_plot = pg.ScatterPlotItem(pen=pg.mkPen("k"), brush=pg.mkBrush("b"), size=5)
        self.fig.addItem(self.temp_shape_plot)
        self.shapes = {}
        self.shapes_plot = pg.ScatterPlotItem(pen=pg.mkPen("k"), brush=pg.mkBrush("r"), size=5)
        self.fig.addItem(self.shapes_plot)
        self.link_from = self.link_to = None
        self.link_fromto_plot = pg.PlotDataItem(pen=pg.mkPen("b", width=2), brush=None)
        self.fig.addItem(self.link_fromto_plot)
        self.links = {}
        self.links_plot = pg.PlotDataItem(pen=pg.mkPen("r", width=2), brush=None)
        self.fig.addItem(self.links_plot)
        self.time_old_shapes = {}
        self.old_shapes_plot = pg.ScatterPlotItem(pen=pg.mkPen(0, 0, 0, 100), brush=pg.mkBrush(0, 255, 0, 100), size=5)
        self.fig.addItem(self.old_shapes_plot)

        tmin = int(min([min(source_pcs.keys()) for source_pcs in pointclouds.values()]))
        tmax = int(max([max(source_pcs.keys()) for source_pcs in pointclouds.values()]))
        self.sliderLayout = QtGui.QHBoxLayout()
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.slider.setMinimum(tmin)
        self.slider.setMaximum(tmax)
        self.slider.setSingleStep(2)
        self.slider.setTickPosition(QtGui.QSlider.TicksBelow)
        self.slider.sliderReleased.connect(self.update_time)
        self.sliderLabel = QtGui.QLabel()
        self.slider.sliderMoved.connect(lambda: self.sliderLabel.setText(f"{self.slider.value()}{'*' if self.slider.value() in self.shapes else ''}"))
        self.sliderLayout.addWidget(self.slider)
        self.sliderLayout.addWidget(self.sliderLabel)
        self.layout.addLayout(self.sliderLayout)
        self.slider.setValue(tmin)
        self.setLayout(self.layout)

    def update_time(self, subsample=10):
        t = self.slider.value()
        # Reset temps
        self.link_from = None
        self.temp_shape = []
        self.time_old_shapes = None
        self.old_shapes_plot.setData(pos=[])
        self.temp_shape_plot.setData(pos=self.temp_shape)

        # Plot lidar and shape at time t
        closest_ts = { source: min(source_pcs.keys(), key=lambda x: abs(t-x)) for source, source_pcs in self.pointclouds.items() }
        closest_pcs = [self.pointclouds[source][closest_t] for source, closest_t in closest_ts.items()]
        pc = np.vstack(closest_pcs)[:,:2]
        brushes = sum([[pg.mkBrush(i, len(self.pointclouds))]*len(closest_pcs[i]) for i in range(len(self.pointclouds))], start=[])
        self.lidar_plot.setData(pos=pc[::subsample], brush=brushes[::subsample])
        self.shapes_plot.setData(pos=[point for shape in self.shapes.get(t, []) for point in shape])
        self.plot_links(t)

    def keyPressEvent(self, event):
        t = self.slider.value()
        mouse = self.getMousePosition()
        if event.key()==QtCore.Qt.Key_Space and mouse is not None and len(self.temp_shape)<4:
            self.temp_shape.append(mouse)
            self.temp_shape_plot.setData(pos=self.temp_shape)
        elif event.key()==QtCore.Qt.Key_Return and len(self.temp_shape)>3:
            if t not in self.shapes: self.shapes[t] = []
            self.shapes[t].append(self.temp_shape)
            self.shapes_plot.setData(pos=[point for shape in self.shapes[t] for point in shape])
            self.temp_shape = []
            self.temp_shape_plot.setData(pos=[])
            self.setWindowTitle("Ground Truth Generator*")
        elif event.key()==ord("L") and mouse is not None and len(self.shapes.get(t, []))>0:
            mouse = np.array(mouse)
            if self.link_from is None:
                time_old_shapes = min(self.shapes.keys(), key=lambda x: abs(x-t)+1000*int(t<=x)) # exclude current time
                print([abs(x-t)+1000*int(t<=x) for x in self.shapes.keys()], time_old_shapes)
                if time_old_shapes<t:
                    self.time_old_shapes = time_old_shapes
                    centers = np.array(self.shapes[t]).mean(axis=1)
                    self.link_from = np.argmin(np.linalg.norm(centers-mouse, axis=1))
                    self.old_shapes_plot.setData(pos=[point for shape in self.shapes.get(self.time_old_shapes, []) for point in shape])
            else:
                centers = np.array(self.shapes[self.time_old_shapes]).mean(axis=1)
                closest = np.argmin(np.linalg.norm(centers-mouse, axis=1))
                if t not in self.links: self.links[t] = {}
                self.links[t][self.link_from] = (self.time_old_shapes, closest)
                self.time_old_shapes = self.link_from = None
                self.old_shapes_plot.setData(pos=[])
                self.plot_links(t)
                self.setWindowTitle("Ground Truth Generator*")
        elif event.key()==ord("S"):
            with open(PICKLE_PATH_GT, "wb") as f:
                pickle.dump((self.shapes, self.links), f)
            self.setWindowTitle("Ground Truth Generator")
        elif event.key()==ord("E"):
            pass

    def plot_links(self, t):
        vertices = []
        centers = { tt: np.array(shapestt).mean(axis=1) for tt, shapestt in self.shapes.items() }
        for i1, (t2, i2) in self.links.get(t, {}).items():
            vertices.append(centers[t][i1])
            vertices.append(centers[t2][i2])
        self.link_fromto_plot.setData(np.array(vertices), connect=np.array([True,False]*(len(vertices)//2)))

    def getMousePosition(self):
        posloc = QtGui.QCursor.pos()
        pos = self.mapFromGlobal(posloc)
        if self.fig.sceneBoundingRect().contains(pos):
            mousePoint = self.fig.getPlotItem().vb.mapSceneToView(pos)
            return mousePoint.x(), mousePoint.y()

if __name__=="__main__":
    args = parse_args()
    if PICKLE_PATH_LIDAR.exists():
        with open(PICKLE_PATH_LIDAR, "rb") as f:
            pointclouds = pickle.load(f)
    else:
        bagpath = Path(args.ROSBAG).expanduser().resolve(strict=True)
        pointclouds = load_pointclouds(bagpath, args.TOPIC)
        with open(PICKLE_PATH_LIDAR, "wb") as f:
            pickle.dump(pointclouds, f)
    #pointclouds = {'q': {1658307938.220871: np.zeros((0,3)), 1658307973.175778: np.zeros((0,3))},  "b": {1658307994.932109: np.zeros((0,3))}}
    #pointclouds = {'q': {1658307938.220871: np.zeros((1,3)), 1658307973.175778: np.zeros((2,3))},  "b": {1658307994.932109: np.zeros((3,3))}}

    app = QtGui.QApplication(sys.argv)
    w = GtWindow(pointclouds)
    PICKLE_PATH_GT = Path(args.output).expanduser().resolve()
    if PICKLE_PATH_GT.exists():
        with open(PICKLE_PATH_GT, "rb") as f:
            w.shapes, w.links = pickle.load(f)
    w.show()
    sys.exit(app.exec_())
