#! /usr/bin/env python3

import sys
import pickle
from yaml import safe_load
from tqdm import tqdm
from pathlib import Path
from argparse import ArgumentParser

from rosbag import Bag

def parse_args():
	parser = ArgumentParser(description="Convert multiception/Perceptions messages to the GT PKL format")
	parser.add_argument("-o", "--output", type=str, help="Path to the converted bag", default="converted.bag")
	parser.add_argument("PATH", type=str, help="Path of the bag to convert")
	parser.add_argument("TOPICS", nargs="+", type=str, help="Topics to convert")
	return parser.parse_args()

def convert(bagpath, topics):
	links = {}
	objects = {}
	a = 0
	tprev = None
	objects_cur = { topic: None for topic in topics }

	with Bag(bagpath) as bag:
		nmsgs = sum(topic_info.message_count for topic, topic_info in bag.get_type_and_topic_info().topics.items() if topic in topics)
		for topic, msg, t in tqdm(bag.read_messages(topics), total=nmsgs):
			objects_cur[topic] = msg.perceptions[0]
			if any(o==None for o in objects_cur.values()): continue
			t = int(max(o.header.stamp.to_sec() for o in objects_cur.values()))
			if t<(a+1): continue # Skip every seconds

			objects[t] = [[(o.state.x, o.state.y)] for o in objects_cur.values()]
			if tprev is not None:
				links[t] = { topics.index(topic): (tprev, topics.index(topic)) for topic, o in objects_cur.items() }

			objects_cur = { topic: None for topic in topics }
			tprev = t
			a = t

	return objects, links

if __name__=="__main__":
	args = parse_args()
	path = Path(args.PATH).expanduser().resolve(strict=True)
	pathout = Path(args.output).expanduser().resolve()
	objects, links = convert(path, args.TOPICS)

	with open(pathout, "wb") as f:
		pickle.dump((objects, links), f)
