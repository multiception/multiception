#! /usr/bin/env python3

import argparse
import numpy as np
from tqdm import tqdm
from pathlib import Path

import rosbag
from sensor_msgs.msg import PointCloud2
from sensor_msgs.point_cloud2 import read_points

if __name__=='__main__':
	parser = argparse.ArgumentParser(
		description="Transforms point clouds from a bag into a series of npy files."
	)
	parser.add_argument('-v', '--verbose', help='Show status', action="store_true")
	parser.add_argument("rosbag", metavar="ROSBAG", type=str, help="Path of the rosbag to process.")
	parser.add_argument("folder", metavar="FOLDER", type=str, help="Path of the resulting npy files.")
	args = parser.parse_args()

	folder = Path(args.folder).expanduser().resolve()
	if not folder.parent.exists():
		raise RuntimeError(f'Path {folder.parent} does not exist')
	else:
		folder.mkdir(exist_ok=True)

	with rosbag.Bag(args.rosbag, 'r') as bag:
		for topic, msg, t in tqdm(bag.read_messages(topics=['/vlp32C/velodyne_points'])):
			points = np.array([point for point in read_points(msg)])
			points[3] /= 255.
			np.save(f'{folder/str(t)}.npy', points[:4])
