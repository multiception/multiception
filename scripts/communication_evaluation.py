#!/usr/bin/env python3

import yaml, argparse
from pathlib import Path
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import rosbag

def parse_args():
    parser = argparse.ArgumentParser(
        description='Loads a local and infra bag to compare timings'
    )
    parser.add_argument('-v', '--verbose', help='Show status', action='store_true')
    parser.add_argument('bagzoe', type=str, help='Path of the local vehicle bag', default='')
    parser.add_argument('baginfra', type=str, help='Path of the infrastructure bag', default='')

    return parser.parse_args()

def load_zoe(bagpath):
    SUPPORTED_TOPICS = ['/span/enuposeCovs', '/perception/infra']
    data = []
    bag = rosbag.Bag(bagpath)
    current_pose = None
    info_dict = yaml.safe_load(bag._get_yaml_info())
    nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in SUPPORTED_TOPICS])

    for topic, msg, t in tqdm(bag.read_messages(topics=SUPPORTED_TOPICS), total=nMsg, desc='Loading messages'):
        if topic == SUPPORTED_TOPICS[0]:
            current_pose = (msg.pose.pose.position.x, msg.pose.pose.position.y)
        elif topic==SUPPORTED_TOPICS[1] and current_pose:
            data.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec(), len(msg.perceptions), current_pose[0], current_pose[1]))

    return np.array(data)

def load_infra(bagpath):
    SUPPORTED_TOPICS = ['/camera/image_raw', '/infra/image', '/infra/obstacle']
    data_img = []
    data_yol = []
    data_obs = []
    bag = rosbag.Bag(bagpath)
    current_pose = None
    info_dict = yaml.safe_load(bag._get_yaml_info())
    nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in SUPPORTED_TOPICS])

    for topic, msg, t in tqdm(bag.read_messages(topics=SUPPORTED_TOPICS), total=nMsg, desc='Loading messages'):
        if topic == SUPPORTED_TOPICS[0]:
            data_img.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec()))
        elif topic == SUPPORTED_TOPICS[1]:
            data_yol.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec()))
        elif topic == SUPPORTED_TOPICS[2]:
            data_obs.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec(), len(msg.perceptions)))

    return np.array(data_img), np.array(data_yol), np.array(data_obs)

if __name__=='__main__':
    args = parse_args()

    if args.verbose:
        from tqdm import tqdm
    else:
        def tqdm(iter, total, desc):
            return iter

    bagpath_zoe = Path(args.bagzoe).resolve()
    bagpath_infra = Path(args.baginfra).resolve()
    pklpath = Path(f'/tmp/test_communication_py_{bagpath_zoe.stem}-{bagpath_infra.stem}.pkl')

    if pklpath.exists():
        if args.verbose: print(f'Loading PKL from {pklpath}')
        with open(pklpath, 'rb') as f: zoe, (infimg, infyol, infobs) = pickle.load(f)
    else:
        if args.verbose: print(f'Loading messages from {bagpath_zoe} and {bagpath_infra}')
        infimg, infyol, infobs = load_infra(args.baginfra)
        zoe = load_zoe(args.bagzoe)
        with open(pklpath, 'wb') as f:
            pickle.dump((zoe, (infimg, infyol, infobs)), f)
    if args.verbose: print(f'Loaded {len(zoe)+len(infimg)+len(infyol)+len(infobs)} messages')

    smooth = 1
    zoe = pd.DataFrame(zoe, columns=['t', 'seq', 'stamp', 'nobs', 'posx', 'posy'])
    infimg = pd.DataFrame(infimg, columns=['t', 'seq', 'stamp'])
    infyol = pd.DataFrame(infyol, columns=['t', 'seq', 'stamp'])
    infobs = pd.DataFrame(infobs, columns=['t', 'seq', 'stamp', 'nobs'])
    for x in [zoe, infimg, infyol, infobs]:
        x.t = pd.to_datetime(x.t*1E9)
        x.stamp = pd.to_datetime(x.stamp*1E9)
        x.set_index('stamp', inplace=True)

    join = infimg.join(infobs, how='outer', rsuffix='obs').join(zoe, how='outer', rsuffix='com')
    joinred = join.iloc[np.abs(join.index-infimg.index[0]).argmin():np.abs(join.index-infimg.index[-1]).argmin()]

    fig = plt.figure(0)
    for x in [joinred.tcom, joinred.tobs, joinred.t]:
        x.diff().dt.total_seconds().plot(alpha=0.7)
    fig.tight_layout()

    fig = plt.figure(1)
    for x in [joinred.tcom, joinred.tobs, joinred.t]:
        (x-joinred.index).dt.total_seconds().plot(alpha=0.7)
        print((x-joinred.index).dt.total_seconds().mean())
        print((x-joinred.index).dt.total_seconds().std())
    fig.tight_layout()

    fig = plt.figure(2)
    joinred['dropped'] = joinred.tcom.isna()
    print(joinred.posx[joinred.dropped], joinred.posy[joinred.dropped])
    plt.scatter(joinred.posx[~joinred.dropped], joinred.posy[~joinred.dropped], alpha=0.05)
    fig.tight_layout()
    plt.axis('equal')

    plt.show()
