#!/usr/bin/env python3

from shlex import split as shellsplit
from subprocess import check_output
from multiprocessing import Pool
from pathlib import Path
from argparse import ArgumentParser
import existence_evaluation

def parse_args():
    parser = ArgumentParser(description='Compare outputs of existence_evaluation.py')
    parser.add_argument("--additional-flags", type=str, help='Additional flags passed to subprocesses', default="")
    parser.add_argument('SCENARIO', type=str, help='List of bagpath:topic-pose:topic-percepts to compare')
    parser.add_argument('PATH_POSE_PERCEP', type=str, nargs="+", help='List of bagpath:topic-pose:topic-percepts to compare')
    return parser.parse_args()

def run_eval(*args):
    try:
        output = check_output(args)
        return Path(output.decode().split("\n")[-2]).resolve(strict=True)
    except Exception as e:
        return None

if __name__=='__main__':
    args = parse_args()
    # Call existence_evaluation.py
    additional_flags = tuple(shellsplit(args.additional_flags))
    path_pose_percepts = [x.split(":") for x in args.PATH_POSE_PERCEP]
    callargs = { 
        f"{Path(bagpath).stem.split('-')[0]}{'-'.join(topic_percepts.split('/')[:-1])}":
        ("rosrun", "multiception", "existence_evaluation.py", "--pose-topic", topic_pose) + \
        ("--cache", f"{Path(bagpath).parent}/eval_{Path(bagpath).stem.split('-')[0]}{'_'.join(topic_percepts.split('/')[:-1])}") + \
        additional_flags + \
        ("--", args.SCENARIO, bagpath, topic_percepts)
        for bagpath, topic_pose, topic_percepts in path_pose_percepts
    }

    with Pool(2) as p:
        outputs = dict(zip(callargs.keys(), p.starmap(run_eval, callargs.values())))

    scores = {}
    y_trues = {}
    y_preds = {}
    errcovs = {}
    gridscores = {}
    for outputname, outputpath in outputs.items():
        if outputpath is None: continue
        with open(outputpath/"objects.pkl", 'rb') as f:
            scores[outputname], y_trues[outputname], y_preds[outputname] = pickle.load(f)
        with open(outputpath/"errors.pkl", 'rb') as f:
            errcovs[outputname] = pickle.load(f)
        with open(outputpath/"grids.pkl", 'rb') as f:
            gridscores[outputname] = pickle.load(f)

    fig, ax = plt.subplots(1, 1)
    times = sorted(set(t for variants in errcovs.values() for thresholds in variants.values() for ids in thresholds.values() for t in ids.keys()))
    for outputname, scorename in scores.items():
        existence_evaluation.disp_PR(scorename, ax=ax, label=outputname)
    plt.legend()
    plt.show()
