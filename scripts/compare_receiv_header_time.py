#! /usr/bin/env python3

import shutil
from rosbag import Bag
from pathlib import Path
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt

def parse_args():
    parser = ArgumentParser(description='Plots header time against reception time for the given bags.')
    parser.add_argument('BAGS', nargs='+', type=str, help='Paths and optionnally topics of rosbags to process (syntax is BAGPATH[:TOPIC[:...]]).')
    return parser.parse_args()

def main(bagpaths):
    bag_topics_to_analyze = {}

    # Retrieve what bag and topics to analyze from CLI
    for b in bagpaths:
        fields = b.split(':')
        bagpath = Path(fields[0]).expanduser().resolve()
        bag_topics_to_analyze[bagpath] = None if len(fields)==1 else fields[1:]

    # Retrieve times from specified bags and topics
    times = {} # { (bag,topic): (received_time, header_time) }
    for path, topics in bag_topics_to_analyze.items():
        with Bag(path) as b:
            for topic, msg, t in b.read_messages(topics):
                if (path,topic) not in times: times[(path,topic)] = []
                times[(path,topic)].append((t.to_sec(), msg.header.stamp.to_sec()))

    for (path,topic), trecvhead in times.items():
        trecv, thead = zip(*trecvhead)
        trecv, thead = np.array(trecv), np.array(thead)
        plt.plot(thead-thead.min(), trecv-thead, label=f'{path.stem}:{topic}')

    plt.tight_layout()
    #plt.axis('equal')
    plt.legend()
    plt.show()

if __name__=='__main__':
    main(parse_args().BAGS)
