#! /usr/bin/env python3

import sys
import pickle
from copy import deepcopy
from pathlib import Path
from argparse import ArgumentParser
from multiprocessing import Pool
from operator import itemgetter
import numpy as np

from multiception import existence, filtering, tools
CONFIGS = {
	'base': {},
	'kalman': { 'filter_cls': filtering.Kalman },
	'ci': { 'filter_cls': filtering.CI },
	'nodet': { 'existence_estimator.__class__': existence.ExistenceEstimatorFilter },
	'naive': { 'existence_estimator.__class__': existence.ExistenceEstimatorNaive, 'existence_estimator.thresh_delete_uncertain': 100, 'existence_estimator.thresh_delete_time': 0.5},
}

def run(pathout, tracker, config):
	tmin, tmax = np.min(tracker.obs_buffer.keys()), np.max(tracker.tracks_buffer.keys())
	tracker.obs_mutex = tools.DummyMutex()
	tracker.tracks_buffer.clear()
	tracker.oidmax_buffer.clear()
	tracker.obs_temp.clear()
	tracker.rollback(tmin)

	for k, v in config.items():
		attrs = k.split('.')
		setattr(tools.resolvattr(tracker, attrs[:-1]), attrs[-1], v)

	try:
		tracker.step(tmax)
		with open(pathout, 'wb') as f: pickle.dump(tracker, f)
	except Exception as e:
		print(f'{pathout} raised', file=sys.stderr)
		raise e

def display(pathsin):
	import matplotlib.pyplot as plt
	from matplotlib.collections import LineCollection

	track_traces = {}
	for path in pathsin:
		track_traces[path] = {}
		with open(path, 'rb') as f:
			o = pickle.load(f)
			for t, tracks in o.tracks_buffer.items():
				for track in tracks:
					if track.oid not in track_traces[path]: track_traces[path][track.oid] = []
					track_traces[path][track.oid].append((track.t, track.x, track.y, track.exist['e']))
			for k in track_traces[path]:
				track_traces[path][k] = np.array(track_traces[path][k])

	colors_hex = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
	colors_rgb = [(int(color[1:3],base=16)/256, int(color[3:5],base=16)/256, int(color[5:7],base=16)/256) for color in colors_hex]
	for path, traces in track_traces.items():
		fig, (axt,axe) = plt.subplots(2, 1)
		for i, (oid, trace) in enumerate(traces.items()):
			axt.add_collection(LineCollection([[(x,y) for _,x,y,_ in trace]], colors=[(*colors_rgb[i%len(colors_rgb)], e) for _,_,_,e in trace]))
			axe.plot(np.array(trace)[:,0], np.array(trace)[:,3], alpha=0.5)
		fig.tight_layout()
		axt.set_title(f'{path.split("/")[-2]} {path.split("/")[-1]}')
		axt.axis('equal')
	plt.show()

if __name__=='__main__':
	parser = ArgumentParser(description='Runs tracking on the given data with different configurations')
	parser.add_argument('-d', '--display', action='store_true', help='Do not run comparisons and display comparisons')
	parser.add_argument('PATHS', type=str, nargs='+', help='Path to pickles')
	arg = parser.parse_args()

	if not arg.display:
		# Prepare configurations to compare
		args = []
		for path in arg.PATHS:
			with open(path, 'rb') as f: tracker = pickle.load(f)
			for config_name, config in CONFIGS.items():
				pathin = Path(path).expanduser().resolve()
				pathout = (pathin.parent/pathin.stem/config_name).with_suffix('.pkl')
				pathout.parent.mkdir(exist_ok=True)

				args.append((pathout, tracker, config))

		# Run each configurations in parallel
		with Pool() as p:
			p.starmap(run, args)
	else:
		display(arg.PATHS)
