#! /usr/bin/env python3

import sys
import pickle
from copy import deepcopy
from pathlib import Path
from argparse import ArgumentParser
from multiprocessing import Pool
from operator import itemgetter
import numpy as np

from multiception import belief, trust, tools
initial_trust_filter = belief.MassFunction({'t': 1.00 ,'tT': 0.00})
initial_trust_local  = belief.MassFunction({'t': 0.95 ,'tT': 0.05})
initial_trust_remote = belief.MassFunction({'t': 0.60 ,'tT': 0.40})

initial_trusts_higher = {}
initial_trusts_higher['filter']              = initial_trust_filter
initial_trusts_higher['lidar']               = initial_trust_local
initial_trusts_higher['mobileye']            = initial_trust_local
initial_trusts_higher['egotracker']          = initial_trust_local
initial_trusts_higher['zoeblue/filter']      = initial_trust_filter
initial_trusts_higher['zoegrey/filter']      = initial_trust_filter
initial_trusts_higher['zoewhite/filter']     = initial_trust_filter
initial_trusts_higher['zoeblue/lidar']       = initial_trust_local
initial_trusts_higher['zoegrey/lidar']       = initial_trust_local
initial_trusts_higher['zoewhite/lidar']      = initial_trust_local
initial_trusts_higher['zoeblue/mobileye']    = initial_trust_local
initial_trusts_higher['zoegrey/mobileye']    = initial_trust_local
initial_trusts_higher['zoewhite/mobileye']   = initial_trust_local
initial_trusts_higher['zoeblue/egotracker']  = initial_trust_local
initial_trusts_higher['zoegrey/egotracker']  = initial_trust_local
initial_trusts_higher['zoewhite/egotracker'] = initial_trust_local
initial_trusts_higher['zoeblue/multracker']  = initial_trust_local
initial_trusts_higher['zoegrey/multracker']  = initial_trust_local
initial_trusts_higher['zoewhite/multracker'] = initial_trust_local

initial_trusts_higher['zoeblue']             = initial_trust_remote
initial_trusts_higher['zoegrey']             = initial_trust_remote
initial_trusts_higher['zoewhite']            = initial_trust_remote

CONFIGS = {
	'no-trust':  { 'mode': trust.TrustBuilder.Mode.NONE },
	'no-conf':   { 'mode': trust.TrustBuilder.Mode.NO_CONF, 'initial_trusts': initial_trusts_higher, 'HLTIME_LOCAL': 1e30, 'HLTIME_REMOTE': 1e30 },
	'glob-conf': { 'mode': trust.TrustBuilder.Mode.GLOBALCONF, 'HLTIME_LOCAL': 3, 'HLTIME_REMOTE': 3 },
	'fuse-conf': { 'mode': trust.TrustBuilder.Mode.FUSEDCONF },
	'sequ-conf': { 'mode': trust.TrustBuilder.Mode.SEQUENTIALCONF },
}

def run(pathout, trustbuilder, config, inputs):
	trustbuilder.mutex = tools.DummyMutex()
	trustbuilder.history.clear()
	trustbuilder.debugtrusts.clear()
	oldtrusts = trustbuilder.static_trusts
	trustbuilder.static_trusts = { k:v for k,v in oldtrusts.items() if 'filter' in k }
	for k, v in config.items():
		attrs = k.split('.')
		setattr(tools.resolvattr(trustbuilder, attrs[:-1]), attrs[-1], v)

	try:
		for inp in inputs: trustbuilder.callback(*inp)
		with open(pathout, 'wb') as f: pickle.dump(trustbuilder, f)
	except Exception as e:
		print(f'{pathout} raised', file=sys.stderr)
		raise e

def display(pathsin):
	import matplotlib.pyplot as plt
	comps = {}
	for path in pathsin:
		with open(path, 'rb') as f:
			o = pickle.load(f)
		for sid, history in o.history.items():
			if sid in o.static_trusts: continue
			if sid not in comps: comps[sid] = {}
			comps[sid][Path(path).stem] = np.array(list((t, tr['t'], tr['T'], tr['tT']) for t, (tr,_,_) in history.items()))

	for sid in comps:
		plt.figure()
		plt.title(sid)
		for comp in comps[sid]:
			plt.plot(*comps[sid][comp][:,:2].T, label=comp)
		plt.legend()
		plt.tight_layout()
		plt.ylim(0, 1)
	plt.show()

if __name__=='__main__':
	parser = ArgumentParser(description='Runs trust-building on the given data with different configurations')
	parser.add_argument('-d', '--display', action='store_true', help='Do not run comparisons and display comparisons')
	parser.add_argument('PATHS', type=str, nargs='+', help='Path to pickles')
	arg = parser.parse_args()

	if not arg.display:
		# Prepare configurations to compare
		args = []
		for path in arg.PATHS:
			with open(path, 'rb') as f: trustbuilder = pickle.load(f)
			history_generator = ((sourceid, t, objs, dete) for sourceid, h in trustbuilder.history.items() for t, (_, objs, dete) in h.items())
			inputs = sorted(history_generator, key=itemgetter(1)) # Sort on time

			for config_name, config in CONFIGS.items():
				pathin = Path(path).expanduser().resolve()
				pathout = (pathin.parent/pathin.stem/config_name).with_suffix('.pkl')
				pathout.parent.mkdir(exist_ok=True)

				args.append((pathout, trustbuilder, config, inputs))

		# Run each configurations in parallel
		with Pool() as p:
			p.starmap(run, args)
	else:
		display(arg.PATHS)
