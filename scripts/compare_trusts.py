#! /usr/bin/env python3

from yaml import safe_load
from argparse import ArgumentParser
from pathlib import Path
from pickle import load
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['font.size'] = 18
plt.rcParams['text.usetex'] = True
plt.rcParams['axes.spines.top'] = False
plt.rcParams['axes.spines.right'] = False

VEHICLE_NAMES = {
	"zoeblue": "$v_1$",
	"zoegrey": "$v_2$",
	"zoewhite": "$v_3$",
}

def parse_args():
	parser = ArgumentParser(description="Plots trusts from several variants")
	parser.add_argument("--blacklist", type=str, help="Comma-separated list of sources to ignore (e.g. 'fiter,lidar')")
	parser.add_argument("--nodisplay", help="Just output tables, no display", action="store_true")
	parser.add_argument("--output", help="Where to save the produced plots", default=None)
	parser.add_argument("--spoof", help="Path to spoof config to plot times where there are attacks", default=None)
	parser.add_argument("--labels", help="Comma-separated ordered labels (e.g. 'label1,label2,...'", default=None)
	parser.add_argument("PICKLES", type=str, nargs="+", help="Path to variant/tru.pkl")
	return parser.parse_args()

def load_bag(path):
	path = Path(path).expanduser().resolve(strict=True)
	trusts = {} # { source: { target: np.array(t, trust, nontrust) } }
	with open(path, "rb") as f:
		histories, trustss = load(f)
		for sid, history in histories.items():
			sid = sid.split("/")[0]
			trusts[sid] = {}
			for tid, masses in history.items():
				trust = np.array([(t, tr['t'], tr['T']) for t, tr in masses.items()])
				#if np.cov(trust[:,1])<0.0001: continue
				tids = tid.split("/")
				tid = tids[0] if tids[1]=="egotracker" else tids[1]
				if tid in blacklist: continue
				trusts[sid][tid] = trust

	return path.parent.stem, trusts

if __name__=="__main__":
	args = parse_args()
	blacklist = args.blacklist.split(",")
	trusts = dict(map(load_bag, args.PICKLES)) # { method: { source: { target: np.array(t, trust, nontrust) } } }
	nsources = len(list(trusts.values())[0])
	ntargets = len(list(list(trusts.values())[0].values())[0])
	fig, axes = plt.subplots(nsources, ntargets, figsize=(nsources*5, ntargets*3.5), sharex=True, sharey=True, tight_layout=True)
	#variants = sorted(trusts)
	if args.labels is None:
		variants_columns = np.array([v.split("_") for v in trusts]).T
		variant_uniques = [col for col in variants_columns if len(set(col))>1]
		variants_names = variant_uniques[0] if len(variant_uniques)>0 else [""] # Retrieve the first column where keys are changing
	else:
		variants_names = args.labels.split(",")

	if args.spoof:
		with open(args.spoof, "r") as f:
			spoof_config = safe_load(f).get("spo", {})
	else:
		spoof_config = {}

	for i, variant in enumerate(trusts):
		for j, sid in enumerate(sorted(trusts[variant])):
			for k, tid in enumerate(sorted(trusts[variant][sid])):
				c = plt.cm.tab10(i)
				t = trusts[variant][sid][tid][:, 0]
				t0 = t.min()
				t -= t0
				trust = trusts[variant][sid][tid][:, 1]
				ntrust = trusts[variant][sid][tid][:, 2]
				axes[j,k].plot(t, trust, label=variants_names[i], alpha=0.8, color=c)
				axes[j,k].plot(t, trust+ntrust, alpha=0.8, color=c, linestyle="--")
				#axes[j,k].fill_between(t, trust+ntrust, trust, color=c, alpha=0.1)
				axes[j,k].set_title("Trust of "+VEHICLE_NAMES[sid]+" in "+VEHICLE_NAMES[tid])
				axes[j,k].grid(alpha=0.1)
				if len(variants_names)>1 and j==0 and k==(ntargets-1): axes[j,k].legend(loc="center left")
				if j==(nsources-1): axes[j,k].set_xlabel(r"Time ($s$)")
				if k==0: axes[j,k].set_ylabel(r"Trust ($m^\mathcal{T}$)")

	for ax in axes.flatten():
		for spoof_sid, spoofconf in spoof_config.items():
			for spoof_type, spoof_group in spoofconf.items():
				for spoof in spoof_group:
					tbeg = spoof["t0"]-t0
					tend = tbeg + spoof["duration"]
					ax.fill_between(t[(t>tbeg) & (t<tend)], 1, 0, color="tab:grey", alpha=0.05)

	if args.output:
		output = Path(args.output).expanduser().resolve()
		output.mkdir(parents=True, exist_ok=True)
		plt.savefig(output/"trust.pdf")

	if not args.nodisplay: plt.show()
