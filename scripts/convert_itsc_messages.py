#! /usr/bin/env python3

import numpy as np
from yaml import safe_load
from tqdm import tqdm
from pathlib import Path
from argparse import ArgumentParser

from rospy import Time
from rosbag import Bag

from multiception.evidential_grid_map import EvidentialGridMap
from multiception.belief import MassFunction
from multiception.msg import Perceptions as NewPerceptions, Perception as NewPerception, Source

MSG_TYPE = "multiception/Perceptions"

def parse_args():
	parser = ArgumentParser(description="Convert multiception/Perceptions messages from the ITSC to BELIEF format")
	parser.add_argument("-o", "--output", type=str, help="Path to the converted bag", default="converted.bag")
	parser.add_argument("PATH", type=str, help="Path of the bag to convert")
	return parser.parse_args()

def convert(path, pathout):
	with Bag(pathout, "w") as bagout:
		with Bag(path) as bag:
			perceptions_topics = { topic: topic_info.message_count for topic, topic_info in bag.get_type_and_topic_info().topics.items() if topic_info.msg_type==MSG_TYPE }
			for topic, msg, t in tqdm(bag.read_messages(topics=perceptions_topics.keys()), desc=f"Converting {path}", total=sum(perceptions_topics.values())):
				msgout = NewPerceptions()
				msgout.header = msg.header
				msgout.sources.append(Source())
				obs = EvidentialGridMap("o,O")
				obs.setGeometry([0,0], 1)
				msgout.sources[0].observability = obs.to_msg()
				subtopics = topic.split("/")
				vehicle, sourcetype, rest = subtopics[1], subtopics[2], subtopics[3:]
				if sourcetype=="egotracker":
					msgout.sources[0].source_id = f"{vehicle}/egotracker"
					msgout.sources[0].type = Source.SOURCE_LOCAL_FUSION
					topic = topic.replace("percepts", "tracks")
				elif sourcetype=="lidar":
					msgout.sources[0].source_id = f"{vehicle}/lidar"
					if "tracks" in rest[0]:
						msgout.sources[0].type = Source.SOURCE_LOCAL_FUSION
						topic = "/".join(["", vehicle, "lidar_tracker"]+rest)
					else:
						msgout.sources[0].type = Source.SOURCE_LOCAL_LIDAR
						if "percepts_" in rest:
							topic = "/".join(subtopics[:-1]+["percepts_sensor"])
				elif sourcetype=="mobileye":
					msgout.sources[0].source_id = f"{vehicle}/mobileye"
					msgout.sources[0].type = Source.SOURCE_LOCAL_VISIONMONO
				elif sourcetype=="multracker":
					msgout.sources[0].source_id = f"{vehicle}/multracker"
					msgout.sources[0].type = Source.SOURCE_LOCAL_FUSION
					topic = topic.replace("percepts", "tracks")
				elif "statep" in sourcetype:
					msgout.sources[0].source_id = f"{vehicle}/filter"
					msgout.sources[0].type = Source.SOURCE_LOCAL_FUSION
				elif "tracks" in sourcetype:
					msgout.sources[0].source_id = f"{vehicle}"
					msgout.sources[0].type = Source.SOURCE_LOCAL_FUSION

				for p in msg.perceptions:
					pout = NewPerception()
					pout.header = p.header
					pout.object_id = p.object_id
					pout.source_id = p.sensor
					pout.existence = MassFunction({"e": p.confidence, "eE": 1-p.confidence}).to_msg()
					pout.t_last_obs = p.header.stamp
					pout.t_first_obs = Time(p.header.stamp.to_sec()+p.object_age.to_sec())
					pout.state.x = p.state.x
					pout.state.y = p.state.y
					pout.state.O = p.state.yaw
					pout.state.v = p.state.v
					pout.state.w = p.state.yawr
					pout.state.covd = np.ravel(np.reshape(p.state.cov, (6,6))[:5, :5])
					pout.state.covi = np.ravel(np.reshape(p.state.cov_ind, (6,6))[:5, :5])
					pout.trace = p.trace
					pout.length = p.length
					pout.width = p.width
					pout.height = p.height
					pout.cov_dimensions = p.cov_dimensions
					pout.classif.value = p.classif.value
					pout.is_dynamic = p.is_dynamic
					pout.debug = p.debug

					msgout.perceptions.append(pout)

				bagout.write(topic, msgout, t)

if __name__=="__main__":
	args = parse_args()
	path = Path(args.PATH).expanduser().resolve(strict=True)
	pathout = Path(args.output).expanduser().resolve()
	convert(path, pathout)
