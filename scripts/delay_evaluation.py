#!/usr/bin/env python3

import yaml, argparse
from pathlib import Path
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import rosbag

def parse_args():
    parser = argparse.ArgumentParser(
        description='Loads a local and infra bag to compare timings'
    )
    parser.add_argument('-v', '--verbose', help='Show status', action='store_true')
    parser.add_argument('bagzoe', type=str, help='Path of the local vehicle bag', default='')
    parser.add_argument('baginfra', type=str, help='Path of the infrastructure bag', default='')

    return parser.parse_args()

def load_zoe(bagpath):
    SUPPORTED_TOPICS = ['/span/enuposeCovs', '/perception/infra']
    data = []
    bag = rosbag.Bag(bagpath)
    current_pose = None
    info_dict = yaml.safe_load(bag._get_yaml_info())
    nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in SUPPORTED_TOPICS])

    for topic, msg, t in tqdm(bag.read_messages(topics=SUPPORTED_TOPICS), total=nMsg, desc='Loading messages'):
        if topic == SUPPORTED_TOPICS[0]:
            current_pose = (msg.pose.pose.position.x, msg.pose.pose.position.y)
        elif topic==SUPPORTED_TOPICS[1] and current_pose:
            data.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec(), len(msg.perceptions), current_pose[0], current_pose[1]))

    return np.array(data)

def load_infra(bagpath):
    SUPPORTED_TOPICS = ['/camera/image_raw', '/infra/image', '/infra/obstacle']
    data_img = []
    data_yol = []
    data_obs = []
    bag = rosbag.Bag(bagpath)
    current_pose = None
    info_dict = yaml.safe_load(bag._get_yaml_info())
    nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in SUPPORTED_TOPICS])

    for topic, msg, t in tqdm(bag.read_messages(topics=SUPPORTED_TOPICS), total=nMsg, desc='Loading messages'):
        if topic == SUPPORTED_TOPICS[0]:
            data_img.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec()))
        elif topic == SUPPORTED_TOPICS[1]:
            data_yol.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec()))
        elif topic == SUPPORTED_TOPICS[2]:
            data_obs.append((t.to_sec(), msg.header.seq, msg.header.stamp.to_sec(), len(msg.perceptions)))

    return np.array(data_img), np.array(data_yol), np.array(data_obs)

def plot_positions(pos):
    plt.figure(1, figsize=(20,10))
    xs, ys, postypes = zip(*[(d['x'], d['y'], d['position_type']) for d in pos])
    plt.scatter(xs, ys, c=postypes, s=2, edgecolors='none', zorder=11, label='_nolegend_',
        vmin=min(POSITION_TYPE_INDEX), vmax=max(POSITION_TYPE_INDEX), norm=POSITION_TYPE_NORM)

    cb = plt.colorbar(spacing='uniform')
    cb.set_ticks((POSITION_TYPE_BOUNDS[1::]+POSITION_TYPE_BOUNDS[:-1:])/2.)
    cb.set_ticklabels(POSITION_TYPES)

    plt.grid()
    plt.axis('equal')
    plt.tight_layout()

def plot_timediff(x, ax=None):
    if ax is None: ax = plt.gca()
    diff = x[:,0] - x[:,2]
    mean = np.mean(diff)
    std = np.std(diff)
    ax.plot(diff)
    ax.plot(np.convolve(diff, np.ones(100), 'valid') / 100)
    print(mean, std)

def plot_timeder(x, i, ax=None):
    if ax is None: ax = plt.gca()
    der = x[1:,i] - x[:-1,i]
    mean = np.mean(der)
    std = np.std(der)
    ax.plot(der)
    ax.plot(np.convolve(der, np.ones(100), 'valid') / 100)
    print(mean, std)

if __name__=='__main__':
    args = parse_args()

    if args.verbose:
        from tqdm import tqdm
    else:
        def tqdm(iter, total, desc):
            return iter

    bagpath_zoe = Path(args.bagzoe).resolve()
    bagpath_infra = Path(args.baginfra).resolve()
    pklpath = Path(f'/tmp/test_communication_py_{bagpath_zoe.stem}-{bagpath_infra.stem}.pkl')

    if pklpath.exists():
        if args.verbose: print(f'Loading PKL from {pklpath}')
        with open(pklpath, 'rb') as f: zoe, (infimg, infyol, infobs) = pickle.load(f)
    else:
        if args.verbose: print(f'Loading messages from {bagpath_zoe} and {bagpath_infra}')
        infimg, infyol, infobs = load_infra(args.baginfra)
        zoe = load_zoe(args.bagzoe)
        with open(pklpath, 'wb') as f:
            pickle.dump((zoe, (infimg, infyol, infobs)), f)
    if args.verbose: print(f'Loaded {len(zoe)+len(infimg)+len(infyol)+len(infobs)} messages')

    smooth = 1
    zoe = pd.DataFrame(zoe, columns=['t', 'seq', 'stamp', 'nobs', 'posx', 'posy'])
    infimg = pd.DataFrame(infimg, columns=['t', 'seq', 'stamp'])
    infyol = pd.DataFrame(infyol, columns=['t', 'seq', 'stamp'])
    infobs = pd.DataFrame(infobs, columns=['t', 'seq', 'stamp', 'nobs'])
    for x in [zoe, infimg, infyol, infobs]:
        x.t = pd.to_datetime(x.t*1E9)
        x.stamp = pd.to_datetime(x.stamp*1E9)
        x.set_index('stamp', inplace=True)

    join = infimg.join(infobs, how='outer', rsuffix='obs').join(zoe, how='outer', rsuffix='com')
    joinred = join.iloc[np.abs(join.index-infimg.index[0]).argmin():np.abs(join.index-infimg.index[-1]).argmin()]

    fig = plt.figure(0)
    for x in [joinred.tcom, joinred.tobs, joinred.t]:
        x.diff().dt.total_seconds().plot(alpha=0.7)
    fig.tight_layout()

    fig = plt.figure(1)
    (joinred.tcom-joinred.tobs).dt.total_seconds().plot(alpha=0.9)
    print('%.5f +/- %.5f' % ((joinred.tcom-joinred.tobs).dt.total_seconds().mean(), (joinred.tcom-joinred.tobs).dt.total_seconds().std()))
    fig.tight_layout()

    fig = plt.figure(2)
    for x in [joinred.tcom, joinred.tobs, joinred.t]:
        (x-joinred.index).dt.total_seconds().plot(alpha=0.7)
        print((x-joinred.index).dt.total_seconds().mean())
        print((x-joinred.index).dt.total_seconds().std())
    fig.tight_layout()

    fig = plt.figure(3)
    #dropped_img = joinred.t.isna().astype(int)
    #dropped_obs = joinred.tobs.isna().astype(int) + 0.1
    #dropped_com = joinred.tcom.isna().astype(int) + 0.2
    #for i, x in enumerate([joinred.tcom, joinred.tobs, joinred.t]):
        #joinred['dropped'] = x.isna().astype(int)
        ##x.dropped[x.dropped==0] = np.nan
        ##x.dropped[x.dropped==1] = i*0.01
        #joinred.dropped.rolling(100, min_periods=1).sum().plot(alpha=0.7,style='.')
    joinred['dropped'] = joinred.tcom.isna()
    #joinred['droppedf'] = joinred.dropped.astype(float)
    #joinred.posx[~joinred.dropped] = np.nan
    #joinred.posy[~joinred.dropped] = np.nan
    print(joinred.posx[joinred.dropped], joinred.posy[joinred.dropped])
    plt.scatter(joinred.posx[~joinred.dropped], joinred.posy[~joinred.dropped], alpha=0.05)
    #joinred.droppedf.rolling('1s', min_periods=1).sum().plot(alpha=0.2,style='.')
    fig.tight_layout()
    plt.axis('equal')

    plt.show()

    #figdiff, ((axdiff1, axdiff2), (axdiff3, axdiff4)) = plt.subplots(2, 2)
    #figder, ((axder1, axder2), (axder3, axder4)) = plt.subplots(2, 2)
    #for x, axdiff, axder in [(zoe,axdiff1,axder1), (infimg,axdiff2,axder2), (infobs,axdiff4,axder4)]:
        #plot_timediff(x, axdiff)
        #plot_timeder(x, 0, axder)
        #plot_timeder(x, 2, axder)
    #figdiff.tight_layout()
    #figder.tight_layout()
    #plt.show()
