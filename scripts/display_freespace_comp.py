import pickle
from time import time
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
from multiception.evidential_grid_map import EvidentialGridMap

exec(open('/home/antoine/ws/multiception_ws/src/multiception/scripts/test_combine_free.py').read())

REAL_DATA = True
if REAL_DATA:
	with open('/tmp/obs.pkl', 'rb') as f: b = pickle.load(f)
	gb, gg, gw = b[0].values()
	gg, gb, gw = deepcopy(gb[0]), deepcopy(gg[0]), deepcopy(gg[0])
	EvidentialGridMap.extendMultiToInclude([gg, gb, gw])
	gb = EvidentialGridMap(gb).normalize()
	gg = EvidentialGridMap(gg).normalize()
	gw = EvidentialGridMap(gg.getEvidentialFrame())
	gw.setGeometry((0,0), 1, gg.getPosition())
else:
	gb = EvidentialGridMap(['o', 'O']); gb.setGeometry([3,3], 1)
	gb[''][:,:]    = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
	gb['o'][:,:]   = np.array([[0.0, 0.0, 0.0], [0.5, 0.5, 0.5], [0.0, 0.0, 0.0]])
	gb['O'][:,:]   = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.5, 0.5, 0.5]])
	gb['o,O'][:,:] = np.array([[1.0, 1.0, 1.0], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]])

	gg = EvidentialGridMap(['o', 'O']); gg.setGeometry([3,3], 1)
	gg[''][:,:]    = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
	gg['o'][:,:]   = np.array([[0.0, 0.5, 0.0], [0.0, 0.5, 0.0], [0.0, 0.5, 0.0]])
	gg['O'][:,:]   = np.array([[0.0, 0.0, 0.5], [0.0, 0.0, 0.5], [0.0, 0.0, 0.5]])
	gg['o,O'][:,:] = np.array([[1.0, 0.5, 0.5], [1.0, 0.5, 0.5], [1.0, 0.5, 0.5]])

	gw = EvidentialGridMap(['o', 'O']); gw.setGeometry([3,3], 1)
	gw[''][:,:]    = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
	gw['o'][:,:]   = np.array([[0.0, 0.5, 0.0], [0.0, 0.5, 0.0], [0.0, 0.5, 0.0]])
	gw['O'][:,:]   = np.array([[0.0, 0.0, 0.5], [0.0, 0.0, 0.5], [0.0, 0.0, 0.5]])
	gw['o,O'][:,:] = np.array([[1.0, 0.5, 0.5], [1.0, 0.5, 0.5], [1.0, 0.5, 0.5]])

N = 1
t0 = time()
for n in range(N):
	print(n)
	g0 = EvidentialGridMap.combineConjunctive([gg, gb], True)
t1 = time()
for n in range(N):
	print(n)
	g3 = EvidentialGridMap.combineConjunctive([gg, gb], False)
t2 = time()
for n in range(N): 
	print(n)
	g1 = combine_detectability_grid({'b':gb, 'g':gg})
t3 = time()
for n in range(N):
	print(n)
	g2 = EvidentialGridMap.combineDetect([gg, gb])
t4 = time()
print(f'ConjNorm: {(t1-t0)/N}, ConjUnNorm: {(t2-t1)/N}, DetePy: {(t3-t2)/N}, DeteCPP: {(t4-t3)/N}')
fig, ((a4, a5, a6), (a1, a2, a3)) = plt.subplots(2,3, sharex=True, sharey=True, tight_layout=True)
#fig, (a4, a5, a1, a2) = plt.subplots(1,4, sharex=True, sharey=True, tight_layout=True)
#fig, ((a4, a5), (a1, a2)) = plt.subplots(2,2, sharex=True, sharey=True, tight_layout=True)
a4.imshow(np.transpose([gb['O'], gb['o'], gb[''], 1-gb['o,O']], [1,2,0])); a4.set_title("Ref 1")
a5.imshow(np.transpose([gg['O'], gg['o'], gg[''], 1-gg['o,O']], [1,2,0])); a5.set_title("Ref 2")
a6.imshow(np.transpose([g3['O'], g3['o'], g3[''], 1-g3['o,O']], [1,2,0])); a6.set_title("Conjunctive (Unnormalized)")
a1.imshow(np.transpose([g0['O'], g0['o'], g0[''], 1-g0['o,O']], [1,2,0])); a1.set_title("Conjunctive")
a2.imshow(np.transpose([g1['O'], g1['o'], g1[''], 1-g1['o,O']], [1,2,0])); a2.set_title("Smet Python")
a3.imshow(np.transpose([g2['O'], g2['o'], g2[''], 1-g2['o,O']], [1,2,0])); a3.set_title("Smet CPP")
plt.show()
