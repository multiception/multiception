#! /usr/bin/env python3

from pathlib import Path
try:
    exec(open(Path(__file__).parent / "existence_evaluation.py").read())
except SystemExit as e:
    pass

posttracks_ = load_post_csv("/mnt/hdd/postprocessing/Overtaking3/objects")
manutracks_ = load_csv("/tmp/manuo3")
posttracks, manutracks = sync_gt_tracks(posttracks_, manutracks_)
gtgrids_ = load_images("/tmp/manuo3")
DEBUG = True
ASSOC_GATE = 10 # m
ASSOC_COST_MAX = 15
CACHEDIR = Path("/tmp/eval")
CACHEDIR.mkdir(parents=True, exist_ok=True)
scores = compute_scores_object_(posttracks, manutracks, ROC_condition_pl, 0)
