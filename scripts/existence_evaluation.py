#!/usr/bin/env python3

import IPython

import hashlib
import sys, csv, yaml, argparse
from tqdm import tqdm
from pathlib import Path
import pickle
import numpy as np
import pandas as pd
import rosbag
import cv2
from multiprocessing import Value
from concurrent.futures import ProcessPoolExecutor
import matplotlib.pyplot as plt
from matplotlib.cm import ScalarMappable, rainbow
from matplotlib.colors import Normalize
from copy import deepcopy
from dataclasses import dataclass
from sortedcontainers import SortedDict
from sklearn import metrics
from itertools import repeat
from functools import partial

from rospkg import RosPack
from roslaunch.substitution_args import resolve_args
from shapely.geometry import Point, Polygon, MultiPolygon, LineString
from shapely.validation import make_valid
from typing import List, Iterable, Dict, Union, NewType, Tuple
from map_lib import MapTile, LoaderSqlite, polygonFromLinks
from multiception.msg import Class
from multiception.tools import ID, Time, TimedDict, TimedIdDict, IdTimedDict
from multiception.belief import MassFunction
from multiception.evidential_grid_map import EvidentialGridMap
from multiception.association import AssociatorMahalanobis
np.set_printoptions(precision=2, linewidth=999, suppress=True, sign=' ')

@dataclass
class Object:
    # x, y, O generic state
    state: np.ndarray = np.zeros(3)
    covar: np.ndarray = np.zeros((3,3))
    state_local: np.ndarray = np.zeros(2)
    state_polar: np.ndarray = np.zeros(2)
    shape: np.ndarray = np.zeros(2)
    exist: np.ndarray = np.array([0,0,3]) # [0]: Exists, [1]: Dont Exist, [2]: Unknown
    is_sign: bool = False

    def __repr__(self):
        return '<(%0.2f %0.2f %0.2f)+-(%0.2f %0.2f %0.2f) (%0.2f %0.2f) %0.2f>' % (*self.state, *self.covar.diagonal(), *self.shape, self.exist[0])

class OccupancyGrid:
    FREE = -100
    UNKNOWN = 0
    OCCUPIED = 100
    def __init__(self, left: float, top: float, cell_size: float, content: np.ndarray):
        self._data = content.astype(np.int8)
        self.resolution = 1/cell_size
        self.left = left
        self.top = top

    @property
    def data(self): return self._data/float(OccupancyGrid.OCCUPIED)

    @property
    def right(self): return self.left+self._data.shape[1]/self.resolution

    @property
    def bot(self): return self.top-self._data.shape[0]/self.resolution

    @property
    def lefttop(self): return np.array((self.left,self.top,1))

    @property
    def rightbot(self): return np.array((self.right, self.bot, 1))

    @property
    def extent(self):
        return np.array((self.left, self.right, self.bot, self.top))

    @property
    def transfo_to_px(self):
        return np.array((
            (0, -self.resolution, self.top*self.resolution),
            (+self.resolution, 0, -self.left*self.resolution),
            (0, 0, 1),
        ))

    def to_px(self, pos):
        x, y = pos[:2]
        return tuple((self.transfo_to_px @ np.array([x,y,1])).astype(np.int32)[:2])

    def __getitem__(self, pos):
        idx = self.transfo_to_px @ pos
        return self._data[idx]/float(OccupancyGrid.OCCUPIED)

    def crop_to(self, othr):
        assert self.resolution==othr.resolution, f"Cannot crop on different resolutions {self.resolution}!={othr.resolution}"
        assert self.left<=othr.left and self.right>=othr.right and self.bot<=othr.bot and self.top>=othr.top, f"{self.extent} is too small to fit {othr.extent}"

        left, top, _ = (self.transfo_to_px @ othr.lefttop).astype(np.int32)
        cropped_data = self._data[left:(left+othr._data.shape[0]), top:(top+othr._data.shape[1])]
        cropped_self = OccupancyGrid(othr.left, othr.top, othr.resolution, cropped_data)
        assert cropped_self._data.shape==othr._data.shape, f"Cropped self is smaller than argument despite being theoretically fitting"
        return cropped_self

    def __repr__(self):
        return f"[{self.left:.2f},{self.right:.2f},{self.bot:.2f},{self.top:.2f} x{self.resolution:.1f}x{self._data.shape}]"

TimedDicT = List#NewType('TimedDicT', TimedDict)
Objects = Dict[ID, Object]
Tracks = TimedDicT[Objects]
OccupancyGrids = TimedDicT[OccupancyGrid]
Association = Tuple[Objects, Objects]
Associations = TimedDicT[List[Association]]
Score = Tuple[float, float, float, float, float, float, float]
Scores = TimedDicT[Score]
RangeBin = float

def parse_args():
    parser = argparse.ArgumentParser(description='Compare the percepts of a rosbag with a ground-truth')
    parser.add_argument('-f', "--force", help='Force computations even if cache exists', action="store_true")
    parser.add_argument('-d', "--debug", help='Activate strong debugging', action="store_true")
    parser.add_argument('-s', "--skip-signs", help='Ignore tracked and gt signs', action="store_true")
    parser.add_argument('-e', "--eval-post", help='Evaluate postprocessing', action="store_true")
    parser.add_argument('-p', "--plot", help='Plot results directly', action="store_true")
    parser.add_argument('-c', "--cache", help='Path to the cache directory (/tmp/SOMEHASH by default)', type=str, default=None)
    parser.add_argument('-t', "--start", type=float, help='Time to start comparisons', default=None)
    parser.add_argument('-l', "--local", type=float, help='Limit tracks and gt within this range', default=None)
    parser.add_argument('-g', "--ground-truth", type=str, help='Path to the parent folder where ground-truth CSVs for each scenarios is stored', default=Path.home()/"GT")
    parser.add_argument('SCENARIO', type=str, help='Path to the folder containing ground-truth CSVs')
    parser.add_argument('PATH_PERCEPTS', type=str, help='Path to the perception rosbag')
    parser.add_argument('TOPIC_PERCEPTS', type=str, help='Topic to compare in the percepts rosbag')
    parser.add_argument('--pose-topic', type=str, help='Topic with poses to transform in local frame', required=True)
    return parser.parse_args()

def load_bag(bagpath: str, topic: str, with_heading: bool=False) -> Tracks:
    percepts = TimedIdDict()
    grids = TimedDict()

    with rosbag.Bag(bagpath) as bag:
        info_dict = yaml.safe_load(bag._get_yaml_info())
        nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic']==topic])

        for topic, msg, t in tqdm(bag.read_messages(topics=[topic]), total=nMsg, desc=f'Loading messages on {topic}'):
            t = msg.header.stamp.to_sec()

            # Parse detectability
            grid = EvidentialGridMap.from_msg(msg.sources[0].observability)
            grids[t] = OccupancyGrid(grid.getPosition()[0]-grid.getLength()[0]/2, grid.getPosition()[1]+grid.getLength()[1]/2, grid.getResolution(), np.rot90(grid['o']-grid['O'], -1)*OccupancyGrid.OCCUPIED)

            # Parse objects
            for p in msg.perceptions:
                state = np.array([p.state.x, p.state.y, p.state.O])
                #covar = np.reshape(p.state.cov, (6,6)) + np.reshape(p.state.cov_ind, (6,6))
                covar = np.reshape(p.state.covi, (5,5)) + np.reshape(p.state.covd, (5,5)) # New version
                if not with_heading: covar[2,2] = 100
                m = MassFunction.from_msg(p.existence)
                exist = np.array([m['e'], m['E'], m['eE']])
                shape = np.array((p.length, p.width))
                is_sign = (p.classif.value >= Class.SIGN_UNKNOWN)
                percepts[t, p.object_id] = Object(state=state, covar=covar[:3,:3], exist=exist, shape=shape, is_sign=is_sign)

    return percepts, grids

def load_csv(basepath: Union[str, Path]) -> Tracks:
    gttracks = TimedIdDict()
    basepath = Path(basepath).expanduser().resolve()
    paths = list(basepath.glob('*.csv'))
    for path in tqdm(paths, total=len(paths), desc='Loading Ground Truth'):
        with open(path) as f:
            oid = int(path.stem)
            lines = csv.reader(f, delimiter=',')
            next(lines, None) # Skip header
            for t, x, y, O, v, l, w in lines:
                state = np.array([x, y, O], dtype=float)
                covar = np.identity(3)*0.5**2
                exist = np.array([1, 0, 0])
                shape = np.array([l, w], dtype=float)
                is_sign = (oid>=10000)
                gttracks[float(t), oid] = Object(state=state, covar=covar,exist=exist, shape=shape, is_sign=is_sign)

    return gttracks

def load_post_csv(basepath: Union[str, Path]) -> Tracks:
    gttracks = TimedIdDict()
    basepath = Path(basepath).expanduser().resolve()
    paths = list(basepath.glob('*.csv'))
    for path in tqdm(paths, total=len(paths), desc='Loading Post Ground Truth'):
        with open(path) as f:
            t = float(path.stem)
            lines = csv.reader(f, delimiter=',')
            for oid, (x, y, O, v, l, w) in enumerate(lines):
                state = np.array([x, y, O], dtype=float)
                covar = np.identity(3)*0.1**2
                exist = np.array([1, 0, 0])
                shape = np.array([l, w], dtype=float)
                is_sign = (oid>=10000)
                gttracks[float(t), oid] = Object(state=state, covar=covar,exist=exist, shape=shape, is_sign=is_sign)

    return gttracks

def load_images(basepath: Union[str, Path]) -> np.ndarray:
    basepath = Path(basepath).expanduser().resolve(strict=True)
    imgs = TimedDict()
    paths = list(basepath.glob('*.png'))
    for path in tqdm(paths, total=len(paths), desc='Loading Ground Truth grids'):
        img = cv2.imread(str(path), cv2.IMREAD_UNCHANGED).T
        with open(path.with_suffix(".yml")) as f:
            infos = yaml.safe_load(f)
            data = np.rot90((img >= 254)*OccupancyGrid.OCCUPIED + (img < 1)*OccupancyGrid.FREE)[::-1,:]
        imgs[float(path.stem)] = OccupancyGrid(*infos['lefttop_corner'], infos['cell_size'], data)
    return imgs

def linear_interp_track(ratio: float, track_before: Object, track_after: Object) -> Object:
    track = deepcopy(track_before)
    track.state = track_before.state*(1-ratio) + track_after.state*ratio
    track.covar = track_before.covar*(1-ratio) + track_after.covar*ratio

    return track

def sync_gt_tracks(tracks: Tracks, gttracks: Tracks) -> Tuple[Tracks, Tracks]:
    '''
    Interpolates tracks to given times
    Drop reference when GT is not available (e.g because times are not aligned)
    '''
    if len(tracks)==1:
        # This is static information, repeat for all times
        #return TimedIdDict((t, deepcopy(tracks.values()[0])) for t in times)
        return tracks, TimedIdDict((t, deepcopy(gttracks.values()[0])) for t in times)

    synced = TimedIdDict()
    synced_gt = TimedIdDict()
    static = tracks.get(0.0, {})
    static_gt = gttracks.get(0.0, {})
    for t, tracks_t in tqdm(tracks.items(), total=len(tracks), desc='Synchronizing Tracks'):
        try:
            ratio_gt, gt_before, gt_after = gttracks.prepare_interp(t)
            for i in (set(gt_before.keys()) | set(gt_after.keys())):
                if i not in gt_before:
                    synced_gt[t, i] = gt_after[i]
                elif i not in gt_after:
                    synced_gt[t, i] = gt_before[i]
                else:
                    synced_gt[t, i] = linear_interp_track(ratio_gt, gt_before[i], gt_after[i])
            for static_i, static_track in static_gt.items():
                synced_gt[t, static_i] = deepcopy(static_track)

            synced[t] = deepcopy(tracks_t)
            for static_i, static_track in static.items():
                synced[t, static_i+1000] = deepcopy(static_track)
        except ValueError as e:
            print(f"Received {e}, dropping frame", file=sys.stderr)

    return synced, synced_gt

def sync_gt_grids(grids: OccupancyGrids, gtgrids: OccupancyGrids) -> Tuple[OccupancyGrids, OccupancyGrids]:
    '''
    Interpolates two sets of grids (typically reference and GT)
    Crop the second set to focus on the same areas as the first one
    Drop reference when GT is not available (e.g because times are not aligned)
    '''
    synced = TimedDict()
    synced_gt = TimedDict()
    ref_to_drop = {}
    for t, grid in tqdm(grids.items(), total=len(grids), desc='Synchronizing Grids'):
        if len(gtgrids)==1:
            synced_gt[t] = deepcopy(gtgrids.values()[0])
            synced[t] = grids[t]
        else:
            try:
                ratio_gt, gt_before, gt_after = gtgrids.prepare_interp(t)
                synced_gt[t] = (gt_before if ratio_gt <= 0.5 else gt_after).crop_to(grid)
                synced[t] = grid
            except ValueError as e:
                print(f"Received {e}, dropping frame", file=sys.stderr)

    return synced, synced_gt

def transfo_to_matrix(x, y, O):
    c, s = np.cos(O), np.sin(O)
    return np.array([
        [c, -s, x],
        [s,  c, y],
        [0,  0, 1]
    ])

def transform_local(tracks: Tracks, poses: Tracks) -> Tracks:
    poses = TimedDict((t, v) for t, iv in poses.items() for v in iv.values())

    # Remove the first or last track time if poses and states dont perfectly overlap
    missing_times = set(tracks).difference(poses)
    for t in missing_times:
        del tracks[t]

    for t, iv in tqdm(tracks.items(), total=len(tracks), desc='Transforming in local reference frame'):
        transfo = np.linalg.inv(transfo_to_matrix(*poses[t].state))
        for v in iv.values():
            v.state_local = ( transfo @ np.hstack((v.state[:2], [1])) )[:2]
            v.state_polar = np.array((np.linalg.norm(v.state_local), np.arctan2(v.state_local[1], v.state_local[0])))

    return tracks

def bin_by_range(assocs: List[Association], tracks: Objects, gttracks: Objects, bin_size: float) -> Dict[float, Tuple[List[Association], Objects, Objects]]:
    ranged_assocs = SortedDict((a.state_polar[0], (a, b)) for a, b in assocs)
    ranged_tracks = SortedDict((a.state_polar[0], a) for a in tracks.values())
    ranged_gttracks = SortedDict((a.state_polar[0], a) for a in gttracks.values())

    #print([(hex(id(k)), k.state_local, hex(id(v)), v.state_local) for k, v in assocs])
    #print('')
    #print([(hex(id(k)), k, hex(id(v)), v.state_local) for k, v in tracks.items()])
    #print('')
    #print([(hex(id(k)), k, hex(id(v)), v.state_local) for k, v in gttracks.items()])
    #print('')
    #print('')
    idx_low_ass = idx_low_tra = idx_low_gtt = 0
    bins = SortedDict()
    range_max = max(*ranged_assocs.keys(), *ranged_tracks.keys(), *ranged_gttracks.keys())
    print(max(ranged_assocs.keys(), default=0), max(ranged_tracks.keys(), default=0), max(ranged_gttracks.keys(), default=0), range_max)
    #print([id(a) for a in gttracks.values()])
    #print(list(ranged_assocs.keys()))
    #print(list(ranged_tracks.keys()), list(tracks.keys()))
    #for k, v in gttracks.items():
        #print('    ', k, v.state, v.state_local, v.state_polar)
    #print(list(ranged_gttracks.keys()), list(gttracks.keys()), [t.state_polar[:2] for t in gttracks.values()])
    for r in np.arange(0, range_max+bin_size, bin_size):
        idx_upp_ass = ranged_assocs.bisect_left(r)
        idx_upp_tra = ranged_tracks.bisect_left(r)
        idx_upp_gtt = ranged_gttracks.bisect_left(r)
        #print(f'{r} {idx_low_ass}:{idx_upp_ass} {idx_low_tra}:{idx_upp_tra} {idx_low_gtt}:{idx_upp_gtt}')
        bins[r] = (ranged_assocs.values()[idx_low_ass:idx_upp_ass], ranged_tracks.values()[idx_low_tra:idx_upp_tra], ranged_gttracks.values()[idx_low_gtt:idx_upp_gtt])
        idx_low_ass = idx_upp_ass
        idx_low_tra = idx_upp_tra
        idx_low_gtt = idx_upp_gtt

    return bins

def compute_ranged_scores(assocs: Associations, tracks: Tracks, gttracks: Tracks) -> TimedDicT[Dict[RangeBin, Score]]:
    scores = TimedDict()

    for t in tqdm(assocs.keys(), total=len(assocs), desc='Computing association scores'):
        scores[t] = SortedDict()
        bins = bin_by_range(assocs[t], tracks[t], gttracks[t], 2)

        for r, (assocs_r, tracks_r, gttracks_r) in bins.items():
            n_assocs, n_tracks, n_gttracks = len(assocs_r), len(tracks_r), len(gttracks_r)
            TP = n_assocs
            FP = n_tracks - n_assocs
            FN = n_gttracks - n_assocs
            TN = 0 # TODO with detectability

            precision = TP / (TP+FP) if (TP+FP)!=0 else 0
            recall = TP / (TP+FN) if (TP+FN)!=0 else 0
            f1 = 2 * (precision*recall)/(precision+recall) if (precision+recall)!=0 else 0

            scores[t][r] = np.array((TP, FP, FN, TN, precision, recall, f1))

    binned_scores = SortedDict()
    for t, rv in scores.items():
        for r, v in rv.items():
            binned_scores[r] = binned_scores.get(r, np.zeros(7)) + v

    return binned_scores

def plot_tracks(tracks: Tracks, ax, local=False, **kwargs):
    # Swap times and IDs
    tracks_swapped = tracks.swap()
    for i, tv in tracks_swapped.items():
        trace = np.array([v.state_local[:2] for v in tv.values()] if local else [v.state[:2] for v in tv.values()])
        ax.plot(trace[:,0], trace[:,1], **kwargs)

def make_object_poly(tracks):
    return np.stack([
        transfo_to_matrix(*track.state) @
        [[ track.shape[0]/2, -track.shape[0]/2, -track.shape[0]/2,  track.shape[0]/2],
         [ track.shape[1]/2,  track.shape[1]/2, -track.shape[1]/2, -track.shape[1]/2],
         [                1,                 1,                 1,                 1]]
    for track in tracks])

def compute_scores_object_scikit(tracks: Tracks, gttracks: Tracks) -> Score:
    y_true = []
    y_pred = []
    associator = AssociatorMahalanobis(gate=ASSOC_GATE, cost_max=ASSOC_COST_MAX)
    for i, t in enumerate(tracks.keys()):
        trackids_t, tracks_t = list(tracks[t].keys()), list(tracks[t].values())
        gttrackids_t, gttracks_t = list(gttracks[t].keys()), list(gttracks[t].values())
        TP = { trackids_t[i]: gttrackids_t[j] for i, j in zip(*associator.associate(tracks_t, gttracks_t)) }
        FP = set(trackids_t) - set(TP.keys())
        FN = set(gttrackids_t) - set(TP.values())

        y_pred.extend(tracks[t][i].exist[0] for i in TP.keys())
        y_true.extend(repeat(1, len(TP)))

        y_pred.extend(tracks[t][i].exist[0] for i in FP)
        y_true.extend(repeat(0, len(FP)))

        y_pred.extend(repeat(0, len(FN)))
        y_true.extend(repeat(1, len(FN)))

    return y_true, y_pred

def compute_scores_object_(tracks: Tracks, gttracks: Tracks, condition, thresh: float) -> Score:
    nTP = 0
    nFP = 0
    nFN = 0
    nTN = 0
    associator = AssociatorMahalanobis(gate=ASSOC_GATE, cost_max=ASSOC_COST_MAX)

    if DEBUG:
        gtgrid0 = gtgrids_.values()[0]
        DEBUGOUT = Path(CACHEDIR, f"{thresh:.2f}")
        DEBUGOUT.mkdir(exist_ok=True)

    for t in tracks.keys():
        gttrackids_t, gttracks_t = list(gttracks[t].keys()), list(gttracks[t].values())
        gen = ((k,v) for k,v in tracks[t].items() if condition(v, thresh))
        tracktuple = tuple(map(list, zip(*gen)))
        if len(tracktuple)>0:
            trackids_t, tracks_t = tracktuple
            TP = { trackids_t[i]: gttrackids_t[j] for i, j in zip(*associator.associate(tracks_t, gttracks_t)) }
            FP = set(trackids_t) - set(TP.keys())
        else:
            TP = dict()
            FP = set()
        FN = set(gttrackids_t) - set(TP.values())

        # Update track scores
        nTP += len(TP)
        nFP += len(FP)
        nFN += len(FN)

        if DEBUG:
            #imgt = 255*np.ones(grid_t._data.shape+(3,), dtype=np.float32)
            SCALE = 4
            img0 = 255*np.ones((gtgrid0._data.shape[0]*SCALE, gtgrid0._data.shape[1]*SCALE, 3), dtype=np.uint8)
            g0 = OccupancyGrid(gtgrid0.left, gtgrid0.top, 1/gtgrid0.resolution*SCALE, img0)

            # Draw road
            road_poly = np.hstack(( road[t].exterior.coords, np.ones((len(road[t].exterior.coords), 1)) )).T
            road_poly_px = (g0.transfo_to_px @ road_poly).T[:,:2].astype(np.int32)
            cv2.fillPoly(img0, [road_poly_px], (210, 210, 210))

            # Draw matches
            for trackid, gttrackid in TP.items():
                cv2.circle(img0, g0.to_px(  tracks[t][  trackid].state), 7, (  0, 255,   0), -1)
                cv2.circle(img0, g0.to_px(gttracks[t][gttrackid].state), 6, (255,   0,   0), -1)
            for trackid in FP:
                cv2.circle(img0, g0.to_px(  tracks[t][  trackid].state), 5, (  0, 128, 255), -1)
            for gttrackid in FN:
                cv2.circle(img0, g0.to_px(gttracks[t][gttrackid].state), 4, (  0,   0, 255), -1)

            # Legend
            img0 = np.ascontiguousarray(np.rot90(img0[::-1], -1))
            for (u, v), color, text in (
                ((10, 15), (  0, 255,   0), "Associated T (TP)"),
                ((10, 30), (255,   0,   0), "Associated GT (TP)"),
                ((10, 45), (  0, 128, 255), "Ghost (FP)"),
                ((10, 60), (  0,   0, 255), "Missed GT (FN)"),
            ):
                cv2.circle(img0, (u, v), 4, color, -1)
                cv2.putText(img0, text, (u+10, v+5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1, cv2.LINE_AA)

            cv2.imwrite(f"{DEBUGOUT}/{t:.2f}.png", img0)

    return thresh, nTP, nFP, nFN, nTN

def compute_scores_grid_(grids: OccupancyGrids, gtgrids: OccupancyGrids, thresh: float) -> Score:
    nTP = 0
    nFP = 0
    nFN = 0
    nTN = 0

    if DEBUG:
        #gtgrid0 = gtgrids_.values()[0]
        DEBUGOUT = Path(CACHEDIR, f"{thresh:.2f}")
        DEBUGOUT.mkdir(exist_ok=True)

    for t in grids.keys():
        data_t, gtdata_t = grids[t].data, gtgrids[t]._data
        fre = (data_t<thresh)
        occ = ((~fre) & (np.abs(data_t)>0.001))
        gtfre, gtocc = (gtdata_t==OccupancyGrid.FREE), (gtdata_t==OccupancyGrid.OCCUPIED)

        TP = gtfre & fre
        FP = gtocc & fre
        FN = gtfre & occ
        TN = gtocc & occ
        nTP += np.sum(TP)
        nFP += np.sum(FP)
        nFN += np.sum(FN)
        nTN += np.sum(TN)

        if DEBUG:
            #img0 = 255*np.ones((gtgrid0._data.shape[0], gtgrid0._data.shape[1], 3), dtype=np.uint8)
            #g0 = OccupancyGrid(gtgrid0.left, gtgrid0.top, gtgrid0.resolution, img0)

            img = 255*np.ones((TP.shape[0], TP.shape[1], 3), dtype=np.uint8)
            img[gtfre|gtocc] = [128, 128, 128]
            img[TP] = [  0, 255,   0]
            img[FP] = [  0, 128, 255]
            img[FN] = [  0,   0, 255]
            img[TN] = [255,   0,   0]
            #top, left = g0.to_px(grids[t].lefttop)
            #img0[left:(left+img.shape[0]), top:(top+img.shape[1])] = img

            cv2.imwrite(f"{DEBUGOUT}/{t:.2f}-occ.png", img)

    return thresh, nTP, nFP, nFN, nTN

def compute_errors_(tracks: Tracks, gttracks: Tracks, condition, thresh: float) -> Score:
    errcovs = TimedIdDict()
    associator = AssociatorMahalanobis(gate=ASSOC_GATE, cost_max=ASSOC_COST_MAX)

    for t in tracks.keys():
        tracks_t = list(filter(partial(condition, thresh=thresh), tracks[t].values()))
        gttracks_t = list(gttracks[t].values())
        gttrackids_t = list(gttracks[t].keys())

        TP = [(tracks_t[i], gttracks_t[j]) for i, j in zip(*associator.associate(tracks_t, gttracks_t))]
        for i, j in zip(*associator.associate(tracks_t, gttracks_t)):
            covar = gttracks_t[j].covar + tracks_t[i].covar
            error = np.hstack((gttracks_t[j].state - tracks_t[i].state, gttracks_t[j].shape - tracks_t[i].shape, gttracks_t[j].shape - tracks_t[i].shape[::-1]))
            error[2] = np.sign(error[2])*np.mod(np.abs(error[2]), np.pi) # Normalize angle diff
            errcovs[t, gttrackids_t[j]] = (error, covar)

    return thresh, errcovs

def compute_scores_object_multiprocessing(*args):
    return compute_scores_object_(TRACKS, GTTRACKS, COND_FUNC, *args)

def compute_scores_grid_multiprocessing(*args):
    return compute_scores_grid_(GRIDS, GTGRIDS, *args)

def compute_errors_multiprocessing(*args):
    return compute_errors_(TRACKS, GTTRACKS, COND_FUNC, *args)

def ROC_condition_pl(o: Object, thresh: float) -> bool: return (1-o.exist[1])>thresh
def ROC_condition_bel(o: Object, thresh: float) -> bool: return o.exist[0]>thresh
def ROC_condition_pig(o: Object, thresh: float) -> bool: return (o.exist[0]+.5*o.exist[2])>thresh

# Global variables for shared and read-only variables used in compute_ROC_curve
# because python sucks at read-only inter-process shared-memory.
# By doing this we avoid copying too much as memory is normally only copied when written to (COW)
TRACKS = None
GRIDS = None
GTTRACKS = None
GTGRIDS = None
COND_FUNC = None

def compute_scores_object(tracks: Tracks, gttracks: Tracks) -> np.ndarray:
    thresholds = np.linspace(0, 1, NTHRESH)

    global TRACKS; TRACKS = tracks
    global GTTRACKS; GTTRACKS = gttracks
    global COND_FUNC; COND_FUNC = ROC_condition_bel
    with ProcessPoolExecutor() as p:
        scores = np.array(list(p.map(compute_scores_object_multiprocessing, thresholds)))
    #scores = np.array([compute_scores_object_multiprocessing(thresh) for thresh in [.41]])

    return scores

def compute_scores_grid(grids: OccupancyGrids, gtgrids: OccupancyGrids) -> np.ndarray:
    thresholds = np.linspace(0, 1, NTHRESH)

    global GRIDS; GRIDS = grids
    global GTGRIDS; GTGRIDS = gtgrids
    with ProcessPoolExecutor() as p:
        scores = np.array(list(p.map(compute_scores_grid_multiprocessing, thresholds)))
    #scores = np.array([compute_scores_grid_multiprocessing(thresh) for thresh in [.41]])

    return scores

def compute_errors(tracks: Tracks, gttracks: Tracks) -> np.ndarray:
    thresholds = np.linspace(0, 1, NTHRESH)

    global TRACKS; TRACKS = tracks
    global GTTRACKS; GTTRACKS = gttracks
    global COND_FUNC; COND_FUNC = ROC_condition_bel
    with ProcessPoolExecutor() as p:
        ret = list(p.map(compute_errors_multiprocessing, thresholds))
    #ret = list(map(compute_errors_multiprocessing, [0.41]))

    errcovs = { thresh: errcovs.swap() for thresh, errcovs in ret }
    return errcovs

def gen_road(poses: Tracks, maptile, dist=None):
    polys_road = TimedDict()
    for t, pose in tqdm(poses.items(), total=len(poses), desc='Generating Road'):
        # Map match self pose
        links = { i: LineString(link.polyline[:2].T) for i, link in maptile.links.items() }
        ids = np.fromiter(links.keys(), dtype=np.int32)
        if dist is None:
            tile = maptile
        else:
            pose = Point(list(pose.values())[0].state[:2])
            distances = np.fromiter((pose.distance(link) for link in links.values()), dtype=np.float32)
            ids_matched = ids[distances<10]
            # Generate polylines up to a distance
            tile = maptile.getSubDirected(ids_matched, dist, True)
            tile.unionWith(maptile.getSubDirected(ids_matched, dist, False))

        # Generate road polygon
        polys_link = [Polygon(poly.T) for poly in polygonFromLinks(tile, 1, 7).values()]
        poly_road = Polygon()
        for p in polys_link:
            if p.is_valid: poly_road = poly_road.union(p)
            else:
                p = make_valid(p)
                if isinstance(p, Polygon):
                    poly_road = poly_road.union(p)
                else:
                    for q in p.geoms:
                        poly_road = poly_road.union(q)
        polys_road[t] = poly_road

    return polys_road

def precondition(tracks: Tracks, road: TimedDicT[Polygon], take_signs) -> Tracks:
    conditionned_tracks = TimedIdDict()
    for t, itrack in tracks.items():
        for i, track in itrack.items():
            if (take_signs or not track.is_sign) and (road is None or Point(track.state[:2]).within(road[t])):
                conditionned_tracks[t, i] = track
    return conditionned_tracks

def P(thresh, TP, FP, FN, TN): return TP+FP
def N(thresh, TP, FP, FN, TN): return TN+FN
def non_null_frac(A, B, default=0):
	# Make sure result is 1 if A==0 by filling with B in those rows
	#C = np.zeros_like(A)
	#C[A!=0] = A[A!=0]
	#C[A==0] = B[A==0]
	#print((A[A!=0]).shape, (B[A==0]).shape, (C==0).sum())
	res = A / (A+B)
	if False:
		res[(res<0.0000001) | (np.isnan(res))] = default
	return res

def TPR(thresh, TP, FP, FN, TN): return non_null_frac(TP, FN, 0)
def TNR(thresh, TP, FP, FN, TN): return non_null_frac(TN, FP)
def FNR(thresh, TP, FP, FN, TN): return non_null_frac(FN, TP)
def FPR(thresh, TP, FP, FN, TN): return non_null_frac(FP, TN)
def PPV(thresh, TP, FP, FN, TN): return non_null_frac(TP, FP)
def precision(thresh, TP, FP, FN, TN): return PPV(thresh, TP, FP, FN, TN)
def recall(thresh, TP, FP, FN, TN): return TPR(thresh, TP, FP, FN, TN)
def f1(thresh, TP, FP, FN, TN):
    p = precision(thresh, TP, FP, FN, TN)
    r = recall(thresh, TP, FP, FN, TN)
    return 2 * (p*r)/(p+r)

def disp_ROC(scores, ax=None, **kwargs):
    if ax is None: ax = plt.gca()
    fpr, tpr = FPR(*scores.T), TPR(*scores.T)
    ax.plot(fpr, tpr, **kwargs)
    ax.axis((-0.01, 1.01, -0.01, 1.01))
    ax.set_xlabel("False Positive Rate")
    ax.set_ylabel("True Positive Rate")
    return fpr, tpr

def disp_PR(scores, ax=None, annotate=False, **kwargs):
    if ax is None: ax = plt.gca()
    p, r = precision(*scores.T), recall(*scores.T)
    if not annotate:
        cond = (~np.isnan(p)) & (~np.isnan(r))
        p, r = p[cond], r[cond]
    ax.plot(r, p, **kwargs)
    if annotate:
        for thresh, x_, y_ in zip(scores[:,0], r, p): 
            ax.annotate(f"{thresh:.2f}", xy=(x_, y_), textcoords='data')
    ax.axis((-0.01, 1.01, -0.01, 1.01))
    ax.set_xlabel("Recall")
    ax.set_ylabel("Precision")
    return p, r

def disp_DET(scores, ax=None, **kwargs):
    if ax is None: ax = plt.gca()
    ax.plot(FPR(*scores.T), FNR(*scores.T), **kwargs)
    ax.axis((-0.01, 1.01, -0.01, 1.01))
    ax.set_xlabel("False Positive Rate")
    ax.set_ylabel("False Negative Rate")
    return ax

def disp_TOC(scores, ax=None, **kwargs):
    if ax is None: ax = plt.gca()
    # https://towardsdatascience.com/roc-vs-toc-bccb7d70ef07?gi=8d2928d44ba7
    tpr = TPR(*scores.T)
    p = P(*scores.T).max()
    n = N(*scores.T).max()
    y = p*TPR(*scores.T)
    x = n*FPR(*scores.T)+y
    ax.plot(x, y, **kwargs)
    for thresh, x_, y_ in zip(scores[:,0], x, y): ax.annotate(f"{thresh:.2f}", xy=(x_, y_), textcoords='data')
    ax.plot([0, p], [0, p], linestyle="dashdot")
    ax.plot([n, n+p], [0, p], linestyle="dashdot")
    ax.set_xlabel("Hits + False Alarms")
    ax.set_ylabel("Hits")
    return ax

def disp_errors(errcovs, times, threshfilt=None):
    fig, axes = None, None
    errnan = np.full((7,), np.nan)
    covnan = np.full((3,3), np.nan)
    for thresh, errcovs_t in errcovs.items():
        if threshfilt and thresh not in threshfilt: continue
        if fig is None:
            fig, axes = plt.subplots(len(errcovs_t), 1, sharex=True, sharey=True)

        for i, errcov in enumerate(errcovs_t.values()):
            errs = np.array([(errcov[t][0] if t in errcov else errnan) for t in times])
            covs = np.array([(errcov[t][1] if t in errcov else covnan) for t in times])

            # Rotate covariance with the error vector
            errnorms = np.linalg.norm(errs[:,:2], axis=1)
            rot = np.stack([ errs[:, 0]/errnorms, errs[:, 1]/errnorms], axis=1)[:, None, :]
            rotated_covs = rot @ covs[:, :2,:2] @ rot.transpose(0, 2, 1)
            covnorms = 3*np.sqrt(rotated_covs[:, 0, 0])
            axes[i].plot(times, errnorms)
            axes[i].plot(times, covnorms)
            print(i, times, errnorms, covnorms)

def disp_mean_errors(errcovs, ax=None, threshfilt=None, samples: int=None, label=None, **kwargs):
    if ax is None: ax = plt.gca()
    if samples is not None:
        threshs = np.fromiter(errcovs.keys(), dtype=float)
        threshfilt = threshs[(len(threshs)*np.cumsum(np.repeat(1/(samples+1), samples))).astype(int)]
    times = sorted(set(t for thresholds in errcovs.values() for ids in thresholds.values() for t in ids.keys()))
    times = np.array(times)

    errnan = np.full((7,), np.nan)
    covnan = np.full((3, 3), np.nan)
    errs_t = []
    covs_t = []
    for thresh, errcovs_t in errcovs.items():
        if threshfilt and thresh not in threshfilt: continue

        for i, errcov in enumerate(errcovs_t.values()):
            errs = np.array([(errcov[t][0] if t in errcov else errnan) for t in times])
            covs = np.array([(errcov[t][1] if t in errcov else covnan) for t in times])

            # Rotate covariance with the error vector
            errnorms = np.linalg.norm(errs[:,:2], axis=1)
            errs_t.append(errnorms)

            rot = np.stack([ errs[:, 0]/errnorms, errs[:, 1]/errnorms], axis=1)[:, None, :]
            rotated_covs = rot @ covs[:, :2,:2] @ rot.transpose(0, 2, 1)
            covnorms = 3*np.sqrt(rotated_covs[:, 0, 0])
            covs_t.append(covnorms)

    times -= times.min()
    mean_err = np.nanmean(errs_t, axis=0)
    mean_cov = np.nanmean(covs_t, axis=0)
    line = ax.plot(times, mean_err, label=label)
    ax.plot(times, mean_cov, c=line[0].get_color(), linestyle="--")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Error (m)")
    ax.axis((times[0], times[-1], 0, 10))

    return mean_err, mean_cov

def disp_size_errors(errcovs, times, ax=None, threshfilt=None):
    if ax is None: ax = plt.gca()
    errnan = np.full((7,), np.nan)
    for thresh, errcovs_t in errcovs.items():
        if threshfilt and thresh not in threshfilt: continue
        errs = np.array([e[3:5] for errcov in errcovs_t.values() for t, (e, c) in errcov.items()])
        ax.scatter(errs[:,0], errs[:,1])
        print(f"Size ({thresh}): RMSE: {np.linalg.norm(errs, axis=1).mean()}, Var: {np.linalg.norm(errs, axis=1).var()}, MeanLen: {np.mean(errs[:,0])}, VarLen: {np.var(errs[:,0])}, MeanWid: {np.mean(errs[:,1])}, VarWid: {np.var(errs[:,1])}")
    ax.set_xlabel("Length error (m)")
    ax.set_ylabel("Width error (m)")
    ax.grid(alpha=0.1)
    ax.axis("equal")

def disp_local_errors(errcovs, poses, ax=None, threshfilt=None):
    if ax is None: ax = plt.gca()

    # Transform errors in local reference frame
    posest = TimedDict((t, v) for t, iv in poses.items() for v in iv.values())
    errlocs_ = []
    for thresh, itv in errcovs.items():
        if threshfilt and thresh not in threshfilt: continue
        for i, tv in itv.items():
            for t, v in tv.items():
                p = np.linalg.inv(transfo_to_matrix(0, 0, posest[t].state[2])) @ np.hstack((v[0][:2], [1]))
                errlocs_.append((thresh, i, p[0], p[1]))

    errlocs = np.array(errlocs_)
    norms = np.linalg.norm(errlocs[:,2:4], axis=1)
    norms_sorted = np.take(norms, np.argsort(norms), axis=0)
    errlocs_sorted = np.take(errlocs, np.argsort(norms), axis=0)
    norms_95 = norms_sorted[:int(len(norms)*0.95)]
    errlocs_95 = errlocs_sorted[:int(len(errlocs)*0.95)]

    ax.scatter(errlocs_95[:,2], errlocs_95[:,3], alpha=0.3)
    ax.axis("equal")
    ax.grid(alpha=0.1)
    ax.set_xlabel("Lateral error (m)")
    ax.set_ylabel("Longitudinal error (m)")
    print(f"RMSE: {norms_95.mean()}, Var: {errlocs_95.var()}, MeanLon: {np.mean(errlocs_95[:,3])}, VarLon: {np.var(errlocs_95[:,3])}, MeanLat: {np.mean(errlocs_95[:,2])}, VarLat: {np.var(errlocs_95[:,2])}")

def disp_stanford(errcovs, ax=None, threshfilt=None, color=None, samples: int=None, label=None):
    if ax is None: ax = plt.gca()

    maxs = 0
    if samples is not None:
        threshs = np.fromiter(errcovs.keys(), dtype=float)
        threshfilt = threshs[(len(threshs)*np.cumsum(np.repeat(1/(samples+1), samples))).astype(int)]

    for thresh, errcovs_t in errcovs.items():
        if len(errcovs_t)==0 or (threshfilt is not None and thresh not in threshfilt): continue
        time = np.array([t for errcov in errcovs_t.values() for t, (e, c) in errcov.items()])
        errs = np.array([e for errcov in errcovs_t.values() for t, (e, c) in errcov.items()])
        covs = np.array([c for errcov in errcovs_t.values() for t, (e, c) in errcov.items()])

        # Rotate covariance with the error vector
        errnorms = np.linalg.norm(errs[:,:2], axis=1)
        rot = np.stack([ errs[:, 0]/errnorms, errs[:, 1]/errnorms], axis=1)[:, None, :]
        rotated_covs = rot @ covs[:, :2,:2] @ rot.transpose(0, 2, 1)
        covnorms = 3*np.sqrt(rotated_covs[:, 0, 0])
        maxs = max(maxs, np.max(errnorms), np.max(covnorms))

        if color is not None:
            cargs = { "c": np.repeat([color], len(errnorms), axis=0) }
        else:
            cargs = { "c": np.repeat(thresh, len(errnorms)), "vmin": 0, "vmax": 1, "cmap": rainbow }
        ax.scatter(errnorms, covnorms, alpha=0.01*thresh, **cargs)

    if label is not None:
        ax.scatter(errnorms[0], covnorms[0], cargs["c"][0], label=label)

    ax.plot((0, maxs), (0, maxs), 'k')
    ax.axis("equal")
    if color is None:
        plt.colorbar(ScalarMappable(norm=Normalize(vmin=0., vmax=1.), cmap=rainbow), ax=ax)

def assiou(TPs, FPs, FNs, ti, gi):
    TPA = sum(1 for TP in TPs.values() for t, g in TP.items() if ti==t and gi==g)
    FNA = sum(1 for TP in TPs.values() for t, g in TP.items() if ti!=t and gi==g) \
        + sum(1 for FN in FNs.values() for g in FN if gi==g)
    FPA = sum(1 for TP in TPs.values() for t, g in TP.items() if ti==t and gi!=g) \
        + sum(1 for FP in FPs.values() for t in FP if ti==t)
    return TPA / (TPA+FPA+FNA)

def HOTA(thresh):
    TP = TimedDict()
    FP = TimedDict()
    FN = TimedDict()

    for t in tracks.keys():
        gen = ((k,v) for k,v in tracks[t].items() if v.exist[0]>=thresh)
        trackids_t, tracks_t = map(list, zip(*gen))
        gttrackids_t, gttracks_t = map(list, zip(*gttracks[t].items()))

        TP[t] = { trackids_t[i]: gttrackids_t[j] for i, j in zip(*associator.associate(tracks_t, gttracks_t)) }
        FP[t] = set(trackids_t) - set(TP[t].keys())
        FN[t] = set(gttrackids_t) - set(TP[t].values())

    nom = sum(assiou(TP, FP, FN, ti, gi) for ti, gi in TP[t].items())
    den = sum(len(TP[t]) + len(FP[t]) + len(FN[t]) for t in TP.keys())
    return np.sqrt(nom / den)

def load_map(scenario: str) -> MapTile:
    # Copied from GT/export_gt :-|
    map_config_file = Path(RosPack().get_path('multiception'), "config", "location", scenario).with_suffix(".yaml").resolve(strict=True)

    # Get map config (reference, radius, etc)
    with open(map_config_file , 'r') as f:
        config = yaml.safe_load(f)

    maptile = MapTile()
    loader = LoaderSqlite(resolve_args(config["map_file_name"]), config['srid'])
    loader.setArea(config["rad_lon"], config["rad_lat"], config['radius'], config['srid'])
    loader.loadMap(maptile, "links=*;linkborders=*;shapepoints=*;nodes=*")
    maptile.connect()

    return maptile

if __name__=='__main__':
    plt.rcParams['text.usetex'] = True
    plt.rcParams['axes.spines.right'] = False
    plt.rcParams['axes.spines.top'] = False

    args = parse_args()
    force = args.force
    path_percepts = Path(args.PATH_PERCEPTS).expanduser().resolve(strict=True)
    path_gt = Path(args.ground_truth, args.SCENARIO).expanduser().resolve(strict=True)
    topic_poses = args.pose_topic
    topic_percepts = args.TOPIC_PERCEPTS

    if args.cache is None:
        md5 = hashlib.md5()
        md5.update(str(path_percepts).encode())
        md5.update(str(path_gt).encode())
        md5.update(str(topic_poses).encode())
        md5.update(str(topic_percepts).encode())
        hash_args = md5.hexdigest()
        CACHEDIR = Path("/tmp", str(hash_args))
    else:
        CACHEDIR = Path(args.cache)

    CACHEDIR.mkdir(parents=True, exist_ok=True)
    CACHES = [Path(CACHEDIR, i).with_suffix(".pkl") for i in ("inputs", "objects", "errors", "grids")]
    print(f"Cache in {CACHEDIR}")

    DEBUG = args.debug
    ASSOC_GATE = 10 # m
    ASSOC_COST_MAX = 15
    NTHRESH = 1 if args.eval_post else 35

    if CACHES[0].exists() and not force:
        print(f"Using precomputed {CACHES[0]}")
        with open(CACHES[0], 'rb') as f:
            poses_, tracks_, grids_, gttracks_, gtgrids_, poses, tracks, grids, gttracks, gtgrids, road = pickle.load(f)
    else:
        for cache in CACHES[0:]: cache.unlink(missing_ok=True)

        if args.eval_post:
            tracks_ = load_post_csv(path_percepts)
            gttracks_ = load_csv(path_gt)
            tracks, gttracks = sync_gt_tracks(tracks_, gttracks_)
            if args.start is not None:
                tracks = tracks.filtered(args.start)
                gttracks = gttracks.filtered(args.start)
            gtgrids_ = load_images(path_gt)
            poses = tracks # Dirty hack to make gen_road work (using only timestamps)
            poses_ = grids_ = grids = gtgrids = TimedDict()
        else:
            poses_, _ = load_bag(path_percepts, topic_poses, True)
            tracks_, grids_ = load_bag(path_percepts, topic_percepts)
            gttracks_ = load_csv(path_gt)
            gtgrids_ = load_images(path_gt)

            tracks_, poses = sync_gt_tracks(tracks_, poses_)
            tracks, gttracks = sync_gt_tracks(tracks_, gttracks_)
            grids, gtgrids = sync_gt_grids(grids_, gtgrids_)

            gttracks = transform_local(gttracks, poses)
            tracks_ = transform_local(tracks_, poses)

        road = None
        if args.local is not None or args.debug:
            maptile = load_map(args.SCENARIO)
            road = gen_road(poses, maptile, args.local)
        tracks = precondition(tracks, road, not args.skip_signs)
        gttracks = precondition(gttracks, road, not args.skip_signs)

        with open(CACHES[0], 'wb') as f:
            pickle.dump((poses_, tracks_, grids_, gttracks_, gtgrids_, poses, tracks, grids, gttracks, gtgrids, road), f)

    if CACHES[1].exists() and not force:
        print(f"Using precomputed {CACHES[1]}")
        with open(CACHES[1], 'rb') as f:
            scores, y_true, y_pred = pickle.load(f)
    else:
        for cache in CACHES[1:]: cache.unlink(missing_ok=True)
        scores = compute_scores_object(tracks, gttracks)
        y_true, y_pred = compute_scores_object_scikit(tracks, gttracks)

        with open(CACHES[1], 'wb') as f:
            pickle.dump((scores, y_true, y_pred), f)

    if args.eval_post:
        print(CACHEDIR)
        sys.exit(0)

    if CACHES[2].exists() and not force:
        print(f"Using precomputed {CACHES[2]}")
        with open(CACHES[2], 'rb') as f:
            errcovs = pickle.load(f)
    else:
        for cache in CACHES[2:]: cache.unlink(missing_ok=True)
        errcovs = compute_errors(tracks, gttracks)

        with open(CACHES[2], 'wb') as f:
            pickle.dump((errcovs), f)

    if CACHES[3].exists() and not force:
        print(f"Using precomputed {CACHES[3]}")
        with open(CACHES[3], 'rb') as f:
            scores_grid = pickle.load(f)
    else:
        for cache in CACHES[3:]: cache.unlink(missing_ok=True)
        scores_grid = compute_scores_grid(grids, gtgrids)

        with open(CACHES[3], 'wb') as f:
            pickle.dump((scores_grid), f)

    if args.plot:
        metrics.PrecisionRecallDisplay.from_predictions(y_true, y_pred)
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        disp_ROC(scores, ax=ax1)
        disp_PR (scores, ax=ax2)
        disp_TOC(scores, ax=ax3)
        disp_DET(scores, ax=ax4)
        disp_errors(errcovs, list(gttracks.keys()))
        plt.show()

    print(CACHEDIR) # To be retrieved by compare_evaluations.py
