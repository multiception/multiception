#! /usr/bin/env python3

import sys
import pickle
from copy import deepcopy
from pathlib import Path
from argparse import ArgumentParser

import numpy as np
from pyqtgraph import QtCore, QtGui, GraphicsLayoutWidget, setConfigOptions, PlotWidget, SignalProxy, InfiniteLine, mkPen, mkColor
from PyQt5.QtWidgets import QStyleFactory, QVBoxLayout, QHBoxLayout, QStackedLayout, QApplication, QMainWindow, QWidget, QComboBox

def compute_tree_height(tree):
	if tree is None: return 0
	return 1 + max(compute_tree_height(subtree) for subtree in tree.values())

def compute_tree_width(tree):
	if tree is None: return 1
	return sum(compute_tree_width(subtree) for subtree in tree.values())

def tree_to_grid_(tree, grid, x0, y0):
	if tree is None: return
	for name, subtree in tree.items():
		w = compute_tree_width(subtree)
		x = x0 + w//2
		grid[y0][x] = name
		tree_to_grid_(subtree, grid, x0, y0+1)
		x0 += w

def tree_to_grid(tree):
	grid = np.zeros((compute_tree_height(tree), compute_tree_width(tree)), dtype='object')
	tree_to_grid_(tree, grid, 0, 0)
	grid[grid==0] = None
	return grid

def create_layout_(tree, grid, x0, y0):
	if tree is None: return
	for name, subtree in tree.items():
		w = compute_tree_width(subtree)
		x = x0 + w//2
		grid[y0][x] = name
		tree_to_grid_(subtree, grid, x0, y0+1)
		x0 += w

def create_layout(tree, widgets, vlayout, hlayouts, level=0):
	if tree is None: return
	if level<=len(hlayouts):
		layout = QHBoxLayout()
		layout.setContentsMargins(0, 0, 0, 0)
		hlayouts.append(layout)
		vlayout.addLayout(layout)

	for name, subtree in tree.items():
		hlayouts[level].addWidget(widgets[name])
		create_layout(subtree, widgets, vlayout, hlayouts, level+1)

def pr(tree, level=0):
	if tree is None: return
	print('  '*level, list(tree.keys()))
	for t in tree.values():
		pr(t, level+1)

class TrustExplorer(QWidget):
	TRUST_TREE = {
		'past': None,
		'meas': {
			'cohe': {
				'cohe-obd': None,
				'cohe-atc': None,
				'cohe-spc': {
					'cohe-spc-ro': None,
					'cohe-spc-bu': None,
				},
			},
			'cons': None,
			'conf': {
				'conf-osi': None,
				'conf-fsi': None,
				'conf-ofi': None,
				'conf-odi': {
					'conf-odi-rc': None,
					'conf-odi-cr': None,
				},
			},
		},
		'hist': None,
	}
	TITLE_FONT_SIZE = 28
	TICK_FONT_SIZE = 20

	def __init__(self):
		super().__init__()
		self.setWindowTitle('Trust Explorer')
		parser = ArgumentParser(description='Plots various trust estimations from multiception/TrustBuilder')
		parser.add_argument('PATHS', type=str, nargs='+', help='Path to pickles')

		self.trusts = {} # Organized as { from: { to: { type: MassFunction, ... }, ... }, ... }
		for path in parser.parse_args().PATHS:
			with open(path, 'rb') as f:
				_, self.trusts = pickle.load(f)
		setConfigOptions(antialias=True, foreground='k', background='w')
		font = QtGui.QFont()
		font.setPixelSize(TrustExplorer.TICK_FONT_SIZE)

		self.moving = False
		self.setAutoFillBackground(True)
		p = self.palette()
		p.setColor(self.backgroundRole(), mkColor('w'))
		self.setPalette(p)
		self.trust_grid = tree_to_grid(TrustExplorer.TRUST_TREE)
		self.figs = {}
		self.crosshairs_v = {}
		self.crosshairs_h = {}
		self.cb = QComboBox()
		self.global_layout = QVBoxLayout()
		self.global_layout.addWidget(self.cb)
		self.stacked_layout = QStackedLayout(self.global_layout)
		self.layouts = []
		self.proxies = []
		for sid_from in sorted(self.trusts):
			self.figs[sid_from] = {}
			self.crosshairs_v[sid_from] = {}
			self.crosshairs_h[sid_from] = {}
			for sid_to in sorted(self.trusts[sid_from]):
				self.figs[sid_from][sid_to] = {}
				self.crosshairs_v[sid_from][sid_to] = {}
				self.crosshairs_h[sid_from][sid_to] = {}
				t = self.trusts[sid_from][sid_to]['t']
				t -= t.min()

				for row, cols in enumerate(self.trust_grid):
					for col, trust_type in enumerate(cols):
						if trust_type is None: continue
						conflict, trust, nontrust = np.cumsum(self.trusts[sid_from][sid_to][trust_type], axis=1).T
						self.figs[sid_from][sid_to][trust_type] = PlotWidget()
						self.figs[sid_from][sid_to][trust_type].setTitle(trust_type, size=f'{TrustExplorer.TITLE_FONT_SIZE}pt')
						#self.figs[sid_from][sid_to][trust_type].plot(t, conflict, pen=(0,0,255), name="0")
						self.figs[sid_from][sid_to][trust_type].plot(t, nontrust, pen=(255,0,0), name="T")
						self.figs[sid_from][sid_to][trust_type].plot(t, trust,    pen=(0,255,0), name="t")
						self.figs[sid_from][sid_to][trust_type].setYRange(0, 1)
						self.figs[sid_from][sid_to][trust_type].setContentsMargins(0, 0, 0, 0)
						self.figs[sid_from][sid_to][trust_type].getAxis("left").tickFont = font
						self.figs[sid_from][sid_to][trust_type].getAxis("bottom").tickFont = font
						self.figs[sid_from][sid_to][trust_type].setContentsMargins(0, 0, 0, 0)
						self.figs[sid_from][sid_to][trust_type].sigRangeChanged.connect(lambda plot, ranges, a=sid_from, b=sid_to: self.plot_moved(ranges, a, b))

						self.crosshairs_v[sid_from][sid_to][trust_type] = InfiniteLine(angle=90, movable=False, pen=mkPen(0.5))
						self.crosshairs_h[sid_from][sid_to][trust_type] = InfiniteLine(angle=0, movable=False,  pen=mkPen(0.5))
						self.crosshairs_v[sid_from][sid_to][trust_type].setPos(-1e5)
						self.crosshairs_h[sid_from][sid_to][trust_type].setPos(-1e5)
						self.figs[sid_from][sid_to][trust_type].addItem(self.crosshairs_v[sid_from][sid_to][trust_type], ignoreBounds=True)
						self.figs[sid_from][sid_to][trust_type].addItem(self.crosshairs_h[sid_from][sid_to][trust_type], ignoreBounds=True)
						self.proxies.append(SignalProxy(self.figs[sid_from][sid_to][trust_type].scene().sigMouseMoved, rateLimit=30, slot=lambda e, a=sid_from, b=sid_to, c=trust_type: self.mouseMoved(*e, a, b, c)))

				vlayout = QVBoxLayout()
				vlayout.setSpacing(50)
				vlayout.setContentsMargins(0, 0, 0, 0)
				hlayouts = []
				create_layout(TrustExplorer.TRUST_TREE, self.figs[sid_from][sid_to], vlayout, hlayouts)
				vlayout_widget = QWidget()
				vlayout_widget.setLayout(vlayout)
				self.cb.addItem(f'Trust of {sid_from} in {sid_to}')
				self.stacked_layout.addWidget(vlayout_widget)
				self.layouts.append(self.figs[sid_from][sid_to])

		self.cb.activated.connect(self.plot_changed)
		self.stacked_layout.setCurrentIndex(0)
		self.setLayout(self.global_layout)

	def plot_changed(self, i):
		self.stacked_layout.setCurrentIndex(i)
		for sid_from in self.figs:
			for sid_to in self.figs[sid_from]:
				for trust_type in self.figs[sid_from][sid_to]:
					self.crosshairs_v[sid_from][sid_to][trust_type].setPos(-1e5)
					self.crosshairs_h[sid_from][sid_to][trust_type].setPos(-1e5)

	def plot_moved(self, ranges, sid_from, sid_to):
		if self.moving: return
		self.moving = True
		for other in self.figs[sid_from][sid_to].values():
			other.setRange(xRange=ranges[0], yRange=ranges[1], padding=0, update=False)
		self.moving = False

	def mouseMoved(self, pos, sid_from, sid_to, trust_type):
		if self.figs[sid_from][sid_to][trust_type].sceneBoundingRect().contains(pos):
			mousePoint = self.figs[sid_from][sid_to][trust_type].getPlotItem().vb.mapSceneToView(pos)
			for trust_type_other in self.figs[sid_from][sid_to]:
				self.crosshairs_v[sid_from][sid_to][trust_type_other].setPos(mousePoint.x())
				self.crosshairs_h[sid_from][sid_to][trust_type_other].setPos(mousePoint.y())

	def _paintLines(self, painter, tree, widgets):
		for name, subtree in tree.items():
			widget = widgets[name]
			if subtree is not None:
				for subname in subtree:
					subwidget = widgets[subname]
					a_abspos = widgets[name].parent().mapTo(widgets[name].window(), QtCore.QPoint(int(widget.x()+widget.width()/2), widget.y()+widget.height()+5));
					b_abspos = widgets[subname].parent().mapTo(widgets[subname].window(), QtCore.QPoint(int(subwidget.x()+subwidget.width()/2), subwidget.y()-5));
					painter.drawLine(a_abspos, b_abspos)
				self._paintLines(painter, subtree, widgets)

	def paintEvent(self, event):
		super().paintEvent(event)
		painter = QtGui.QPainter(self)
		painter.setPen(mkPen(0.4, width=1.5))
		painter.setRenderHint(QtGui.QPainter.Antialiasing);
		painter.setRenderHint(QtGui.QPainter.HighQualityAntialiasing);
		self._paintLines(painter, TrustExplorer.TRUST_TREE, self.layouts[self.stacked_layout.currentIndex()])

if __name__=='__main__':
	app = QApplication(sys.argv)
	tp = TrustExplorer()
	tp.show()
	app.exec()
