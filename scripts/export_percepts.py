import rospy
import pickle
import pyqtgraph as pg
from pyqtgraph import QtGui, QtCore
from grid_map_msgs.msg import GridMap as GridMapMsg

from multiception.msg import Perceptions
from multiception.object import Object, plotdebug_objects
from multiception.evidential_grid_map import EvidentialGridMap, plotdebug_gridmap

VEHICLES = { 'zoeblue': '/zoeblue/statep', 'zoegrey': '/zoeblue/lidar/tracks', 'zoewhite': 'Rien' }

class PerceptionsExporter(QtCore.QObject):
	signal_update = QtCore.pyqtSignal()
	def __init__(self, vehicles):
		super().__init__()
		rospy.init_node("perception_exporter", anonymous=True)
		self.win = pg.GraphicsLayoutWidget(show=True, title="Perceptions Exporter")
		self.gms = { v: [] for v in vehicles }
		self.objs = { v: [] for v in vehicles }
		self.items = { v: [] for v in vehicles }
		self.figs = { v: self.win.addPlot(title=v) for v in vehicles }

		self.signal_update.connect(self.draw)
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(lambda: self.draw(True))
		self.timer.start(2000)
		for fig in self.figs.values():
			fig.setAspectLocked(lock=True, ratio=1)

		self.subs = { k: rospy.Subscriber(v, Perceptions, self.callback, k) for k,v in vehicles.items() }

	def callback(self, msg, vehicle):
		self.gms[vehicle] = [EvidentialGridMap.from_msg(s.observability) for s in msg.sources]
		self.objs[vehicle] = [Object.from_msg(p) for p in msg.perceptions]
		self.items[vehicle] = [plotdebug_objects(obj)[0] for obj in self.objs[vehicle]] + \
			[plotdebug_gridmap(gm) for gm in self.gms[vehicle]]
		self.signal_update.emit()

	@QtCore.pyqtSlot()
	def draw(self, close_only=False):
		if rospy.is_shutdown():
			self.win.close()
			return
		if close_only:
			return

		for v, fig in self.figs.items():
			fig.clear()
			for item in self.items[v]:
				fig.addItem(item)

if __name__=='__main__':
	pg.setConfigOption('background', 'w')
	pg.setConfigOption('foreground', 'k')
	pe = PerceptionsExporter(VEHICLES)
	QtGui.QApplication.instance().exec_()
	rospy.spin()

	print('Exporting to /tmp/obs.pkl...')
	with open('/tmp/obs.pkl', 'wb') as f:
		pickle.dump((pe.gms, pe.objs), f)
