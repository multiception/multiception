#! /usr/bin/env python3

from yaml import safe_dump
from argparse import ArgumentParser

def parse_args():
	parser = ArgumentParser()
	parser.add_argument("--filter", action="append", required=True)
	parser.add_argument("--exist", action="append", required=True)
	parser.add_argument("--detect", action="append", required=True)
	parser.add_argument("--trust", action="append", required=True)
	parser.add_argument("--spoof", action="append", required=True)
	parser.add_argument("OUTPUT")
	return parser.parse_args()

if __name__=="__main__":
	args = parse_args()
	for filt in args.filter:
		for exist in args.exist:
			for detect in args.detect:
				for trust in args.trust:
					for spoof in args.spoof:
						with open(f"{args.OUTPUT}/{filt}_{exist}_{detect}_{trust}_{spoof}.yaml", "w") as f:
							safe_dump({
								"bases": [
									f"filter_{filt}",
									f"exist_{exist}",
									f"detect_{detect}",
									f"trust_{trust}",
									f"spoof_{spoof}",
								]
							}, f)
