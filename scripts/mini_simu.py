import sys
import copy
import pickle
from time import time
import numpy as np
from numpy.linalg import inv, det
import scipy.optimize
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore
np.set_printoptions(precision=2, linewidth=999, suppress=True)

### Parameters
SIM_STEPS = 100                          # Number of steps of this simulation
SIM_DT = 0.1                             # Time between each steps (s)
INIT_STATES = [(5,1),(2,2),(0,3)]        # Initial states (and number) of robots in the simulation
OBS_MOD_COM = np.identity(2)             # Observation model over the whole state (Track-to-Track)
OBS_MOD_GNSS  = np.array([[1, 0]])       # Observation model to self
OBS_COV_GNSS  = np.diag([1.0**2])        # Observation covariance to self
OBS_FAC_GNSS  = 0.0                      # Correlation factor of the observation noise to self (0: white -> 1: colored)
OBS_MOD_LIDAR = np.array([[1, 0]])       # Observation model to other
OBS_COV_LIDAR = np.diag([2.0**2])        # Observation covariance to other
OBS_FAC_LIDAR = 0.0                      # Correlation factor of the observation noise to other (0: white -> 1: colored)
EVO_COVD = np.diag([0.12**2])            # Evolution model covariance (dependent part of split)
EVO_COVI = np.diag([0.00**2])            # Evolution model covariance (independent part of split - Kalman)
EVO_MAT = np.array([[SIM_DT/2],[1]])
EVO_MOD = np.array([[1, SIM_DT],[0, 1]])

FILTERS = { f:i for i,f in enumerate([
	'GT',
	'KF', 'CI',
	'S1', 'S2',
	'C1', 'C2', 'K2',
	'T1', 'T2',
])}
FILTER_NAMES = { i:f for f,i in FILTERS.items() }

PENS = {
	FILTERS['KF']: { 'Err': pg.mkPen(color=(0, 0, 255, 220), width=1.5), 'Cov': pg.mkPen(color=(0, 0, 255, 90), width=2) },
	FILTERS['C1']: { 'Err': pg.mkPen(color=(255, 0, 0, 220), width=1.5), 'Cov': pg.mkPen(color=(255, 0, 0, 90), width=2) },
	FILTERS['CI']: { 'Err': pg.mkPen(color=(128,0,128, 220), width=1.5), 'Cov': pg.mkPen(color=(128,0,128, 90), width=2) },
	FILTERS['S1']: { 'Err': pg.mkPen(color=(0, 255, 0, 220), width=1.5), 'Cov': pg.mkPen(color=(0, 255, 0, 90), width=2), 'Coi': pg.mkPen(color=(0, 255, 0, 90), style=QtCore.Qt.DashLine, width=2)},
	FILTERS['T1']: { 'Err': pg.mkPen(color=(50,50,  0, 220), width=1.5), 'Cov': pg.mkPen(color=(50,50,  0, 90), width=2), 'Coi': pg.mkPen(color=(50,50,  0, 90), style=QtCore.Qt.DashLine, width=2)},
}
PENS[FILTERS['K2']] = PENS[FILTERS['KF']]
PENS[FILTERS['C2']] = PENS[FILTERS['C1']]
PENS[FILTERS['S2']] = PENS[FILTERS['S1']]
PENS[FILTERS['T2']] = PENS[FILTERS['T1']]

### Simulation
class Robot:
	def __init__(self, x, v, N):
		self.state = np.array([x, v],dtype=float)
		self.obserr_ego = np.zeros_like(self.state)
		self.obserr_oth = np.zeros((N, len(self.state)))

	def evo(self, s, dt):
		self.state[:] = EVO_MOD@self.state + EVO_MAT@[np.sin(s)-np.sin(s-dt)]

	def obs(self, others):
		# Observation of Ego
		self.obserr_ego[:] = np.random.multivariate_normal(np.zeros(OBS_COV_GNSS.shape[0]), OBS_COV_GNSS) + OBS_FAC_GNSS*self.obserr_ego[:]
		obs_self = OBS_MOD_GNSS@(self.state + self.obserr_ego)
		covd_self =    OBS_FAC_GNSS *OBS_COV_GNSS
		covi_self = (1-OBS_FAC_GNSS)*OBS_COV_GNSS

		# Observation of the others (artificially adding the pose error/cov)
		obs_others = []
		for i, other in enumerate(others):
			self.obserr_oth[i,:] = np.random.multivariate_normal(np.zeros(OBS_COV_LIDAR.shape[0]), OBS_COV_LIDAR) + OBS_FAC_LIDAR*self.obserr_oth[i,:]
			obs_other = OBS_MOD_LIDAR@(other.state-self.state+self.obserr_oth[i])
			covd_other =    OBS_FAC_LIDAR *OBS_COV_LIDAR
			covi_other = (1-OBS_FAC_LIDAR)*OBS_COV_LIDAR
			obs_others.append((obs_other, covd_other, covi_other))

		return (obs_self,covd_self,covi_self), obs_others

### Filtering & Tracking
def Kalman(x, Pd, Pi, y, Rd, Ri, C):
	P = Pd+Pi; R = Rd+Ri
	d = y - C@x
	D = C @ P @ C.T + R
	K = P @ C.T @ inv(D)

	x = x + K@d
	P = (np.identity(len(x)) - K@C) @ P
	return x, np.zeros_like(P), P, 2

def CI(x, Pd, Pi, y, Rd, Ri, C):
	Pinv = inv(Pd+Pi)
	Rinv = inv(Rd+Ri)

	f = (lambda w: 1/det( w*Pinv + (1-w)*C.T@Rinv@C ))
	w = scipy.optimize.minimize_scalar(f, method='bounded', bounds=(0,1)).x

	P = inv( w*Pinv + (1-w)*C.T@Rinv@C )
	x = P @ (w*Pinv@x + (1-w)*C.T@Rinv@y)

	return x, np.zeros_like(P), P, w

def SCI(x, Pd, Pi, y, Rd, Ri, C):
	I = np.identity(Pd.shape[0])
	f = lambda w: det( (I-((Pd/w + Pi)@C.T@inv(C@(Pd/w + Pi)@C.T + Rd/(1-w) + Ri))@C)@(Pd/w + Pi) )
	w = scipy.optimize.minimize_scalar(f, method='bounded', bounds=(0.001,0.999)).x

	P = Pd/w + Pi
	R = Rd/(1-w) + Ri
	K = P@C.T@inv(C@P@C.T + R)

	x = x + K@(y-C@x)
	P  = (I-K@C)@P @(I-K@C).T + K@R @K.T
	Pi = (I-K@C)@Pi@(I-K@C).T + K@Ri@K.T
	Pd = P - Pi

	return x, Pd, Pi, w

def track(x, Pd, Pi, obs, dt, C, filter_fun):
	### Prediction
	if dt>0:
		F = np.array([[1, dt], [0, 1]])
		G = np.array([[dt/2],[1]])

		# Compute the evolved state
		x = F @ x
		Pd = F @ Pd @ F.T + G@EVO_COVD@G.T
		Pi = F @ Pi @ F.T + G@EVO_COVI@G.T

	### Update
	w = -1
	for y, Rd, Ri in obs:
		x, Pd, Pi, w = filter_fun(x, Pd, Pi, y, Rd, Ri, C)

	return x, Pd, Pi, w

class Simulation(object):
	def __init__(self):
		self.robots = [Robot(*state, len(INIT_STATES)) for state in INIT_STATES]
		self.ws  = np.zeros((SIM_STEPS,len(FILTERS),len(self.robots),len(self.robots)))
		self.xs  = np.zeros((SIM_STEPS,len(FILTERS),len(self.robots),len(self.robots), 2))
		self.Pds = np.zeros((SIM_STEPS,len(FILTERS),len(self.robots),len(self.robots), 2,2))
		self.Pis = np.zeros((SIM_STEPS,len(FILTERS),len(self.robots),len(self.robots), 2,2))
		self.ys  = np.zeros((SIM_STEPS,len(self.robots),len(self.robots), 1))
		self.Rds = np.zeros((SIM_STEPS,len(self.robots),len(self.robots), 1,1))
		self.Ris = np.zeros((SIM_STEPS,len(self.robots),len(self.robots), 1,1))
		self.ts  = np.zeros((SIM_STEPS,len(FILTERS)))
		self.Pds[0] = np.identity(2)*100
		self.Pis[0] = np.identity(2)*100

		# Main Loop
		for s in range(1, SIM_STEPS):
			self.sim_step(s)
		for s in range(1, SIM_STEPS):
			self.fus_step(s)

	def rrange(self, exclude=-1):
		return [i for i in range(len(self.robots)) if i!=exclude]

	def sim_step(self, s):
		# Get the robots up to time
		for robot in self.robots:
			robot.evo(s, SIM_DT)

		for k,robot in enumerate(self.robots):
			self.xs[s,0,k,k] = robot.state
			obs_self, obs_others = robot.obs([self.robots[j] for j in self.rrange(k)])
			self.ys[s,k,k], self.Rds[s,k,k], self.Ris[s,k,k] = obs_self
			for j_obs, j in enumerate(self.rrange(k)):
				self.xs[s,0,k,j] = self.robots[j].state
				self.ys[s,k,j], self.Rds[s,k,j], self.Ris[s,k,j] = obs_others[j_obs]

	def fus_step(self, s):
		sf = FILTERS

		# First phase of filtering: taking self measurement for different modalities
		filt_phase1 = { sf['KF']:Kalman, sf['CI']:Kalman, sf['S1']:SCI, sf['S2']:SCI, sf['C1']:CI, sf['C2']:CI, sf['K2']:Kalman, sf['T1']:SCI }

		for f, filter_fun in filt_phase1.items():
			t = time()
			# GNSS observation
			for k in self.rrange():
				self.xs[s,f,k,k], self.Pds[s,f,k,k], self.Pis[s,f,k,k], self.ws[s,f,k,k] = track(
					self.xs[s-1,f,k,k], self.Pds[s-1,f,k,k], self.Pis[s-1,f,k,k],
					[(self.ys[s,k,k], self.Rds[s,k,k], self.Ris[s,k,k])],
					SIM_DT, OBS_MOD_GNSS, filter_fun
				)

				# LiDAR observations
				for j in self.rrange(k):
					ykj  = OBS_MOD_LIDAR@self.xs[s,f,k,k] + self.ys[s,k,j]
					Rkjd = OBS_MOD_LIDAR@self.Pds[s,f,k,k]@OBS_MOD_LIDAR.T + self.Rds[s,k,j]
					Rkji = OBS_MOD_LIDAR@self.Pis[s,f,k,k]@OBS_MOD_LIDAR.T + self.Ris[s,k,j]

					self.xs[s,f,k,j], self.Pds[s,f,k,j], self.Pis[s,f,k,j], self.ws[s,f,k,j] = track(
						self.xs[s-1,f,k,j], self.Pds[s-1,f,k,j], self.Pis[s-1,f,k,j],
						[(ykj, Rkjd, Rkji)],
						SIM_DT, OBS_MOD_LIDAR, filter_fun
					)

			self.ts[s,f] = (time()-t)*1000

		# Alternative first phase taking not observations but tracks from other phase 1
		filt_phase1bis = { (sf['T1'], sf['T2']):SCI }

		for (f1, f2), filter_fun in filt_phase1bis.items():
			t = time()
			for k in self.rrange():
				for j in self.rrange():
					self.xs[s,f2,k,j], self.Pds[s,f2,k,j], self.Pis[s,f2,k,j], self.ws[s,f2,k,j] = track(
						self.xs[s-1,f2,k,j], self.Pds[s-1,f2,k,j], self.Pis[s-1,f2,k,j],
						[(self.xs[s,f1,k,j], self.Pds[s  ,f1,k,j], self.Pis[s,f1,k,j])],
						SIM_DT, OBS_MOD_COM, filter_fun
					)

			self.ts[s,f2] = (time()-t)*1000

		# Second phase taking estimates from other robots
		filt_phase2 = { sf['CI']:CI, sf['C2']:CI, sf['K2']:Kalman, sf['S2']:SCI, sf['T2']:SCI }

		# Keep shared estimates separate temporarily to simulate parallel communications & fusion
		estimates = {}
		for f, filter_fun in filt_phase2.items():
			t = time()
			for k in self.rrange():
				estimates[k] = {}
				for j in self.rrange():
					estimates[k,j] = track(
						self.xs[s,f,k,j], self.Pds[s,f,k,j], self.Pis[s,f,k,j],
						[(self.xs[s,f,c,j], self.Pds[s,f,c,j], self.Pis[s,f,c,j]) for c in self.rrange(k)],
						0, OBS_MOD_COM, filter_fun
					)

			for k in self.rrange():
				for j in self.rrange():
					self.xs[s,f,k,j], self.Pds[s,f,k,j], self.Pis[s,f,k,j], self.ws[s,f,k,j] = estimates[k,j]

			self.ts[s,f] += (time()-t)*1000

	def plot(self, k, j):
		sf = FILTERS
		win = pg.GraphicsWindow()
		win.resize(950, 950)
		win.setWindowTitle('{} {}'.format(k, j))
		plot_figx1 = win.addPlot()
		plot_figx2 = win.addPlot()
		plot_figx1.showGrid(x=False, y=True, alpha=0.1)
		plot_figx2.showGrid(x=False, y=True, alpha=0.1)
		plot_figx2.setXLink(plot_figx1); plot_figx2.setYLink(plot_figx1)
		#plot_figx1.setLabel('left', text='Error', units='m')
		#plot_figx1.setLabel('bottom', text='Time', units='s')
		win.nextRow()
		plot_figv1 = win.addPlot()
		plot_figv2 = win.addPlot()
		plot_figv1.showGrid(x=False, y=True, alpha=0.1)
		plot_figv2.showGrid(x=False, y=True, alpha=0.1)
		plot_figv1.setXLink(plot_figx1); plot_figv1.setYLink(plot_figx1)
		plot_figv2.setXLink(plot_figx1); plot_figv2.setYLink(plot_figx1)
		win.nextRow()
		plot_figw1 = win.addPlot()
		plot_figw2 = win.addPlot()
		plot_figw1.setYRange(0, 1)
		plot_figw2.setYRange(0, 1)
		plot_figw2.setXLink(plot_figw1); plot_figw2.setYLink(plot_figw1)

		t = np.arange(1, SIM_STEPS)*SIM_DT

		for f in [ sf['S1'], sf['T1'] ]:
			plot_figw1.plot(t,  self.ws[1:,f,k,j], pen=PENS[f]['Err'])

		for f in [ sf['S1'], sf['T1'] ]:
			plot_figx1.plot(t,  self.xs[1:,0,k,j,0]-self.xs[1:,f,k,j,0], pen=PENS[f]['Err'])
			plot_figv1.plot(t,  self.xs[1:,0,k,j,1]-self.xs[1:,f,k,j,1], pen=PENS[f]['Err'])
			plot_figx1.plot(t,  3*np.sqrt(self.Pds[1:,f,k,j,0,0]+self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Cov'])
			plot_figx1.plot(t, -3*np.sqrt(self.Pds[1:,f,k,j,0,0]+self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Cov'])
			plot_figv1.plot(t,  3*np.sqrt(self.Pds[1:,f,k,j,1,1]+self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Cov'])
			plot_figv1.plot(t, -3*np.sqrt(self.Pds[1:,f,k,j,1,1]+self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Cov'])
			if 'Coi' in PENS[f].keys():
				plot_figx1.plot(t,  3*np.sqrt(self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Coi'])
				plot_figx1.plot(t, -3*np.sqrt(self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Coi'])
				plot_figv1.plot(t,  3*np.sqrt(self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Coi'])
				plot_figv1.plot(t, -3*np.sqrt(self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Coi'])

		for f in [ sf['S2'], sf['T2'] ]:
			plot_figw2.plot(t,  self.ws[1:,f,k,j], pen=PENS[f]['Err'])
		for f in [ sf['S2'], sf['T2'] ]:
			plot_figx2.plot(t,  self.xs[1:,0,k,j,0]-self.xs[1:,f,k,j,0], pen=PENS[f]['Err'])
			plot_figv2.plot(t,  self.xs[1:,0,k,j,1]-self.xs[1:,f,k,j,1], pen=PENS[f]['Err'])
			plot_figx2.plot(t,  3*np.sqrt(self.Pds[1:,f,k,j,0,0]+self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Cov'])
			plot_figx2.plot(t, -3*np.sqrt(self.Pds[1:,f,k,j,0,0]+self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Cov'])
			plot_figv2.plot(t,  3*np.sqrt(self.Pds[1:,f,k,j,1,1]+self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Cov'])
			plot_figv2.plot(t, -3*np.sqrt(self.Pds[1:,f,k,j,1,1]+self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Cov'])
			if 'Coi' in PENS[f].keys():
				plot_figx2.plot(t,  3*np.sqrt(self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Coi'])
				plot_figx2.plot(t, -3*np.sqrt(self.Pis[1:,f,k,j,0,0]), pen=PENS[f]['Coi'])
				plot_figv2.plot(t,  3*np.sqrt(self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Coi'])
				plot_figv2.plot(t, -3*np.sqrt(self.Pis[1:,f,k,j,1,1]), pen=PENS[f]['Coi'])

		self.output(k,j, False)
		return win

	def output(self, k, j, newline):
		sf = FILTERS
		filters = { sf['KF']:sf['CI'], sf['C1']:sf['C2'], sf['S1']:sf['S2'], sf['T1']:sf['T2'] }
		fmt = '{}:{} {} {:0.2f}±{:0.2f} {:0.2f}±{:0.2f} ({:0.2f}%) {:0.2f}ms'
		for f1, f2 in filters.items():
			print(fmt.format(k, j,
				FILTER_NAMES[f1],
				np.mean(np.abs( self.xs[1:,0,k,j,0]-self.xs[1:,f1,k,j,0] )),
				np.mean( 3*np.sqrt(self.Pds[1:,f1,k,j,0,0]+self.Pis[1:,f1,k,j,0,0]) ),
				np.mean(np.abs( self.xs[1:,0,k,j,1]-self.xs[1:,f1,k,j,1] )),
				np.mean( 3*np.sqrt(self.Pds[1:,f1,k,j,1,1]+self.Pis[1:,f1,k,j,1,1]) ),
				np.mean(
					(np.abs(self.xs[1:,0,k,j,0]-self.xs[1:,f1,k,j,0]) < 3*np.sqrt(self.Pds[1:,f1,k,j,0,0]+self.Pis[1:,f1,k,j,0,0])) &
					(np.abs(self.xs[1:,0,k,j,1]-self.xs[1:,f1,k,j,1]) < 3*np.sqrt(self.Pds[1:,f1,k,j,1,1]+self.Pis[1:,f1,k,j,1,1]))
				)*100.,
				np.mean( self.ts[1:,f1] )),
			fmt.format(k, j,
				FILTER_NAMES[f2],
				np.mean(np.abs( self.xs[1:,0,k,j,0]-self.xs[1:,f2,k,j,0] )),
				np.mean( 3*np.sqrt(self.Pds[1:,f2,k,j,0,0]+self.Pis[1:,f2,k,j,0,0]) ),
				np.mean(np.abs( self.xs[1:,0,k,j,1]-self.xs[1:,f2,k,j,1] )),
				np.mean( 3*np.sqrt(self.Pds[1:,f2,k,j,1,1]+self.Pis[1:,f2,k,j,1,1]) ),
				np.mean(
					(np.abs(self.xs[1:,0,k,j,0]-self.xs[1:,f2,k,j,0]) < 3*np.sqrt(self.Pds[1:,f2,k,j,0,0]+self.Pis[1:,f2,k,j,0,0])) &
					(np.abs(self.xs[1:,0,k,j,1]-self.xs[1:,f2,k,j,1]) < 3*np.sqrt(self.Pds[1:,f2,k,j,1,1]+self.Pis[1:,f2,k,j,1,1]))
				)*100.,
				np.mean( self.ts[1:,f2] ))
			, sep='\n' if newline else '  then  ')

if __name__=='__main__':
	sim = Simulation()
	app = pg.mkQApp()
	pg.setConfigOptions(antialias=True, background='w', foreground='k')
	p00 = sim.plot(0, 0)
	p22 = sim.plot(1, 1)
	p02 = sim.plot(0, 1)
	#for k in range(3):
		#for j in range(3):
			#sim.output(k,j, True)

	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		app.exec_()
