#! /usr/bin/env python

# IMPORT
import os
import sys
import time
import yaml
import pickle
import argparse
from multiprocessing import Pool

import numpy as np
import scipy.linalg
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm
import pandas as pd
from numpy import cos, sin, sqrt, pi
from pathlib import Path

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg

from rospkg import RosPack
from rosbag import Bag
from tf.transformations import euler_from_quaternion
from nav_msgs.msg import Odometry
from roslaunch.substitution_args import resolve_args
from map_lib import LoaderSqlite, MapTile
from map_server_msgs.msg import RoadSignal
from multiception.msg import Class
from multiception.object import StaticObject
from multiception.association import PenaltyMatrix, mahalanobis_class

RS_TYPES  = { 1: 'Traffic Light', 2: 'Stop', 3: 'Yield', 4: 'Street Light', 5: 'Traffic Sign', 6: 'Other' }
P_TYPES   = { 101: 'Unknown', 102: 'Yield', 103: 'Stop', 104: 'Speed', 105: 'Priority', 106: 'Direction',
              107: 'Roundabout', 108: 'Traffic Light', 109: 'Street Light', 110: 'Pedestrian', 111: 'Road Arrow' }

COLORS = { 'LightRed':   (240,0,0), 'DarkRed':   (140,0,0), 'Orange': (255,165,0), 'Pink': (255,192,203),
           'LightBlue':  (0,0,240), 'DarkBlue':  (0,0,140), 'Black': (0,0,0), 'Gray': (128,128,128),
           'LightGreen': (0,240,0), 'DarkGreen': (0,140,0), 'Yellow': (240,240,0) }

RS_COLORS = { 1: COLORS['Orange'], 2: COLORS['DarkRed'],   3: COLORS['LightRed'],
              4: COLORS['Gray'],   5: COLORS['LightBlue'], 6: COLORS['Black'] }
P_COLORS  = { 101: COLORS['Black'],      102: COLORS['LightRed'],  103: COLORS['DarkRed'],
              104: COLORS['LightGreen'], 105: COLORS['DarkGreen'], 106: COLORS['Pink'],
              107: COLORS['DarkBlue'],   108: COLORS['Orange'],    109: COLORS['Gray'],
              110: COLORS['Yellow'],     111: COLORS['LightBlue'] }

MAP_PERCEPT_CORRESP = {
	RoadSignal.TYPE_TRAFFICLIGHT: Class.SIGN_TRAFFICLIGHT,
	RoadSignal.TYPE_STOP: Class.SIGN_STOP,
	RoadSignal.TYPE_YIELD: Class.SIGN_YIELD,
	RoadSignal.TYPE_STREETLIGHT: Class.SIGN_STREETLIGHT,
	RoadSignal.TYPE_TRAFFICLIGHT: Class.SIGN_TRAFFICLIGHT
}

#~ ERR_MAX = 2
ERR_MAX = 20
#GATE = 4 # LiDAR
GATE = 20 # Mobileye
CLASS_MALUS = 15
TOPIC = '/mobileye/percepts'
KEEP_ONLY_MULTISENSOR = False
CONFUSION_MAT_PATH = Path(RosPack().get_path('multiception'), "config", "class_penalty.yml")
PKL_DIR = Path('/mnt/hdd/mobileye_eval')
ASSOCS_PATH = PKL_DIR/'assocs.pkl'
BLACKL_PATH = PKL_DIR/'blacklist_mobileye.pkl'
EXCLUDED_RS_TYPES = (4, 7)

blacklist = []

def clicked(plot, points, df):
	global blacklist
	blacklist.extend([p.data() for p in points])
	for p in points:
		p.setPen('b', width=2)

# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)
def header2seconds(header):
	return header.stamp.secs + 1e-9*header.stamp.nsecs

def parse_args():
	# Ex: rosrun multiception mobileye_evaluation.py -vc `rospack find multiception`/config/location/Localization2020/BetweenRoundabout1_static.yaml /mnt/hdd/mobileye_eval/MobileyeEvalBetweenRoundabouts.bag
	parser = argparse.ArgumentParser(description='Plots')
	parser.add_argument('-v', '--verbose', help='Show status', action='store_true')
	parser.add_argument('--pkl', help='Save observation pickle')
	requiredNamed = parser.add_argument_group('required named arguments')
	requiredNamed.add_argument('-c', '--config', help='Config file with map reference frame', required=True)
	parser.add_argument('bag', type=str, help='Bags to read')

	return parser.parse_args()

def load_map(map_config_file):
	map_config_file = Path(map_config_file).expanduser().resolve()

	# Get map config (reference, radius, etc)
	with open(map_config_file , 'r') as f:
		config = yaml.safe_load(f)

	maptile = MapTile()
	loader = LoaderSqlite(resolve_args(config["map_file_name"]), config['srid'])
	loader.setArea(config["rad_lon"], config["rad_lat"], config['radius'], config['srid'])
	loader.loadMap(maptile, "linkborders=*;roadsignals=*")

	list_rs = [(0.0, rs.geometry[0], rs.geometry[1], 0, MAP_PERCEPT_CORRESP.get(rs.type, Class.SIGN_UNKNOWN)) for _, rs in maptile.roadSignals.items() if rs.type not in EXCLUDED_RS_TYPES]
	dfrs = pd.DataFrame(list_rs, columns=tuple("txyOc"))
	dfrs.set_index(pd.to_datetime(dfrs["t"], unit="s"), inplace=True)

	return maptile, dfrs

def plot_map(map_, dfmap, fig):
	# Build a single array containing all points of all polylines + mask over link border's transitions
	lbs = [lb.polyline[:2] for _, lb in map_.linkBorders.items()]
	lb_connections = np.concatenate([[True]*(lb.shape[1]-1)+[False] for lb in lbs])
	lbs_points = np.hstack(lbs).T
	fig.plot(lbs_points, connect=lb_connections)

	#~ cm = pg.ColorMap(np.arange(min(RS_TYPES.keys()), max(RS_TYPES.keys())+1), ['r', 'g', 'b', 'c', 'm', 'y'])
	#~ rs_types_range = np.arange(min(RS_TYPES.keys()), max(RS_TYPES.keys())+1)
	#~ rss = plt.scatter(dfmap['x'], dfmap['y'], c=dfmap['c'], norm=BoundaryNorm(boundaries=rs_types_range-0.5, ncolors=256), zorder=5, cmap='Set2')
	#~ rscb = plt.colorbar(rss, spacing='uniform')
	#~ rscb.set_ticks(rs_types_range)
	#~ rscb.set_ticklabels([RS_TYPES[key] for key in rs_types_range])
	#~ rscb.ax.set_ylabel('Map Type')

	roadsigns_plot = pg.ScatterPlotItem(size=1, pen=None, pxMode=False)
	spots = [{'pos': [x+(np.random.random()-0.5)*1.25,y+(np.random.random()-0.5)*1.25], 'data': 1, 'brush': P_COLORS[c], 'symbol': 'o', 'size': 1} for x,y,c in np.asarray(dfmap[['x','y','c']])]
	roadsigns_plot.addPoints(spots)
	fig.addItem(roadsigns_plot)

def load_pose(bagfile, topic):
	"""
	Convert rosbag into pandas dataframe using specified Odometry topic
	"""

	dict_odom = { "t": [], "x": [], "y": [], "O": [], "covx": [], "covy": [] }

	with Bag(bagfile, "r") as bag:
		for _, msg, _ in bag.read_messages(topics=[topic]):
			dict_odom["t"].append(header2seconds(msg.header))
			dict_odom["x"].append(msg.pose.pose.position.x)
			dict_odom["y"].append(msg.pose.pose.position.y)
			dict_odom["O"].append(euler_from_quaternion((
				msg.pose.pose.orientation.x,
				msg.pose.pose.orientation.y,
				msg.pose.pose.orientation.z,
				msg.pose.pose.orientation.w
			))[2])
			#~ dict_odom["cov"].append(np.array(msg.pose.covariance).reshape((6, 6)))
			dict_odom["covx"].append(msg.pose.covariance[0])
			dict_odom["covy"].append(msg.pose.covariance[7])

	df = pd.DataFrame(dict_odom)
	df.set_index(pd.to_datetime(df["t"], unit="s"), inplace=True)

	return df

def plot_pose(df, fig):
	points = df[['x', 'y']]
	covs = df['covx']+df['covy']
	print('Trajectory covariance: Min={}, Mean={}, Max={}'.format(covs.min(), covs.mean(), covs.max()))
	#~ lc = LineCollection([points], cmap='RdYlGn_r', norm=plt.Normalize(0, covs.max()))
	#~ lc.set_array(covs)
	#~ lc.set_linewidth(1)
	#~ line = plt.gca().add_collection(lc)
	#~ cb = plt.colorbar(line)
	#~ cb.ax.set_ylabel('Pose quality (m)')
	fig.plot(df['x'], df['y'], pen='g')

def load_perceptions(bagfile, topic, keep_only_multisensor):
	dict_obs = { "t": [], "x": [], "y": [], "O": [], "covx": [], "covy": [], "covxy": [], "c": [], "i": [] }
	i = 0

	with Bag(bagfile, "r") as bag:
		for _, msg, msg_t in bag.read_messages(topics=[topic]):
			for p in msg.perceptions:
				if not keep_only_multisensor or '+' in p.sensor:
					dict_obs["t"].append(header2seconds(msg.header)) # msg_t.to_sec()
					dict_obs["x"].append(p.state.x)
					dict_obs["y"].append(p.state.y)
					dict_obs["O"].append(p.state.yaw)
					dict_obs["covx"].append(p.state.cov[0])
					dict_obs["covy"].append(p.state.cov[7])
					dict_obs["covxy"].append(p.state.cov[1])
					dict_obs["c"].append(p.classif.value)
					dict_obs["i"].append(i)
				i+=1

	df = pd.DataFrame(dict_obs)
	df.set_index(pd.to_datetime(df["t"], unit="s"), inplace=True)
	

	return df

def get_ellipse(mean, cov, alpha, **kwargs):
	s = np.arange(0, 2*np.pi, 0.05)
	x = np.dot(mean, np.ones((1, s.shape[0]))) + np.dot(scipy.linalg.sqrtm(-2*np.log(alpha)*cov), np.array([np.cos(s), np.sin(s)]))
	return x

def plot_perceptions(df, fig):
	p_types_range = np.arange(min(P_TYPES.keys()), max(P_TYPES.keys())+1)
	#~ plt.scatter(df['x'], df['y'], c=df['c'], norm=BoundaryNorm(boundaries=p_types_range-0.5, ncolors=256), edgecolors='none', zorder=3)
	#~ fig.plot(df['x'], df['y'], pen=None, symbol='d')
	#~ rscb = plt.colorbar(spacing='uniform')
	#~ rscb.set_ticks(p_types_range)
	#~ rscb.set_ticklabels([P_TYPES[key] for key in p_types_range])
	#~ rscb.ax.set_ylabel('Perception Type')

	# Prepare the perception mean as spot
	perceptions_plot = pg.ScatterPlotItem(size=0.1, pen=None, pxMode=False)
	spots = [{'pos': [x,y], 'data': i, 'brush': P_COLORS[c], 'symbol': 'd', 'size': 0.1} for x,y,c,i in np.asarray(df[['x','y','c','i']])]
	perceptions_plot.addPoints(spots)
	perceptions_plot.sigClicked.connect(lambda plot, points: clicked(plot, points, df))

	# Prepare the perception covariance as ellipse
	ellipsis = [ get_ellipse( np.array([[x],[y]]), np.array([[cx,cxy], [cxy,cy]]), 0.99 ) for x,y,cx,cy,cxy in np.asarray(df[['x','y','covx','covy','covxy']]) ]
	points = np.concatenate(ellipsis, axis=1)
	points_conn = np.concatenate([[True]*(ellipsis[0].shape[1]-1)+[False]]*len(ellipsis))
	fig.plot(points[0], points[1], connect=points_conn)
	fig.addItem(perceptions_plot)

	legend = pg.LegendItem(offset=(70,20))
	legend.setParentItem(fig)
	for classif in P_TYPES.keys():
		item = pg.ScatterPlotItem(size=15, pen=P_COLORS[classif], pxMode=True, symbol='s', brush=P_COLORS[classif])
		legend.addItem(item, P_TYPES[classif])

def sync_dataframe(df1, df2):
	df2 = df2.append(pd.DataFrame(index=df1.index), sort=True).sort_index()
	df2 = df2.interpolate(method='time').loc[df1.index]
	df1 = df1.loc[df2.index.drop_duplicates()]
	df1 = df1.groupby(df1.index).first()
	df2 = df2.groupby(df2.index).first()
	return df1, df2

def transform_perception_work(df):
	c = cos(df["locO"])
	s = sin(df["locO"])
	c2 = c**2
	s2 = s**2
	cs = c*s

	"""
	R*X = 
				[x]
				[y]
	[c, -s]		[cx-sy]
	[s,  c]		[sx+cy]
	
	R*P*R.T = 
	         [x, xy]          [ c, s]
	         [xy, y]          [-s, c]
	[c, -s]  [cx-sxy cxy-sy]  [c(cx-sxy)-s(cxy-sy) s(cx-sxy)+c(cxy-sy)]
	[s,  c]  [sx+cxy sxy+cy]  [c(sx+cxy)-s(sxy+cy) s(sx+cxy)+c(sxy+cy)]
	=
	[c²x+s²y-2csxy     sc(x-y)+xy(c²-y²)]
	[cs(x-y)+xy(c²-y²) s²x+c²y+2csxy]
	"""

	dfw = df.copy()
	dfw['x'] = df['locx'] + c*df['x'] - s*df['y']
	dfw['y'] = df['locy'] + s*df['x'] + c*df['y']
	dfw['O'] = df['locO'] + df['O']

	dfw['covx'] = c2*df['covx'] + s2*df['covy'] - 2*cs*df['covxy']
	dfw['covy'] = s2*df['covx'] + c2*df['covy'] + 2*cs*df['covxy']
	dfw['covxy'] = cs*(df['covx']-df['covy']) + df['covxy']*(c2-s2)

	return dfw

DETECTIONS = None
MAPOBJS = None
CONFUSION_MAT = None

def compute_cost_multiprocess(i):
	return [mahalanobis_class(DETECTIONS[i], mapob, CONFUSION_MAT, CLASS_MALUS) for mapob in MAPOBJS]

def associate(df, dfmap):
	global DETECTIONS
	global MAPOBJS
	global CONFUSION_MAT
	# Use a precomputed matrix & association from pickle if possible
	if os.path.exists(ASSOCS_PATH):
		with open(ASSOCS_PATH, 'rb') as pkl:
			return pickle.load(pkl)
	else:
		CONFUSION_MAT = PenaltyMatrix.from_yaml(CONFUSION_MAT_PATH)
		DETECTIONS = []
		MAPOBJS = []

		# Transform df to object lists for compatibility with other association functions
		for i, (x, y, c, covx, covy) in enumerate(np.asarray(df[['x', 'y', 'c', 'covx', 'covy']])):
			o = StaticObject()
			o.state[:2] = x,y
			o.covin[:2,:2] = np.diag([covx, covy])
			o.classif = c
			DETECTIONS.append(o)

		for i, (x, y, c) in enumerate(np.asarray(dfmap[['x', 'y', 'c']])):
			o = StaticObject()
			o.state[:2] = x,y
			o.covar[:2,:2] = np.diag([.5, .5])
			o.classif = c
			MAPOBJS.append(o)

		with Pool() as p:
			mat = np.array(list(p.map(
				compute_cost_multiprocess,
				range(len(DETECTIONS))
			)))

		assocs = list(zip(range(mat.shape[0]), np.argmin(mat, axis=1), np.min(mat, axis=1)))
		return assocs

def compute_errors(df, dfmap, assocs, err_max):
	df['assoc'] = [assoc[1] for assoc in assocs]
	df['ex'] = [df['x'].iloc[assoc[0]]-dfmap['x'].iloc[assoc[1]] for assoc in assocs]
	df['ey'] = [df['y'].iloc[assoc[0]]-dfmap['y'].iloc[assoc[1]] for assoc in assocs]
	df['d'] = [assoc[2] for assoc in assocs] # association distance

	c = cos(df["O"]+np.pi/2)
	s = sin(df["O"]+np.pi/2)

	df["elon"] =  c*df["ex"] + s*df["ey"]
	df["elat"] = -s*df["ex"] + c*df["ey"]

	df["er"] = sqrt((df["ex"]**2 + df["ey"]**2))
	df["eO"] = np.arctan2(df["elat"], df["elon"])

	assocs = np.asarray(assocs)
	boolarray = np.asarray(df['d']<err_max)
	assocsreduced = assocs[boolarray]
	dfreduced = df[boolarray]

	return dfreduced, assocsreduced

def plot_assocs(df, dfmap, assocs, fig):
	assoc_lines = np.zeros((len(assocs)*2, 2))
	assoc_lines[0::2] = df[['x', 'y']].iloc[[a[0] for a in assocs]]
	assoc_lines[1::2] = dfmap[['x', 'y']].iloc[[a[1] for a in assocs]]
	fig.plot(assoc_lines[:, 0], assoc_lines[:, 1], connect='pairs', pen=(50,50,255,10))

	#~ for i, assoc in enumerate(assocs):
		#~ x, y = df[['x', 'y']].iloc[assoc[0]]
		#~ xm, ym = dfmap[['x', 'y']].iloc[assoc[1]]

		#~ plt.plot([x,xm], [y,ym], color='purple')
		#~ plt.text((x+xm)/2, (y+ym)/2, '{}'.format(i))

def transform_map_body(df):
	c = cos(df["locO"])
	s = sin(df["locO"])

	# Inverse of transform_perception_work
	dfb = df.copy()
	dfb['x'] = c*(df['mapx']-df['locx']) + s*(df['mapy']-df['locy'])
	dfb['y'] = c*(df['locx']-df['mapx']) + s*(df['mapy']-df['locy'])
	dfb['O'] = df['locO'] - df['O']

	return dfb

def create_win_and_fig(title, winx, winy, plotx0, plotx1, ploty0, ploty1, padding=0):
	win = pg.GraphicsWindow()
	win.resize(winx, winy)
	win.setWindowTitle(title)
	fig = win.addPlot(title=title)
	fig.setXRange(plotx0, plotx1, padding=padding)
	fig.setYRange(ploty0, ploty1, padding=padding)
	return win, fig

if __name__=='__main__':
	args = parse_args()
	bagfile = args.bag

	map_, df_map = load_map(args.config)

	# Load dataset
	t0 = time.time()
	df_loc = load_pose(bagfile, '/span/enuposeCovs')
	df_p = load_perceptions(bagfile, TOPIC, KEEP_ONLY_MULTISENSOR)
	t1 = time.time()

	# Load blacklist from pkl if exists
	# Remove blacklisted perceptions
	if os.path.exists(BLACKL_PATH):
		with open(BLACKL_PATH, 'rb') as pkl:
			blacklist = [int(x) for x in list(set(pickle.load(pkl)))] # Remove duplicates
			print('Removing {} blacklisted elements from {}'.format(len(blacklist), len(df_p)))
			keep = np.ones(len(df_p), dtype=bool)
			keep[blacklist] = False
			df_p = df_p[keep]

	## Sync & Merge dataframes
	# Localization and body-referenced perceptions -> working-referenced perceptions for overall display
	#~ df_loc, df_p = sync_dataframe(df_loc, df_p)
	df_p, df_loc = sync_dataframe(df_p, df_loc)
	df_locp = pd.concat([df_loc.add_prefix("loc"), df_p], axis=1, sort=True)
	if 'percepts_' in TOPIC: # If evaluated percepts are in the body frame, transform them to work
		df_locp = transform_perception_work(df_locp)

	df_locp_bl = df_locp.dropna()
	t2 = time.time()

	assocs = associate(df_locp_bl, df_map)
	t3 = time.time()

	df_locp_reduced, assocs_reduced = compute_errors(df_locp_bl, df_map, assocs, ERR_MAX)
	t4 = time.time()

	print('Association error: Min={}, Mean={}, Max={}, Var={}'.format(df_locp_bl.er.min(), df_locp_bl.er.mean(), df_locp_bl.er.max(), np.var(df_locp_bl.er)))
	print('Reduced association error: Min={}, Mean={}, Max={},  Var={}'.format(df_locp_reduced.er.min(), df_locp_reduced.er.mean(), df_locp_reduced.er.max(), np.var(df_locp_reduced.er)))
	print('')
	print('# Assoc {:.3f}/{:.3f}  Mean Error lon={:.3f}±{:.3f} lat={:.3f}± {:.3f}  Absolute Error lon={} lat={}'.format(
		len(df_locp_reduced), len(df_locp),
		df_locp_reduced['elon'].mean(), np.var(df_locp_reduced['elon']),
		df_locp_reduced['elat'].mean(), np.var(df_locp_reduced['elat']),
		np.abs(df_locp_reduced['elon']).mean(), np.abs(df_locp_reduced['elat']).mean()
	))
	print('')

	# Plotting
	pg.setConfigOption('background', 'w')
	pg.setConfigOption('foreground', 'k')
	pg.setConfigOptions(antialias=True)

	## Overall view
	win0, fig0 = create_win_and_fig('Overall View', 1000, 1000, -100, 1700, -100, 1700, 0)
	fig0.setLabels(left='y (m)', bottom='x (m)')
	fig0.showGrid(True, True, 0.2)
	plot_pose(df_loc.dropna(), fig0)
	plot_assocs(df_locp_bl, df_map, assocs_reduced, fig0)
	plot_perceptions(df_locp_bl, fig0)
	plot_map(map_, df_map, fig0)
	t5 = time.time()

	## Error Plots in Working
	win1, fig1 = create_win_and_fig('Error Plot (wrt. Working Frame)', 1000, 1000, -ERR_MAX, ERR_MAX, -ERR_MAX, ERR_MAX, 0)
	fig1.setLabels(left='y (m)', bottom='x (m)')
	fig1.showGrid(True, True, 0.8)
	errorwork_plot = pg.ScatterPlotItem(size=10, pen=None, brush=pg.mkColor(44,137,160,30), pxMode=True)
	errorwork_plot.addPoints( [{'pos': [ex,ey], 'data': 1, 'symbol': 'o'} for ex,ey in np.asarray(df_locp_reduced[['ex','ey']])] )
	fig1.addItem(errorwork_plot)
	t6 = time.time()

	## Error Plots in Body
	win2, fig2 = create_win_and_fig('Error Plot (wrt. Body Frame)', 1000, 1000, -ERR_MAX, ERR_MAX, -ERR_MAX, ERR_MAX, 0)
	fig2.setLabels(left='lon (m)', bottom='lat (m)')
	fig2.showGrid(True, True, 0.8)
	errorwork_plot = pg.ScatterPlotItem(size=10, pen=None, brush=pg.mkColor(44,137,160,30), pxMode=True)
	errorwork_plot.addPoints( [{'pos': [elon,elat], 'data': 1, 'symbol': 'o'} for elon,elat in np.asarray(df_locp_reduced[['elon','elat']])] )
	fig2.addItem(errorwork_plot)

	#~ ## Error over time
	#~ win3 = pg.GraphicsWindow(); fig3 = win3.addPlot()
	#~ fig3.setLabels(left='Error (|m|)', bottom='Time (s)')
	#~ fig3.plot(df_locpwork_reduced['er'])
	
	#~ win4 = pg.GraphicsWindow(); fig4 = win4.addPlot()
	#~ fig4.setLabels(left='Error (|m|)', bottom='Time (s)')
	#~ fig4.plot(df_locpwork_reduced['eO'])
	t7 = time.time()
	t8 = time.time()

	print('{:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f} {:.2f}'.format(t1-t0, t2-t1, t3-t2, t4-t3, t5-t4, t6-t5, t7-t6, t8-t7))

	plt.rcParams['text.usetex'] = True
	plt.rcParams['axes.spines.right'] = False
	plt.rcParams['axes.spines.top'] = False
	fig, ax = plt.subplots(1, 1, figsize=(4, 4), tight_layout=True)
	ax.scatter(df_locp_reduced.elon, df_locp_reduced.elat, alpha=0.1)
	ax.grid(alpha=0.2)
	ax.axis((GATE, -GATE, -GATE, GATE))
	r = np.linalg.norm([df_locp_reduced.elon[df_locp_reduced.er<GATE], df_locp_reduced.elat[df_locp_reduced.er<GATE]], axis=0)
	print(f"RMSE {r.mean()} +/- {r.var()}")
	ax.set_xlabel("Lateral error (m)")
	ax.set_ylabel("Longitudinal error (m)")
	plt.savefig(Path(__file__).with_suffix('.pdf'), transparent=True)
	plt.show()

	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		QtGui.QApplication.instance().exec_()

	# Save the blacklist
	if len(blacklist)>0:
		if not os.path.exists(PKL_DIR):
			os.makedirs(PKL_DIR)
		with open(BLACKL_PATH, 'wb') as pkl:
			pickle.dump(blacklist, pkl)
	exit()
