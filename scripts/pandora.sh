#! /usr/bin/env bash

VIDEOS_FOLDER="$1"
if [[ -z "$VIDEOS_FOLDER" ]]
then
    VIDEOS_FOLDER="."
fi

OUTPUT_FILE="$2"
if [[ -z "$OUTPUT_FILE" ]]
then
    OUTPUT_FILE="$VIDEOS_FOLDER/pandora.mp4"
fi
echo "'$VIDEOS_FOLDER' '$OUTPUT_FILE'"

VIDEO_COLOR=$(find $VIDEOS_FOLDER -name '*front_color*.mp4')
VIDEO_LEFT=$(find $VIDEOS_FOLDER -name '*left_grey*.mp4')
VIDEO_BACK=$(find $VIDEOS_FOLDER -name '*back_grey*.mp4')
VIDEO_FRONT=$(find $VIDEOS_FOLDER -name '*front_grey*.mp4')
VIDEO_RIGHT=$(find $VIDEOS_FOLDER -name '*right_grey*.mp4')

if [[ -z "$VIDEO_COLOR" || -z "$VIDEO_LEFT" || -z "$VIDEO_BACK" || -z "$VIDEO_FRONT" || -z "$VIDEO_RIGHT" ]]
then
    echo "One video or more is missing"
    exit 1
fi

ffmpeg -i $VIDEO_COLOR \
       -i $VIDEO_LEFT -filter_complex "scale=640:360, overlay=0:0" \
       -i $VIDEO_FRONT -filter_complex "scale=640:360, overlay=640:0" \
       $OUTPUT_FILE
