#! /usr/bin/env bash

sleep $4
rosbag play --clock --quiet --rate="$3" $1/$2/*/organized/*.bag
