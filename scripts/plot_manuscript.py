#!/usr/bin/env python3

import sys
import pickle
from pathlib import Path
from tqdm import tqdm
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt
import existence_evaluation

plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 18
plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.spines.top'] = False

def parse_args():
    parser = ArgumentParser(description="Plots pre-computed scores (from compare_evaluation.py)")
    parser.add_argument("--stanford", help="Plot the stanford diagrams", action="store_true")
    parser.add_argument("--error", help="Plot the error curves", action="store_true")
    parser.add_argument("--obj-PR", help="Plot the PR curves for object existence", action="store_true")
    parser.add_argument("--grid-PR", help="Plot the PR curves for free space", action="store_true")
    parser.add_argument("--grid-ROC", help="Plot the ROC curves for free space", action="store_true")
    parser.add_argument("--nodisplay", help="Just output tables, no display", action="store_true")
    parser.add_argument("--svg", help="Save as SVG, not PDF", action="store_true")
    parser.add_argument("--output", help="Where to save the produced plots", default=None)
    parser.add_argument("--labels", help="Comma-separated ordered labels (e.g. 'label1,label2,...'", default=None)
    parser.add_argument("EVALDIR_PATH", type=str, nargs="+", help="List of paths where pre-computed score pickles are stored")
    return parser.parse_args()

if __name__=="__main__":
    args = parse_args()
    paths = [Path(path).expanduser().resolve() for path in args.EVALDIR_PATH]
    for path in paths:
        if not path.exists() or not path.is_dir():
            print(f"Warning: {path} does not exists/is not a directory", file=sys.stderr)
    paths = [path for path in paths if path.exists() and path.is_dir()]
    labels = args.labels.split(",") if args.labels else [path.stem.replace("_", "-") for path in paths]
    if args.output:
        output = Path(args.output).expanduser().resolve()
        output.mkdir(parents=True, exist_ok=True)

    objscores = {}
    gridscores = {}
    errcovs = {}
    for label, path in tqdm(zip(labels, paths), desc="Loading inputs", total=len(paths)):
        with open(path/"objects.pkl", "rb") as f:
            objscores[label], _, _ = pickle.load(f)
        with open(path/"errors.pkl", "rb") as f:
            errcovs[label] = pickle.load(f)
        with open(path/"grids.pkl", "rb") as f:
            gridscores[label] = pickle.load(f)

    outputcolnames = []
    outputcolumns = { label: [] for label in labels }
    ext = "pdf" if not args.svg else "svg"

    if args.stanford:
        fig, ax = plt.subplots(1, 1, figsize=(5, 5), tight_layout=True)
        for label, errcov in errcovs.items():
            existence_evaluation.disp_stanford(errcov, ax=ax, label=label, samples=1)
        ax.legend(loc="upper right")
        if args.output: plt.savefig(output/f"stanford.{ext}")

    if args.error:
        fig, ax = plt.subplots(1, 1, figsize=(10, 4), tight_layout=True)
        for label, errcov in errcovs.items():
            e, stddev = existence_evaluation.disp_mean_errors(errcov, ax=ax, label=label, samples=1)
            outputcolumns[label].extend((np.nanmean(e), np.nanmean(stddev)/3, np.nanmean(e<stddev)))
        ax.legend(loc="upper left")
        outputcolnames.extend(("Mean RMSE", "Mean Std", "Consistency"))
        if args.output: plt.savefig(output/f"error.{ext}")

    if args.obj_PR:
        fig, ax = plt.subplots(1, 1, figsize=(5, 5), tight_layout=True)
        rows = []
        for label, score in objscores.items():
            p, r = existence_evaluation.disp_PR(score, ax=ax, label=label)
            auc = np.trapz(p[::-1], r[::-1])
            f1max = np.nanmax(2 * (p*r)/(p+r))
            outputcolumns[label].extend((f1max, auc))
        ax.legend(loc="lower left")
        outputcolnames.extend(("Obj Max F1", "Obj AUC"))
        if args.output: plt.savefig(output/f"obj-PR.{ext}")

    if args.grid_PR:
        fig, ax = plt.subplots(1, 1, figsize=(5, 5), tight_layout=True)
        for label, score in gridscores.items():
            existence_evaluation.disp_PR(score, ax=ax, label=label)
        ax.legend(loc="lower left")
        if args.output: plt.savefig(output/f"grid-PR.{ext}")

    if args.grid_ROC:
        fig, ax = plt.subplots(1, 1, figsize=(5, 5), tight_layout=True)
        for label, score in gridscores.items():
            fpr, tpr = existence_evaluation.disp_ROC(score, ax=ax, label=label)
            auc = np.trapz(tpr, fpr)
            outputcolumns[label].extend((auc,))
        ax.legend(loc="lower right")
        outputcolnames.extend(("FS AUC",))
        if args.output: plt.savefig(output/f"grid-ROC.{ext}")

    # Print output as latex matrix
    print(r"\begin{table}[ht!]")
    print(r"    \centering")
    print(r"    \caption{\label{}.}")
    print(r"    \resizebox{.99\linewidth}{!}{\begin{tabular}{"+"|".join(["r"]+["c"]*len(outputcolnames))+"}")
    print(r"        " + " & \small ".join([""]+outputcolnames) + r" \\\hline%")
    for rowname, row in outputcolumns.items():
        print(r"        \small "+rowname+" & " + " & ".join(f"${col:.2f}$" for col in row) +r" \\\hline%")
    print(r"    \end{tabular}}")
    print(r"\end{table}")

    if not args.nodisplay:
        plt.show()
