#!/usr/bin/env python3

import yaml, argparse
from pathlib import Path
import numpy as np
import pandas as pd
import rosbag
import matplotlib.pyplot as plt
np.set_printoptions(precision=2, linewidth=999, suppress=True, sign=' ')

def parse_args():
    parser = argparse.ArgumentParser(
        description='Plots the trust of all cars to all sources'
    )
    parser.add_argument('-v', '--verbose', help='Show status', action='store_true')
    parser.add_argument('bag', type=str, help='Path to rosbag')
    return parser.parse_args()

def load_bag(bagpath, topics):
    percepts = { '/'.join(topic.split('/')[1:-1]):[] for topic in topics}
    print(percepts)

    with rosbag.Bag(bagpath) as bag:
        info_dict = yaml.safe_load(bag._get_yaml_info())
        nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in topics])

        for topic, msg, t in tqdm(bag.read_messages(topics=topics), total=nMsg, desc='Loading messages'):
            source =  '/'.join(topic.split('/')[1:-1])
            for p in msg.perceptions:
                percepts[source].append(p)

    return percepts

def dfs_from_bag(bagpath, topics):
    percepts = load_bag(bagpath, topics)
    percept_dfs = { }
    for source in percepts.keys():
        percept_dfs[source] = np.array([(p.state.x, p.state.y) for p in percepts[source]])

    return percept_dfs

if __name__=='__main__':
    args = parse_args()

    if args.verbose:
        from tqdm import tqdm
    else:
        def tqdm(iter, total, desc):
            return iter

    sources = ['zoeblue/lidar', 'zoeblue/mobileye', 'zoegrey/lidar', 'zoewhite/lidar']
    path_bag = Path(args.bag).resolve()
    dfs = dfs_from_bag(path_bag, [f'/{source}/percepts_' for source in sources])

    for i, source in enumerate(sources):
        fig = plt.figure(i)
        ax = fig.add_subplot(projection='polar')
        plt.title(source)
        percentiles = sorted(list(range(0,105,5)) + [97.5,99])
        r = np.linalg.norm(dfs[source], axis=1)
        O = np.arctan2(dfs[source][:,1], dfs[source][:,0])
        print(source)
        print(np.vstack(( percentiles, np.percentile(r, percentiles), np.percentile(np.abs(O), percentiles) )))
        print('')
        ax.scatter(O, r)

        plt.figure((i+1)*10)
        percentiles = np.arange(0, 100, 0.1)
        plt.subplot(211).plot(percentiles, np.percentile(r, percentiles))
        plt.subplot(212).plot(percentiles, np.percentile(O, percentiles))
    plt.show()
