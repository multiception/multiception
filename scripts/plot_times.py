#! /usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from rosbag import Bag
from argparse import ArgumentParser

def parse_args():
    parser = ArgumentParser(description='Plots the message and header time of the given bag-topic tuples.')
    parser.add_argument('BAGTOPICS', nargs='+', type=str, help='Path of rosbags to process in the form path:topic.')
    return parser.parse_args()

if __name__=='__main__':
    args = parse_args()
    bagtopics_ = [tuple(bt.split(":")) for bt in args.BAGTOPICS]
    bagtopics = { bag: set() for bag, topic in bagtopics_ }
    for bag, topic in bagtopics_: bagtopics[bag].add(topic)

    times = {}
    for path, topics in bagtopics.items():
        with Bag(path) as bag:
            for topic, msg, t in tqdm(bag.read_messages(topics), desc=str(path)):
                #k = f"{path.split('/')[0]}:{path.split('/')[-1].split('.')[0]}"
                k = topic
                if k not in times:
                    times[k] = []
                times[k].append((t.to_sec(), msg.header.stamp.to_sec()))

    for k, time in times.items():
        time = np.array(time)
        print(time.shape)
        plt.plot(time[:,0], label=f"{k}:rec", alpha=.2)
        plt.plot(time[:,1], label=f"{k}:hea", alpha=.2)
    plt.legend()
    plt.show()
