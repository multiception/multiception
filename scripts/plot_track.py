#!/usr/bin/env python3

import yaml, argparse
from pathlib import Path
import numpy as np
import pandas as pd
import rosbag
import matplotlib.pyplot as plt

def parse_args():
    parser = argparse.ArgumentParser(
        description='Plots the trust of all cars to all sources'
    )
    parser.add_argument('-v', '--verbose', help='Show status', action='store_true')
    parser.add_argument('bag', type=str, help='Path to rosbag')
    return parser.parse_args()

def load_bag(bagpath, topics):
    tracks = { topic.split('/')[1]:{} for topic in topics}

    with rosbag.Bag(bagpath) as bag:
        info_dict = yaml.safe_load(bag._get_yaml_info())
        nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in topics])

        for topic, msg, t in tqdm(bag.read_messages(topics=topics), total=nMsg, desc='Loading messages'):
            source = topic.split('/')[1]
            for p in msg.perceptions:
                if p.object_id not in tracks[source].keys(): tracks[source][p.object_id] = {}
                tracks[source][p.object_id][msg.header.stamp.to_sec()] = p

    return tracks

def dfs_from_bag(bagpath, topics):
    tracks = load_bag(bagpath, topics)
    track_dfs = { k:{} for k in tracks.keys() }
    for source in tracks.keys():
        for oid in tracks[source].keys():
            track_dfs[source][oid] = np.array([(p.state.x, p.state.y) for t, p in tracks[source][oid].items()])

    return track_dfs

if __name__=='__main__':
    args = parse_args()

    if args.verbose:
        from tqdm import tqdm
    else:
        def tqdm(iter, total, desc):
            return iter

    sources = ['zoeblue', 'zoegrey', 'zoewhite', 'globaltracker', 'globaltracker_obs']
    path_bag = Path(args.bag).resolve()
    dfs = dfs_from_bag(path_bag, [f'/{source}/tracks' for source in sources])

    for i, source in enumerate(sources):
        plt.figure(i)
        plt.title(source)
        plt.axis('equal')
        plt.tight_layout()
        for oid, positions in dfs[source].items():
            plt.plot(positions[:,0], positions[:,1])
    plt.show()
