#!/usr/bin/env python3

import yaml, argparse
from pathlib import Path
import numpy as np
import pandas as pd
import rosbag
import matplotlib.pyplot as plt

from multiception.belief import MassFunction

def parse_args():
    parser = argparse.ArgumentParser(
        description='Plots the trust of all cars to all sources'
    )
    parser.add_argument('-v', '--verbose', help='Show status', action='store_true')
    parser.add_argument('bag', type=str, help='Path to rosbag')
    return parser.parse_args()

def load_bag(bagpath, topics):
    trusts = { topic.split('/')[1]:{} for topic in topics}

    with rosbag.Bag(bagpath) as bag:
        info_dict = yaml.safe_load(bag._get_yaml_info())
        nMsg = sum([topic_info['messages'] for topic_info in info_dict['topics'] if topic_info['topic'] in topics])

        for topic, msg, t in tqdm(bag.read_messages(topics=topics), total=nMsg, desc='Loading messages'):
            source_from = topic.split('/')[1]
            for source_to, trust in zip(msg.sources, msg.trusts):
                if source_to not in trusts[source_from].keys(): trusts[source_from][source_to] = {}
                trusts[source_from][source_to][msg.header.stamp.to_sec()] = MassFunction.from_msg(trust)

    return trusts

def dfs_from_trusts(bagpath, topics):
    trusts = load_bag(bagpath, topics)
    trust_dfs = { k:[] for k in trusts.keys() }
    notrust_dfs = { k:[] for k in trusts.keys() }
    for source_from in trusts.keys():
        dfs = [pd.DataFrame([(t, trust['t']) for t, trust in ttrust.items()], columns=['t', source_to]).set_index('t') for source_to, ttrust in trusts[source_from].items()]
        trust_dfs[source_from] = pd.DataFrame([], columns=['t']).set_index('t').join(dfs, how='outer', sort=True)

        dfs = [pd.DataFrame([(t, trust['T']) for t, trust in ttrust.items()], columns=['t', source_to]).set_index('t') for source_to, ttrust in trusts[source_from].items()]
        notrust_dfs[source_from] = pd.DataFrame([], columns=['t']).set_index('t').join(dfs, how='outer', sort=True)

    return trust_dfs, notrust_dfs

if __name__=='__main__':
    args = parse_args()
    plt.rcParams['text.usetex'] = True

    if args.verbose:
        from tqdm import tqdm
    else:
        def tqdm(iter, total, desc):
            return iter

    path_bag = Path(args.bag).resolve()
    vehicles = ['zoeblue', 'zoegrey', 'zoewhite']
    trust_dfs, notrust_dfs = dfs_from_trusts(path_bag, [f'/{v}/trust' for v in vehicles])

    ax = {}
    t0 = trust_dfs['zoeblue']['zoeblue/velodyne'].index[0]
    #fig, ((ax['zoeblue,zoeblue/velodyne'],ax['zoeblue,zoegrey'],ax['zoeblue,zoewhite']),(ax['zoegrey,zoeblue'],ax['zoegrey,zoegrey/velodyne'],ax['zoegrey,zoewhite']),(ax['zoewhite,zoeblue'],ax['zoewhite,zoegrey'],ax['zoewhite,zoewhite/velodyne'])) = plt.subplots(3,3, sharex=True, sharey=True, tight_layout=True)
    #fig, ((ax['zoeblue,zoegrey'],ax['zoeblue,zoewhite']),(ax['zoegrey,zoeblue'],ax['zoegrey,zoewhite']),(ax['zoewhite,zoeblue'],ax['zoewhite,zoegrey'])) = plt.subplots(3,2, sharex=True, sharey=True, tight_layout=True)
    fig, ((ax['zoeblue,zoegrey'],ax['zoegrey,zoeblue'],ax['zoewhite,zoeblue']),(ax['zoeblue,zoewhite'],ax['zoegrey,zoewhite'],ax['zoewhite,zoegrey'])) = plt.subplots(2,3, sharex=True, sharey=True, tight_layout=True)
    for v1 in vehicles:
        for v2 in vehicles:
            if v1==v2:
                #v2 = v1 + '/velodyne'
                continue
            a = ax[','.join([v1,v2])]
            t = trust_dfs[v1][v2]
            T = trust_dfs[v1][v2] + notrust_dfs[v1][v2]
            a.fill_between(T.index-t0, T, t, color='red', alpha=.025)
            a.fill_between(t.index-t0, t, 0, color='green', alpha=.025)
            a.plot(T.index-t0, T, color='red', linestyle='dashed')
            a.plot(t.index-t0, t, color='green')
            a.set_title('$~^%im^\\mathcal{T}_%i$' % (vehicles.index(v1)+1,vehicles.index(v2)+1))
    plt.show()
