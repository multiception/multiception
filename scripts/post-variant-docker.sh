#! /usr/bin/env bash
# Supposed to be launched inside a multiception container (built with multiception/workspace/Dockerfile) ran with
# - docker run -it --rm --volume ~/ws/multiception_ws:/WS --volume ~/Overtaking3-Raw.bag:/input.bag --volume /tmp:/tmp multiception

# Command launched with cases to study, e.g:
# - bash src/multiception/scripts/post-variant-docker.sh {caut,mode,bold}_{caut,mode,bold}_{none,coop,both}_none_none
# - bash src/multiception/scripts/post-variant-docker.sh bold_bold_both_{caut,mode,bold}_none-{global,fused,sequential}{yager,dempster}
# - bash src/multiception/scripts/post-variant-docker.sh bold_bold_both_{none,caut,mode,bold}_{none,easy}
# - bash src/multiception/scripts/post-variant-docker.sh {caut,mode,bold}_{caut,mode,bold}_{none,coop,both}_none_none bold_bold_both_{none,caut,mode,bold}_{none,easy}

set -e
trap 'if [[ $(jobs -pr|wc -l) != 0 ]]; then kill $(jobs -pr); fi' SIGINT SIGUSR1 SIGTERM EXIT

catkin build -DCMAKE_BUILD_TYPE=Release
source /WS/devel/setup.bash
SCENARIO=Multiception/Overtaking3
TMPDIR="/tmp/multiception-`date +%y%m%d%H%M%S`"
mkdir -p $TMPDIR
NJOBS=0
NPROC=$(nproc)
NRAM=$(free -g | grep Mem: | awk '{print $2}')

for variant in $@
do
	rosrun multiception post.py --output $TMPDIR/$variant.bag --cache $TMPDIR/$variant --variant /WS/src/multiception/config/postv2/$variant.yaml -- /input.bag $SCENARIO 2> $TMPDIR/$variant-errors.log &
	NJOBS=$(($NJOBS+1))
#	echo "$variant $NJOBS $((8*$NJOBS)) > $NPROC || $((15*$NJOBS)) > $NRAM"
#	sleep 5 &
	if [[ "$((8*$NJOBS))" -ge "$NPROC" ]]
	then
		wait
		NJOBS=0
	fi

	if [[ "$((15*$NJOBS))" -ge "$NRAM" ]]
	then
		wait
		NJOBS=0
	fi
done
wait

echo "Summary of errors:"
for f in $TMPDIR/*-errors.log;
do 
	printf "============= $f =============\n"
	cat $f
done
