#! /usr/bin/env python3

import sys
import pickle
import itertools
import logging
import hashlib
from yaml import safe_load
from tqdm import tqdm
from copy import deepcopy
from pathlib import Path
from argparse import ArgumentParser
from multiprocessing import Pool
import numpy as np

import rospy
from rosbag import Bag
from roslaunch.substitution_args import resolve_args
from map_lib import LoaderSqlite, MapTile, polygonFromLinks
from multiception import object, association
from multiception.evidential_grid_map import EvidentialGridMap
from multiception.tracking import Tracker, FILTERS, ASSOCIATORS, EXISTENCE_ESTIMATORS, INITIAL_TRUSTS
from multiception.belief import MassFunction
from multiception.trust import TrustBuilder
from multiception.tools import TimedDict, IdTimedDict, DummyMutex, DummySource, WhiteNoiseGenerator, BrownNoiseGenerator
from multiception.object import SimulatedVehicle
from multiception.cfg import TrackingConfig, TrustConfig
from multiception.msg import Perceptions, Perception, Source

def parse_args():
	parser = ArgumentParser(description="Runs an ego tracker on the given bag")
	parser.add_argument('-f', "--force", help='Force computations even if cache exists', action="store_true")
	parser.add_argument('-v', "--variant", help='Run configuration variant', type=str, default=None)
	parser.add_argument('-o', "--output", help='Path to output bag', type=str, default=None)
	parser.add_argument('-c', "--cache", help='Path to the cache directory (/tmp/SOMEHASH by default)', type=str, default=None)
	parser.add_argument("PATH", type=str, help="Path to rosbag containing inputs")
	parser.add_argument("SCENARIO", help='Name of the scenario (or dataset) being run. Used for map loading')
	return parser.parse_args()

def load_bag(path, topics):
	objects = IdTimedDict()
	sources = IdTimedDict()
	with Bag(path) as bag:
		nmsgs = sum([topic_info.message_count for topic, topic_info in bag.get_type_and_topic_info().topics.items() if topic in topics])
		for topic, msg, t in tqdm(bag.read_messages(topics=topics), desc=f"Loading {path}", total=nmsgs):
			for source in msg.sources:
				objects[source.source_id, msg.header.stamp.to_sec()] = [object.Object.from_msg(percept) for percept in msg.perceptions if percept.source_id==source.source_id  and not (percept.state.x>313 and percept.state.x<316 and percept.state.y>104 and percept.state.y<105)] # TODO: REMOVE THIS DIRTY HACK USED TO FIX OVERTAKING3
				sources[source.source_id, msg.header.stamp.to_sec()] = DummySource(EvidentialGridMap.from_msg(source.observability))
	return objects, sources

def export_bag(path, topic, sourceid, tracks, sources):
	path = Path(path)
	openflag = "a" if path.exists() else "w"
	with Bag(path, openflag, compression="bz2") as bag:
		times = sorted(set(tracks[sourceid].keys()) & set(sources[sourceid].keys()))
		for i, t in tqdm(enumerate(times), desc=f"Exporting {path}:{topic}", total=len(times)):
			msg = Perceptions()
			msg.header.seq = i
			msg.header.stamp = rospy.Time(t)
			msg.header.frame_id = "world"
			msg.sources.append(Source())
			msg.sources[0].source_id = sourceid
			msg.sources[0].type = Source.SOURCE_LOCAL_FUSION
			msg.sources[0].observability = EvidentialGridMap.to_msg(sources[sourceid, t].observability)
			for track in tracks[sourceid, t]:
				p = track.to_msg()
				p.debug = f'{track.oid}'
				p.source_id = sourceid
				p.header.frame_id = "world"
				#p.trace = np.ravel([(t.x, t.y) for t in self.tracker.get_history(track.oid)])
				msg.perceptions.append(p)

			bag.write(topic, msg, rospy.Time(t))

def setup_tracker(config_path, variant):
	config = deepcopy(TrackingConfig.defaults)
	with open(config_path) as f:
		config.update(safe_load(f))
	config.update(variant)

	# Resolve ROS params
	for k, v in config.items():
		if isinstance(v, str):
			config[k] = resolve_args(v)

	config["buffer_dur"] = 3600
	#config["source_id"] = sourceid
	evo_noises = {
		object.StaticObject         : np.diag([config["noise_ST_x"], config["noise_ST_y"], config["noise_ST_O"] ]),
		object.ConstantVelObject    : np.diag([config["noise_CA_x"], config["noise_CA_y"], config["noise_CA_vx"], config["noise_CA_vy"]]),
		object.CoordinatedTurnObject: np.diag([config["noise_CT_x"], config["noise_CT_y"], config["noise_CT_O"] , config["noise_CT_v"] , config["noise_CT_w"] ])
	}

	associator_cls = ASSOCIATORS[config["associator"]]
	if associator_cls is association.AssociatorMahalanobisClass or associator_cls in association.AssociatorMahalanobisClass.__subclasses__():
		associator = associator_cls(config["assoc_gate"], config["assoc_cost_max"], config["assoc_penalty_file"], config["assoc_penalty_cost"])
	else:
		associator = associator_cls(config["assoc_gate"], config["assoc_cost_max"])

	tracker = Tracker(None, None, {}, 0, 0)
	tracker.associator = associator
	tracker.obs_mutex = DummyMutex()
	tracker.filter_cls = FILTERS[config["filter"]]
	tracker.existence_estimator = EXISTENCE_ESTIMATORS[config["existence_estimator"]](**config)
	tracker.debugplot = False
	tracker.tqdm = tqdm
	tracker.freq = config["frequency"]
	tracker.evo_noises = evo_noises
	tracker.buffers_duration = config["buffer_dur"]
	tracker.merge_cost_max = config["merge_cost_max"]
	tracker.heading_dir_unknown = config["heading_dir_unknown"]
	tracker.trusts = deepcopy(INITIAL_TRUSTS)
	tracker.UNTRUSTWORTHY_DILUTION = {
		object.StaticState: np.diag([2, 2, np.pi/8])**2,
		object.ConstantVelState: np.diag([2, 2, 0.2, 0.2])**2,
		object.CoordinatedTurnState: np.diag([2, 2, np.pi/8, 0.3, np.pi/16])**2,
	}

	return tracker

def setup_truster(config_path, variant, map_config_path):
	config = deepcopy(TrustConfig.defaults)
	with open(config_path) as f:
		config.update(safe_load(f))
	config.update(variant)
	with open(map_config_path) as f:
		map_config = safe_load(f)

	# Resolve ROS params
	for k, v in config.items():
		if isinstance(v, str):
			config[k] = resolve_args(v)
	for k, v in map_config.items():
		if isinstance(v, str):
			map_config[k] = resolve_args(v)

	tbuilder = TrustBuilder()
	tbuilder.mutex = DummyMutex()
	tbuilder.BUFFER_DURATION = 3600
	tbuilder.mode = tbuilder.Mode(config["mode"])
	tbuilder.combi_fun = tbuilder.COMBI_FUNS[config["rule"]]
	tbuilder.HLTIME_LOCAL  = config["half_life_time_local"]
	tbuilder.HLTIME_REMOTE = config["half_life_time_remote"]
	tbuilder.COST_MISPLACED_OBJECTS      = config["cost_misplaced_objects"]
	tbuilder.COST_NONDETECTABLE_OBJECTS  = config["cost_nondetectable_objects"]
	tbuilder.FREESPACE_CONFIRMATION_AREA = config["freespace_confirmation_area"]

	# Setup weights
	tbuilder.weights['past']        = config["w_past"]
	tbuilder.weights['cohe']        = config["w_cohe"]
	tbuilder.weights['cohe-obd']    = config["w_cohe_obd"]
	tbuilder.weights['cohe-atc']    = config["w_cohe_atc"]
	tbuilder.weights['cohe-spc']    = config["w_cohe_spc"]
	tbuilder.weights['cohe-spc-ro'] = config["w_cohe_spc_ro"]
	tbuilder.weights['cohe-spc-bu'] = config["w_cohe_spc_bu"]
	tbuilder.weights['cons']        = config["w_cons"]
	tbuilder.weights['conf']        = config["w_conf"]
	tbuilder.weights['conf-osi']    = config["w_conf_osi"]
	tbuilder.weights['conf-fsi']    = config["w_conf_fsi"]
	tbuilder.weights['conf-ofi']    = config["w_conf_ofi"]
	tbuilder.weights['conf-odi']    = config["w_conf_odi"]
	tbuilder.weights['conf-odi-rc'] = config["w_conf_odi_rc"]
	tbuilder.weights['conf-odi-cr'] = config["w_conf_odi_cr"]

	# Setup static trusts
	static_names = config["static_names"].split(';') if len(config["static_names"])>0 else []
	static_val_trust = config["values_trust"].split(';') if len(config["values_trust"])>0 else []
	static_val_nottrust = config["values_nottrust"].split(';') if len(config["values_nottrust"])>0 else []
	if len(static_names)==len(static_val_trust) and len(static_names)==len(static_val_nottrust):
		for name, t, T in zip(static_names, static_val_trust, static_val_nottrust):
			tbuilder.set_static_source(name, MassFunction({ 't':float(t),'T':float(T),'tT':1-float(t)-float(T) }))
	else:
		rospy.logwarn('Not the same number of static trust values and names: %i!=%i!=%i', len(static_names), len(static_val_trust), len(static_val_nottrust))

	# Setup initial trusts
	for c in ("", "zoeblue/", "zoegrey/", "zoewhite/"):
		tbuilder.initial_trusts[f"{c}filter"] = MassFunction({'t': 1.0 ,'tT': 0.0})
		for source in ('lidar', 'mobileye', 'egotracker', 'multracker'):
			tbuilder.initial_trusts[f"{c}{source}"] = MassFunction({'t': 0.8 ,'tT': 0.2})
	for c in ("zoeblue", "zoegrey", "zoewhite"):
		tbuilder.initial_trusts[c] = MassFunction({'t': 0.0 ,'tT': 1.0})

	# Setup maptile
	maptile = MapTile()
	map_loader = LoaderSqlite(map_config["map_file_name"], map_config['srid'])
	map_loader.setArea(map_config["rad_lon"], map_config["rad_lat"], map_config['radius'], map_config['srid'])
	map_loader.loadMap(maptile, "links=*;building=*;!wall=no")
	maptile.connect()
	tbuilder.set_map(maptile)

	return tbuilder

def run_tracker(sourceid):
	# Use global variables for trackers, INPUTS, tracks and sources
	# This may seem dirty at first but this avoids copying those big arrays to subprocesses
	# In addition, (INPUTS, tracks, sources) are read-only, and trackers is not used outside this process
	logger = logging.getLogger('rosout')
	logger.addHandler(logging.FileHandler(str(WORK_DIR / f"{sourceid.replace('/', '_')}.log")))
	logger.setLevel(rospy.impl.rosout._rospy_to_logging_levels[rospy.DEBUG])

	tracker = trackers[sourceid]
	tracker.obs_temp = IdTimedDict((i, tv) for i, tv in tracks.items() if i in INPUTS[sourceid]).swap()
	tracker.sources = IdTimedDict((i, tv) for i, tv in sources.items() if i in INPUTS[sourceid])
	tmin, tmax = tracker.obs_temp.keys()[0]-1, tracker.obs_temp.keys()[-1]+1
	tracker.debug_name = sourceid
	tracker.t = tmin
	if sourceid in trust_histories: tracker.trusts = trust_histories[sourceid]
	tracker.step(tmax)

	filtered_tracks = tracker.tracks_buffer if tracker.freq==0 else TimedDict(tracker.tracks_buffer.closest(t) for t in np.arange(tmin, tmax, 1/tracker.freq))

	return (
		sourceid,
		filtered_tracks,
		TimedDict((t, np.extract(tracker.existence_estimator.validate(ttracks), ttracks)) for t, ttracks in filtered_tracks.items()),
		TimedDict((t, DummySource(tracker.obscon_buffer[t])) for t in filtered_tracks.keys()),
	)

def create_noise_generator(magnitude, amount, size=3, color="white", bias=0, **kwargs):
	cls = WhiteNoiseGenerator if color=="white" else BrownNoiseGenerator
	cov = np.eye(size)*magnitude
	return [cls(cov, bias=bias) for i in range(amount)]

def run_spoofing(sourceid, tracks, spoof_config, map_config_path):
	# Load map (for link ignoring and ghost following)
	with open(map_config_path) as f:
		map_config = safe_load(f)
	for k, v in map_config.items():
		if isinstance(v, str):
			map_config[k] = resolve_args(v)
	maptile = MapTile()
	map_loader = LoaderSqlite(map_config["map_file_name"], map_config['srid'])
	map_loader.setArea(map_config["rad_lon"], map_config["rad_lat"], map_config['radius'], map_config['srid'])
	map_loader.loadMap(maptile, "links=*")
	maptile.connect()

	# Initialize ghosts
	ghosts = [SimulatedVehicle(maptile=maptile, **ghost_config) for ghost_config in spoofs["ghosts"]]

	# Initialize links to ignore
	if "ignored-links" in spoof_config:
		links_ = []
		links_to_id_ = []
		for lid, link in maptile.links.items():
			links_.append(link.polyline)
			links_to_id_ += [lid]*link.polyline.shape[1]
		links = np.hstack(links_)[:2,:-1].T
		links_to_id = np.array(links_to_id_)[:-1]
		timed_ignored_links = [[a['t0'], a['t0']+a.get('duration', 1E30), a['links']] for a in spoofs.get('ignored-links', [])]

	noise_generators_pose = [(scramble['t0'], scramble['t0']+scramble.get('duration', 1E30), create_noise_generator(**scramble)) for scramble in spoofs.get("scramble-pose", [])]
	noise_generators_size = [(scramble['t0'], scramble['t0']+scramble.get('duration', 1E30), create_noise_generator(**scramble)) for scramble in spoofs.get("scramble-size", [])]
	np.random.seed(0) # Pseudo-random for reproductible results between variants
	tprev = tracks[sourceid].keys()[0]

	for t in tqdm(tracks[sourceid].keys(), desc=f"Spoofing {sourceid}", total=len(tracks[sourceid])):
		# Map match and ignore objects on certain links
		if "ignored-links" in spoof_config:
			ignored_links = { link_id for tbeg, tend, link_ids in timed_ignored_links for link_id in link_ids if t>tbeg and t<tend }
			object_positions = np.array([o.state[:2] for o in tracks[sourceid, t]])
			distances = np.linalg.norm(object_positions[None,:,:]-links[:,None,:], axis=2)
			map_matched_links = links_to_id[distances.argmin(axis=0)]
			tracks[sourceid, t] = [o for i, o in enumerate(tracks[sourceid, t]) if map_matched_links[i] not in ignored_links]

		# Predict and add ghosts
		predicted_ghosts = [ghost.step(t) for ghost in ghosts]
		tracks[sourceid, t].extend([ghost.toCoordinatedTurnObject() for ghost in predicted_ghosts if ghost is not None])

		# Scramble poses
		for tbeg, tend, scramblers in noise_generators_pose:
			if t>tbeg and t<tend:
				for i, j in enumerate(np.random.choice(range(len(tracks[sourceid, t])), min(len(tracks[sourceid, t]), len(scramblers)), replace=False)):
					tracks[sourceid, t][j].state[:3] += scramblers[i].rand(t-tprev)

		# Scramble sizes
		for tbeg, tend, scramblers in noise_generators_size:
			if t>tbeg and t<tend:
				for i, j in enumerate(np.random.choice(range(len(tracks[sourceid, t])), min(len(tracks[sourceid, t]), len(scramblers)), replace=False)):
					tracks[sourceid, t][j].shape[:3] += scramblers[i].rand(t-tprev)

		tprev = t

def run_truster(sourceid):
	# Use global variables for trusters, INPUTS, tracks and sources
	# This may seem dirty at first but this avoids copying those big arrays to subprocesses
	# In addition, (INPUTS, tracks, sources) are read-only, and trusters is not used outside this process
	logger = logging.getLogger('rosout')
	logger.addHandler(logging.FileHandler(str(WORK_DIR / f"{sourceid.replace('/', '_')}_trust.log")))
	logger.setLevel(rospy.impl.rosout._rospy_to_logging_levels[rospy.DEBUG])

	swapped_tracks = IdTimedDict((i, tv) for i, tv in tracks.items() if i in INPUTS[sourceid]).swap()
	truster = trusters[sourceid]
	truster.t = swapped_tracks.keys()[0]

	for inputt in tqdm(swapped_tracks.keys(), desc=f"Trusting {sourceid}", total=len(swapped_tracks)):
		rospy.loginfo("================================================= %.4f =================================================", inputt)
		for inputid in swapped_tracks[inputt].keys():
			truster.callback(inputid, inputt, tracks[inputid, inputt], sources[inputid, inputt].observability)

	return (
		sourceid,
		IdTimedDict((i, TimedDict((t, v[0]) for t, v in tv.items())) for i, tv in truster.history.items()),
		truster.debugtrusts
	)

def load_variant(path, root=""):
	if not path: return { k: {} for k in ("raw", "lid", "ego", "spo", "tru", "mul") }
	path = Path(root, path).with_suffix(".yaml").resolve(strict=True)
	with open(path) as f: variant = safe_load(f)
	subvariants = [load_variant(base, path.parent) for base in variant.get("bases", [])]

	resolved_variant = { key: {} for key in set(variant.keys()).union(k for subvariant in subvariants for k in subvariant)-set(["bases"])}
	for key in resolved_variant:
		for subvariant in subvariants:
			resolved_variant[key].update(subvariant.get(key, {}))
		resolved_variant[key].update(variant.get(key, {}))

	return resolved_variant

if __name__=="__main__":
	np.seterr(all='raise', under='ignore') 
	INPUT_TOPICS = (
		"/zoeblue/statep", "/zoeblue/lidar/percepts",
		"/zoegrey/statep", "/zoegrey/lidar/percepts",
		"/zoewhite/statep", "/zoewhite/lidar/percepts",
	)
	path_config = Path(resolve_args("$(find multiception)"))/"config"
	args = parse_args()
	pathin = Path(args.PATH).expanduser().resolve(strict=True)
	path_map_config = (path_config / "location" / args.SCENARIO).with_suffix(".yaml").resolve(strict=True)
	force = args.force

	if args.cache is None:
		md5 = hashlib.md5()
		md5.update(str(pathin).encode())
		md5.update(str(args.variant).encode())
		hash_args = md5.hexdigest()
		WORK_DIR = Path("/tmp", hash_args)
	else:
		WORK_DIR = Path(args.cache)

	CACHES = [WORK_DIR/f"{i}.pkl" for i in ("raw", "lidar", "ego", "tru", "mul")]
	if args.force:
		for sub in WORK_DIR.iterdir(): sub.unlink()
	WORK_DIR.mkdir(parents=True, exist_ok=True)
	print(f"Cache in {WORK_DIR}")
	OUTPUT = Path(args.output).expanduser().resolve() if args.output else WORK_DIR/"post.bag"
	OUTPUT_UNFILTERED = OUTPUT.with_name(OUTPUT.stem+"-unfiltered"+OUTPUT.suffix)

	CARS = { "zoeblue", "zoegrey" ,"zoewhite" }
	INPUTS = {
		"zoeblue/lidar":  { "zoeblue/lidar" },
		"zoegrey/lidar":  { "zoegrey/lidar" },
		"zoewhite/lidar": { "zoewhite/lidar" },
		"zoeblue/egotracker":  { "zoeblue/filter",  "zoeblue/lidar" },
		"zoegrey/egotracker":  { "zoegrey/filter",  "zoegrey/lidar" },
		"zoewhite/egotracker": { "zoewhite/filter", "zoewhite/lidar" },
		"zoeblue/multracker":  { "zoeblue/filter",  "zoeblue/lidar",  "zoegrey/egotracker", "zoewhite/egotracker" },
		"zoegrey/multracker":  { "zoegrey/filter",  "zoegrey/lidar",  "zoeblue/egotracker", "zoewhite/egotracker" },
		"zoewhite/multracker": { "zoewhite/filter", "zoewhite/lidar", "zoeblue/egotracker", "zoegrey/egotracker" },
	}

	variant = load_variant(args.variant)
	spoof_config_path = variant.get("spoof", None)
	if spoof_config_path:
		with open(spoof_config_path) as f:
			spoof_config = safe_load(f)

	lidar_trackers = { f"{c}/lidar": setup_tracker(path_config / "tracking" / "lidar.yaml", variant["lid"]) for c in CARS }
	ego_trackers = { f"{c}/egotracker": setup_tracker(path_config / "tracking" / "ego.yaml", variant["ego"]) for c in CARS }
	mul_trackers = { f"{c}/multracker": setup_tracker(path_config / "tracking" / "mul.yaml", variant["mul"]) for c in CARS }
	trusters = { f"{c}/multracker": setup_truster(path_config / "trust.yaml", variant["tru"], path_map_config) for c in CARS }
	trackers = dict(**lidar_trackers, **ego_trackers, **mul_trackers)
	trust_histories, trustss = {}, {}

	# Load inputs
	if not CACHES[0].exists():
		tracks, sources = load_bag(pathin, INPUT_TOPICS)

		with open(CACHES[0], "wb") as f: pickle.dump((tracks, sources), f)
		for cache in CACHES[1:]: cache.unlink(missing_ok=True)
	else:
		print(f"Using precomputed {CACHES[0]}")
		with open(CACHES[0], "rb") as f: tracks, sources = pickle.load(f)

	# Run LiDAR trackers in parallel
	if not CACHES[1].exists():
		with Pool() as p: rets = p.map(run_tracker, list(lidar_trackers.keys()))
		for sourceid, tracks_unfilt, track, source in rets:
			tracks[sourceid+"/unfilt"], tracks[sourceid], sources[sourceid+"/unfilt"], sources[sourceid] = tracks_unfilt, track, source, source

		with open(CACHES[1], "wb") as f: pickle.dump((tracks, sources), f)
		for cache in CACHES[2:]: cache.unlink(missing_ok=True)

		for c in CARS:
			export_bag(OUTPUT, f"/{c}/statep", f"{c}/filter", tracks, sources)
			export_bag(OUTPUT, f"/{c}/lidar_tracker/tracks", f"{c}/lidar", tracks, sources)
			export_bag(OUTPUT_UNFILTERED, f"/{c}/statep", f"{c}/filter", tracks, sources)
			export_bag(OUTPUT_UNFILTERED, f"/{c}/lidar_tracker/tracks", f"{c}/lidar/unfilt", tracks, sources)
	else:
		print(f"Using precomputed {CACHES[1]}")
		with open(CACHES[1], "rb") as f: tracks, sources = pickle.load(f)

	# Run ego trackers in parallel
	if not CACHES[2].exists():
		with Pool() as p: rets = p.map(run_tracker, list(ego_trackers.keys()))
		for sourceid, tracks_unfilt, track, source in rets:
			tracks[sourceid+"/unfilt"], sources[sourceid+"/unfilt"] = tracks_unfilt, source
			tracks[sourceid], sources[sourceid] = track, source
			tracks[sourceid.split("/")[0]], sources[sourceid.split("/")[0]] = track, source

		with open(CACHES[2], "wb") as f: pickle.dump((tracks, sources), f)
		for cache in CACHES[3:]: cache.unlink(missing_ok=True)

		for c in CARS:
			export_bag(OUTPUT, f"/{c}/egotracker/tracks", f"{c}/egotracker", tracks, sources)
			export_bag(OUTPUT_UNFILTERED, f"/{c}/egotracker/tracks", f"{c}/egotracker/unfilt", tracks, sources)
	else:
		print(f"Using precomputed {CACHES[2]}")
		with open(CACHES[2], "rb") as f: tracks, sources = pickle.load(f)

	# Spoof objects
	for car, spoofs in variant["spo"].items():
		sourceid = f"{car}/egotracker"
		run_spoofing(sourceid, tracks, spoofs, path_map_config)
		export_bag(OUTPUT, f"/{car}/spoofed/tracks", sourceid, tracks, sources)

	# Run trust builders in parallel
	if not CACHES[3].exists():
		#rets = [run_truster(sourceid) for sourceid in mul_trackers.keys()]
		with Pool() as p: rets = p.map(run_truster, list(mul_trackers.keys()))
		for sourceid, trust_history, trusts in rets:
			trust_histories[sourceid], trustss[sourceid] = trust_history, trusts

		with open(CACHES[3], "wb") as f: pickle.dump((trust_histories, trustss), f)
		for cache in CACHES[4:]: cache.unlink(missing_ok=True)
	else:
		print(f"Using precomputed {CACHES[3]}")
		with open(CACHES[3], "rb") as f: trust_histories, trustss = pickle.load(f)

	# Run mul trackers in parallel
	if not CACHES[4].exists():
		#rets = [run_tracker(sourceid for sourceid in mul_trackers.keys()]
		with Pool() as p: rets = p.map(run_tracker, list(mul_trackers.keys()))
		for sourceid, tracks_unfilt, track, source in rets:
			tracks[sourceid+"/unfilt"], tracks[sourceid], sources[sourceid+"/unfilt"], sources[sourceid] = tracks_unfilt, track, source, source

		with open(CACHES[4], "wb") as f: pickle.dump((tracks, sources), f)
		for cache in CACHES[5:]: cache.unlink(missing_ok=True)

		for c in CARS:
			export_bag(OUTPUT, f"/{c}/multracker/tracks", f"{c}/multracker", tracks, sources)
			export_bag(OUTPUT_UNFILTERED, f"/{c}/multracker/tracks", f"{c}/multracker/unfilt", tracks, sources)
	else:
		print(f"Using precomputed {CACHES[4]}")
		with open(CACHES[4], "rb") as f: tracks, sources = pickle.load(f)
