import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped
from tf_conversions import transformations

if __name__=='__main__':
	rospy.init_node('euler_from_quaternion')
	rospy.Subscriber('input', PoseWithCovarianceStamped, lambda msg: print(transformations.euler_from_quaternion([
		msg.pose.pose.orientation.x,
		msg.pose.pose.orientation.y,
		msg.pose.pose.orientation.z,
		msg.pose.pose.orientation.w]))
	)
	rospy.spin()
