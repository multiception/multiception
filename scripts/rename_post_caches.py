#! /usr/bin/env python3

from argparse import ArgumentParser
from itertools import starmap
from functools import partial
from pathlib import Path
from hashlib import md5

def md5hash(inputbag, prefix, variant, mode=None, rule=None):
	hashed = md5()
	varstring = f"{prefix}{variant}{'_'+mode if mode else ''}{'_'+rule if rule else ''}.yaml"
	print(variant, mode, rule, varstring)
	hashed.update(inputbag.encode())
	hashed.update(varstring.encode())
	return hashed.hexdigest()

parser = ArgumentParser(description='Renames cache dirs from post-variant-docker.sh into sensible names')
parser.add_argument("--dry-run", help='Do not actually rename folders, just show what is would do', action="store_true")
parser.add_argument("--inputbag", type=str, help='Path to the input bag', default="/input.bag")
parser.add_argument("--prefix", type=str, help='Prefix for yaml variant files', default="/WS/src/multiception/config/post/trust_")
parser.add_argument('PARENTFOLDER', type=str, help='Path to folder containing both *.bag and cache directories')
args = parser.parse_args()

dirin = Path(args.PARENTFOLDER).expanduser().resolve(strict=True)
inputbag = str(Path(args.inputbag).expanduser().resolve(strict=False))
prefix = str(Path(args.prefix).expanduser().resolve(strict=False))
variants = { (variant:=tuple(b.stem.split("-")[0].split("_"))): md5hash(inputbag, prefix, *variant) for b in dirin.glob("*-unfiltered.bag") }
caches = { d: d.stem for d in dirin.iterdir() if d.is_dir() and len(d.stem)==32 }

missed_cache = set(caches.values()) - set(variants.values())
if len(missed_cache)>0: print("Missed the following caches:", missed_cache)
missed_hash = set(variants.values()) - set(caches.values())
if len(missed_cache)>0: print("Missed the following hashes:", missed_hash)

matches = { ck: ck.with_name("_".join(vk)) for ck, cv in caches.items() for vk, vv in variants.items() if cv==vv }
print("Will rename: ", matches)
if not args.dry_run:
	for orig, dest in matches.items():
		orig.rename(dest)
