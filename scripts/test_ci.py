#! /bin/env python

import time
import argparse

from multiception.ci import naive,CI,FCI,IFCI,ITCI,ICI,SCI,IMF, _mahalanobis_dist, _euclidean_dist

import numpy as np
import scipy.linalg
import matplotlib
import matplotlib.pyplot as plt
np.set_printoptions(precision=3, formatter={'float': '{: 0.3f}'.format})

matplotlib.rcParams['axes.spines.right'] = False
matplotlib.rcParams['axes.spines.top'] = False
matplotlib.rcParams['font.size'] = 22
LWIDTH_MIN = 4
LWIDTH_MAX = 7

def KF(x1, C1, x2, C2):
	K = C1 @ np.linalg.pinv(C1+C2)
	d = x2-x1

	return x1+K@d, (np.identity(2)-K) @ C1, 0.5

def plot_ellipse(mean, cov, alpha, ax, **kwargs):
	s = np.arange(0, 2*np.pi, 0.01)
	x = np.dot(mean, np.ones((1, s.shape[0]))) + np.dot(scipy.linalg.sqrtm(-2*np.log(alpha)*cov), np.array([np.cos(s), np.sin(s)]))
	if 'opacity' in kwargs:
		opacity = kwargs['opacity']
		del kwargs['opacity']
	else:
		opacity = 0.9
	lines = ax.plot(x[0], x[1], alpha=opacity, **kwargs)
	ax.plot(mean[0], mean[1], '+', color=lines[0].get_color(), alpha=opacity)

def rot(t):
	return np.array([[np.cos(t),np.sin(t)],[-np.sin(t),np.cos(t)]])

LAYOUTS = {
	1: (1, 1),
	2: (1, 2),
	3: (1, 3),
	4: (2, 2),
	5: (2, 3),
	6: (2, 3),
	7: (3, 3),
}

CIs = {
	'naive': naive,
	'kf': KF,
	'ci': CI,
	'fci': FCI,
	'ifci': IFCI,
	#'nci': NCI,
	'imf': IMF,
	'ici': ICI,
	'sci': SCI,
	#'cu': CU,
	'itci': ITCI,
}

colors = {
	'naive': 'k',
	'kf': 'tab:blue',
	'ci': 'tab:orange',
	'ifci': 'tab:green',
	'ici': 'tab:red',
	'itci': 'tab:purple',
	'fci': 'k',
	'imf': 'tab:brown',
	'sci': 'tab:pink',
}

SITUATIONS = {
	'all'       : None,
	'baseline'  : (np.array([[0.0, 0.0]]).T, np.array([[2.2,0.0],[0.0,0.5]]),
	               np.array([[0.0, 0.0]]).T, np.array([[0.5,0.0],[0.0,2.2]])),
	'smallgap'  : (np.array([[0.0, 0.0]]).T, np.array([[1.0,0.9],[0.9,1.0]]),
	               np.array([[0.1, 0.2]]).T, np.array([[1.0,-.9],[-.9,1.0]])),
	'biggap'    : (np.array([[-2., 0.0]]).T, np.array([[1.0,1.0],[1.0,1.5]]),
	               np.array([[-.5, -1.5]]).T, np.array([[0.5,-.5],[-.5,0.8]])),
	'similar'   : (np.array([[-.0, -1.]]).T, np.array([[2.1,0.8],[0.8,1.1]]),
	               np.array([[1.0, 0.5]]).T, np.array([[2.0,0.8],[0.8,1.0]])),
#	'coherent'  : (np.array([[0.0, 0.0]]).T, np.array([[0.7,0.3],[0.3,0.7]]),
#	               np.array([[-3., 1.0]]).T, np.array([[.07,-.01],[-.01,.08]])),
#	'improving' : (np.array([[0.0, 0.0]]).T, np.array([[0.7,0.3],[0.3,0.8]]),
#	               np.array([[-1., 0.5]]).T, np.array([[.07,-.01],[-.01,.08]])),
#	'incoherent': (np.array([[-2., 1.0]]).T, np.array([[.01,0.0],[0.0,.01]]),
#	               np.array([[2.0, -1.]]).T, np.array([[.02,0.0],[0.0,.02]])),
	'cplxrotat':  (np.array([[0.0, 0.0]]).T, np.array([[2.00,0.00],[0.00,0.40]]),
	               np.array([[0.0, 0.0]]).T, np.array([[2.00,0.00],[0.00,0.40]])),
	'cplxtrans':  (np.array([[0.0, 0.0]]).T, np.array([[2.0,0.0],[0.0,0.5]]),
	               np.array([[0.0, 0.0]]).T, np.array([[2.0,0.0],[0.0,0.5]])),
}

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-a', type=float, help="Alpha-risk coefficient when displaying ellipses.", default=10e-3)
	parser.add_argument('-c', type=float, help="Correlation factor when using SCI.", default=0.)
	parser.add_argument('-d', help="Flag to display the figure instead of saving it to pdf.", action='store_true')
	parser.add_argument('-r', help="Flag to add a small random noise to predefined situations.", action='store_true')
	parser.add_argument('-s', type=str, nargs="*", help="List or one situation name.", choices=list(SITUATIONS.keys()), default='all')
	parser.add_argument("ALGOS", type=str, nargs="*", help="List or one algorithm name", choices=list(CIs.keys()), default=list(CIs.keys()))
	args = parser.parse_args()

	alpha = args.a
	situations = list(SITUATIONS.keys())[1:] if 'all' in args.s else args.s
	algorithms = [args.ALGOS] if isinstance(args.ALGOS, str) else args.ALGOS
	correl_coef = args.c
	i = 0
	j = 0
	row, col = LAYOUTS[len(situations)]

	fig, ax = plt.subplots(row, col, sharex=True, sharey=True, squeeze=False, figsize=(6*col,6*row), tight_layout=True)
	#plt.subplots_adjust(left=0.05, bottom=0.05, right=0.98, top=0.97, wspace=0.1, hspace=0.15)
	plt.tight_layout()

	x3, C3 = np.array([[0.0, 0.0]]).T, np.array([[2.0,0.0],[0.0,2.0]])

	for situation in situations:
		x1, C1, x2, C2 = SITUATIONS[situation]

		if args.r:
			x1 += np.random.normal(0, np.linalg.det(C1)/10, (2,1))
			C1 += np.random.normal(0, np.linalg.det(C2)/10, (2,2))
			x2 += np.random.normal(0, np.linalg.det(C2)/10, (2,1))
			C2 += np.random.normal(0, np.linalg.det(C2)/10, (2,2))

		print(('From ({}, {}, {:.3f}) and ({}, {}, {:.3f}); |(x1,C1)-(x2,C2)|={:.3f} ({:.3f}):'.format(x1.flatten(), C1.flatten(), np.linalg.det(C1), x2.flatten(), C2.flatten(), np.linalg.det(C2), _mahalanobis_dist(x1,C1,x2,C2), _euclidean_dist(x1,C1,x2,C2))))
		plot_ellipse(x1, C1, alpha, ax[j,i], color='black', opacity=0.3, label='x, P', linewidth=LWIDTH_MAX)
		plot_ellipse(x2, C2, alpha, ax[j,i], color='black', opacity=0.3, label='y, R', linewidth=LWIDTH_MAX)
		#plot_ellipse(x3, C3, alpha, ax[j,i], color='lightgrey', label='x3, C3')

		for b, alg in enumerate(algorithms):
			a = ((LWIDTH_MAX-LWIDTH_MIN)*(len(algorithms)-b-1))/len(algorithms)
			print(b, a)
			if 'sci' in alg:
				C1d, C1i = C1*correl_coef, C1*(1-correl_coef)
				C2d, C2i = C2*correl_coef, C2*(1-correl_coef)
				t0 = time.time()
				x1x2, C1C2i, C1C2d, w1w2 = CIs[alg](x1, C1i, C1d, x2, C2i, C2d)
				t1 = time.time()
				print(('    {}: {:.3f}  ({} {}{}, {:.3f}, {:.3f}) in {:.3f}ms'.format(alg, w1w2, x1x2.flatten() , C1C2i.flatten() , C1C2d.flatten() , np.linalg.det(C1C2i) ,np.linalg.det(C1C2d) , (t1-t0)*1000)))
				#plot_ellipse(x1, C1i, alpha, ax[j,i], label=alg.upper()+'i', linestyle='--')
				#plot_ellipse(x1, C1d, alpha, ax[j,i], label=alg.upper()+'d', linestyle='--')
				#plot_ellipse(x2, C2i, alpha, ax[j,i], label=alg.upper()+'i', linestyle='--')
				#plot_ellipse(x2, C2d, alpha, ax[j,i], label=alg.upper()+'d', linestyle='--')
				#plot_ellipse(x1x2, C1C2i, alpha, ax[j,i], label=alg.upper()+'i', linestyle='--')
				#plot_ellipse(x1x2, C1C2d, alpha, ax[j,i], label=alg.upper()+'d', linestyle='--')
				plot_ellipse(x1x2, C1C2i+C1C2d, alpha, ax[j,i], label=f'{alg.upper()} ({correl_coef})', color=colors[alg], linewidth=LWIDTH_MIN)
			elif 'cplxrotat' in situation:
				x1x2, C1C2, w1w2 = x1, C1, 0
				step = 10
				for t in np.arange(np.pi/step, np.pi, np.pi/step):
					x2, C2 = x1, rot(t).T @ C1 @ rot(t)
					x1x2, C1C2, w1w2 = CIs[alg](x1x2, C1C2, x2, C2)
					plot_ellipse(x2, C2, alpha, ax[j,i], color='grey', label='', linewidth=LWIDTH_MIN)
				plot_ellipse(x1x2, C1C2, alpha, ax[j,i], label=alg.upper(), color=colors[alg], linewidth=LWIDTH_MIN)
			elif 'cplxtrans' in situation:
				x1x2, C1C2, w1w2 = x1, C1, 0
				dstep = 3
				dmax = 2
				for t in np.arange(dmax/dstep, dmax, dmax/dstep):
					x2, C2 = x1+np.array([[t,t]]).T, C1
					x1x2, C1C2, w1w2 = CIs[alg](x1x2, C1C2, x2, C2)
					plot_ellipse(x2, C2, alpha, ax[j,i], color='grey', label='', linewidth=LWIDTH_MIN)
				plot_ellipse(x1x2, C1C2, alpha, ax[j,i], label=alg.upper(), color=colors[alg], linewidth=LWIDTH_MIN)
			else:
				t0  = time.time()
				x1x2, C1C2, w1w2 = CIs[alg](x1, C1, x2, C2)
				t1  = time.time()

				### Property Tests
				x2x1, C2C1, w2w1 = CIs[alg](x2, C2, x1, C1)
				x2x3, C2C3, w2w3 = CIs[alg](x2, C2, x3, C3)
				x1x2x2, C1C2C2, w1w2w2 = CIs[alg](x1x2, C1C2, x2, C2)
				x1x2x3, C1C2C3, w1w2w3 = CIs[alg](x1x2, C1C2, x3, C3)
				x2x3x1, C2C3C1, w2w3w1 = CIs[alg](x2x3, C2C3, x1, C1)

				#print x1x2.flatten(), C1C2.flatten()
				#print x1x2x2.flatten(), C1C2C2.flatten()
				properties = {}
				properties['commutative'] = np.allclose(x1x2, x2x1, atol=10e-3)     and np.allclose(C1C2, C2C1, atol=10e-3)
				properties['idempotent']  = np.allclose(x1x2, x1x2x2, atol=10e-3)   and np.allclose(C1C2, C1C2C2, atol=10e-3)
				properties['associative'] = np.allclose(x1x2x3, x2x3x1, atol=10e-3) and np.allclose(C1C2C3, C2C3C1, atol=10e-3)

				print(('    {}: {:.3f}  ({}, {}, {:.3f}) in {:.3f}ms {}'.format(alg, w1w2, x1x2.flatten() , C1C2.flatten() , np.linalg.det(C1C2) , (t1-t0)*1000, ' ' .join([key for key,val in list(properties.items()) if val]))))
				plot_ellipse(x1x2, C1C2, alpha, ax[j,i], label=alg.upper(), opacity=0.7, color=colors[alg], linestyle='--', linewidth=LWIDTH_MIN+a)
				#plot_ellipse(x1x2x2, C1C2C2, alpha, ax[j,i], label=alg.upper())

		ax[j,i].axis((-5,5,-5,5))
		#ax[j,i].axis('equal')
		#ax[j,i].grid(True)
		ax[j,i].set_xlabel('({})'.format('abcdefg'[j*col+i]))
		if i==1 and j==0: ax[j,i].legend()
		i += 1
		if i==col: j+=1; i=0

	if args.d:
		plt.show()
	else:
		s = 'all' if args.s=='all' else '_'.join(args.s)
		fig.savefig('comp_{}.pdf'.format('_'.join(algorithms)), transparent=True)

if __name__=='__main__': main()
