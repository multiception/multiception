import numpy as np
import itertools
from multiception.belief import MassFunction, powerset

def smets_partial_combine_2(m1, m2):
	if m1[m1.frame()]==1.0:
		m1[m1.frame()] -= 1E-6
		m1[m1.frame()-{'O'}] += 1E-6
	if m2[m2.frame()]==1.0:
		m2[m2.frame()] -= 1E-6
		m2[m2.frame()-{'O'}] += 1E-6

	frameUnion = m1.frame() | m2.frame()
	frameInter = m1.frame() & m2.frame()
	m1_cond,m2_cond = m1.condition(frameInter, normalization=False), m2.condition(frameInter, normalization=False)
	mO = m1_cond.combine_conjunctive(m2_cond, normalization=False)
	#print(m1_cond, m2_cond, mO)

	m = MassFunction({frameUnion:0})
	for A in powerset(m1.frame()|m2.frame()):
		A1 = A & m1.frame()
		A2 = A & m2.frame()
		A0 = A & frameInter
		m12 = m1[A1]/m1_cond[A0] if m1_cond[A0]>0 else 0.
		m21 = m2[A2]/m2_cond[A0] if m2_cond[A0]>0 else 0.
		m[A] = m12 * m21 * mO[A1&A2]
		#print(f'    {"".join(A)}:{m[A]:.2f} = {"".join(A1)}:{m1[A1]}/{m1_cond[A0]} * {"".join(A2)}:{m2[A2]}/{m2_cond[A0]} * {"".join(A1&A2)}:{mO[A1&A2]}')

	#print(sum(m.values()), m)
	return m

def smets_partial_combine_n(masses):
	if len(masses)==0: return

	#print(' '.join([f'{m}' for m in masses]))
	m = masses[0]
	for i in range(1, len(masses)):
		m = smets_partial_combine_2(m, masses[i])

	return m

def combine_detectability(ms):
	m = smets_partial_combine_n(ms).normalize()
	#print(m, sum(m.values()), m.pl('O'))
	D = min(1, m.pl('O'))
	return MassFunction({'oO': m[m.frame()], 'O': D-m[m.frame()], 'o':1-D})

def combine_detectability_grid(grids_):
	from copy import deepcopy
	from multiception.evidential_grid_map import EvidentialGridMap
	grids = deepcopy(grids_)
	EvidentialGridMap.extendMultiToInclude(list(grids.values()))
	refgrid = list(grids.values())[0]
	(nrows, ncols), length, resolution, origin = refgrid.getSize(), refgrid.getLength(), refgrid.getResolution(), refgrid.getPosition()

	masses = [
		[
			[
				MassFunction({'':g[''][row,col], k:g['o'][row,col], 'O':g['O'][row,col], k+'O':g['o,O'][row,col]}) for col in range(ncols)
			] for row in range(nrows)
		] for k,g in grids.items()
	]

	output = [
		[
			combine_detectability([m[row][col] for m in masses]) for col in range(ncols)
		] for row in range(nrows)
	]

	grid = EvidentialGridMap(['o', 'O'])
	grid.setGeometry(length, resolution, origin)
	grid[''][:,:] = 0
	grid['o'][:,:] = [[output[row][col]['o'] for col in range(ncols)] for row in range(nrows)]
	grid['O'][:,:] = [[output[row][col]['O'] for col in range(ncols)] for row in range(nrows)]
	grid['o,O'][:,:] = [[output[row][col]['oO'] for col in range(ncols)] for row in range(nrows)]
	return grid

def combine(ms):
	m = MassFunction({'oO':1})
	for a in ms:
		d = a.frame()-frozenset('O')
		m &= MassFunction({'':a[''], 'o':a[d], 'O':a['O'], 'oO':a[a.frame()]})
	return m
	
#m1 = MassFunction({'A': 0.6, 'B': 0.1, 'AB': 0.3})
#m2 = MassFunction({'C': 0.2, 'B': 0.7, 'BC': 0.1})
#print(combine_detectability(m1, m2))
#print()

#kk = 0.0
#m0 = MassFunction({'a': 0.0,    'O': 0.0,    'aD': 1.00})
#m1 = MassFunction({'i': 0.0+kk, 'O': 0.7,    'iD': 0.3-kk})
#m2 = MassFunction({'j': 0.0+kk, 'O': 0.5,    'jD': 0.5-kk})
#m3 = MassFunction({'k': 0.8,    'O': 0.0+kk, 'kD': 0.2-kk})
#m4 = MassFunction({'l': 0.5,    'O': 0.0+kk, 'lD': 0.5-kk})

#m = MassFunction({'klD':0})
#for i in powerset('kD'):
	#for j in powerset('lD'):
		#print(''.join(i), ''.join(j), m3[i], m4[j], m3[i]*m4[j])
		#m[i|j] += m3[i]*m4[j]
#print(m)
#print(MassFunction({'oO': m['ikD'], 'O': m.pl('O')-m['ikD'], 'o':1-m.pl('O')}))
#print(m3|m4)
#print()

#print(combine([m0, m1]), combine_detectability([m0, m1]), "\n")
#print(combine([m0, m3]), combine_detectability([m0, m3]), "\n")
#print(combine([m1, m2]), combine_detectability([m1, m2]), "\n")
#print(combine([m3, m4]), combine_detectability([m3, m4]), "\n")
#print(combine([m1, m3]), combine_detectability([m1, m3]), "\n")
#print(combine([m1, m2, m3]), combine_detectability([m1, m2, m3]), "\n")
#print(combine([m1, m2, m4]), combine_detectability([m1, m2, m4]), "\n")
#print(combine([m1, m2, m3, m4]), combine_detectability([m1, m2, m3, m4]))

#print()
#print(combine_detectability([m3, m4]))
#print()
#print(combine_detectability([m1, m3]))
#print()
#print(combine_detectability([m4, m2]))
