import pickle
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from multiception.trust import freespace_conf
from multiception.belief import MassFunction, mass_twogauss
from multiception.evidential_grid_map import EvidentialGridMap, plotdebug_gridmap
from scipy import ndimage
from scipy.stats import norm

EPS = np.finfo(float).eps

def mutual_information_2d(x, y, sigma=1, normalized=False):
    """
    Computes (normalized) mutual information between two 1D variate from a
    joint histogram.
    Parameters
    ----------
    x : 1D array
        first variable
    y : 1D array
        second variable
    sigma: float
        sigma for Gaussian smoothing of the joint histogram
    Returns
    -------
    nmi: float
        the computed similariy measure
    """
    bins = (256, 256)
    jh = np.histogram2d(x, y, bins=bins)[0]

    # smooth the jh with a gaussian filter of given sigma
    ndimage.gaussian_filter(jh, sigma=sigma, mode='constant',
                                 output=jh)

    # compute marginal histograms
    jh = jh + EPS
    sh = np.sum(jh)
    jh = jh / sh
    s1 = np.sum(jh, axis=0).reshape((-1, jh.shape[0]))
    s2 = np.sum(jh, axis=1).reshape((jh.shape[1], -1))

    # Normalised Mutual Information of:
    # Studholme,  jhill & jhawkes (1998).
    # "A normalized entropy measure of 3-D medical image alignment".
    # in Proc. Medical Imaging 1998, vol. 3338, San Diego, CA, pp. 132-143.
    if normalized:
        mi = ((np.sum(s1 * np.log(s1)) + np.sum(s2 * np.log(s2)))
                / np.sum(jh * np.log(jh))) - 1
    else:
        mi = ( np.sum(jh * np.log(jh)) - np.sum(s1 * np.log(s1))
               - np.sum(s2 * np.log(s2)))

    return mi

def mutual_information(hgram):
    """ Mutual information for joint histogram
    """
    # Convert bins counts to probability values
    pxy = hgram / float(np.sum(hgram))
    px = np.sum(pxy, axis=1) # marginal for x over y
    py = np.sum(pxy, axis=0) # marginal for y over x
    px_py = px[:, None] * py[None, :] # Broadcast to multiply marginals
    # Now we can do the calculation using the pxy, px_py 2D arrays
    nzs = pxy > 0 # Only non-zero pxy values contribute to the sum
    return np.sum(pxy[nzs] * np.log(pxy[nzs] / px_py[nzs]))

def jousselme_distance(m1: MassFunction, m2: MassFunction):
	elems = tuple(m1.all())
	d = np.array([m1[x]-m2[x] for x in elems])
	D = np.array([[len(x.intersection(y))/len(x.union(y)) if y else 0 for y in elems] for x in elems])
	return np.sqrt(0.5 * d @ D @ d)

def divergence(m1: MassFunction, m2: MassFunction):
	D1 = sum([(1/(2**len(F)-1)) * m1[F] * np.log(m1[F]/m2[F]) if m1[F]>0 and m2[F]>0 else 1 for F in m1.focal()])
	D2 = sum([(1/(2**len(F)-1)) * m2[F] * np.log(m2[F]/m1[F]) if m1[F]>0 and m2[F]>0 else 1 for F in m2.focal()])
	return (D1+D2)/2

def div_freespace(f1: float, f2: float, s=0.1):
	return f1 * np.abs(2 * norm.cdf(f2, 0, s) - 1) * (2 * norm.cdf(f2, f1-np.sign(f1)*s, (1-np.abs(f1))*s) - 1)

plt.figure(0); plt.title('Curve of the manual method')
plt.plot(np.arange(-1,1.01,0.01), [div_freespace(0.7, x) for x in np.arange(-1,1.01,0.01)])
plt.plot(np.arange(-1,1.01,0.01), [div_freespace(-0.7, x) for x in np.arange(-1,1.01,0.01)])

j = np.array([(vi,vj,jousselme_distance(MassFunction({'v':vi if vi>0 else 0, 'V':-vi if vi<0 else 0, 'vV':1-abs(vi) }), MassFunction({'v':vj if vj>0 else 0, 'V':-vj if vj<0 else 0, 'vV':1-abs(vj)}))) for vj in np.arange(-1,1,0.01) for vi in np.arange(-1,1,0.01)])
d = np.array([(vi,vj,divergence(MassFunction({'v':vi if vi>0 else 0, 'V':-vi if vi<0 else 0, 'vV':1-abs(vi) }), MassFunction({'v':vj if vj>0 else 0, 'V':-vj if vj<0 else 0, 'vV':1-abs(vj)}))) for vj in np.arange(-1,1,0.01) for vi in np.arange(-1,1,0.01)])
#m = np.array([(vi,vj,div_freespace(vi,vj)) for vj in np.arange(0,1,0.01) for vi in np.arange(0,1,0.01)])
plt.figure(1); plt.title('Theroetical values -- Jousselme Distance'); plt.scatter(j[:,0], j[:,1], c=2*(1-j[:,2])-1); plt.colorbar(); plt.xlabel('Vi'); plt.ylabel('Vj')
plt.figure(2); plt.title('Theroetical values -- Deng-Deng Divergence'); plt.scatter(d[:,0], d[:,1], c=np.minimum(1,d[:,2])); plt.colorbar(); plt.xlabel('Vi'); plt.ylabel('Vj')
xx,yy=np.mgrid[0:1:0.01,0:1:0.01]
vi,vj = yy,1-xx
s = 0.05
plt.figure(3); plt.title('Theroetical values -- Manual method'); plt.imshow(norm.cdf(vj, vi-2*s, s)*vi); plt.colorbar(); plt.xlabel('Vi'); plt.ylabel('Vj')

with open(Path('obs.pkl').resolve(), 'rb') as f:
	gms,objs = pickle.load(f)

EvidentialGridMap.extendMultiToInclude([gm[0] for gm in gms.values() if len(gm)>0])
gms2 = { k:v[0].normalize() for k,v in gms.items() if len(v)>0 }
frb = np.stack([gms2['zoeblue'][k] for k in ['','o','O','o,O']],axis=-1)#; frb[:,:,3] = 1-frb[:,:,3]
frg = np.stack([gms2['zoegrey'][k] for k in ['','o','O','o,O']],axis=-1)#; frg[:,:,3] = 1-frg[:,:,3]
#frw = np.stack([gms2['zoewhite'][k] for k in ['','o','O','o,O']],axis=-1)#; frw[:,:,3] = 1-frw[:,:,3]

#dist1d = np.array([jousselme_distance(MassFunction.from_array(m1, 'vV'), MassFunction.from_array(m2,'vV'))for m1,m2 in zip(frw.reshape(frb.shape[0]*frb.shape[1], frb.shape[2]), frg.reshape(frg.shape[0]*frg.shape[1], frg.shape[2]))])
#div1d = np.array([divergence(MassFunction.from_array(m1, 'vV'), MassFunction.from_array(m2,'vV')) for m1,m2 in zip(frw.reshape(frw.shape[0]*frw.shape[1], frw.shape[2]), frg.reshape(frg.shape[0]*frg.shape[1], frg.shape[2]))])
##dif1d = np.array([div_freespace(m1[2], m2[2]) for m1,m2 in zip(frw.reshape(frb.shape[0]*frb.shape[1], frb.shape[2]), frg.reshape(frg.shape[0]*frg.shape[1], frg.shape[2]))])
#diff = div_freespace(frw, frg)

#frs = [frb[:,:,2], frg[:,:,2]]
frs = [v[0].normalize() for v in gms.values() if len(v)>0]
ins= [frb[:,:,3], frg[:,:,3]]
fig, axes = plt.subplots(len(frs)+1, len(frs)+1, tight_layout=True, sharex=True, sharey=True)

axes[0][0].imshow(np.zeros_like(frs[0]['O']), vmin=0, vmax=1)
for i in range(len(frs)):
	axes[1+i][0].imshow(frs[i]['O'], vmin=0, vmax=1)
	axes[0][1+i].imshow(frs[i]['O'], vmin=0, vmax=1)
	for j in range(len(frs)):
		diff = freespace_conf(frs[i], frs[j])
		axes[1+i,1+j].imshow(diff)#, vmin=0, vmax=1)

#ref = 25*25 * norm.cdf(0.9, 0.9-2*s, s)*0.9
#fig, axes = plt.subplots(len(frs), len(frs), tight_layout=True, sharex=True, sharey=True)
#for i in range(len(frs)):
	#for j in range(len(frs)):
		#diff = norm.cdf(frs[j], frs[i]-2*s, s)*frs[i]
		#diff2 = diff[np.abs(diff)>0.01]
		#axes[i,j].plot(np.sort(diff2))
		#a = (1-ins[i])*(1-ins[j])
		#val = np.trapz(diff2)
		#truc = 1-np.exp(-(val*np.log(1/(1-0.9))) / ref)
		#axes[i,j].set_title(f'{val:.2f}')
		#print(i, j, val, ref, truc)

#win = 'equal'#[60,140,72,140]
#plt.figure(10); plt.title('Freespace -- White'); plt.axis(win); plt.imshow(frw);
#plt.figure(11); plt.title('Freespace -- Grey'); plt.axis(win); plt.imshow(frg);
#plt.figure(12); plt.title('Experimental result -- Jousselme Distance'); plt.axis(win); plt.imshow(1/dist1d.reshape(frb.shape[0], frb.shape[1])); plt.colorbar()
#plt.figure(13); plt.title('Experimental result -- Deng-Deng Divergence'); plt.axis(win); plt.imshow(1/div1d.reshape(frb.shape[0], frb.shape[1])); plt.colorbar()
#plt.figure(14); plt.title('Experimental result -- Manual method'); plt.axis(win); plt.imshow(np.maximum(0, diff[:,:,2]), vmin=0, vmax=1); plt.colorbar()
#print(np.maximum(0, diff[:,:,2]).mean())

hist_2d, x_edges, y_edges = np.histogram2d(frb[:,:,2].ravel(), frg[:,:,2].ravel(), bins=10)
non_zeros = hist_2d != 0
hist_2d_log = np.zeros(hist_2d.shape)
hist_2d_log[non_zeros] = np.log(hist_2d[non_zeros])
print("Mutual info v: ", mutual_information(hist_2d))
plt.figure(20); plt.title('Histogram -- Vis'); plt.imshow(hist_2d.T, origin='lower'); plt.plot(frb[:,:,2].ravel()*9, frg[:,:,2].ravel()*9, 'r.'); plt.xlabel('Vi'); plt.ylabel('Vj')
plt.figure(21); plt.title('Log Histogram -- Vis'); plt.imshow(hist_2d_log.T, origin='lower'); plt.plot(frb[:,:,2].ravel()*9, frg[:,:,2].ravel()*9, 'r.'); plt.xlabel('log(Vi)'); plt.ylabel('log(Vj)')

hist_2d, x_edges, y_edges = np.histogram2d(frb[:,:,1].ravel(), frg[:,:,1].ravel(), bins=10)
non_zeros = hist_2d != 0
hist_2d_log = np.zeros(hist_2d.shape)
hist_2d_log[non_zeros] = np.log(hist_2d[non_zeros])
print("Mutual info V: ", mutual_information(hist_2d))
plt.figure(22); plt.title('Histogram -- NonVis'); plt.imshow(hist_2d.T, origin='lower'); plt.plot(frb[:,:,1].ravel()*9, frg[:,:,1].ravel()*9, 'r.'); plt.xlabel('vi'); plt.ylabel('vj')
plt.figure(23); plt.title('Log Histogram -- NonVis'); plt.imshow(hist_2d_log.T, origin='lower'); plt.plot(frb[:,:,1].ravel()*9, frg[:,:,1].ravel()*9, 'r.'); plt.xlabel('log(vi)'); plt.ylabel('log(vj)')

plt.show()
