import numpy as np

def rotate(a, MAX_ITER=100):
	O = 0
	dO = 0.1
	Os = []
	turned = []

	while abs(dO)>0.0001 and len(Os)<MAX_ITER:
		t = np.array([[np.cos(O), -np.sin(O)],[np.sin(O), np.cos(O)]]) @ a
		turned.append(t)
		Os.append(O)

		Onew = np.arctan2(*np.linalg.eig(np.cov(np.hstack((a, t))))[1][::-1,0])
		dO = O-Onew
		O = Onew

	return Os, turned

def try_rotations(a):
	O = np.arctan2(*np.linalg.eig(np.cov(a))[1][::-1,0])
	Os = O+np.arange(-np.pi/8, np.pi/8, np.pi/80)
	turned = []

	for O in Os:
		t = np.array([[np.cos(O), -np.sin(O)],[np.sin(O), np.cos(O)]]) @ a
		turned.append(t)

	return turned

if __name__=='__main__':
	A = np.array([[0,1,2,3,4,5,6,7,8,9,10],[4,3,2,1,2,3,4,5,6,7,8]])
	B = np.array([[0,2,4,6,8,10],[6,5,4,3,2,1]])

	#OsA, turnedA = rotate(A, 100)
	#OsB, turnedB = rotate(B, 3)
	#for t in turnedA:
		#plt.plot(*t, alpha=0.1)
	#plt.plot(*t)
	#print(len(OsA))
	turnedB = try_rotations(B)
	for t in turnedB:
		plt.plot(*t, alpha=0.1)
	imin = np.argmin([np.cov(t)[1,0] for t in turnedB])
	plt.plot(*turnedB[imin])
	plt.axis('equal')
	plt.show()
