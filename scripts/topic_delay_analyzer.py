#! /usr/bin/env python3

import sys
import logging
from threading import Lock
from argparse import ArgumentParser
import numpy as np
from pyqtgraph import QtGui, QtCore, GraphicsLayoutWidget, setConfigOptions

import rospy
from roslib.message import get_message_class

np.set_printoptions(precision=4, linewidth=999, suppress=True, sign=' ')

# In number of messages
DEFAULT_BUFFER_SIZE = 1000
DEFAULT_WINDOW_SIZE = 10

class TopicDelayAnalyzer:
    def __init__(self, topics, max_window_size=DEFAULT_WINDOW_SIZE, buffer_size=DEFAULT_BUFFER_SIZE):
        self.topics = topics
        self._times = np.ones((len(topics)+1, buffer_size)) * np.inf # First row is the header time
        self.head = self.tail = 0 # Ring buffer indices
        self.max_window_size = max_window_size
        self.t0 = None

    def update_times(self, ros_time, msg_time, topic):

        if not self.t0:
            self.t0 = msg_time

        col_, = np.where(np.abs(self.times[0]-msg_time)<0.001)
        if len(col_)==1:
            col = col_[0]
        elif len(col_)==0:
            # If the header time has never been received, start a new column
            col = self.head
            self.head += 1
            self._times[0, col] = msg_time
            if (self.head-self.tail)>=self.max_window_size:
                self.self.tail += 1
        else:
            return

        self._times[self.topics.index(topic)+1, col] = ros_time

    @property
    def times(self):
        tail = self.tail % self._times.shape[1]
        head = self.head % self._times.shape[1]
        return np.hstack(( self._times[:, tail:head], self._times[:, head:tail] ))

class TopicDelayAnalyzerOnline(TopicDelayAnalyzer):
    def __init__(self, topics, tcp_nodelay, max_window_size=DEFAULT_WINDOW_SIZE, buffer_size=DEFAULT_BUFFER_SIZE):
        super().__init__(topics, max_window_size, buffer_size)
        self.subscribers = [rospy.Subscriber(topic, rospy.AnyMsg, self.callback, topic, tcp_nodelay=tcp_nodelay) for topic in topics]
        self.mutex = Lock()
        self.display_mutex = Lock()

    def callback(self, raw_msg, topic):
        ros_time = rospy.get_rostime().to_sec()
        msg_class = get_message_class(raw_msg._connection_header['type'])
        msg = msg_class().deserialize(raw_msg._buff)
        msg_time = msg.header.stamp.to_sec()
        with self.mutex:
            self.update_times(ros_time, msg_time, topic)
        with self.display_mutex:
            self.display(topic, ros_time)

    def display(self, topic, t):
        times = self.times
        delays = times[1:]-times[:-1]
        delays = delays[:,np.isfinite(np.sum(delays, axis=0))] # Drop columns where there is at least one NaN/inf
        rospy.loginfo("At time %f%s", t, "\n  ".join([""]+[f"{topic:50}: {np.nanmean(delays[i]):.4f} +/- {np.nanstd(delays[i]):.4f}" for i, topic in enumerate(self.topics)]))

class TopicDelayAnalyzerOffline(TopicDelayAnalyzer):
    def __init__(self, bags, topics):
        # TODO: Implement TopicDelayAnalyzerOffline class
        raise NotImplemented

class TopicDelayPlotter(TopicDelayAnalyzerOnline, QtCore.QObject):
    signal_update = QtCore.pyqtSignal()

    def __init__(self, *args, **kwargs):
        TopicDelayAnalyzerOnline.__init__(self, *args, **kwargs)
        QtCore.QObject.__init__(self)
        self.win = GraphicsLayoutWidget(show=True)
        self.fig = self.win.addPlot()
        self.fig.addLegend()
        self.plot_data_items = { topic: self.fig.plot([], [], pen=(i, len(self.topics)+1), name=topic) for i, topic in enumerate(self.topics) }
        self.signal_update.connect(self.draw)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda: self.draw(True))
        self.timer.start(2000)
        self.last_topic = None

    def display(self, topic, t):
        self.last_topic = topic
        self.signal_update.emit()

    @QtCore.pyqtSlot()
    def draw(self, close_only=False):
        if rospy.is_shutdown():
            self.win.close()
        elif not close_only:
            times = self.times
            row = self.topics.index(self.last_topic)+1
            nonnan = np.isfinite(times[row])
            header_times = times[0]-self.t0
            message_times = times[row]-times[0]
            message_times[~nonnan] = 0
            self.plot_data_items[self.last_topic].setData(header_times, message_times, connect=nonnan, skipFiniteCheck=True)

if __name__=="__main__":
    parser = ArgumentParser(description="Computes and prints the delay of successive topics")
    # TODO add window and buffer params
    parser.add_argument("--nodelay", action="store_true", help="Sets tpc_nodelay for this node")
    parser.add_argument("--plot", action="store_true", help="Plot delays instead of printing means")
    parser.add_argument("TOPICS", type=str, nargs="+", help="Topics to study in order")
    args = parser.parse_args()

    if len(args.TOPICS)==0 or rospy.is_shutdown():
        sys.exit(0)

    rospy.init_node("topic_delay_analyzer", anonymous=True)

    published_topics = next(zip(*rospy.get_published_topics()))
    for topic in args.TOPICS:
        if topic not in published_topics:
            rospy.logwarn("Topic '%s' does not seem to be published.", topic)

    if args.plot:
        setConfigOptions(background="w", foreground="k")
        topic_delay_analyzer = TopicDelayPlotter(args.TOPICS, args.nodelay, 1000)
        pg_instance = QtGui.QApplication.instance().exec_()
    else:
        topic_delay_analyzer = TopicDelayAnalyzerOnline(args.TOPICS, args.nodelay, 1000)
        rospy.spin()
