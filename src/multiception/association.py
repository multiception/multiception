import yaml
import numpy as np
from typing import List, Tuple
from scipy.linalg import norm
from scipy.spatial import distance
from scipy.optimize import linear_sum_assignment
from pyqtgraph import mkPen, PlotCurveItem

from multiception.object import State, Object, plotdebug_objects

class Associator:
	def __init__(self, gate: float, cost_max: float):
		self.gate = gate # Fast euclidean gate before more complex comparisons
		self.cost_max = cost_max
		self.cost_mat = np.ones((0,0), dtype=np.float32)

	def _compute_cost_mat(self, objsA: List[State], objsB: List[State], fun, **kwargs):
		'''
		Fills the cost matrix over all objects A and B that are in gate
		'''
		self.cost_mat = np.ones((len(objsA), len(objsB)), dtype=np.float32) * 1E3

		# First pass of gating to remove obvious outliers with a simple calculation
		gate_mask = Associator.compute_fast_euclidean_gating(objsA, objsB, self.gate)
		gated_rows, gated_cols, gated_cost_mat, gated_mask = Associator.filter(self.cost_mat, gate_mask)

		# Those that passed the first gating are further compared
		for i_gated, i in enumerate(gated_rows):
			for j_gated, j in enumerate(gated_cols):
				if gated_mask[i_gated,j_gated]:
					self.cost_mat[i,j] = gated_cost_mat[i_gated,j_gated] = fun(objsA[i], objsB[j], **kwargs)

		# Second pass of gating to remove potential but less obvious outliers
		#reduced_rows, reduced_cols, reduced_cost_mat, reduced_mask = Associator.filter(gated_cost_mat, gated_cost_mat<self.cost_max)
		#return gated_rows[reduced_rows], gated_cols[reduced_cols], reduced_cost_mat
		return gated_rows, gated_cols, gated_cost_mat

	@staticmethod
	def compute_fast_euclidean_gating(objsA: List[State], objsB: List[State], gate: float):
		'''
		Computes the euclidean distance between all objects A and objects B
		and fill a boolean (mask) matrix where they are below `gate`
		'''
		statesA = np.vstack( [a.state[:2] for a in objsA] )
		statesB = np.vstack( [b.state[:2] for b in objsB] )
		euclidean_dists = np.linalg.norm(statesA[:,None]-statesB[:], axis=2)
		return euclidean_dists <= gate

	@staticmethod
	def filter(matrix, mask):
		'''
		Removes remove rows and columns of `matrix` where there is not at least one `mask` cell set to `True`.
		mask is a boolean matrix masking elements of `matrix`
		'''
		gated_rows = np.where(np.any(mask, axis=1))[0]
		gated_cols = np.where(np.any(mask, axis=0))[0]
		gated_matrix = matrix[gated_rows,:][:,gated_cols]
		gated_mask = mask[gated_rows,:][:,gated_cols]
		return gated_rows, gated_cols, gated_matrix, gated_mask

	@staticmethod
	def get_unassociated_idx(objects: List[object], associated_idx: np.ndarray):
		return np.setdiff1d(np.arange(len(objects), dtype=int), associated_idx)

	def associate(self, objsA: List[State], objsB: List[State], cost_fun, assign_fun=linear_sum_assignment, **kwargs) -> Tuple[List[int], List[int]]:
		if len(objsA)==0 or len(objsB)==0: return [],[]
		gated_rows, gated_cols, gated_cost_mat = self._compute_cost_mat(objsA, objsB, cost_fun, **kwargs)

		# Second pass of gating to remove less obvious outliers that cannot be associated
		reduced_rows, reduced_cols, reduced_cost_mat, _ = self.filter(gated_cost_mat, gated_cost_mat<self.cost_max)
		idxA_, idxB_ = assign_fun(reduced_cost_mat)
		idxA = idxA_[reduced_cost_mat[idxA_, idxB_]<self.cost_max]
		idxB = idxB_[reduced_cost_mat[idxA_, idxB_]<self.cost_max]

		return gated_rows[reduced_rows[idxA]], gated_cols[reduced_cols[idxB]]

class AssociatorMahalanobis(Associator):
	def __init__(self, gate: float, cost_max: float):
		super().__init__(gate, cost_max)

	def associate(self, objsA: List[State], objsB: List[State], **kwargs) -> Tuple[List[int], List[int]]:
		return super().associate(objsA, objsB, mahalanobis, **kwargs)

class AssociatorMahalanobisClass(Associator):
	def __init__(self, gate: float, cost_max: float, penalty_class_file: str, class_penalty_cost: float):
		super().__init__(gate, cost_max)
		self.class_penalty_mat = PenaltyMatrix.from_yaml(penalty_class_file)
		self.class_penalty_cost = class_penalty_cost

	def associate(self, objsA: List[State], objsB: List[State], **kwargs) -> Tuple[List[int], List[int]]:
		return super().associate(objsA, objsB, mahalanobis_class, penalty_mat=self.class_penalty_mat, class_cost=self.class_penalty_cost, **kwargs)

class PenaltyMatrix:
	'''
	Abstraction class between a matrix of penalty to use with `mahalanobis_class` and object classes
	'''
	def __init__(self, mat: np.ndarray, classes: np.ndarray):
		self.penalties = { classes[i]:{classes[j]:cell for j,cell in enumerate(row)} for i,row in enumerate(mat) }

	@staticmethod
	def from_yaml(path: str):
		with open(path, 'r') as f:
			content = yaml.safe_load(f)
		classes = np.array(content['classes'])
		mat = np.array(content['mat'])
		return PenaltyMatrix(mat, classes)

	def __getitem__(self, idx):
		if len(idx)!=2: raise ValueError('Indices must be a pair of int')
		return self.penalties[idx[0]][idx[1]]

def euclidean(a: State, b: State):
	return norm(a.state[:2]-b.state[:2])

def mahalanobis(a: State, b: State):
	u = a.state[:2]
	v = b.state[:2]
	VI = np.linalg.inv(a.covar[:2,:2]+b.covar[:2,:2])
	return distance.mahalanobis(u, v, VI)

def mahalanobis_class(a: Object, b: Object, penalty_mat: PenaltyMatrix = None, class_cost: float = None):
	return mahalanobis(a, b) + penalty_mat[a.classif, b.classif]*class_cost

def plotdebug_assocs(assocs: Tuple[List[int],List[int]], objsA: List[Object], objsB: List[Object], **kwargs):
	if assocs is not None and len(assocs)>0 and len(assocs[0])>0:
		if 'opacity' not in kwargs: kwargs['opacity'] = 255
		if 'col' not in kwargs: kwargs['col'] = (20, 20, 20, kwargs['opacity'])

		assoc_points = [((objsA[i].x, objsA[i].y), (objsB[j].x, objsB[j].y)) for i, j in zip(*assocs)]
		assoc_lines = np.vstack(assoc_points)
		assoc_connections = np.concatenate( [[True,False]]*len(assoc_points) )
		return PlotCurveItem(assoc_lines[:,0], assoc_lines[:,1], connect=assoc_connections, brush=kwargs['col'], pen=mkPen(color=kwargs['col'], width=2))
	else:
		return None

if __name__=='__main__':
	import pyqtgraph as pg
	from pyqtgraph import QtGui, GraphicsLayoutWidget
	from multiception.msg import Class
	from multiception.object import StaticObject
	pg.setConfigOption('background', 'w')
	pg.setConfigOption('foreground', 'k')
	win = GraphicsLayoutWidget(show=True, title="Test Association Plotting")
	fig = win.addPlot(title='Assoc')

	a = StaticObject(); a.classif = Class.SIGN_UNKNOWN; a.state[:] = 10, 20, 0.1; a.covde[:,:] = [[2,1,0],[1,4,0],[0,0,1]]; a.shape = (5,2,2)
	b = StaticObject(); b.classif = Class.UNKNOWN; b.state[:] = 15, 15, 1.5; b.covde[:,:] = [[4,-1,0],[-1,2,0],[0,0,1]]; b.shape = (3,4,2)

	items = plotdebug_assocs([a, a, a, a], [b, b], ([1, 2], [1, 0]))
	for item in items:
		if item:
			fig.addItem(item)
	QtGui.QApplication.instance().exec_()
