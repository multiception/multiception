#! /bin/env python

import time
import numpy as np
from numpy.linalg import inv, det
import scipy.optimize
from numba import njit, types

ConstArray1D = types.Array(types.float32, 1, 'C', readonly=True)
ConstArray2D = types.Array(types.float32, 2, 'C', readonly=True)

def _euclidean_dist(x1, P1, x2, P2):
	return np.sqrt(np.sum(x1-x2)**2)

def _mahalanobis_dist(x1, P1, x2, P2):
	return ((x1-x2).T @ inv(P1+P2) @ (x1-x2))[0,0]

def _kullback_leibler_dist(x1, P1, x2, P2):
	dx = x1-x2
	I2 = inv(P2)
	detP2P1 = det(P2) / det(P1)
	d = ( np.log(detP2P1) + dx.T@I2@dx + np.trace(P1@I2) - x1.size ).flatten()[0] / 2
	return d

def _ci_new(x1, P1, x2, P2, w, C):
	I1 = inv(P1)
	I2 = inv(P2)
	CI1 = inv(C@P1@C.T)
	P = inv( I1 + (w-1)*C.T@CI1@C + (1-w)*C.T@I2@C )
	x = P @ (I1@x1 + (w-1)*C.T@CI1@C@x1 + (1-w)*C.T@I2@x2)

	return x, P, w

def _ci(x1, P1, x2, P2, w, C):
	I1 = inv(P1)
	I2 = inv(P2)
	P = inv(w*I1 + (1-w)*C.T@I2@C )
	x = P @ (w*I1@x1 + (1-w)*C.T@I2@x2)

	return x, P, w

def naive(x1, P1, x2, P2, C):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	I1 = inv(P1)
	I2 = C.T@inv(P2)@C
	P = P1@P2 #inv(I1 + I2)
	x = P @ (I1@x1 + I2@x2)
	return x, P, -1.0

def CI_new(x1, P1, x2, P2, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	f = (lambda w, I1, I2: 1/det(w*I1 + (1-w)*I2))
	w = scipy.optimize.minimize_scalar(f, bounds=(0,1), method='bounded', args=(inv(C@P1@C.T), inv(P2))).x
	return _ci_new(x1, P1, x2, P2, w, C)

def CI(x1, P1, x2, P2, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	f = (lambda w, I1, I2: 1/det(w*I1 + (1-w)*I2))
	w = scipy.optimize.minimize_scalar(f, bounds=(0,1), method='bounded', args=(inv(P1), C.T@inv(P2)@C)).x
	return _ci(x1, P1, x2, P2, w, C)

def FCI(x1, P1, x2, P2, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	detP1, detP2 = det((P1,P2))
	return _ci(x1, P1, x2, P2, (detP1/(detP1+detP2)), C)

def IFCI(x1, P1, x2, P2, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	I1 = inv(C@P1@C.T)
	I2 = inv(P2)
	detI1, detI2, detI12 = det((I1,I2,I1+I2))
	return _ci(x1, P1, x2, P2, (detI12-detI2+detI1)/(2*detI12), C)

def ITCI(x1, P1, x2, P2, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	Cx1 = C@x1
	CP1 = C@P1@C.T
	d12, d21 = _kullback_leibler_dist(Cx1, CP1, x2, P2), _kullback_leibler_dist(x2, P2, Cx1, CP1)
	w = d12/(d12+d21)
	return _ci(x1, P1, x2, P2, w, C)

def NCI(x1, P1, x2, P2, C=None):
	pass

def IMF(x1, P1, x2, P2, x0, P0):
	I1 = inv(P1)
	I2 = inv(P2)
	I0 = inv(P0)
	P = inv(I1 + I2 - I0)
	x = np.dot(P, np.dot(I1,x1) + np.dot(I2,x2) - np.dot(I0,x0))
	return x, P, 2

def ICI(x1, P1, x2, P2, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	f = (lambda w, I1, I2: det( inv(w*I1 + (1-w)*I2) ))
	w = scipy.optimize.minimize_scalar(f, bounds=(0,1), method='bounded', args=(inv(P1), C.T@inv(P2)@C)).x

	CP2 = inv(C.T@inv(P2)@C)
	Gamma = w*P1 + (1-w)*CP2
	gamma = w*x1 + (1-w)*C.T@x2
	x,P,_ = IMF(x1, P1, C.T@x2, CP2, gamma, Gamma)
	return x, P, w

def SCI_new(x1, P1d, P1i, x2, P2d, P2i, C=None):
	if C is None and len(x1)==len(x2):
		C = np.identity(len(x1))

	f = lambda w: 1/det( inv(C@(P1d/w + P1i)@C.T) + inv(P2d/(1-w) + P2i) )
	w = scipy.optimize.minimize_scalar(f, method='bounded', bounds=(0.001,0.999)).x

	I1 = inv(inv( inv(P1d)+C.T@((w-1)*inv(C@P1d@C.T))@C) + P1i)
	I2 = inv(P2d/(1-w) + P2i)

	P = inv(I1+C.T@I2@C)
	x = P @ (I1@x1 + C.T@I2@x2)
	Pi = P @ (I1@P1i@I1 + C.T@I2@P2i@I2@C) @ P
	Pd = P - Pi
	return x,Pd,Pi,w

@njit((ConstArray1D,ConstArray2D,ConstArray2D,ConstArray1D,ConstArray2D,ConstArray2D,ConstArray2D), fastmath=True, nogil=True)
def SCI_opti(x1, P1d, P1i, x2, P2d, P2i, C):
	def f1(Pd, Pi, Rd, Ri, H, w): # Basic Kalman form
		P = Pd/w + Pi
		R = Rd/(np.float32(1)-w) + Ri
		return det(P - P @ H.T @ inv(H@P@H.T+R) @ H @ P)

	I = np.eye(P1d.shape[0], dtype=np.float32)
	ws = np.arange(0+0.01, 1, 0.01, dtype=np.float32)
	dets = np.array([f1(P1d, P1i, P2d, P2i, C, w) for w in ws], dtype=np.float32)
	w = ws[np.argmin(dets)]

	P1 = P1d/w + P1i
	P2 = P2d/(np.float32(1)-w) + P2i
	K = P1@C.T@inv(C@P1@C.T + P2)

	x = x1 + K@(x2-C@x1)
	P  = (I-K@C)@P1 @(I-K@C).T + K@P2 @K.T
	Pi = (I-K@C)@P1i@(I-K@C).T + K@P2i@K.T
	Pd = P - Pi

	return x, Pd, Pi, w

def SCI(x1, P1d, P1i, x2, P2d, P2i, C):
	x, Pd, Pi, w = SCI_opti(
		x1 .astype(dtype=np.float32, subok=False),
		P1d.astype(dtype=np.float32),
		P1i.astype(dtype=np.float32),
		x2 .astype(dtype=np.float32),
		P2d.astype(dtype=np.float32),
		P2i.astype(dtype=np.float32),
		C  .astype(dtype=np.float32)
	)
	return x, Pd, Pi, w

def CU(x1, P1, x2, P2):
	m = (x1+x2)/2
	m,_,w = ITCI(x1,P1,x2,P2)
	I = np.identity(x1.shape[0])
	P1 = P1 + np.dot((m-x1).T, m-x1)
	P2 = P2 + np.dot((m-x2).T, m-x2)
	S = scipy.linalg.sqrtm(P2)
	invS = inv(S)
	R = np.dot(invS.T, np.dot(P1, invS)) 
	D,V = np.linalg.eig(R)
	D = np.diag(D)
	P = np.dot(S.T, np.dot(V, np.dot(np.maximum(D,I), np.dot(V, S.T))))

	return m, P, w

def _is_pos_def(A):
	return np.all(np.linalg.eigvals(A) > 0)

def _is_larger(A, B):
	return _is_pos_def(A-B)

def _CU_objective_func(x):
	w, px, pxy, py = x
	return det(np.array([[px, pxy],[pxy,py]]))

def CUi(x1, P1, x2, P2):
	P0 = np.maximum(P1,P2)
	constraintP1 = (lambda x: _is_larger(np.array([[x[1], x[2]],[x[2],x[3]]]), P1 + (1-x[0])**2 * np.dot(x1-x2, (x1-x2).T)) )
	constraintP2 = (lambda x: _is_larger(np.array([[x[1], x[2]],[x[2],x[3]]]), P2 + (  x[0])**2 * np.dot(x1-x2, (x1-x2).T)) )
	w,px,pxy,py = scipy.optimize.minimize(_CU_objective_func, x0=[0.5, P0[0,0], P0[1,0], P0[1,1]], method='SLSQP', constraints=[{'type': 'eq', 'fun': constraintP1}, {'type': 'eq', 'fun': constraintP2}]).x
	P = np.array([[px, pxy],[pxy,py]])
	x = w*x1 + (1-w)*x2
	return x, P, w
