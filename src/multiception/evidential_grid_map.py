import numpy as np
from pyqtgraph import ImageItem, QtCore, ColorMap
from grid_map.convert import from_msg, to_msg
from perception_utils_python import EvidentialGridMapBinding

class EvidentialGridMap(EvidentialGridMapBinding):
	def __and__(self, othr):
		return EvidentialGridMap.combineConjunctive([self, othr])

	# Reimplement methods from GridMap to avoid diamond inheritence
	@classmethod
	def from_msg(cls, msg):
		return from_msg(msg, cls)

	def to_msg(self):
		return to_msg(self)

def plotdebug_gridmap(gm: EvidentialGridMap, **kwargs) -> None:
	if gm is not None:
		values = np.stack((gm['O'], gm['o'], gm[''], 1-gm['o,O']), axis=-1)
		img = ImageItem(np.rot90(values, k=2), levels=[[0,1]]*values.shape[-1], **kwargs)
		img.setRect(QtCore.QRectF(
			gm.getPosition()[0]-gm.getLength()[0]/2,
			gm.getPosition()[1]-gm.getLength()[1]/2,
			gm.getLength()[0],
			gm.getLength()[1],
		))
	else:
		img = None

	return img

if __name__=='__main__':
	import pickle
	import rospy
	from roslib.message import get_message_class
	import pyqtgraph as pg
	from pyqtgraph import QtGui, GraphicsLayoutWidget
	from grid_map_msgs.msg import GridMap as GridMapMsg
	from multiception.msg import Perceptions

	class GridMapDisplayer(QtCore.QObject):
		signal_update = QtCore.pyqtSignal()

		def __init__(self):
			super().__init__()
			rospy.init_node('grid_map_displayer', anonymous=True)
			pg.setConfigOption('background', 'w')
			pg.setConfigOption('foreground', 'k')

			self.win = GraphicsLayoutWidget(show=True, title="Test Grid Map Plotting")
			self.fig = self.win.addPlot(title='Grid Map')
			self.fig.setAspectLocked(lock=True, ratio=1)
			self.img = None
			self.gm = None
			self.proxy = pg.SignalProxy(self.fig.scene().sigMouseMoved, rateLimit=10, slot=self.mouseMoved)
			self.signal_update.connect(self.draw)
			self.debugplot_timer = QtCore.QTimer()
			self.debugplot_timer.timeout.connect(lambda: self.draw(True))
			self.debugplot_timer.start(2000)
			self.sub = rospy.Subscriber('input', rospy.AnyMsg, self.callback)

		def callback(self, anymsg):
			msg_name = anymsg._connection_header['type']
			msg_class = get_message_class(msg_name)
			msg = msg_class().deserialize(anymsg._buff)

			if msg_name=='multiception/SourceStamped':
				self.gm = EvidentialGridMap.from_msg(msg.source.observability)
			elif msg_name=='multiception/Perceptions' and len(msg.sources)>0:
				self.gm = EvidentialGridMap.from_msg(msg.sources[0].observability)
			elif msg_name=='grid_map_msgs/GridMap':
				self.gm = EvidentialGridMap.from_msg(msg)
			else:
				rospy.logwarn('Unrecognized message: %s', msg_name)
				return

			rospy.loginfo('Received %s', self.gm)
			self.img = plotdebug_gridmap(self.gm)
			self.signal_update.emit()

		@QtCore.pyqtSlot()
		def draw(self, close_only=False):
			if rospy.is_shutdown():
				self.win.close()
				return
			if close_only:
				return

			self.fig.clear()
			if self.img: self.fig.addItem(self.img)

		def mouseMoved(self, evt):
			pos = evt[0]
			if self.gm is not None and self.fig.sceneBoundingRect().contains(pos):
				point = self.fig.vb.mapSceneToView(pos)
				print([self.gm.atPosition(layer, [ point.x(), point.y()]) for layer in self.gm.getLayers()] if self.gm.isInside([ point.x(), point.y()]) else [])

	gmd = GridMapDisplayer()
	QtGui.QApplication.instance().exec_()
