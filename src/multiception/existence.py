#! /usr/bin/env python3

import rospy
from collections.abc import Iterable
from typing import List, Tuple, Union
import numpy as np
from scipy.spatial import distance
from multiception.belief import MassFunction, exp_twopoints, prob_exppercent, mass_twogauss
from multiception.evidential_grid_map import EvidentialGridMap
from multiception.object import Object

Objects = List[Object]
ObservabilityGrid = EvidentialGridMap
ObservabilityGrids = List[EvidentialGridMap]

def compute_similarity(obj, oth) -> MassFunction:
	sim_posi  = compute_similarity_posi (obj, oth)
	sim_kine  = compute_similarity_kine (obj, oth)
	sim_size  = compute_similarity_size (obj, oth)
	sim_class = compute_similarity_class(obj, oth)
	sim_chara = compute_similarity_chara(obj, oth)

	try:
		simi = sim_posi & sim_kine & sim_size & sim_class & sim_chara
	except FloatingPointError as e:
		rospy.logdebug('FPE simi: %s %s %s %s %s %s', e, sim_posi, sim_kine, sim_size, sim_class, sim_chara)
		simi = MassFunction({'sS':1})

	return simi

def compute_similarity_posi(obj: Object, oth: Object) -> MassFunction:
	# Half similar at 1.5, full unknown at 3 and half dissimilar at 4.5
	diff_obj = obj.prepare_diff(oth)
	mahadist = distance.mahalanobis(diff_obj.state[:2], np.zeros_like(diff_obj.state[:2]), np.linalg.inv(diff_obj.covar[:2,:2]))
	similarity_posi = mass_twogauss('sS', 0.75, 0.375, 3, mahadist)

	rospy.logdebug('   simpose  %s %.2f %s', diff_obj.state[:2], mahadist, similarity_posi)
	return similarity_posi

def compute_similarity_kine(obj: Object, oth: Object) -> MassFunction:
	# Half dissimilar at 4.5
	diff_obj = obj.prepare_diff(oth)
	try:
		mahadist = distance.mahalanobis(diff_obj.state[2:], np.zeros_like(diff_obj.state[2:]), np.linalg.inv(diff_obj.covar[2:,2:]))
	except FloatingPointError as e:
		rospy.logdebug('FPE kine: %s %s %s', e, diff_obj.state[2:], diff_obj.covar[2:,2:].flatten())
		return MassFunction({'sS':1})

	similarity_kine = mass_twogauss('sS', -2.25, 0.375, 6, mahadist)
	rospy.logdebug('   simkine  %s %s %.2f %s', diff_obj.state[2:], diff_obj.covar[2:,2:].flatten(), mahadist, similarity_kine)
	return similarity_kine

def compute_similarity_size(obj: Object, oth: Object) -> MassFunction:
	diff_size = obj.shape-oth.shape

	# Non-dissimilar up to 0.75, half dissimilar at 1.75m, full dissimilar at 2.75m
	similarity_size_l = mass_twogauss('sS', -3.0, 0.375, 4.0, diff_size[0])
	similarity_size_w = mass_twogauss('sS', -3.0, 0.375, 4.0, diff_size[1])
	similarity_size_h = mass_twogauss('sS', -3.0, 0.375, 4.0, diff_size[2])

	try:
		similarity_size = similarity_size_l & similarity_size_w & similarity_size_h
	except FloatingPointError as e:
		rospy.logdebug('FPE size: %s %s %s %s %s %s', e, obj.shape, oth.shape, similarity_size_l, similarity_size_w, similarity_size_h)
		return MassFunction({'sS':1})

	rospy.logdebug('   simsize  %s %s %s %s %s %s', obj.shape, oth.shape, similarity_size_l, similarity_size_w, similarity_size_h, similarity_size)
	return similarity_size

def compute_similarity_class(obj: Object, oth: Object) -> MassFunction:
	# TODO
	return MassFunction({'sS':1})

def compute_similarity_chara(obj: Object, oth: Object) -> MassFunction:
	# TODO
	return MassFunction({'sS':1})

def compute_observability_point(point: np.ndarray, observability: ObservabilityGrid) -> MassFunction:
	'''
	Computes an observability measure for the given point.

	Observability is how much a point is space should or should not have been seen given a
	set of field of views (free space, cautious field of view and optimistic field of view).
	'''
	if observability and observability.getLayers() and observability.isInside(point):
		observability_vals = [observability.atPosition(layer, point) for layer in observability.getLayers()]
	else:
		observability_vals = [0,0,0,1]

	return MassFunction.from_array(observability_vals, 'oO')

def compute_observability_object(obj: Object, observability: ObservabilityGrid) -> MassFunction:
	'''
	Uses the observability measure on the four corners of the object and on all FOVs.
	'''

	#m = MassFunction({'oO':1})
	#corners = obj.corners().T
	#for p in corners:
		#m &= compute_observability_point(p, observability)

	#return m
	return compute_observability_point(obj.state[:2], observability)

def compute_observability_object_partial(obj: Object, observability: ObservabilityGrid) -> MassFunction:
	return max([compute_observability_point(p, observability) for p in obj.corners().T] + [compute_observability_point(obj.state[:2], observability)], key=lambda m: m['o'])

class ExistenceEstimatorBase:
	def __init__(self, **kwargs):
		return

	def prepare(self, **kwargs):
		return

	def predict(self, obj: Object, dt: float, **kwargs) -> None:
		return

	def update_associated(self, obj: Object, oth: Object, trust: MassFunction, **kwargs) -> None:
		return

	def update_unassociated(self, obj: Object, trust: MassFunction, **kwargs) -> None:
		return

	def initialize(self, obj: Object, trust: MassFunction, **kwargs) -> None:
		return

	def validate(self, objs: Objects, **kwargs) -> List[bool]:
		return [True] * len(objs)

	def invalidate(self, objs: Objects, **kwargs) -> List[bool]:
		return self.destroy(objs)

	def destroy(self, objs: Objects, **kwargs) -> List[bool]:
		return [False] * len(objs)

class ExistenceEstimatorNaive(ExistenceEstimatorBase):
	def __init__(self, **kwargs):
		self.thresh_delete_time = kwargs.get('thresh_delete_time', 0.5)
		self.thresh_delete_uncertain = kwargs.get('thresh_delete_uncertain', 100)

	def destroy(self, objs: Objects, **kwargs) -> List[bool]:
		return [obj.ageobs>0.5 and np.trace(obj.covar[:2,:2])>100 for obj in objs]

class ExistenceEstimatorFilter(ExistenceEstimatorBase):
	def __init__(self, **kwargs):
		self.max_decay_time    = kwargs.get('max_decay_time', 1)
		self.max_deterioration = kwargs.get('max_deterioration', 0.1)
		self.value_birth       = kwargs.get('value_birth', 0.1)
		self.thresh_valid      = kwargs.get('thresh_valid', 0.1)
		self.thresh_invalid    = kwargs.get('thresh_invalid', 0.1)
		self.thresh_delete_nonexistent = kwargs.get('thresh_delete_nonexistent', 0.1)
		self.thresh_delete_unknown     = kwargs.get('thresh_delete_unknown', 0.95)

	def prepare(self, **kwargs):
		return

	def predict(self, obj: Object, dt: float, **kwargs) -> None:
		rospy.logdebug('ExDecay     %.2f %.2f %.2f', self.max_decay_time, dt, MassFunction.get_decay_alpha(self.max_decay_time, dt))
		obj.exist = obj.exist.decay(self.max_decay_time, dt)

	def update_associated(self, obj: Object, oth: Object, trust: MassFunction, **kwargs) -> None:
		similarity = compute_similarity(obj, oth).discount(trust.pl('T'))
		e_impr = similarity['s']
		m_impr = oth.exist.discount(1-e_impr)

		rospy.logdebug('ExImprovem  %s %s %s', similarity, oth.exist, m_impr)
		obj.exist &= m_impr

	def initialize(self, obj: Object, trust: MassFunction, **kwargs) -> None:
		obj.exist = MassFunction({'e': self.value_birth, 'eE': 1-self.value_birth})

	def validate(self, objs: Objects, **kwargs) -> List[bool]:
		return [obj.exist['e']>self.thresh_valid for obj in objs]

	def invalidate(self, objs: Objects, **kwargs) -> List[bool]:
		return self.destroy(objs)

	def destroy(self, objs: Objects, **kwargs) -> List[bool]:
		return [obj.exist['E']>self.thresh_delete_nonexistent or obj.exist['eE']>self.thresh_delete_unknown for obj in objs]

class ExistenceEstimatorObservability(ExistenceEstimatorFilter):
	def update_associated(self, obj: Object, oth: Object, trust: MassFunction, observability_consensus: ObservabilityGrid, observability_other: ObservabilityGrid, **kwargs) -> None:
		similarity = compute_similarity(obj, oth)#.discount(trust.pl('T')) # Trust is already present in the observability of other
		observability_obj_consensus = compute_observability_object(oth, observability_consensus)
		observability_obj_other = compute_observability_object(oth, observability_other)
		alpha_asso = similarity.bel('s') * observability_obj_consensus.pl('o') * observability_obj_other.bel('o')
		m_asso = oth.exist.discount(1-alpha_asso)

		rospy.logdebug('ExImprovem  %s %s %s %s %s %s', trust, similarity, observability_obj_consensus, observability_obj_other, oth.exist, m_asso)
		obj.exist &= m_asso

	def update_unassociated(self, obj: Object, trust: MassFunction, observability_consensus: ObservabilityGrid, observability_other: ObservabilityGrid, **kwargs) -> None:
		observability_obj_consensus = compute_observability_object(obj, observability_consensus)
		observability_obj_other = compute_observability_object(obj, observability_other)
		alpha_unasso = observability_obj_consensus.pl('o') * observability_obj_other.bel('o')
		m_unasso = MassFunction({'E': self.max_deterioration, 'eE': 1-self.max_deterioration}).discount(1-alpha_unasso)

		rospy.logdebug('ExDeterior  %s %s %s %s', trust, observability_obj_consensus, observability_obj_other, m_unasso)
		obj.exist &= m_unasso

	def initialize(self, obj: Object, trust: MassFunction, observability: ObservabilityGrid, **kwargs) -> None:
		observability_obj = compute_observability_object(obj, observability)
		old_exist = obj.exist
		obj.exist = old_exist.discount(1-observability_obj.bel('o'))
		rospy.logdebug('New object %s; %s %s -> %s', obj, observability_obj, old_exist, obj.exist)
