#!/usr/bin/env python

import numpy as np
from abc import ABC, abstractmethod

from multiception import ci

'''
States and models from Khan “Comparison of Track to Track Fusion Methods for Nonlinear Process and Measurement Models”, 2019
'''

class Measurement(object):
	def __init__(self, t=0., n=0, m=0):
		self.t     = t
		self.meas  = np.zeros(n, dtype=np.float32)
		self.pred  = np.zeros(n, dtype=np.float32)
		self.model = np.zeros((n,m), dtype=np.float32)
		self.noisd = np.zeros((n,n), dtype=np.float32)
		self.noisi = np.zeros((n,n), dtype=np.float32)

	@property
	def noise(self):
		return self.noisd+self.noisi

	@staticmethod
	def model_from_cov(cov, maxcov=100):
		observed = np.where(cov.diagonal()<maxcov)[0]
		C = np.identity(len(cov), dtype=np.float32)[observed,:]
		return C

class Filter(ABC):
	@classmethod
	def step(cls, state, dt, evonoise, measurements=[], command=None):
		cls.predict(state, dt, evonoise, command)
		if measurements:
			cls.update(state, dt, measurements)

	@classmethod
	@abstractmethod
	def predict(cls, state, dt, evonoise, command=None):
		'''
		Returns the predicted state and covariance matrix using the evolution model. To override.
		'''
		return

	@classmethod
	@abstractmethod
	def update(cls, state, dt, measurements=[]):
		'''
		Returns the updated state and covariance matrix using the measurements. To override.
		'''
		return

class Kalman(Filter):
	@classmethod
	def predict(cls, state, dt, evonoise, command=None):
		model, noisd, prediction = state.prepare_evolution(dt, evonoise, command)
		state.state = prediction
		state.covde = model @ state.covde @ model.T + noisd
		return model, noisd

	@classmethod
	def update(cls, state, dt, measurements=[]):
		iden = np.identity(len(state.state))

		for obs in measurements:
			innov_covar = obs.model @ state.covde @ obs.model.T + obs.noisd
			kalman_gain = state.covde @ obs.model.T @ np.linalg.pinv(innov_covar)
			innov = obs.meas - obs.pred

			state.state += kalman_gain @ innov
			state.covde = (iden - kalman_gain @ obs.model) @ state.covde

class CI(Kalman):
	@classmethod
	def update(cls, state, dt, measurements):
		for obs in measurements:
			state.state, state.covde, w = ci.CI(state.state, state.covde, obs.meas, obs.noisd, obs.model)

class OldCI(CI):
	@classmethod
	def update(cls, state, dt, measurements):
		for obs in measurements:
			state.state, state.covde, w = ci.CI_Old(state.state, state.covde, obs.meas, obs.noisd, obs.model)

class IFCI(CI):
	@classmethod
	def update(cls, state, dt, measurements):
		for obs in measurements:
			state.state, state.covde, w = ci.IFCI(state.state, state.covde, obs.meas, obs.noisd, obs.model)

class ITCI(CI):
	@classmethod
	def update(cls, state, dt, measurements):
		for obs in measurements:
			state.state, state.covde, w = ci.ITCI(state.state, state.covde, obs.meas, obs.noisd, obs.model)

class SCI(Kalman):
	@classmethod
	def predict(cls, state, dt, evonoise, command=None):
		model, noisd = super().predict(state, dt, evonoise, command)
		state.covin = model @ state.covin @ model.T

	@classmethod
	def update(cls, state, dt, measurements=[]):
		for obs in measurements:
			state.state, state.covde, state.covin, w = ci.SCI(state.state, state.covde, state.covin, obs.meas, obs.noisd, obs.noisi, obs.model)

class OldSCI(SCI):
	@classmethod
	def update(cls, state, dt, measurements=[]):
		for obs in measurements:
			state.state, state.covde, state.covin, w = ci.SCI_Old(state.state, state.covde, state.covin, obs.meas, obs.noisd, obs.noisi, obs.model)
