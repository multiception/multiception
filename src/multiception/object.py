#!/usr/bin/env python3

import copy
from rospy import Time
import numpy as np
from typing import List, Union, Tuple
from collections.abc import Iterable
from abc import ABC, abstractmethod
from scipy.linalg import sqrtm
from pyqtgraph import PlotCurveItem, ScatterPlotItem, mkPen

from multiception.msg import Perception, Class, State as StateMsg, StateStamped
from multiception.belief import MassFunction
from multiception.filtering import Measurement

class State(ABC):
	Size = 0
	_ConversionFunctions = {}

	def __init__(self):
		cls = type(self)
		self.state = np.zeros(cls.Size, dtype=np.float32)
		self.covde = np.identity(cls.Size, dtype=np.float32)*1000.
		self.covin = np.identity(cls.Size, dtype=np.float32)*1000.

	@property
	def covar(self):
		return self.covde+self.covin

	@classmethod
	def _register_conversion(cls, clstarget, fun):
		if cls not in State._ConversionFunctions.keys():
			State._ConversionFunctions[cls] = {}
		State._ConversionFunctions[cls][clstarget] = fun

	def _find_conversion(clssource, clstarget):
		'''
		Recursively climb the inheritence tree to find the best function to convert the
		object at hand to an arbitrary other class.
		'''
		fun = None
		if clssource in State._ConversionFunctions.keys():
			if clstarget in State._ConversionFunctions[clssource].keys():
				fun = State._ConversionFunctions[clssource][clstarget]
			else:
				for supcls in clstarget.__bases__:
					fun = State._find_conversion(clssource, supcls)
					if fun: break
		else:
			for supcls in clssource.__bases__:
				fun = State._find_conversion(supcls, clstarget)
				if fun: break
		return fun

	def _cast(self, clstarget):
		converted = clstarget()
		for k,v in self.__dict__.items():
			if k in converted.__dict__ and k not in ['state', 'covde', 'covin']:
				converted.__dict__[k] = copy.deepcopy(v)
		return converted

	def convert(self, clstarget, ignore_covde=False, ignore_covin=False):
		conversion_fun = State._find_conversion(type(self), clstarget)
		if conversion_fun:
			converted = self._cast(clstarget)
			return conversion_fun(self, converted, ignore_covde, ignore_covin)
		else:
			raise ValueError(f'No conversion function found between {type(self).__name__} and {clstarget.__name__}')

class EvolvableState(State):
	def __init__(self, t=0.):
		super().__init__()
		self.t = t

	def prepare_evolution(self, dt, noise, command=None):
		if dt<0:
			if dt<-1E-2: raise ValueError('Negative timestep {}'.format(dt))
			else: dt = 0
		model = self.prepare_evolution_model(dt)
		noise = self.prepare_evolution_noise(dt, noise)
		prediction = self.prepare_evolution_prediction(dt, model, command)
		return model, noise, prediction

	@abstractmethod
	def prepare_evolution_model(self, dt, command=None):
		'''Computes the evolution model matrix (N*N).'''
		return

	def prepare_evolution_noise(self, dt, noise):
		'''Computes the process noise matrix (N*N). To override if more than base noise.'''
		return dt*noise

	def prepare_evolution_prediction(self, dt, model, command=None):
		'''Computes the predicted vector (N*1). To override if the model is non-linear.'''
		if command is None: command = np.zeros_like(self.state)
		return model @ self.state + command

	def prepare_observation(self, othr):
		C = self.prepare_observation_model(othr)
		x, y = self.prepare_observation_measurement(othr, C)

		m = Measurement(self.t)
		m.pred  = x
		m.meas  = y
		m.model = C
		m.noisd = C @ self.covde @ C.T
		m.noisi = C @ self.covin @ C.T
		return m

	def prepare_observation_model(self, othr):
		'''
		Return the observation matrix.
		Simple 1-to-1 case provided by `EvolvableState.prepare_evolution_model`, override for other behaviours.
		'''
		return Measurement.model_from_cov(self.covar)

	def prepare_observation_measurement(self, othr, model):
		'''
		Return the observation matrix.
		Linear case provided by `EvolvableState.prepare_observation_measurement`, override for other behaviours.
		'''
		return model @ othr.state, model @ self.state

class StaticState(EvolvableState):
	Size = 3

	def __init__(self, x=0., y=0., O=0.):
		super().__init__()
		self.state[:3] = x,y,O

	@property
	def x(self): return self.state[0]
	@property
	def y(self): return self.state[1]
	@property
	def O(self): return self.state[2]
	@property
	def v(self): return 0

	@classmethod
	def _from_msg(cls, msg, self=None):
		if self is None: self = cls()
		self.t = msg.header.stamp.to_sec()
		self.state[:] = msg.state.x, msg.state.y, msg.state.O
		self.covde[:,:] = np.array(msg.state.covd).reshape((5,5))[:3,:3]
		self.covin[:,:] = np.array(msg.state.covi).reshape((5,5))[:3,:3]
		return self

	def to_msg(self, msg=None):
		if msg is None: msg = StateStamped()
		msg.header.stamp = Time(self.t)
		msg.state.x, msg.state.y, msg.state.O = self.state
		empty5by5 = np.identity(5)*1000
		empty5by5[:3,:3] = self.covde; msg.state.covd = empty5by5.reshape(5*5)
		empty5by5[:3,:3] = self.covin; msg.state.covi = empty5by5.reshape(5*5)
		return msg

	def _convert_Static(self, converted, ignore_covde, ignore_covin):
		converted.state = self.state
		if not ignore_covde: converted.covde = self.covde
		if not ignore_covin: converted.covin = self.covin
		return converted

	def _convert_ConstantVel(self, converted, ignore_covde, ignore_covin):
		V_CONST = 10E-5
		converted.state[:] = self.x, self.y, V_CONST*np.cos(self.O), V_CONST*np.sin(self.O), 0
		if not ignore_covde or not ignore_covin:
			transfo_noise = np.diag([0,0, 0,0])
			transfo = np.array([
				[1, 0, 0],
				[0, 1, 0],
				[0, 0, -np.sin(self.O)*V_CONST],
				[0, 0,  np.cos(self.O)*V_CONST],
				[0, 0, 0]
			])

			if not ignore_covde:
				converted.covde = transfo @ self.covde @ transfo.T + transfo_noise
			if not ignore_covin:
				converted.covin = transfo @ self.covin @ transfo.T + transfo_noise

		return converted

	def _convert_CoordinatedTurn(self, converted, ignore_covde, ignore_covin):
		converted.state[:] = self.x, self.y, self.O, 0,0
		if not ignore_covde or not ignore_covin:
			transfo_noise = np.diag([0,0,0, 1000,1000])
			transfo = np.array([
				[1, 0, 0],
				[0, 1, 0],
				[0, 0, 1],
				[0, 0, 0],
				[0, 0, 0]
			])

			if not ignore_covde:
				converted.covde = transfo @ self.covde @ transfo.T + transfo_noise
			if not ignore_covin:
				converted.covin = transfo @ self.covin @ transfo.T + transfo_noise

		return converted


	@staticmethod
	def _normalize_angle_diff(a, b):
		'''b is the reference angle while a is modified to be as close to it as possible (modulo 2pi)'''
		a += 2*np.pi * np.floor((b-a)/(2*np.pi))
		if (b-a)>np.pi: a += 2*np.pi
		elif (a-b)>np.pi: a -= 2*np.pi
		return a

	def normalize_angle_diff(self, othr):
		if isinstance(othr, StaticState): othr = othr.O
		self.state[2] = StaticState._normalize_angle_diff(self.O, othr)

	def prepare_evolution_model(self, dt, command=None):
		return np.identity(StaticState.Size)

class ConstantVelState(EvolvableState):
	Size = 4

	def __init__(self, x=0.,y=0., vx=0.,vy=0.):
		super().__init__()
		self.state[:] = x,y,vx,vy

	@property
	def x(self):  return self.state[0]
	@property
	def y(self):  return self.state[1]
	@property
	def vx(self): return self.state[2]
	@property
	def vy(self): return self.state[3]
	@property
	def v(self): return np.sqrt(self.vx**2 + self.vy**2)

	@classmethod
	def _from_msg(cls, msg, self=None):
		if self is None: self = cls()
		self.t = msg.header.stamp.to_sec()
		self.state[:] = msg.state.x, msg.state.y, msg.state.O, msg.state.v
		self.covde[:,:] = np.array(msg.state.covd).reshape((5,5))[:4,:4]
		self.covin[:,:] = np.array(msg.state.covi).reshape((5,5))[:4,:4]
		return self

	def to_msg(self, msg=None):
		if msg is None: msg = StateStamped()
		msg.header.stamp = Time(self.t)
		msg.state.x, msg.state.y, msg.state.O, msg.state.v = self.state
		empty5by5 = np.identity(5)*1000
		empty5by5[:4,:4] = self.covde; msg.state.covd = empty5by5.reshape(5*5)
		empty5by5[:4,:4] = self.covin; msg.state.covi = empty5by5.reshape(5*5)
		return msg

	def _convert_Static(self, converted, ignore_covde, ignore_covin):
		x,y,vx,vy = self.x,self.y, self.vx,self.vy
		yaw = np.arctan2(vy, vx)
		normsq = vx*vx + vy*vy

		converted.state[:] = self.x, self.y, yaw

		if not ignore_covde or not ignore_covin:
			if normsq<1E-3:
				# Jacobian of the state transition function
				transfo_noise = np.diag([[0,0,1000]])
				transfo = np.array([
					[1, 0, 0, 0],
					[0, 1, 0, 0],
					[0, 0, 0, 0]
				])
			else:
				# Jacobian of the state transition function
				transfo_noise = np.diag([[0,0,0]])
				transfo = np.array([
					[1, 0, 0, 0],
					[0, 1, 0, 0],
					[0, 0, -y/normsq, x/normsq]
				])

			if not ignore_covde:
				converted.covde = transfo @ self.covde @ transfo.T + transfo_noise
			if not ignore_covin:
				converted.covin = transfo @ self.covin @ transfo.T + transfo_noise

		return converted

	def _convert_ConstantVel(self, converted, ignore_covde, ignore_covin):
		converted.state = self.state
		if not ignore_covde: converted.covde = self.covde
		if not ignore_covin: converted.covin = self.covin
		return converted

	def _convert_CoordinatedTurn(self, converted, ignore_covde, ignore_covin):
		x,y,vx,vy = self.x,self.y, self.vx,self.vy
		yaw = np.arctan2(vy, vx)
		c, s = np.cos(yaw), np.sin(yaw)
		normsq = vx*vx + vy*vy
		norm = np.sqrt(normsq)

		converted.state[:] = x, y, yaw, norm, 0
		if not ignore_covde or not ignore_covin:
			if normsq<1E-3:
				transfo_noise = np.diag([0,0,1000,1000,1000])
				transfo = np.array([
					[1, 0, 0, 0],
					[0, 1, 0, 0],
					[0, 0, 0, 0],
					[0, 0, 0, 0],
					[0, 0, 0, 0]])
			else:
				transfo_noise = np.diag([0,0,1,1,1000])
				transfo = np.array([
					[1, 0, 0, 0],
					[0, 1, 0, 0],
					[0, 0, -vy/normsq, vx/normsq],
					[0, 0,  vx/norm  , vy/norm  ],
					[0, 0, 0, 0]])

			if not ignore_covde:
				converted.covde = transfo @ self.covde @ transfo.T + transfo_noise
			if not ignore_covin:
				converted.covin = transfo @ self.covin @ transfo.T + transfo_noise

		return converted

	def prepare_evolution_model(self, dt, command=None):
		return np.array([
			[1, 0, dt,  0],
			[0, 1,  0, dt],
			[0, 0,  1,  0],
			[0, 0,  0,  1]])

class CoordinatedTurnState(StaticState):
	Size = 5

	def __init__(self, x=0.,y=0.,O=0.,v=0.,w=0.):
		super().__init__(x, y, O)
		self.state[3:] = v, w

	@property
	def v(self): return self.state[3]
	@property
	def w(self): return self.state[4]

	@staticmethod
	def _normalize_speed(O, v, w):
		'''
		Returns normalized yaw, speed and yaw-rate:
		  - speeds below 1 km/h makes the speed and yaw-rate be ignored
		  - negative speed are considered as having the vehicle the wrong way around, in which case the angle is turned pi radians and the speed made positive
		'''
		if abs(v) < 0.28:
			v = 0
			w = 0
		elif v<0:
			v *= -1
			O += np.pi

		if abs(w)<0.05:
			w = 0
		else:
			a=w
			w = np.mod(w, np.sign(w)*np.pi/2)

		return O, v, w

	def normalize_speed(self):
		'''Returns normalized yaw and speed (see `CoordinatedTurnState._normalize_speed`)'''
		self.state[2:5] = CoordinatedTurnState._normalize_speed(*self.state[2:5])

	@classmethod
	def _from_msg(cls, msg, self=None):
		if self is None: self = cls()
		self.state[:] = msg.state.x, msg.state.y, msg.state.O, msg.state.v, msg.state.w
		self.covde[:,:] = np.array(msg.state.covd).reshape((5,5))
		self.covin[:,:] = np.array(msg.state.covi).reshape((5,5))
		self.t = msg.header.stamp.to_sec()
		self.normalize_speed()
		return self

	def to_msg(self, msg=None):
		if msg is None: msg = StateStamped()
		msg.header.stamp = Time(self.t)
		msg.state.x, msg.state.y, msg.state.O, msg.state.v, msg.state.w = self.state
		msg.state.covd = self.covde.reshape(5*5)
		msg.state.covi = self.covin.reshape(5*5)
		return msg

	def _convert_Static(self, converted, ignore_covde, ignore_covin):
		converted.state[:] = self.x, self.y, self.O
		if not ignore_covde or not ignore_covin:
			transfo_noise = np.diag([[0,0,0]])
			transfo = np.array([
				[1, 0, 0, 0, 0],
				[0, 1, 0, 0, 0],
				[0, 0, 1, 0, 0]
			])

			if not ignore_covde:
				converted.covde = transfo @ self.covde @ transfo.T + transfo_noise
			if not ignore_covin:
				converted.covin = transfo @ self.covin @ transfo.T + transfo_noise

		return converted

	def _convert_ConstantVel(self, converted, ignore_covde, ignore_covin):
		# Note: state is not in the same order as in the Khan paper
		c, s = np.cos(self.O), np.sin(self.O)
		converted.state[:] = self.x, self.y, self.v*c, self.v*s
		if not ignore_covde or not ignore_covin:
			transfo_noise = np.diag([0,0, 1,1]).T
			transfo = np.array([
				[1, 0, 0, 0, 0],
				[0, 1, 0, 0, 0],
				[0, 0, -self.v*s, c, 0],
				[0, 0,  self.v*c, s, 0]
			])

			if not ignore_covde:
				converted.covde = transfo @ self.covde @ transfo.T + transfo_noise
			if not ignore_covin:
				converted.covin = transfo @ self.covin @ transfo.T + transfo_noise

		return converted

	def _convert_CoordinatedTurn(self, converted, ignore_covde, ignore_covin):
		converted.state = self.state
		if not ignore_covde: converted.covde = self.covde
		if not ignore_covin: converted.covin = self.covin
		return converted

	def prepare_evolution_model(self, dt, command=None):
		x,y,O,v,w = self.state
		return np.identity(len(self.state)) + dt*np.array([
			[0, 0, -v*np.sin(O)*np.cos(w), np.cos(O)*np.cos(w), -v*np.cos(O)*np.sin(w)],
			[0, 0,  v*np.cos(O)*np.cos(w), np.sin(O)*np.cos(w), -v*np.sin(O)*np.sin(w)],
			[0, 0, 0, np.sin(w)/3, (v*np.cos(w))/3],
			[0]*5,
			[0]*5
		])

	def prepare_evolution_prediction(self, dt, model, command=None):
		x,y,O,v,w = self.state
		#if command is None: command = np.zeros_like(self.state)
		return self.state + dt*np.array([
			v*np.cos(O)*np.cos(w),
			v*np.sin(O)*np.cos(w),
			(v*np.sin(w))/3,
			0,
			0#command[0]
		])

if StaticState not in State._ConversionFunctions.keys():
	State._ConversionFunctions[StaticState] = {
		StaticState:          StaticState._convert_Static,
		ConstantVelState:     StaticState._convert_ConstantVel,
		CoordinatedTurnState: StaticState._convert_CoordinatedTurn
	}
if ConstantVelState not in State._ConversionFunctions.keys():
	State._ConversionFunctions[ConstantVelState] = {
		StaticState:          ConstantVelState._convert_Static,
		ConstantVelState:     ConstantVelState._convert_ConstantVel,
		CoordinatedTurnState: ConstantVelState._convert_CoordinatedTurn
	}
if CoordinatedTurnState not in State._ConversionFunctions.keys():
	State._ConversionFunctions[CoordinatedTurnState] = {
		StaticState:          CoordinatedTurnState._convert_Static,
		ConstantVelState:     CoordinatedTurnState._convert_ConstantVel,
		CoordinatedTurnState: CoordinatedTurnState._convert_CoordinatedTurn
	}

class Object(EvolvableState):
	def __init__(self):
		super().__init__()
		self.classif = Class.UNKNOWN
		self.exist = MassFunction({'eE': 1})
		self.oid = 0
		self.source_ids = frozenset()
		self.shape = np.zeros(3)
		self.t0 = self.tobs = 0

	@property
	def age(self):
		return self.t-self.t0

	@property
	def ageobs(self):
		return self.t-self.tobs

	@staticmethod
	def from_msg(msg):
		'''
		Determines the right object class for this message and calls its `_from_msg` function.
		Not to be overriden
		'''
		supported_cls = [subcls for subcls in Object.__subclasses__() if subcls._supports_msg(msg)]
		if len(supported_cls)>0:
			self = supported_cls[0]._from_msg(msg)
			return self
		else:
			raise ValueError('Unsupported message type, no Object subclass is registered to handle it')

	@classmethod
	@abstractmethod
	def _supports_msg(cls, msg):
		'''Returns true if this class is the right one for the given message (e.g: thanks to its `classif` field)'''
		return

	@classmethod
	@abstractmethod
	def _from_msg(cls, msg, self=None):
		'''
		Parses a `Perception` message to fill an `Object` or subclass.
		Common fields are handled in `Object._from_msg` but state and covariances should be done in an override.
		Optionnal `self` argument to let the actual object construction to an arbitrary function.
		'''
		if self is None: self = cls()
		self.shape[:] = msg.length, msg.width, msg.height
		self.classif = msg.classif.value
		self.exist = MassFunction.from_msg(msg.existence)
		self.oid = msg.object_id
		self.source_ids = frozenset({msg.source_id})
		self.t0 = msg.t_first_obs.to_sec()
		self.tobs = msg.t_last_obs.to_sec()
		return self

	@abstractmethod
	def to_msg(self, msg=None):
		'''
		Serializes an `Object` or subclass to a `Perception` message.
		Common fields are handled in `Object.to_msg` but state and covariances should be done in an override.
		'''
		if msg is None: msg = Perception()
		msg.existence = self.exist.to_msg()
		msg.object_id = self.oid % (2**16-1)
		msg.classif.value = self.classif
		msg.source_id = ','.join(self.source_ids)
		msg.length, msg.width, msg.height = self.shape
		msg.t_first_obs = Time(self.t0)
		msg.t_last_obs = Time(self.tobs)
		return msg

	def __repr__(self):
		statecls = type(self).__name__[:4]
		return f'(#{self.oid} {self.classif} {statecls}, {self.state} {self.covar.diagonal()} {np.trace(self.covar):0.2f}, t:{self.t%1000:0.3f} o:{self.ageobs:0.2f} a:{self.age:0.2f} e:{self.exist["e"]:.2f} {self.exist["E"]:.2f} {self.exist["eE"]:.2f})'

	def __disp__(self):
		return self.__repr__()

	def corners(self, close=False):
		x, y, O = self.convert(StaticState).state
		if close:
			corners = [[ self.shape[0]/2, -self.shape[0]/2, -self.shape[0]/2,  self.shape[0]/2,  self.shape[0]/2],
			           [ self.shape[1]/2,  self.shape[1]/2, -self.shape[1]/2, -self.shape[1]/2,  self.shape[1]/2],
			           [               1,                1,                1,                1,               1]]
		else:
			corners = [[ self.shape[0]/2, -self.shape[0]/2, -self.shape[0]/2,  self.shape[0]/2],
			           [ self.shape[1]/2,  self.shape[1]/2, -self.shape[1]/2, -self.shape[1]/2],
			           [               1,                1,                1,                1]]
		trans = np.array([[np.cos(O), -np.sin(O), x],
		                  [np.sin(O),  np.cos(O), y],
		                  [         0,         0, 1]])

		return (trans @ corners)[:2]

	def prepare_diff(self, othr):
		diff = self.convert(type(othr))
		if isinstance(diff, StaticState) or isinstance(diff, CoordinatedTurnState):
			diff.normalize_angle_diff(othr)
		diff.state = diff.state - othr.state
		diff.covde = diff.covde + othr.covde
		diff.covin = diff.covin + othr.covin
		return diff

class StaticObject(StaticState, Object):
	@classmethod
	def _supports_msg(cls, msg):
		return msg.classif.value>=Class.SIGN_UNKNOWN

	@classmethod
	def _from_msg(cls, msg, self=None):
		if self is None: self = cls()
		return Object._from_msg(msg, StaticState._from_msg(msg, self))

	def to_msg(self, msg=None):
		if msg is None: msg = Perception()
		return Object.to_msg(self, StaticState.to_msg(self, msg))

	def prepare_observation(self, othr, heading_dir_unknown=False):
		if type(self)!=type(othr):
			raise ValueError(f'{self} does not match model of associated track {othr}')
		return super().prepare_observation(othr)

class ConstantVelObject(ConstantVelState, Object):
	@classmethod
	def _supports_msg(cls, msg):
		return msg.classif.value<Class.SIGN_UNKNOWN and \
		       msg.classif.value not in [Class.OBST_BICYCLE, Class.OBST_MOTORCYCLE, Class.OBST_CAR, Class.OBST_BUS, Class.OBST_TRUCK]

	@classmethod
	def _from_msg(cls, msg, self=None):
		if self is None: self = cls()
		return Object._from_msg(msg, ConstantVelState._from_msg(msg, self))

	def to_msg(self, msg=None):
		if msg is None: msg = Perception()
		return Object.to_msg(self, ConstantVelState.to_msg(self, msg))

	def prepare_observation(self, othr, heading_dir_unknown=False):
		if type(self)!=type(othr):
			raise ValueError(f'{self} does not match model of associated track {othr}')

		if heading_dir_unknown:
			# When the estimated heading is precise but the oriention vector direction is unknown
			# change the heading to match that of the track, but only once the track has converged
			if othr.covar[2,2]<2**2 and othr.covar[3,3]<2**2:
				track_yaw, obs_yaw = np.arctan2(othr.state[3], othr.state[2]), np.arctan2(self.state[3], self.state[2])
				obs_yaw = track_yaw + np.mod(obs_yaw-track_yaw+np.pi/2, np.pi) - np.pi/2
				self.state[2:4] = np.linalg.norm(self.state[2:4]) * np.array([np.cos(obs_yaw), np.sin(obs_yaw)])
			else:
				self.covde[2,2] += 1000
				self.covde[3,3] += 1000

		return super().prepare_observation(othr)

class CoordinatedTurnObject(CoordinatedTurnState, Object):
	@classmethod
	def _supports_msg(cls, msg):
		return msg.classif.value in [Class.OBST_BICYCLE, Class.OBST_MOTORCYCLE, Class.OBST_CAR, Class.OBST_BUS, Class.OBST_TRUCK]

	@classmethod
	def _from_msg(cls, msg, self=None):
		if self is None: self = cls()
		return Object._from_msg(msg, CoordinatedTurnState._from_msg(msg, self))

	def to_msg(self, msg=None):
		if msg is None: msg = Perception()
		return Object.to_msg(self, CoordinatedTurnState.to_msg(self, msg))

	def prepare_observation(self, othr, heading_dir_unknown=False):
		if type(self)!=type(othr):
			raise ValueError(f'{self} does not match model of associated track {othr}')

		if heading_dir_unknown:
			if othr.covar[2,2]<(np.pi/4)**2:
				self.state[2] = othr.state[2] + np.mod(self.state[2]-othr.state[2] + np.pi/2, np.pi) - np.pi/2
			else:
				self.covde[2,2] += 1000
		else:
			self.normalize_angle_diff(othr)

		return super().prepare_observation(othr)

class SimulatedVehicle(CoordinatedTurnObject):
	def __init__(self, maptile=None, **kwargs):
		super().__init__()
		self.current_vertex_id = 0
		self.tbeg = kwargs['t0']
		self.tend = self.tbeg + kwargs.get('duration', 1e30) # No duration is continue indefinitely
		self.repeat = kwargs.get('path-repeats', False)
		self.path_link_ids = kwargs['path-links']
		self.state[3] = kwargs['v']
		self.shape[:] = 5, 3, 2
		self.covin[:,:] = np.diag([3, 2, 0.1, 0.1, 0.1])
		self.covde[:,:] = 0
		self.exist['e'], self.exist['eE'] = 0.7, 0.3
		self.oid = 999
		self.classif = Class.OBST_CAR
		self.path = None
		if maptile is not None:
			self.set_path(maptile)
		self.t = None

	def set_path(self, maptile):
		self.path = np.hstack([maptile.links[link_id].polyline for link_id in self.path_link_ids])[:2,:].T
		self.state[:2] = self.path[0]
		diff = self.path[1]-self.path[0]
		self.state[2] = np.arctan2(diff[1], diff[0])

	def step(self, t):
		if self.path is None or t<self.tbeg or t>self.tend or self.current_vertex_id<0: return None
		if self.t is None: self.t = self.t0 = t

		current_vertex = self.path[self.current_vertex_id]
		next_vertex = self.path[self.current_vertex_id+1] if self.current_vertex_id<(len(self.path)-1) else self.path[0]

		self.state = self.prepare_evolution_prediction(t-self.t, None)
		self.t = self.tobs = t

		# The vehicle is past the target point, thus changes edge
		while np.linalg.norm(self.state[:2]-current_vertex)>np.linalg.norm(current_vertex-next_vertex):
			self.current_vertex_id += 1

			if self.current_vertex_id==(len(self.path)-1):
				if self.repeat:
					self.current_vertex_id = 0
				else:
					# -1 means that the path is finished
					self.current_vertex_id = -1
					return None

			current_vertex = self.path[self.current_vertex_id]
			next_vertex = self.path[self.current_vertex_id+1] if self.current_vertex_id<(len(self.path)-1) else self.path[0]

		diff = next_vertex-current_vertex
		self.state[2] = np.arctan2(diff[1], diff[0])
		return self

	def toCoordinatedTurnObject(self):
		return CoordinatedTurnObject._from_msg(self.to_msg())

def plotdebug_states(states: Union[List[State], State], risk: float = 0.03, **kwargs) -> Tuple[PlotCurveItem, ScatterPlotItem]:
	'''
	Plot objects and their associations. For debug purposes
	'''
	if states is None: states = []
	if not isinstance(states, Iterable): states = [states]
	if 'col' not in kwargs: kwargs['col'] = (20, 20, 20, kwargs.get('alpha', 255))

	ELLIPSE_STEP = 0.05
	circle = np.arange(0, (2+ELLIPSE_STEP)*np.pi, ELLIPSE_STEP*np.pi)

	if len(states)>0:
		means = np.array([(s.x, s.y) for s in states])
		ellipses = [s.state[:2] + (sqrtm(-2*np.log(risk)*s.covar[:2,:2]) @ np.array([np.cos(circle), np.sin(circle)])).T for s in states]
		ellipses_lines = np.vstack(ellipses)
		ellipses_connections = np.concatenate( [[True]*int(2/ELLIPSE_STEP)+[False]]*len(states) )
		ellipses_item = PlotCurveItem(ellipses_lines[:,0], ellipses_lines[:,1], connect=ellipses_connections, brush=kwargs['col'], pen=mkPen(color=kwargs['col'], width=2))
		means_item = ScatterPlotItem(means[:,0], means[:,1], pen=None, symbol='+', pxMode=False, size=0.1)
	else:
		ellipses_item = means_item = None

	return ellipses_item, means_item

def plotdebug_objects(objs: Union[List[Object], Object], **kwargs):
	if objs is None: objs = []
	if not isinstance(objs, Iterable): objs = [objs]
	if 'col' not in kwargs: kwargs['col'] = (20, 20, 20, kwargs.get('alpha', 255))

	if len(objs)>0:
		ellipses_item, means_item = plotdebug_states(objs, 0.01, **kwargs)
		corners = [obj.corners(close=True).T for obj in objs]
		shape_lines = np.vstack(corners)
		shape_connections = np.concatenate( [[True]*(4)+[False]]*len(objs) )
		shape_item = PlotCurveItem(shape_lines[:,0], shape_lines[:,1], connect=shape_connections, brush=kwargs['col'], pen=mkPen(color=kwargs['col'], width=2))
	else:
		ellipses_item = means_item = shape_item = None

	return ellipses_item, means_item, shape_item

if __name__=='__main__':
	import rospy
	import pickle
	from roslib.message import get_message_class
	import pyqtgraph as pg
	from pyqtgraph import QtCore, QtGui, GraphicsLayoutWidget
	from grid_map_msgs.msg import GridMap as GridMapMsg
	from multiception.msg import Perceptions
	np.set_printoptions(precision=2, linewidth=999, suppress=True, sign=' ')

	class ObjectsDisplayer(QtCore.QObject):
		signal_update = QtCore.pyqtSignal()

		def __init__(self):
			super().__init__()
			rospy.init_node('objects_displayer')
			pg.setConfigOption('background', 'w')
			pg.setConfigOption('foreground', 'k')

			self.win = GraphicsLayoutWidget(show=True, title="Test Objects Plotting")
			self.fig = self.win.addPlot(title='Objects')
			self.items = None
			self.percepts = None
			self.positions = None
			self.proxy = pg.SignalProxy(self.fig.scene().sigMouseMoved, rateLimit=10, slot=self.mouseMoved)
			self.signal_update.connect(self.draw)
			self.debugplot_timer = QtCore.QTimer()
			self.debugplot_timer.timeout.connect(lambda: self.draw(True))
			self.debugplot_timer.start(2000)
			self.sub = rospy.Subscriber('input', rospy.AnyMsg, self.callback)

		def callback(self, anymsg):
			msg_name = anymsg._connection_header['type']
			msg_class = get_message_class(msg_name)
			msg = msg_class().deserialize(anymsg._buff)

			if msg_name=='multiception/Perceptions' and len(msg.sources)>0:
				self.percepts = [Object.from_msg(p) for p in msg.perceptions]
			elif msg_name=='multiception/Perception':
				self.percepts =  [Object.from_msg(msg)]
			else:
				rospy.logwarn('Unrecognized message: %s', msg_name)
				return

			rospy.loginfo('Received %s', self.percepts)
			if len(self.percepts)>0:
				self.items = plotdebug_objects(self.percepts)
				self.positions = np.stack([obj.state[:2] for obj in self.percepts]).T
			self.signal_update.emit()

		@QtCore.pyqtSlot()
		def draw(self, close_only=False):
			if rospy.is_shutdown():
				self.win.close()
				return
			if close_only:
				return

			self.fig.clear()
			if self.items:
				for item in self.items:
					self.fig.addItem(item)

		def mouseMoved(self, evt):
			pos = evt[0]
			if self.percepts and self.fig.sceneBoundingRect().contains(pos):
				mouse_point = self.fig.vb.mapSceneToView(pos)
				point = np.array([mouse_point.x(), mouse_point.y()])
				dists = np.linalg.norm(self.positions-point[:,np.newaxis], axis=0)
				if np.min(dists)<3:
					closest = self.percepts[np.argmin(dists)]
					print(closest)

	gmd = ObjectsDisplayer()
	QtGui.QApplication.instance().exec_()
