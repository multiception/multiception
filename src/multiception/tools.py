#! /usr/bin/env python3

import numpy as np
from copy import deepcopy
from typing import Tuple, Union, Optional, Iterable, Set, Dict, Any, Callable
from sortedcontainers import SortedDict
from itertools import filterfalse
#from collections import OrderedDict

ID = Union[str, int]
Time = float

def resolvattr(base, attrs):
	'''
	Like getattr, but recursively finds subobjects in attrs
	(e.g: resolvattr(self, ["spam", "egg"]) will return self.spam.egg
	'''
	return resolvattr(getattr(base, attrs[0]), attrs[1:]) if len(attrs)>0 else base

class DummyEvent:
	'''
	Class that looks like a rospy.Timer event with a given time
	'''

	def __init__(self, t):
		self.current_real = t

class DummySource:
	'''
	Class that looks like a multiception.Source but only for the observability member
	'''

	def __init__(self, obs):
		self.observability = obs

class DummyMutex:
	'''
	Class that looks like a mutex but locks nothing
	for cases where mutex are used in code but not needed
	since this code is not ran in parallel (e.g: trackers in trust-builder)
	'''

	def __enter__(self):
		pass
	def __exit__(self, exc_type, exc_val, exc_tb):
		pass

class TimedDict(SortedDict):
	'''
	Helper class for time-based dictionnaries
	(sorts the items and helper functions to get latest or closest items)
	'''
	def __init__(self, *args, default_time: Time=-1, default_val: Any=None, **kwargs):
		'''
		Creates a `TimedDict` from the given iterable.
		`default_time`: Key returned if dict is empty
		`default_val`: Value returned if dict is empty
		'''
		super().__init__(*args, **kwargs)
		self.default_time = default_time
		self.default_val = default_val

	@property
	def default_item(self):
		return (self.default_time, self.default_val)

	def __getitem__(self, t: Time) -> Any:
		return super().get(t, self.default_val)

	def copy(self):
		return self.__class__(self.items(), default_time=self.default_time, default_val=self.default_val)

	__copy__ = copy

	def latest(self) -> Tuple[Time, Any]:
		'''
		Returns the last element of the dict
		'''
		return next(reversed(self.items()), self.default_item)

	def latest_t(self) -> Time:
		'''
		See `TimedDict.latest`
		'''
		return next(reversed(self.keys()) ,self.default_time)

	def latest_val(self) -> Any:
		'''
		See `TimedDict.latest`
		'''
		return next(reversed(self.values()), self.default_val)

	def closest(self, ttarget: Time, sign=0) -> Tuple[Time, Any]:
		'''
		Returns the closest key to `ttarget`.
		If `sign==0`, no key is ignored
		If `sign> 0`, keys before `ttarget` are ignored, including `ttarget`
		If `sign< 0`, keys after  `ttarget` are ignored, including `ttarget`
		If `sign>+1`, keys before `ttarget` are ignored, excluding `ttarget`
		If `sign<-1`, keys after  `ttarget` are ignored, excluding `ttarget`
		'''
		t = self.closest_t(ttarget, sign)
		return (t, self[t]) if t!=self.default_time else self.default_item

	def closest_t(self, ttarget: Time, sign=0) -> Time:
		'''
		See `TimedDict.closest`
		'''
		tcomp = np.fromiter(self.keys(), dtype=Time)
		if   sign==0: pass
		elif sign< 0: tcomp = tcomp[tcomp<=ttarget]
		elif sign> 0: tcomp = tcomp[tcomp>=ttarget]
		elif sign<-1: tcomp = tcomp[tcomp<ttarget]
		elif sign>+1: tcomp = tcomp[tcomp>ttarget]

		return tcomp[np.argmin(np.abs(tcomp-ttarget))] if len(tcomp)>0 else self.default_time

	def closest_val(self, ttarget, sign=0) -> Any:
		'''
		See `TimedDict.closest`
		'''
		t = self.closest_t(ttarget, sign)
		return self[t] if t!=self.default_time else self.default_val

	def filtered(self, tmin, tmax=1E30):
		'''
		Returns a TimedDict whose times are in the range [tmin, tmax[
		'''
		expr = filter(lambda item: item[0]>=tmin and item[0]<tmax, self.items())
		return type(self)(expr, default_time=self.default_time, default_val=self.default_val)

	def prepare_interp(self, t: Time) -> Tuple[float, Any, Any]:
		idx_upp = self.bisect(t)
		size = len(self)
		if size==0 or idx_upp==size:
			raise ValueError(f'Requested time {t} is outside stored times')

		idx_low = idx_upp - 1
		t_upp, v_upp = self.items()[idx_upp]
		t_low, v_low = self.items()[idx_low]
		ratio = (t-t_low) / (t_upp-t_low)
		return ratio, v_low, v_upp

class IdTimedDict(dict):
	def __init__(self, *args, default_time: Time=-1, default_val: Any=None, **kwargs):
		'''
		Creates a `IdTimedDict`: { id: { t : val } } where id can be a int or string.
		`iterable`: Iterable used to initialize the dict
		`default_time`: Key returned if dict is empty
		`default_val`: Value returned if dict is empty
		'''
		super().__init__(*args, **kwargs)
		self.default_time = default_time
		self.default_val = default_val

	@property
	def default_item(self):
		return (self.default_time, self.default_val)

	def __getitem__(self, key: Union[ID, Tuple[ID, Time]]) -> Any:
		if isinstance(key, int) or isinstance(key, str):
			return super(IdTimedDict, self).get(key, TimedDict(default_time=self.default_time, default_val=self.default_val))
		else:
			return self[key[0]].get(key[1])

	def __setitem__(self, key: Tuple[ID, Time], val: Any):
		if isinstance(key, int) or isinstance(key, str):
			super(IdTimedDict, self).__setitem__(key, val)
		else:
			if key[0] not in self.keys(): self[key[0]] = TimedDict(default_time=self.default_time, default_val=self.default_val)
			super(IdTimedDict, self).__getitem__(key[0])[key[1]] = val

	def _resolve_ids(self, ids: Optional[Union[ID, Iterable[ID]]]) -> Set[ID]:
		'''
		Turns None, one id or multiple ids into a resolvable set of ids.
		'''
		if ids is None:
			return set(self.keys())
		elif isinstance(ids, str) or isinstance(ids, int):
			ids = set([ids])

		filtered_ids = ids & set(self.keys())
		return filtered_ids

	def latest(self, ids: Union[ID, Iterable[ID]]=None) -> Union[Tuple[Time, Any], Dict[ID, Tuple[Time, Any]]]:
		'''
		Returns the latest element of the given id or a generator thereof if multiple ids are provided
		`ids`: None, one ID (string or int) or multiple IDs
		'''
		resolved_ids = self._resolve_ids(ids)
		if ids is not None and len(resolved_ids)==1:
			for i in resolved_ids: pass
			return super(IdTimedDict, self).__getitem__(i).latest()
		else:
			def gen_function():
				for i in resolved_ids:
					yield i, super(IdTimedDict, self).__getitem__(i).latest()
			return gen_function()

	def latest_t(self, ids: Union[ID, Iterable[ID]]=None) -> Union[Time, Dict[ID, Time]]:
		'''
		See `IdTimedDict.latest`
		'''
		resolved_ids = self._resolve_ids(ids)
		if ids is not None and len(resolved_ids)==1:
			for i in resolved_ids: pass
			return super(IdTimedDict, self).__getitem__(i).latest_t()
		else:
			def gen_function():
				for i in resolved_ids:
					yield i, super(IdTimedDict, self).__getitem__(i).latest_t()
			return gen_function()

	def latest_val(self, ids: Union[ID, Iterable[ID]]=None) -> Union[Any, Dict[ID, Any]]:
		'''
		See `IdTimedDict.latest`
		'''
		resolved_ids = self._resolve_ids(ids)
		if ids is not None and len(resolved_ids)==1:
			for i in resolved_ids: pass
			return super(IdTimedDict, self).__getitem__(i).latest_val()
		else:
			def gen_function():
				for i in resolved_ids:
					yield i, super(IdTimedDict, self).__getitem__(i).latest_val()
			return gen_function()

	def closest(self, ttarget, ids=None, sign=0) -> Union[Tuple[Time, Any], Dict[ID, Tuple[Time, Any]]]:
		'''
		Returns the item closest to ttarget for the given id or a generator thereof if multiple ids are provided
		`ttarget`: target time
		`ids`: None, one ID (string or int) or multiple IDs
		If `sign==0`, no time is ignored
		If `sign> 0`, times before ttarget are ignored
		If `sign< 0`, times after ttarget are ignored
		'''
		resolved_ids = self._resolve_ids(ids)
		if ids is not None and len(resolved_ids)==1:
			for i in resolved_ids: pass
			return super(IdTimedDict, self).__getitem__(i).closest(ttarget, sign)
		else:
			def gen_function():
				for i in resolved_ids:
					yield i, super(IdTimedDict, self).__getitem__(i).closest(ttarget, sign)
			return gen_function()

	def closest_t(self, ttarget, ids=None, sign=0):
		'''
		See `IdTimedDict.closest`
		'''
		resolved_ids = self._resolve_ids(ids)
		if ids is not None and len(resolved_ids)==1:
			for i in resolved_ids: pass
			return super(IdTimedDict, self).__getitem__(i).closest_t(ttarget, sign)
		else:
			def gen_function():
				for i in resolved_ids:
					yield i, super(IdTimedDict, self).__getitem__(i).closest_t(ttarget, sign)
			return gen_function()

	def closest_val(self, ttarget, ids=None, sign=0):
		'''
		See `IdTimedDict.closest`
		'''
		resolved_ids = self._resolve_ids(ids)
		if ids is not None and len(resolved_ids)==1:
			for i in resolved_ids: pass
			return super(IdTimedDict, self).__getitem__(i).closest_val(ttarget, sign)
		else:
			def gen_function():
				for i in resolved_ids:
					yield i, super(IdTimedDict, self).__getitem__(i).closest_val(ttarget, sign)
			return gen_function()

	def filtered(self, tmin, tmax=1E30):
		generator_expr = ((i, filtered_timed_dict) for i in self.keys() if len(filtered_timed_dict:=super(IdTimedDict, self).__getitem__(i).filtered(tmin, tmax))>0)
		return IdTimedDict(generator_expr, default_time=self.default_time, default_val=self.default_val)

	def swap(self):
		swapped = TimedIdDict()
		for i, tv in self.items():
			for t, v in tv.items():
				swapped[t, i] = v
		return swapped



class TimedIdDict(TimedDict):
	def __init__(self, *args, default_val: Any=dict(), **kwargs):
		'''
		Creates a `TimedIdDict` { t: { id: ... } } from the given iterable.
		'''
		super().__init__(*args, default_val=default_val, **kwargs)

	def __getitem__(self, key: Union[Time, Tuple[Time, ID]]) -> Any:
		if isinstance(key, float):
			return super(TimedIdDict, self).get(key, deepcopy(self.default_val))
		else:
			return super(TimedIdDict, self).get(key[0], deepcopy(self.default_val)).get(key[1])

	def __setitem__(self, key: Tuple[Time, ID], val: Any):
		if isinstance(key, float):
			super(TimedIdDict, self).__setitem__(key, val)
		else:
			if key[0] not in self.keys():
				super(TimedIdDict, self).__setitem__(key[0], deepcopy(self.default_val))
			super(TimedIdDict, self).__getitem__(key[0])[key[1]] = val

	def move(self, other: '__class__', predicate: Callable[[Tuple[Time, Dict[ID, Any]]], bool]) -> '__class__':
		'''
		Moves all items to other, optionally contrained to a single time
		'''
		other.update(filter(predicate, self.items()))
		filtered_self = TimedIdDict(filterfalse(predicate, self.items()))
		return filtered_self
		#for t, id_vals in self.items():
			#for i, val in id_vals:
				#if predicate(t, i, val):
					#other[t, i] = val

	def swap(self):
		swapped = IdTimedDict()
		for t, iv in self.items():
			for i, v in iv.items():
				swapped[i, t] = v
		return swapped

class WhiteNoiseGenerator:
	def __init__(self, cov, bias):
		self.cov = np.asarray(cov)
		self.bias = np.full(len(self.cov), bias)

	def rand(self, dt: float):
		cov = self.cov/20 + (np.random.uniform(0, 100)<5)*self.cov/2 # Sporadically give a large noise
		return np.random.multivariate_normal(self.bias, cov*dt)

class BrownNoiseGenerator(WhiteNoiseGenerator):
	def __init__(self, cov, bias):
		super().__init__(self, cov, bias)
		self.center = np.zeros(len(self.cov))

	def rand(self, dt: float):
		self.center += super().rand(self, dt)
		return self.center

if __name__=='__main__':
	import pickle

	def test_TimedDict():
		d = TimedDict()
		d[20.1] = 1
		d[16.8] = 2
		d[900.6] = 3
		d[200.0] = 4
		d[200.0] = 6
		d[12.4] = 5

		print(d)
		print("Latest", d.latest(), d.latest_t(), d.latest_val())
		print("Closest to 10.2", d.closest(10.2), d.closest_t(10.2), d.closest_val(10.2))
		print("Closest to 10.2 Neg", d.closest(10.2, -1), d.closest_t(10.2, -1), d.closest_val(10.2, -1))
		print("Closest to 10.2 Pos", d.closest(10.2, 1), d.closest_t(10.2, 1), d.closest_val(10.2, 1))
		print("Filtered between 3 and 500", d.filtered(3,500))
		with open("/tmp/test.pkl", "wb") as f: pickle.dump(d, f)
		with open("/tmp/test.pkl", "rb") as f: e = pickle.load(f)
		print(e==d)

	def test_IdTimedDict():
		d = IdTimedDict()
		d['toto', 0.1] = 1
		d['tata', 6.8] = 2
		d['toto', 900.6] = 3
		d['tata', 201.0] = 4
		d['titi', 987.0] = 0
		d['toto', 200.0] = 6
		d['tata', 2.4] = 5

		print(d)
		print("Latest toto", d.latest('toto'), d.latest_t('toto'), d.latest_val('toto'))
		print("Latest tata/toto", IdTimedDict(d.latest({'tata', 'toto'})), IdTimedDict(d.latest_t({'tata', 'toto'})), IdTimedDict(d.latest_val({'tata', 'toto'})))

		print("Closest toto to 10.2", d.closest(10.2, 'toto'), d.closest_t(10.2, 'toto'), d.closest_val(10.2, 'toto'))
		print("Closest tata/toto to 10.2", IdTimedDict(d.closest(10.2, {'tata', 'toto'})), IdTimedDict(d.closest_t(10.2, {'tata', 'toto'})), IdTimedDict(d.closest_val(10.2, {'tata', 'toto'})))
		print("Closest to 10.2", IdTimedDict(d.closest(10.2)), IdTimedDict(d.closest_t(10.2)), IdTimedDict(d.closest_val(10.2)))

		print("Closest toto to 10.2 neg", d.closest(10.2, 'toto', -1), d.closest_t(10.2, 'toto', -1), d.closest_val(10.2, 'toto', -1))
		print("Closest tata/toto to 10.2 neg", IdTimedDict(d.closest(10.2, {'tata', 'toto'}, -1)), IdTimedDict(d.closest_t(10.2, {'tata', 'toto'}, -1)), IdTimedDict(d.closest_val(10.2, {'tata', 'toto'}, -1)))
		print("Closest to 10.2 neg", IdTimedDict(d.closest(10.2, None, -1)), IdTimedDict(d.closest_t(10.2, None, -1)), IdTimedDict(d.closest_val(10.2, None, -1)))

		print("Closest toto to 10.2 pos", d.closest(10.2, 'toto', 1), d.closest_t(10.2, 'toto', 1), d.closest_val(10.2, 'toto', 1))
		print("Closest tata/toto to 10.2 pos", IdTimedDict(d.closest(10.2, {'tata', 'toto'}, 1)), IdTimedDict(d.closest_t(10.2, {'tata', 'toto'}, 1)), IdTimedDict(d.closest_val(10.2, {'tata', 'toto'}, 1)))
		print("Closest to 10.2 pos", IdTimedDict(d.closest(10.2, None, 1)), IdTimedDict(d.closest_t(10.2, None, 1)), IdTimedDict(d.closest_val(10.2, None, 1)))

		print("Filtered between 3 and 500", d.filtered(3,500))
		with open("/tmp/test.pkl", "wb") as f: pickle.dump(d, f)
		with open("/tmp/test.pkl", "rb") as f: e = pickle.load(f)
		print(e==d)

	def test_TimedIdDict():
		d = TimedIdDict()
		d[0.1, 'toto'] = 1
		d[6.8, 'tata'] = 2
		d[900.6, 'toto'] = 3
		d[201.0, 'tata'] = 4
		d[987.0, 'titi'] = 0
		d[200.0, 'toto'] = 6
		d[2.4, 'tata'] = 5

		e = TimedIdDict()
		e[0.1, 'toto'] = 11
		e[6.8, 'tutu'] = 12
		e[36.3, 'tata'] = 13
		e[42.5, 'tete'] = 14
		e[200., 'titi'] = 15

		print(d)
		print(e)
		f = d.move(e, lambda t_iv: t_iv[0]<100)
		print(e)
		print(f)
		print("Filtered between 3 and 500", d.filtered(3, 500))
		with open("/tmp/test.pkl", "wb") as f: pickle.dump(d, f)
		with open("/tmp/test.pkl", "rb") as f: e = pickle.load(f)
		print(e==d)

	print("==== Testing TimedDict ====")
	test_TimedDict()

	print("\n\n==== Testing IdTimedDict ====")
	test_IdTimedDict()

	print("\n\n==== Testing TimedIdDict ====")
	test_TimedIdDict()
