#!/usr/bin/env python

import copy
import traceback
import numpy as np
from threading import Lock
from collections.abc import Iterable
from typing import List, Tuple, Union

import rospy
from multiception.cfg import TrackingConfig
from multiception.belief import MassFunction
from multiception.tools import TimedDict, IdTimedDict, TimedIdDict
from multiception.object import StaticState, ConstantVelState, CoordinatedTurnState, Object, plotdebug_objects
from multiception.evidential_grid_map import EvidentialGridMap, plotdebug_gridmap
from multiception.association import plotdebug_assocs, AssociatorMahalanobis, AssociatorMahalanobisClass
from multiception.filtering import Kalman, SCI, IFCI, ITCI, CI, OldSCI, OldCI
from multiception.existence import ExistenceEstimatorNaive, ExistenceEstimatorFilter, ExistenceEstimatorObservability
np.set_printoptions(precision=2, linewidth=999, suppress=True, sign=' ', formatter={'float': '{:0.2f}'.format})

ObservabilityGrid = EvidentialGridMap
ObservabilityGrids = List[EvidentialGridMap]
Objects = List[Object]
Assocs = Tuple[List[int], List[int]]

FILTERS = {
	TrackingConfig.Tracking_Kalman: Kalman,
	TrackingConfig.Tracking_SCI:    SCI,
	TrackingConfig.Tracking_IFCI:   IFCI,
	TrackingConfig.Tracking_ITCI:   ITCI,
	TrackingConfig.Tracking_CI:     CI,
	TrackingConfig.Tracking_OldSCI: OldSCI,
	TrackingConfig.Tracking_OldCI:  OldCI,
}

ASSOCIATORS = {
	TrackingConfig.Tracking_Mahalanobis:      AssociatorMahalanobis,
	TrackingConfig.Tracking_MahalanobisClass: AssociatorMahalanobisClass,
}

EXISTENCE_ESTIMATORS = {
	TrackingConfig.Tracking_Naive:         ExistenceEstimatorNaive,
	TrackingConfig.Tracking_Filtering:     ExistenceEstimatorFilter,
	TrackingConfig.Tracking_Observability: ExistenceEstimatorObservability,
}

INITIAL_TRUST_FILTER = MassFunction({'t': 1.0 ,'tT': 0.0})
INITIAL_TRUST_LOCAL  = MassFunction({'t': 0.8 ,'tT': 0.2})
INITIAL_TRUST_REMOTE = MassFunction({'t': 0.0 ,'tT': 1.0})

INITIAL_TRUSTS = IdTimedDict()
INITIAL_TRUSTS['filter', 0.0]     = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['lidar', 0.0]      = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['mobileye', 0.0]   = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['egotracker', 0.0] = INITIAL_TRUST_LOCAL

INITIAL_TRUSTS['zoeblue/filter', 0.0]      = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['zoegrey/filter', 0.0]      = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['zoewhite/filter', 0.0]     = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['zoeblue/lidar', 0.0]       = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoegrey/lidar', 0.0]       = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoewhite/lidar', 0.0]      = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoeblue/mobileye', 0.0]    = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoegrey/mobileye', 0.0]    = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoewhite/mobileye', 0.0]   = INITIAL_TRUST_LOCAL
# Trust is only estimated on zoe/lidar and zoe/mobileye, not zoe/egotracker to avoid redundant estimation.
# Also, the observability of the egotracker is the combined observability of its sensors wheigted by their trust
# Thus in the multracker, we take the egotracker visibility as is (trust=1) because trust reduction already happened
INITIAL_TRUSTS['zoeblue/egotracker', 0.0]  = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['zoegrey/egotracker', 0.0]  = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['zoewhite/egotracker', 0.0] = INITIAL_TRUST_FILTER
INITIAL_TRUSTS['zoeblue/multracker', 0.0]  = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoegrey/multracker', 0.0]  = INITIAL_TRUST_LOCAL
INITIAL_TRUSTS['zoewhite/multracker', 0.0] = INITIAL_TRUST_LOCAL

INITIAL_TRUSTS['zoeblue', 0.0]  = INITIAL_TRUST_REMOTE
INITIAL_TRUSTS['zoegrey', 0.0]  = INITIAL_TRUST_REMOTE
INITIAL_TRUSTS['zoewhite', 0.0] = INITIAL_TRUST_REMOTE

class Tracker:
	def __init__(self, filter_cls, associator, evo_noises, freq, buffers_duration=0):
		'''
		self.t is the current tracker time (last update)
		self.obs_buffer store observation over the last `buffers_duration` seconds. OrderedDict (time, Dict(source, obslist)).
		self.obs_temp stores observations between an add_obs and an update. Same structure as obs_buffer.
		self.tracks_buffer stores tracks over the last `buffers_duration` seconds. Ordered dictionnary of previous track lists by time.
		buffers_duration: in seconds, 0 means no buffering (only keep obs and state since last update).
		'''
		self.t = .0
		self.freq = freq
		self.tracks_buffer = TimedDict(default_time=0, default_val=[])
		self.oidmax_buffer = TimedDict(default_val=2)
		self.obscon_buffer = TimedDict()
		self.obs_buffer = TimedIdDict()
		self.obs_temp = TimedIdDict()
		self.sources = IdTimedDict()
		self.trusts = IdTimedDict()
		self.filter_cls = filter_cls
		self.associator = associator
		self.evo_noises = evo_noises
		self.buffers_duration = buffers_duration
		self.heading_dir_unknown = False
		self.merge_cost_max = 0
		self.obs_mutex = Lock()
		self.debugplot = False
		self.debugplot_items = {}
		self.existence_estimator = None
		self.debug_name = ""
		self.tqdm = lambda it, **kwargs: it
		self.UNTRUSTWORTHY_DILUTION = { # Covariance matrices added to objects of untrustworthy or unknown sources
			StaticState: np.zeros((3,3)),
			ConstantVelState: np.zeros((4,4)),
			CoordinatedTurnState: np.zeros((5,5)),
		}

	def _filter_buffers(self, t: float) -> None:
		'''
		Removes elements older than self.buffers_duration seconds, then sort from most ancient to most recent
		'''
		min_time = t - self.buffers_duration - (2/self.freq if self.freq>0 else 0) # Add two iterations to the buffer duration for good measure

		rospy.logdebug('---- Removing < %.4f ----', min_time)
		# TODO: dispatch in a threading executor
		# FIXME: Filter trusts as well (being careful for initial trust that have t=0)
		self.tracks_buffer = self.tracks_buffer.filtered(min_time)
		self.oidmax_buffer = self.oidmax_buffer.filtered(min_time)
		self.obs_buffer = self.obs_buffer.filtered(min_time)
		self.obs_temp = self.obs_temp.filtered(min_time)
		self.sources = self.sources.filtered(min_time)
		self.obscon_buffer = self.obscon_buffer.filtered(min_time)
		#self.trusts = self.trusts.filtered(min_time)

	def get_history(self, oid) -> Objects:
		return [track for tracklist in self.tracks_buffer.values() for track in tracklist if track.oid==oid]

	def add_obs(self, source_id: str, t: float, obs: Objects) -> None:
		'''
		Add an observation to the intermediary observation buffer.
		`obs` can either be an obseration or iterator of observations
		'''
		with self.obs_mutex:
			self.obs_temp[t, source_id] = obs

	def add_source(self, source_id: str, t: float, source) -> None:
		with self.obs_mutex:
			self.sources[source_id, t] = source

	def set_trust(self, source_id: str, t: float, trust: MassFunction) -> None:
		with self.obs_mutex:
			self.trusts[source_id, t] = trust

	def rollback(self, t_target: float) -> None:
		'''
		Rolls backs the different buffers for when an Out-Of-Sequence (OOS) measurement is received.
		Removes buffered tracks and oidmax stored after the oos and move observations buffered after
		the oos in the "to be processed" observation buffer.
		'''

		rospy.logdebug('---- Rolling back %.4f -> %.4f ----', self.t, t_target)
		self.tracks_buffer = self.tracks_buffer.filtered(0, t_target)
		self.oidmax_buffer = self.oidmax_buffer.filtered(0, t_target)
		self.obscon_buffer = self.obscon_buffer.filtered(0, t_target)

		self.t = self.tracks_buffer.latest_t() # FIXME: make its default time 0 if this crashes
		self.obs_buffer = self.obs_buffer.move(self.obs_temp, lambda t_iv: t_iv[0]>self.t)

		rospy.logdebug('State is %s tracks at time %.4f with observation steps at %s', len(self.tracks_buffer.latest_val()), self.t, list(self.obs_temp.keys()))

	def step(self, t: float) -> Tuple[Objects, ObservabilityGrid]:
		rospy.logdebug('')
		rospy.logdebug('----- Step %.4f -> %.4f -----', self.t, t)

		tracks = observability_consensus = None
		with self.obs_mutex:
			#self._filter_buffers(t)

			if self.debugplot:
				self.debugplot_items = {}

			if len(self.obs_temp)>0:
				# Determine oldest temporary observation and trigger a Out-Of-Sequence if needed
				# FIXME: take the frequency into account
				oostime = min([t]+[ot for ot in self.obs_temp.keys()])
				if self.t>oostime:
					self.rollback(oostime)

				# From there, we are at time self.t, and want to fuse tracks with all temporary (newly added) observations, then predict up to time t
				for tobs, sourceobjects in self.tqdm(self.obs_temp.items(), desc=f"Tracking {self.debug_name}", total=len(self.obs_temp)):
					for sourceid, objectlist in sourceobjects.items():
						rospy.logdebug('---- Tracking %.4f -> %.4f (%s) ----', self.t, tobs, sourceid)
						tracks, observability_consensus = self.step_inner(objectlist, tobs, sourceid)

				# Move temporary observations in the observation buffer
				self.obs_buffer = self.obs_temp
				self.obs_temp = TimedIdDict()

		if tracks is None or observability_consensus is None:
			tracks = self.tracks_buffer.latest_val()
			observability_consensus = self.obscon_buffer.latest_val()

		# Bring the track up to current time when not message-driven
		if self.freq>0:
			rospy.logdebug('---- Predicting %.4f -> %.4f ----', self.t, t)
			tracks = self.predict_tracks(copy.deepcopy(tracks), t, observability_consensus)

		rospy.logdebug('')
		return np.extract(self.existence_estimator.validate(tracks), tracks), observability_consensus

	def step_inner(self, observations: Objects, t: float, sourceid: str) -> Tuple[Objects, ObservabilityGrid]:
		tracks = copy.deepcopy(self.tracks_buffer.latest_val())
		observations = np.array(observations)

		for obs in observations:
			rospy.logdebug('Observation %s from "%s"', obs, sourceid)
			if obs.t>t:
				rospy.logwarn('Observation %s is after tracker time %f (%f)', obs, t%1000, obs.t%1000)

		trusts = {}
		observabilities = {}
		for sid, st in self.sources.closest_t(t, sign=-1):
			if st==-1: continue
			dt = t-st# if st>0.0 else 0.0
			alpha = MassFunction.get_decay_alpha(1, dt)
			trusts[sid] = self.trusts.closest_val(st, sid, -1)
			trust_decayed = trusts[sid].discount(alpha)
			trust_discount = trust_decayed.pl('T')
			observabilities[sid] = EvidentialGridMap(self.sources[sid, st].observability).discount(trust_discount)
			rospy.logdebug('Prepare Vis %s from %.2f to %.2f: %s*%.2f=%s (%.2f) %s', sid, st%1000, t%1000, trusts[sid], alpha, trust_decayed, trust_discount, observabilities[sid])

		try:
			obss = list(observabilities.values())
			observability_consensus = EvidentialGridMap.combineDetect(obss)
		except MemoryError as e:
			import pickle
			path = f'/tmp/{np.random.randint(9999999)}.pkl'
			rospy.logwarn('Error caused dump of %s in %s:\n%s', obss, path, ''.join(traceback.format_exception(None, e, e.__traceback__)))
			with open(path, 'wb') as f:
				pickle.dump(obss, f)

		rospy.logdebug('--- Predict   ---')
		self.predict_tracks(tracks, t, observability_consensus)

		rospy.logdebug('--- Associate ---')
		assocs = self.associate_tracks(tracks, observations)
		if self.debugplot and sourceid:
			self.debugplot_items[sourceid] = [
				plotdebug_gridmap(observabilities[sourceid], opacity=0.95) if sourceid in trusts else None,
				plotdebug_assocs(assocs, tracks, observations),
				*plotdebug_objects(tracks, col=(31, 119, 180)),
				*plotdebug_objects(observations, col=(255, 127, 14), alpha=trusts[sourceid]['t']),
			]

		rospy.logdebug('--- Update    ---')
		self.update_tracks(tracks, observations, assocs, observability_consensus, observabilities[sourceid], trusts[sourceid], sourceid)

		rospy.logdebug('--- Manage    ---')
		_, oidmax = self.create_tracks(tracks, observations, assocs, trusts[sourceid], observabilities[sourceid], sourceid, t)
		self.remove_tracks(tracks)
		#self.merge_tracks(tracks)

		if self.debugplot:
			self.debugplot_items[''] = [plotdebug_gridmap(observability_consensus, opacity=0.95), *plotdebug_objects(tracks)]

		### Wrap up
		self.t = t
		self.tracks_buffer[t] = tracks
		self.oidmax_buffer[t] = oidmax
		self.obscon_buffer[t] = observability_consensus
		self.obscon_buffer[t].setTimestamp(int(t*10e9))
		return tracks, observability_consensus

	def predict_tracks(self, tracks: Objects, t: float, observability_consensus: ObservabilityGrid) -> Objects:
		'''
		Predict tracks up to time t.
		Apply the evolution model associated to each track and discount the existence probability.
		'''
		for track in tracks:
			dt = t-track.t
			evo_noise = self.evo_noises[type(track)]
			try:
				self.filter_cls.predict(track, dt, evo_noise)
			except ValueError as e:
				rospy.logwarn('Prediction of %s caused %s %s', track, ''.join(traceback.format_exception(None, e, e.__traceback__)), ([tr.t for tr in tracks], t, self.t))
				return tracks

			if isinstance(track, CoordinatedTurnState):
				track.normalize_speed()

			self.existence_estimator.predict(track, dt, observability_consensus=observability_consensus)
			# Dirty hack: cap uncertainty at 1000 to avoid infinities with bad filtering
			#track.covde[-1,-1] = min(track.covde[-1,-1], 1000)
			#track.covin[-1,-1] = min(track.covin[-1,-1], 1000)

			track.t = t
			rospy.logdebug('Predict     %s', track)

		return tracks

	def associate_tracks(self, tracks: Objects, observations: Objects) -> Assocs:
		'''
		Associate tracks and observations, putting all-out-of-gate associations aside
		'''
		try:
			assocs = self.associator.associate(tracks, observations)
		except FloatingPointError as e:
			rospy.logwarn("FPE assoc: %s %s %s", e, tracks, observations)
			assocs = [], []

		rospy.logdebug('%s %.2f\n%s', assocs, self.associator.gate, self.associator.cost_mat)
		return assocs

	def update_tracks(self, tracks: Objects, observations: Objects, assocs: Assocs, observability_consensus: ObservabilityGrid, observability: ObservabilityGrid, trust: MassFunction, sourceid: str) -> Objects:
		'''
		Updates tracks with their associated observation
		'''
		# Reduce the existence probability when unobserved
		unassociated_idx = self.associator.get_unassociated_idx(tracks, assocs[0])
		for i in unassociated_idx:
			self.existence_estimator.update_unassociated(tracks[i], trust=trust, observability_consensus=observability_consensus, observability_other=observability)
			rospy.logdebug('Unassoc Upd %s', tracks[i])

		for ti, oi in zip(*assocs):
			track = tracks[ti]
			obs = observations[oi]
			try:
				meas = obs.prepare_observation(track, self.heading_dir_unknown)
			except ValueError as e:
				# Types of track and obs are mismatched, output a warning and convert obs to carry on anyway
				rospy.logwarn('Mismatch     %s', e)
				meas = obs.convert(type(track)).prepare_observation(track, self.heading_dir_unknown)

			rospy.logdebug('Associate   %s', track)
			rospy.logdebug('     with   %s (d=%.1f)', obs, self.associator.cost_mat[ti,oi])

			# Dilute the covariance of unknown or untrustworthy sources
			dilution_matrix = [self.UNTRUSTWORTHY_DILUTION[subclass] for subclass in type(obs).mro() if subclass in self.UNTRUSTWORTHY_DILUTION][0]
			meas.noisd += meas.model @ dilution_matrix @ meas.model.T * trust.pl('T')
			rospy.logdebug('Lessen to   %s (T=%.2f)', meas.noise.diagonal(), trust.pl('T'))

			self.existence_estimator.update_associated(track, obs, trust=trust, observability_consensus=observability_consensus, observability_other=observability)
			if np.all(obs.shape<10):
				track.shape = obs.shape
			track.tobs = max(track.tobs, obs.t)
			track.source_id = sourceid

			if not issubclass(self.filter_cls, SCI):
				# Explicitely remove the independent part if the filter is not capable of handling it
				track.covde += track.covin; track.covin[:,:] = 0
				meas.noisd += meas.noisi; meas.noisi[:,:] = 0
			try:
				self.filter_cls.update(track, 0, [meas])
			except np.linalg.LinAlgError as e:
				rospy.logwarn("LinAlgError update: %s (%s %s)", e, track, meas)

			# Dirty hack: cap uncertainty at 1000 to avoid infinities with bad filtering
			#track.covde[-1,-1] = min(track.covde[-1,-1], 1000)
			#track.covin[-1,-1] = min(track.covin[-1,-1], 1000)

			if isinstance(track, CoordinatedTurnState):
				track.normalize_speed()
			rospy.logdebug('Updated     %s', track)

		return tracks

	def remove_tracks(self, tracks: Objects) -> Objects:
		'''
		Removes tracks whose existence probability is too low
		"Either I *know* you don't exist or I don't have sufficient information to believe you still exist"
		'''
		tracks2del = self.existence_estimator.destroy(tracks)
		for i in range(len(tracks)-1, -1, -1):
			if tracks2del[i] or np.any(tracks[i].covar.diagonal()>5000) or np.any(tracks[i].covar.diagonal()<0.001) or np.any(tracks[i].shape>8):
				rospy.logdebug('Remove      %s', tracks[i])
				del tracks[i]

		return tracks

	def create_tracks(self, tracks: Objects, observations: Objects, assocs: Assocs, trust: MassFunction, observability: ObservabilityGrid, sourceid: str, t: float) -> Tuple[Objects, int]:
		'''
		Create tracks from unassociated detections
		'''
		unassociated_idx = self.associator.get_unassociated_idx(observations, assocs[1])
		new_tracks = copy.deepcopy(observations[unassociated_idx])
		oidmax = self.oidmax_buffer.latest_val()

		for track in new_tracks:
			self.existence_estimator.initialize(track, trust, observability=observability)
			track.source_id = sourceid
			track.oid = oidmax
			track.t0 = t
			track.tobs = t
			oidmax += 1

			# When the heading orientation is known but not the direction, start with a high uncertainty on the heading
			# It will be incorporated once the real orientation is tracked
			if self.heading_dir_unknown:
				if isinstance(track, ConstantVelState): # [x, y, vx, vy, ...]
					track.covde[2, 2] += 1000
					track.covde[3, 3] += 1000
				elif isinstance(track, CoordinatedTurnState): # [x, y, O, ...]
					track.covde[2, 2] += 1000

			# Dirty hack: cap uncertainty at 1000 to avoid infinities with bad filtering
			#track.covde[-1,-1] = min(track.covde[-1,-1], 1000)
			#track.covin[-1,-1] = min(track.covin[-1,-1], 1000)

			# Explicitely remove the independent part if the filter is not capable of handling it
			if not issubclass(self.filter_cls, SCI):
				track.covde += track.covin; track.covin[:,:] = 0

			rospy.logdebug('Create      %s from %s', track, sourceid)
			tracks.append(track)

		return tracks, oidmax

	def merge_tracks(self, tracks: Objects) -> Object:
		'''
		Merge tracks that they are associated too closely
		'''
		cost_max_backup = self.associator.cost_max
		self.associator.cost_max = self.merge_cost_max
		merge_mat, mergeable_idx = self.associator.associate(tracks, tracks)

		if len(mergeable_idx)>1:
			np.fill_diagonal(merge_mat, np.inf)
			mergeA_idx, mergeB_idx = linear_sum_assignment(merge_mat)

			# Remove tracks that can be merged
			# Second associability check for cases where the cost after assignment is still too high
			for i in range(len(mergeA_idx)-1, -1, -1):
				if merge_mat[mergeA_idx[i], mergeB_idx[i]]<self.associator.cost_max:
					rospy.logdebug('Merging     %s', tracks[mergeA_idx[i]])
					rospy.logdebug('   with     %s', tracks[mergeB_idx[i]])
					del tracks[mergeB_idx[i]]

		self.associator.cost_max = cost_max_backup
		return tracks

	# Pickling support
	def __getstate__(self):
		state = self.__dict__.copy()
		# Don't pickle obs_mutex because Lock() cannot be pickled
		del state['obs_mutex']
		return state
	def __setstate__(self, state):
		self.__dict__.update(state)
		# Add obs_mutex back since it doesn't exist in the pickle
		self.obs_mutex = Lock()
