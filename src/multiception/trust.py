#!/usr/bin/env python3

from enum import Enum
import traceback
from copy import deepcopy
import rospy
import numpy as np
from typing import List, Tuple
from shapely.geometry import MultiPolygon, MultiPoint, MultiLineString
from threading import Lock
from scipy.stats import norm
from pathlib import Path

from multiception.tools import IdTimedDict
from multiception.belief import MassFunction, mass_twogauss
from multiception.object import Object, StaticObject, ConstantVelObject, CoordinatedTurnObject
from multiception.tracking import Tracker
from multiception.filtering import SCI
from multiception.existence import compute_observability_object, compute_observability_object_partial, compute_similarity, ExistenceEstimatorObservability
from multiception.association import plotdebug_assocs, AssociatorMahalanobisClass
from multiception.evidential_grid_map import EvidentialGridMap, plotdebug_gridmap

DetectabilityGrid = EvidentialGridMap
DetectabilityGrids = List[DetectabilityGrid]
Objects = List[Object]
Assocs = Tuple[List[int], List[int]]
History = Tuple[str, int, MassFunction, Objects, DetectabilityGrid]

def freespace_conf(ref: np.ndarray, oth: np.ndarray, sigma: float=0.05):
	'''!Computes the freespace confirmation.
	To be confirmed, a cell needs to have close or above the reference value.
	A cell whose value is opposite 
	The more sure a cell is, the more confirmed it is:
	\f( \operatorname{erf}\left(\frac{m_2(A)-m_1(A)+\sigma}{(1-m_1(A))\sigma\sqrt{2}}\right)m_1(A) \f)
	
	@ref is the reference observability grid
	@oth is the received observability grid
	@sigma describes how fast the transition between no info and information occurs
	'''

	ref, oth = EvidentialGridMap(ref), EvidentialGridMap(oth)
	try:
		EvidentialGridMap.extendMultiToInclude([ref, oth])
	except MemoryError as e:
		import pickle
		path = f'/tmp/{np.random.randint(9999999)}.pkl'
		rospy.logwarn('Error caused dump of %s in %s:\n%s', obss, path, ''.join(traceback.format_exception(None, e, e.__traceback__)))
		with open(path, 'wb') as f:
			pickle.dump(obss, f)

	Oref, Ooth = ref['O'], oth['O']
	Oref[np.isnan(Oref)] = 0
	Ooth[np.isnan(Ooth)] = 0

	d = norm.cdf(Ooth, Oref-2*sigma, sigma)*Oref
	d[d<2*sigma] = 0
	return d

class TrustBuilder:
	class DummyDetectability:
		def __init__(self, d):
			self.observability = d

	class Mode(Enum):
		NONE = 0 # Trust every source with value NONE_TRUST
		NO_CONF = 1 # Apply coherency and consistency check but no confirmation
		GLOBALCONF = 2 # In addition to NO_CONF, apply a confirmation check based on the weighted sum of all sources
		FUSEDCONF = 3 # In addition to NO_CONF, apply a confirmation check based on the weighted sum of all sources except the one being confirmed
		SEQUENTIALCONF = 4 # In addition to NO_CONF, sequentially apply a confirmation check against all sources except theone being confirmed

	COMBI_FUNS = {
		0: MassFunction.combine_dempster,
		1: MassFunction.combine_unnormalized,
		2: MassFunction.combine_yager,
	}

	def __init__(self):
		self.t = 0
		self.mode = TrustBuilder.Mode.NONE
		self.mutex = Lock()
		self.links = None
		self.buildings = None
		self.references = {}
		self.freesp_confs = {}
		self.static_trusts = {}
		self.initial_trusts = {}
		self.debugplot_refs = {}
		self.history = IdTimedDict(default_val=[])

		# For debugging purposes, all computed trusts are stored
		# First in debugtrustt for the latest ones, then concatenated in debugtrusts as np arrays
		self.debugtrustt = {
			'hist': MassFunction({'tT':1}),
			'past': MassFunction({'tT':1}),
			'meas': MassFunction({'tT':1}),
			'cohe': MassFunction({'tT':1}),
			'cohe-obd': MassFunction({'tT':1}),
			'cohe-atc': MassFunction({'tT':1}),
			'cohe-spc': MassFunction({'tT':1}),
			'cohe-spc-ro': MassFunction({'tT':1}),
			'cohe-spc-bu': MassFunction({'tT':1}),
			'cons': MassFunction({'tT':1}),
			'conf': MassFunction({'tT':1}),
			'conf-osi': MassFunction({'tT':1}),
			'conf-fsi': MassFunction({'tT':1}),
			'conf-ofi': MassFunction({'tT':1}),
			'conf-odi': MassFunction({'tT':1}),
			'conf-odi-rc': MassFunction({'tT':1}),
			'conf-odi-cr': MassFunction({'tT':1}),
		}
		self.debugtrusts = { }
		self.combi_fun = None
		
		self.consistency_predictor_cls = SCI
		class_penalty_file = Path(__file__).parent.parent.parent/'config'/'class_penalty.yml'
		self.consistency_associator = AssociatorMahalanobisClass(10, 5, class_penalty_file, 5)
		self.consistency_evo_noises = {
			StaticObject         : np.diag([0.02, 0.02, 0.02]),
			ConstantVelObject    : np.diag([0.01, 0.01, 0.05, 0.05]),
			CoordinatedTurnObject: np.diag([0.01, 0.01, 0.05, 0.10, 0.30])
		}

		# TODO: Make the noises bigger/smaller?
		self.confirmation_predictor_cls = self.consistency_predictor_cls
		self.confirmation_associator = AssociatorMahalanobisClass(10, 5, class_penalty_file, 5)
		self.confirmation_evo_noises = self.consistency_evo_noises
		self.confirmation_existence_estimator = ExistenceEstimatorObservability(
			max_decay_time=2,
			max_deterioration=0.1,
			thresh_valid=0,
			thresh_invalid=0,
			thresh_delete_nonexistent=0,
			thresh_delete_unknown=1
		)

		# Parameters
		self.BUFFER_DURATION = 2
		self.HLTIME_LOCAL = 10.0
		self.HLTIME_REMOTE = 2.0
		self.weights = { k: 1 for k in self.debugtrustt }
		self.COST_MISPLACED_OBJECTS = .25
		self.COST_NONDETECTABLE_OBJECTS = .1
		self.FREESPACE_CONFIRMATION_AREA = 20
		self.PROPERTY_BOUNDARIES = {
			'v': [-10, 1, 25],  # Half bad below -30km/h (-8m/s) or above 50km/h (15m/s)
			'l': [-3, 0.5, 22], # Half bad at 20m
			'w': [-3, 0.5,  6], # Half bad at 4m
			'h': [-3, 0.5,  7], # Half bad at 5m
		}

	def set_trust_mode(self, mode: Mode):
		self.mode = mode

	def combine(self, m1, m2, *ms):
		# See COMBI_FUNS
		m = self.combi_fun(m1, m2)
		for mx in ms:
			m = self.combi_fun(m, mx)
		return m

	def set_map(self, maptile):
		with self.mutex:
			self.buildings = MultiPolygon([building.polyline[:2].T for building in maptile.getWay("building=*;!wall=no")])
			self.links = MultiLineString([maptile.links[i].polyline[:2].T for i in maptile.links])

	def set_static_source(self, sourceid, static_trust):
		with self.mutex:
			self.static_trusts[sourceid] = static_trust

	def get_latest_trusts(self):
		with self.mutex:
			return { sid: (t, trust) for sid, (t, (trust, objs, dete)) in self.history.latest() }

	def callback(self, sourceid: str, t: float, objs: Objects, dete: DetectabilityGrid):
		with self.mutex:
			# Clear old messages and sources
			self.history = self.history.filtered(t-self.BUFFER_DURATION)
			self.debugtrustt = { k: MassFunction({'tT':1}) for k in self.debugtrustt }

			rospy.logdebug("---- Received %i objects from %s (%.2f) ----", len(objs), sourceid, t%1000)

			if self.mode==TrustBuilder.Mode.NONE:
				self.debugtrustt['hist'] = self.initial_trusts[sourceid]
				self.references[sourceid] = []
			elif sourceid in self.static_trusts:
				self.debugtrustt['hist'] = self.static_trusts[sourceid]
				self.references[sourceid] = []
			else:
				# Run the different checks
				rospy.logdebug("--- Coherency ---")
				self.coherency(sourceid, t, objs, dete)

				rospy.logdebug("--- Consistency ---")
				self.consistency(sourceid, t, objs, dete)

				rospy.logdebug("--- Confirmation ---")
				self.confirmation(sourceid, t, objs, dete)

				# Compute the fusion of these checks
				rospy.logdebug("--- Fusion ---")
				if sourceid in self.history:
					t_past_trust, (past_trust, _, _) = self.history.closest(t, sourceid, -1)
					dt = t - t_past_trust
					alpha = MassFunction.get_decay_alpha(self.HLTIME_REMOTE if sourceid in ['zoeblue','zoegrey','zoewhite'] else self.HLTIME_LOCAL, dt)
					self.debugtrustt['past'] = past_trust.discount(alpha)
					rospy.logdebug("Decay old trust %s = %s * (D(%.2f-%.2f)=%.2f)", self.debugtrustt['past'], past_trust, t, t_past_trust, alpha)
				else:
					self.debugtrustt['past'] = self.initial_trusts[sourceid]
					rospy.logdebug("Initializing trust %s", self.debugtrustt['past'])

				self.debugtrustt['meas'] = self.combine(
					self.debugtrustt['cohe'].discount(1-self.weights['cohe']),
					self.debugtrustt['cons'].discount(1-self.weights['cons']),
					self.debugtrustt['conf'].discount(1-self.weights['conf'])
				)
				self.debugtrustt['hist'] = self.combine(
					self.debugtrustt['past'].discount(1-self.weights['past']),
					self.debugtrustt['meas'],
				)

				rospy.logdebug("New Trust %s = %s %s %s %s",
					self.debugtrustt['hist'],
					self.debugtrustt['past'],
					self.debugtrustt['cohe'],
					self.debugtrustt['cons'],
					self.debugtrustt['conf']
				)
				rospy.logdebug("")

			# Add received objects and detectability to buffers
			self.history[sourceid, t] = (self.debugtrustt['hist'], objs, dete)
			if sourceid not in self.debugtrusts:
				self.debugtrusts[sourceid] = { k: np.zeros((0,3)) for k in self.debugtrustt }
				self.debugtrusts[sourceid]['t'] = np.zeros(0)

			for k,v in self.debugtrustt.items():
				self.debugtrusts[sourceid][k] = np.vstack((self.debugtrusts[sourceid][k], (v[''], v['t'], v['T'])))
			self.debugtrusts[sourceid]['t'] = np.hstack((self.debugtrusts[sourceid]['t'], t))

	def coherency(self, sourceid: str, t: float, objs: Objects, dete: DetectabilityGrid) -> MassFunction:
		positions = MultiPoint([(o.x, o.y) for o in objs])

		# Objects are close to roads
		self.debugtrustt['cohe-spc-ro'] = MassFunction({'tT':1})
		distances = [self.links.distance(p) for p in positions.geoms] if self.links else []
		for d in distances:
			self.debugtrustt['cohe-spc-ro'] &= mass_twogauss('tT', -6, 1, 14, d) # Half bad a 10m

		# Objects are outside buildings
		n_objs_in_buildings = sum([self.buildings.contains(p) for p in positions.geoms]) if self.buildings else 0
		cost_objs_in_buildings = 1-(1-self.COST_MISPLACED_OBJECTS)**n_objs_in_buildings
		self.debugtrustt['cohe-spc-bu'] = MassFunction({'T': cost_objs_in_buildings, 'tT': 1-cost_objs_in_buildings})
		if self.buildings:
			rospy.logdebug('Buildings %s', [p for p in positions.geoms if self.buildings.contains(p)])

		# Communicated objects are far enough from one another
		#n_objs_in_buildings = sum([self.buildings.contains(p) for p in positions.geoms]) if self.buildings else 0
		#cost_objs_in_buildings = 1-(1-self.COST_MISPLACED_OBJECTS)**n_objs_in_buildings
		#trust_buildings = MassFunction({'T': cost_objs_in_buildings, 'tT': 1-cost_objs_in_buildings})

		# Objects are detectable
		objs_dete = [compute_observability_object_partial(o, dete) for o in objs]
		n_nondetectable_objs = sum([obs['o']<0.05 for obs in objs_dete])
		cost_nondetectable_objs = 1-(1-self.COST_NONDETECTABLE_OBJECTS)**n_nondetectable_objs
		self.debugtrustt['cohe-obd'] = MassFunction({'T': cost_nondetectable_objs, 'tT': 1-cost_nondetectable_objs})
		for obj, obs in zip(objs, objs_dete):
			if obs['o']<0.05:
				rospy.logdebug('Non-Detectable %s (%s)', obj, obs)

		#self.debugtrustt['cohe-obd'] = MassFunction({'tT':1})
		#for o in objs:
			#m_observable = 
			#rospy.logdebug('FoV %s %s', o, m_observable)
			#self.debugtrustt['cohe-obd'] &= MassFunction({'T': 1-observable, 'tT': observable}).discount(o.exist.bel('e'))
		#self.debugtrustt['cohe-obd'] = self.debugtrustt['cohe-obd'].discount(0.5)

		# Objects are within boundaries
		self.debugtrustt['cohe-atc'] = MassFunction({'tT':1})
		for o in objs:
			trust_boundary_speed = mass_twogauss('tT', *self.PROPERTY_BOUNDARIES['v'], o.v)
			trust_boundary_speed['T'] += trust_boundary_speed['t']; trust_boundary_speed['t'] = 0 # Speeds below negative thresh are also incorrect
			trust_boundary_length = mass_twogauss('tT', *self.PROPERTY_BOUNDARIES['l'], o.shape[0])
			trust_boundary_width  = mass_twogauss('tT', *self.PROPERTY_BOUNDARIES['w'], o.shape[1])
			trust_boundary_height = mass_twogauss('tT', *self.PROPERTY_BOUNDARIES['h'], o.shape[2])
			trust_boundary = trust_boundary_speed & trust_boundary_length & trust_boundary_width & trust_boundary_height
			trust_boundary_discounted = trust_boundary.discount(o.exist['e'])
			if trust_boundary_discounted['T']>0:
				rospy.logdebug('Boundaries %s = %.2f:%s %.2f:%s %.2f:%s %.2f:%s',
					trust_boundary_discounted,
					o.v, trust_boundary_speed,
					o.shape[0], trust_boundary_length,
					o.shape[1], trust_boundary_width,
					o.shape[2], trust_boundary_height,
				)
			self.debugtrustt['cohe-atc'] &= trust_boundary_discounted

		self.debugtrustt['cohe-spc'] = self.debugtrustt['cohe-spc-ro'].discount(1-self.weights['cohe-spc-ro']) & \
		                               self.debugtrustt['cohe-spc-bu'].discount(1-self.weights['cohe-spc-bu'])
		self.debugtrustt['cohe'] = self.debugtrustt['cohe-spc'].discount(1-self.weights['cohe-spc']) & \
		                           self.debugtrustt['cohe-obd'].discount(1-self.weights['cohe-obd']) & \
		                           self.debugtrustt['cohe-atc'].discount(1-self.weights['cohe-atc'])
		rospy.logdebug('Coherency %s = %s (%s) %s %s %s',
			self.debugtrustt['cohe'],
			self.debugtrustt['cohe-spc-ro'], distances,
			self.debugtrustt['cohe-spc-bu'],
			self.debugtrustt['cohe-obd'],
			self.debugtrustt['cohe-atc'])
		return self.debugtrustt['cohe']

	def consistency(self, sourceid: str, t: float, objs: Objects, dete: DetectabilityGrid) -> MassFunction:
		# Predict past objects up to now to check that objects are consistent in time
		history_objs = deepcopy(self.history.closest_val(t, sourceid, -1)[1]) if sourceid in self.history else []
		for o in history_objs:
			try:
				self.consistency_predictor_cls.predict(o, t-o.t, self.consistency_evo_noises[type(o)])
			except ValueError as e:
				rospy.logwarn('Prediction of %s caused %s', o, ''.join(traceback.format_exception(None, e, e.__traceback__)))

			if isinstance(o, CoordinatedTurnObject):
				o.normalize_speed()

		try:
			assocs = self.consistency_associator.associate(history_objs, objs)
		except FloatingPointError as e:
			rospy.logwarn("FPE consistency assoc: %s %s %s", e, history_objs, objs)
			assocs = [], []
		rospy.logdebug("Comparing %i history to %i objs (%i associated)", len(history_objs), len(objs), len(assocs[0]))
		
		similarity = MassFunction({'sS':1})
		for hist_i, curr_i in zip(*assocs):
			sim = compute_similarity(history_objs[hist_i], objs[curr_i])
			similarity &= sim.discount(0.01)
			rospy.logdebug("Associated %s", history_objs[hist_i])
			rospy.logdebug("      with %s %s", objs[curr_i], sim)

		self.debugtrustt['cons'] = MassFunction({'T': similarity['S'], 'tT': 1-similarity['S']})
		return self.debugtrustt['cons']

	def confirmation(self, sourceid: str, t: float, objs: Objects, dete: DetectabilityGrid) -> MassFunction:
		self.references[sourceid] = self.retrieve_and_update_reference(sourceid, t, objs, dete)
		self.debugtrustt['conf'] = MassFunction({'tT': 1})
		for ref_objs, ref_dete in self.references[sourceid]:
			self.debugtrustt['conf'] &= self.confirmation_inner(sourceid, t, objs, dete, ref_objs, ref_dete)
		return self.debugtrustt['conf']

	def confirmation_inner(self, sourceid: str, t: float, com_objs: Objects, com_dete: DetectabilityGrid, ref_objs: Objects, ref_dete: DetectabilityGrid) -> MassFunction:
		# Associate objects with reference
		try:
			assocs = self.confirmation_associator.associate(ref_objs, com_objs)
		except FloatingPointError as e:
			rospy.logwarn("FPE confirmation assoc: %s %s %s", e, ref_objs, com_objs)
			assocs = [], []
		rospy.logdebug("Comparing %i ref to %i comp (%i associated)", len(ref_objs), len(com_objs), len(assocs[0]))

		# Compute the similarity of associated objects
		similarity = MassFunction({'sS':1})
		for ref_idx, com_idx in zip(*assocs):
			observability_assoc_by_ref = compute_observability_object(com_objs[com_idx], ref_dete)
			alpha = 1-ref_objs[ref_idx].exist.pl('e') * com_objs[com_idx].exist.bel('e') * observability_assoc_by_ref.bel('o')
			sim = compute_similarity(ref_objs[ref_idx], com_objs[com_idx])
			sim_discounted = sim.discount(alpha)
			similarity &= sim_discounted
			rospy.logdebug("Associated %s", ref_objs[ref_idx])
			rospy.logdebug("      with %s", com_objs[com_idx])
			rospy.logdebug("           %s %s %s %s %s", observability_assoc_by_ref, ref_objs[ref_idx].exist, com_objs[com_idx].exist, sim, sim_discounted)
		self.debugtrustt['conf-osi'] = MassFunction({'t': similarity['s'], 'tT': 1-similarity['s']})

		# Compute the cost of missed objects both in reference and comp
		missed_ref = self.confirmation_associator.get_unassociated_idx(ref_objs, assocs[0])
		observability_missed_ref = MassFunction({'oO':1})
		for i in missed_ref:
			observability_missed_ref_by_ref = compute_observability_object(ref_objs[i], ref_dete)
			observability_missed_ref_by_com = compute_observability_object(ref_objs[i], com_dete)
			aplha = 1-ref_objs[i].exist['e']*observability_missed_ref_by_ref['o']
			observability_missed_ref_by_com_discounted = observability_missed_ref_by_com.discount(aplha)
			observability_missed_ref &= observability_missed_ref_by_com_discounted
			rospy.logdebug("Missed ref %s %s %s %s (%s %f %f %s)", ref_objs[i], observability_missed_ref_by_ref, observability_missed_ref_by_com, observability_missed_ref_by_com_discounted, observability_missed_ref_by_com.frame(), ref_objs[i].exist['e'], observability_missed_ref_by_ref['o'], aplha)
		self.debugtrustt['conf-odi-rc'] = MassFunction({'T': observability_missed_ref['o'], 'tT': 1-observability_missed_ref['o']})

		missed_com = self.confirmation_associator.get_unassociated_idx(com_objs, assocs[1])
		observability_missed_com = MassFunction({'oO':1})
		for i in missed_com:
			observability_missed_com_by_com = compute_observability_object(com_objs[i], com_dete)
			observability_missed_com_by_ref = compute_observability_object(com_objs[i], ref_dete)
			alpha = 1-com_objs[i].exist['e']*observability_missed_com_by_com['o']
			observability_missed_com_by_ref_discounted = observability_missed_com_by_ref.discount(alpha)
			observability_missed_com &= observability_missed_com_by_ref_discounted
			rospy.logdebug("Missed Comp %s %s %s %s", com_objs[i], observability_missed_com_by_com, observability_missed_com_by_ref, observability_missed_com_by_ref_discounted)
		self.debugtrustt['conf-odi-cr'] = MassFunction({'T': observability_missed_com['o'], 'tT': 1-observability_missed_com['o']})

		# Received objects are in my freespace
		nonobservabilities = MassFunction({'oO':1})
		for o in com_objs:
			nonobservability = compute_observability_object_partial(o, ref_dete)
			nonobservability['oO'] += nonobservability['o']; nonobservability['o'] = 0 # Detectability is normal here, we are interested in non-detectability
			nonobservability_discounted = nonobservability.discount(1-o.exist['e'])
			nonobservabilities &= nonobservability_discounted
			rospy.logdebug("Obj In Free %s %s %s", o, nonobservability, nonobservability_discounted)
		self.debugtrustt['conf-ofi'] = nonobservabilities.on_frame('tT')

		# Freespace overlap
		self.freesp_confs[sourceid] = freespace_conf(ref_dete, com_dete)
		freespace_conf_area = np.trapz(self.freesp_confs[sourceid].flatten())
		freespace_conf_ref = self.FREESPACE_CONFIRMATION_AREA*self.FREESPACE_CONFIRMATION_AREA * norm.cdf(0.9, 0.9-2*0.5, 0.5)*0.9
		freespace_conf_val = 1-np.exp(-(freespace_conf_area*np.log(1/(1-0.9))) / freespace_conf_ref)
		rospy.logdebug("Free Conf %.2f = %.2f %.2f", freespace_conf_val, freespace_conf_area, freespace_conf_ref)
		self.debugtrustt['conf-fsi'] = MassFunction({'t': freespace_conf_val, 'tT':1-freespace_conf_val})

		# Fusion
		self.debugtrustt['conf-odi'] = self.debugtrustt['conf-odi-rc'].discount(1-self.weights['conf-odi-rc']) & \
		                               self.debugtrustt['conf-odi-cr'].discount(1-self.weights['conf-odi-cr'])
		conf = self.combine(
			self.debugtrustt['conf-osi'].discount(1-self.weights['conf-osi']),
			self.debugtrustt['conf-odi'].discount(1-self.weights['conf-odi']),
			self.debugtrustt['conf-ofi'].discount(1-self.weights['conf-ofi']),
			self.debugtrustt['conf-fsi'].discount(1-self.weights['conf-fsi']),
		)

		rospy.logdebug("Confirmation %s = %s %s %s %s %s",
			conf,
			self.debugtrustt['conf-osi'],
			self.debugtrustt['conf-odi-rc'],
			self.debugtrustt['conf-odi-cr'],
			self.debugtrustt['conf-ofi'],
			self.debugtrustt['conf-fsi']
		)

		return conf

	def retrieve_and_update_reference(self, sourceid: str, t: float, objs: Objects, dete: DetectabilityGrid) -> List[Tuple[Objects, DetectabilityGrid]]:
		if self.mode==TrustBuilder.Mode.NO_CONF:
			return []
		else:
			history = [(hsid, ht, *self.history[hsid, ht]) for hsid, ht in self.history.closest_t(t, sign=-1) if self.mode==TrustBuilder.Mode.GLOBALCONF or hsid!=sourceid]
			if len(history)==0:
				return []

			# Merge the sources before doing checks if not sequential
			if self.mode!=TrustBuilder.Mode.SEQUENTIALCONF:
				return [self.predict_reference(t, history)]
			else:
				return [self.predict_reference(t, [h]) for h in history]

	def predict_reference(self, t: float, history: List[History]) -> Tuple[Objects, DetectabilityGrid]:
		tracker = Tracker(self.confirmation_predictor_cls, self.confirmation_associator, self.confirmation_evo_noises, 1)
		tracker.existence_estimator = self.confirmation_existence_estimator
		# TODO: ?Add past reference as the tracks buffer to have some sort of memory?

		tmin = t
		for rsid, rt, rtrust, robjs, rdets in history:
			tracker.add_obs(rsid, rt, robjs)
			tracker.add_source(rsid, rt, TrustBuilder.DummyDetectability(rdets))
			tracker.set_trust(rsid, rt, rtrust)
			if rt<tmin: tmin = rt

		tracker.t = tmin
		return tracker.step(t)

	# Pickling support
	def __getstate__(self):
		state = self.__dict__.copy()
		# Don't pickle mutex because Lock() cannot be pickled
		del state['mutex']
		return state

	def __setstate__(self, state):
		self.__dict__.update(state)
		# Add mutex back since it doesn't exist in the pickle
		self.mutex = Lock()
